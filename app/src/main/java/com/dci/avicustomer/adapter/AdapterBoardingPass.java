package com.dci.avicustomer.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.dci.avicustomer.R;
import com.dci.avicustomer.activities.ActivityLatestNews;
import com.dci.avicustomer.interfaces.OnImageRemoveListener;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.ModelHome;
import com.dci.avicustomer.models.ModelTripDetails;

import java.io.File;
import java.util.List;

public class AdapterBoardingPass extends RecyclerView.Adapter<AdapterBoardingPass.DrawerViewHolder> {
    int type=0;
    OnItemClickListener onItemClickListener;
    OnImageRemoveListener onImageRemoveListener;
    private List<ModelTripDetails.BoardImg> boardinglist;
    private List<ModelTripDetails.ETicketImg> eTicketlist;
    private Context mContext;

    public AdapterBoardingPass(Context mContext, List<ModelTripDetails.BoardImg> boardingList,
                               List<ModelTripDetails.ETicketImg> eticketList,int type) {
        this.mContext = mContext;
        this.eTicketlist = eticketList;
        this.boardinglist = boardingList;
        this.type = type;
        //type==1 is edittype
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public AdapterBoardingPass.DrawerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_boarding_image, parent, false);
        return new AdapterBoardingPass.DrawerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterBoardingPass.DrawerViewHolder holder, final int position) {
        holder.file_name.setVisibility(View.GONE);
        if (boardinglist!=null&&boardinglist.size()!=0){
            if (boardinglist.get(position).getType()==null){
                Glide.with(mContext).load(boardinglist.get(position).getImage()).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable final GlideException e,
                                                final Object model, final Target<Drawable> target,
                                                final boolean isFirstResource) {
                        try{
                            if (boardinglist.get(position).getImage()!=null&&
                                    !boardinglist.get(position).getImage().equals("")){
                                holder.file_name.setVisibility(View.VISIBLE);
                                holder.file_name.setText(boardinglist.get(position).getImage().substring(boardinglist.get(position).getImage().lastIndexOf("/")+1));
                                holder.img_boarding.setBackground(mContext.getResources().getDrawable(R.drawable.backround_file_thum));
                                holder.file_name.setSelected(true);
                            }
                        }
                        catch (Exception e1){

                        }

                        return false;
                    }

                    @Override
                    public boolean onResourceReady(final Drawable resource,
                                                   final Object model,
                                                   final Target<Drawable> target,
                                                   final DataSource dataSource,
                                                   final boolean isFirstResource) {
                        return false;
                    }
                }).into(holder.img_boarding);
            }
            else {
                File file=new File(boardinglist.get(position).getImage());
                Glide.with(mContext).load(file).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable final GlideException e,
                                                final Object model, final Target<Drawable> target,
                                                final boolean isFirstResource) {
                        try {
                            if (boardinglist.get(position).getImage()!=null&&!boardinglist.get(position).getImage().equals("")){
                                holder.file_name.setVisibility(View.VISIBLE);
                                holder.file_name.setText(boardinglist.get(position).getImage().substring(boardinglist.get(position).getImage().lastIndexOf("/")+1));
                                holder.img_boarding.setBackground(mContext.getResources().getDrawable(R.drawable.backround_file_thum));
                                holder.file_name.setSelected(true);
                            }
                        }
                        catch (Exception e2){

                        }


                        return false;
                    }

                    @Override
                    public boolean onResourceReady(final Drawable resource,
                                                   final Object model,
                                                   final Target<Drawable> target,
                                                   final DataSource dataSource,
                                                   final boolean isFirstResource) {
                        holder.file_name.setVisibility(View.GONE);


                        return false;
                    }
                }).into(holder.img_boarding);
            }



        }
        else {
            if (eTicketlist.get(position).getType()==null&&eTicketlist.size()!=0){
                Glide.with(mContext).load(eTicketlist.get(position).getImage()).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable final GlideException e,
                                                final Object model, final Target<Drawable> target,
                                                final boolean isFirstResource) {
                        try {
                            if (eTicketlist.get(position).getImage()!=null&&!eTicketlist.get(position).getImage().equals("")){
                                holder.file_name.setVisibility(View.VISIBLE);
                                holder.file_name.setText(eTicketlist.get(position).getImage().substring(eTicketlist.get(position).getImage().lastIndexOf("/")+1));
                                holder.img_boarding.setBackground(mContext.getResources().getDrawable(R.drawable.backround_file_thum));
                                holder.file_name.setSelected(true);
                            }
                        }
                        catch (Exception e3){

                        }


                        return false;
                    }

                    @Override
                    public boolean onResourceReady(final Drawable resource,
                                                   final Object model,
                                                   final Target<Drawable> target,
                                                   final DataSource dataSource,
                                                   final boolean isFirstResource) {
                        holder.file_name.setVisibility(View.GONE);

                        return false;
                    }
                }).into(holder.img_boarding);
            }
            else {
                File file=new File(eTicketlist.get(position).getImage());
                Glide.with(mContext).load(file).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable final GlideException e,
                                                final Object model, final Target<Drawable> target,
                                                final boolean isFirstResource) {
                        try {
                            if (eTicketlist.get(position).getImage()!=null&&!eTicketlist.get(position).getImage().equals("")){
                                holder.file_name.setVisibility(View.VISIBLE);
                                holder.file_name.setText(eTicketlist.get(position).getImage().substring(eTicketlist.get(position).getImage().lastIndexOf("/")+1));
                                holder.img_boarding.setBackground(mContext.getResources().getDrawable(R.drawable.backround_file_thum));
                                holder.file_name.setSelected(true);
                            }
                        }
                        catch (Exception e5){

                        }

                        return false;
                    }

                    @Override
                    public boolean onResourceReady(final Drawable resource,
                                                   final Object model,
                                                   final Target<Drawable> target,
                                                   final DataSource dataSource,
                                                   final boolean isFirstResource) {
                        holder.file_name.setVisibility(View.GONE);

                        return false;
                    }
                }).into(holder.img_boarding);
            }


            //Glide.with(mContext).load(eTicketlist.get(position).getImage()).into(holder.img_boarding);
        }
        holder.img_boarding.setClipToOutline(true);
        if (type==1){
            holder.img_cancel.setVisibility(View.VISIBLE);
        }

        holder.img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (eTicketlist==null){
                    onImageRemoveListener.OnIMageremove(position,v,1);
                }
                else if (boardinglist==null){
                    onImageRemoveListener.OnIMageremove(position,v,2);
                }
            }
        });

    }
    public void setEdtitType(int type){
        this.type=type;


    }

    public void updateRowIndex(int pos) {
        type = pos;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (boardinglist!=null){
            return boardinglist.size();
        }
        else {
            return eTicketlist.size();
        }

    }

    public void setonItemClick(OnItemClickListener listener) {
        this.onItemClickListener = listener;

    }
    public void setOnIMageRemove(OnImageRemoveListener listener){
        this.onImageRemoveListener=listener;
    }

    public void updateindex(int index) {
        this.type = index;
        notifyDataSetChanged();
    }

    class DrawerViewHolder extends RecyclerView.ViewHolder {

        ImageView img_boarding,img_cancel;
        TextView file_name;



        public DrawerViewHolder(View itemView) {
            super(itemView);
            //  tv_offer_des = itemView.findViewById(R.id.tv_offer_des);

            img_boarding = itemView.findViewById(R.id.img_boarding);
            img_cancel= itemView.findViewById(R.id.img_cancel);
            file_name= itemView.findViewById(R.id.file_name);

            //tv_offer_percentage= itemView.findViewById(R.id.tv_offer_percentage);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onItemClickListener!=null){
                        onItemClickListener.OnItemClickListener(getAdapterPosition(),view);
                    }



                    // item_lay.setBackgroundColor(mContext.getResources().getColor(R.color.orrange));

//                    int position = getAdapterPosition();
//                    ((MainActivity) mContext).showMenuItems(position);
                }
            });

        }
    }

}
