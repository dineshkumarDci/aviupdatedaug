package com.dci.avicustomer.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.dci.avicustomer.R;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.ModelCompletedTrips;
import com.dci.avicustomer.models.ModelMyTrips;

import java.util.List;

public class AdapterCompletedTrips extends
        RecyclerView.Adapter<AdapterCompletedTrips.DrawerViewHolder> {
    int type;
    OnItemClickListener onItemClickListener;
    private List<ModelCompletedTrips.Datum> home_data_list;
    private Context mContext;
    String imagepath;


    public AdapterCompletedTrips(Context mContext,
                                 List<ModelCompletedTrips.Datum> home_data_list) {
        this.mContext = mContext;
        this.home_data_list = home_data_list;
        this.type = type;
        this.imagepath=imagepath;
        Log.d("onBindVieswHolder", "onBindViewHolder: "+home_data_list.size());
    }

    @Override
    public AdapterCompletedTrips.DrawerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_my_trips,
                parent,
                false);
        return new AdapterCompletedTrips.DrawerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterCompletedTrips.DrawerViewHolder holder, final int position) {
        ModelCompletedTrips.Datum myTrip=home_data_list.get(position);
        holder.img_refresh.setVisibility(View.GONE);
        if (myTrip.getDep_date_time()!=null&&myTrip.getDep_time()!=null){
            holder.tv_time.setText(UtilsDefault.dateFormat(myTrip.getDep_date_time())+" | "+
                    UtilsDefault.timeFormat(myTrip.getDep_time()));
        }
        holder.tv_dept_code.setText(myTrip.getDep_airport_code());
        holder.tv_arrive_code.setText(myTrip.getArr_airport_code());
        holder.tv_dept_city.setText(myTrip.getDep_airport_city()+","+myTrip.getDep_airport_country());
        holder.tv_arrival_city.setText(myTrip.getArr_airport_city()+","+myTrip.getArr_airport_country());
        holder.tv_flightcode.setText(myTrip.getFlight_code());
        holder.tv_arrival_city.setSelected(true);
        holder.tv_dept_city.setSelected(true);
        holder.img_airline_icon.setClipToOutline(true);
        holder.tv_airlinenames.setText(myTrip.getAirline());


    }



    public void updateRowIndex(int pos) {
        type = pos;
        notifyDataSetChanged();
    }
    public void rotateAnimation(View imageView){
        RotateAnimation rotate = new RotateAnimation(0,360,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(1000);
        rotate.setRepeatCount(4);
        rotate.setRepeatMode(Animation.INFINITE);
        rotate.setInterpolator(new LinearInterpolator());
        imageView.startAnimation(rotate);

    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
    @Override
    public int getItemCount() {
        return home_data_list.size();
    }

    public void setonItemClick(OnItemClickListener listener) {
        this.onItemClickListener = listener;

    }


    public void updateindex(int index) {
        this.type = index;
        notifyDataSetChanged();
    }
    public void setOnItemClick(OnItemClickListener listener){
        if (listener!=null){
            this.onItemClickListener=listener;
        }

    }

    class DrawerViewHolder extends RecyclerView.ViewHolder {
        TextView tv_time,tv_dept_code,tv_dept_city,tv_arrive_code,
                tv_arrival_city,tv_flightcode,tv_airlinenames;
        ImageView img_refresh,img_airline_icon;
        public DrawerViewHolder(View itemView) {
            super(itemView);
            tv_dept_code = itemView.findViewById(R.id.tv_dept_code);
            tv_time = itemView.findViewById(R.id.tv_time);
            img_airline_icon= itemView.findViewById(R.id.img_airline_icon);
            tv_arrive_code= itemView.findViewById(R.id.tv_arrive_code);
            img_refresh = itemView.findViewById(R.id.img_refresh);
            tv_airlinenames= itemView.findViewById(R.id.tv_airlinenames);
            tv_flightcode= itemView.findViewById(R.id.tv_flightcode);
            tv_arrival_city= itemView.findViewById(R.id.tv_arrival_city);
            tv_dept_city= itemView.findViewById(R.id.tv_dept_city);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //onItemClickListener.OnItemClickListener(getAdapterPosition(),view);
                    // item_lay.setBackgroundColor(mContext.getResources().getColor(R.color.orrange));

//                    int position = getAdapterPosition();
//                    ((MainActivity) mContext).showMenuItems(position);
                }
            });
            img_refresh.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                   // onItemClickListener.OnItemClickListener(getAdapterPosition(),v);
                   // rotateAnimation(v);
                }
            });
        }
    }
//    public int setActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.activehome;
//                break;
//            case 1:
//                id=  R.drawable.activeaccount;
//
//                break;
//            case 2:
//                id= R.drawable.activelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.activewallet;
//                break;
//            case 4:
//                id=  R.drawable.activeorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.activewishlish;
//                break;
//            case 6:
//                id=  R.drawable.activereceipe;
//                break;
//            case 7:
//                id=  R.drawable.activelogout;
//                break;
//            default:
//                id=  R.drawable.activelogout;
//
//        }
//        return id;
//
//
//    }
//    public int setInActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.inactivehome;
//                break;
//            case 1:
//                id=  R.drawable.inactiveaccount;
//
//                break;
//            case 2:
//                id= R.drawable.inactivelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.inactivewallet;
//                break;
//            case 4:
//                id=  R.drawable.inactiveorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.inactivewishlish;
//                break;
//            case 6:
//                id=  R.drawable.inactivereceipe;
//                break;
//            case 7:
//                id=  R.drawable.inactivelogout;
//                break;
//            default:
//                id=  R.drawable.inactivelogout;
//
//        }
//        return id;
//
//
//    }

}
