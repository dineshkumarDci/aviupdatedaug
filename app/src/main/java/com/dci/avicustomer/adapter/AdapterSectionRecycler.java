package com.dci.avicustomer.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.avicustomer.R;
import com.dci.avicustomer.SectionedRecycleView.SectionRecyclerViewAdapter;
import com.dci.avicustomer.activities.ActivityHome;
import com.dci.avicustomer.activities.ActivitySearch;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.CommonMsgModel;
import com.dci.avicustomer.models.ModelSearchResult;
import com.dci.avicustomer.models.ModelUserProfile;
import com.dci.avicustomer.models.SectionedDataModel;
import com.dci.avicustomer.retrofit.AVIAPI;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdapterSectionRecycler extends SectionRecyclerViewAdapter<SectionedDataModel,
        ModelSearchResult.Airport, AdapterSectionRecycler.SectionViewHolder,
        AdapterSectionRecycler.ChildViewHolder> {

    Context context;
    OnItemClickListener listener;
    List<SectionedDataModel> sectionItemList;
    ProgressDialog progressDialog;
    @Inject
    public AVIAPI aviapi;

    public void shwProgress() {
        progressDialog.setMessage("Please wait");
        progressDialog.setCancelable(false);

        progressDialog.show();
        ProgressBar progressbar = (ProgressBar) progressDialog.findViewById(android.R.id.progress);
        progressbar.getIndeterminateDrawable().setColorFilter(Color.parseColor("#16172b"), android.graphics.PorterDuff.Mode.SRC_IN);
    }
    public void hideprogress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    public AdapterSectionRecycler(Context context,
                                  List<SectionedDataModel> sectionItemList,
                                  OnItemClickListener listener) {
        super(context, sectionItemList);
        this.context = context;
        this.listener=listener;
        this.sectionItemList=sectionItemList;
        ShoppingApplication.getContext().getComponent().inject(this);
        progressDialog=new ProgressDialog(context);
    }

    @Override
    public SectionViewHolder onCreateSectionViewHolder(ViewGroup sectionViewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_sectioned_header,
                sectionViewGroup, false);
        return new SectionViewHolder(view);
    }

    @Override
    public ChildViewHolder onCreateChildViewHolder(ViewGroup childViewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_search_list,
                childViewGroup, false);
        return new ChildViewHolder(view);
    }

    @Override
    public void onBindSectionViewHolder(SectionViewHolder sectionViewHolder, int sectionPosition, SectionedDataModel section) {
        sectionViewHolder.name.setText(section.getSectionText());
        sectionViewHolder.tv_numberof_item.setText(String.valueOf(section.getChildItems().size()));

    }

    @Override
    public void onBindChildViewHolder(ChildViewHolder childViewHolder, int sectionPosition, int childPosition, ModelSearchResult.Airport child) {
        childViewHolder.name.setText(child.getName());
        childViewHolder.tv_cityname.setText(UtilsDefault.checkNull(child.getCity_name()));
        if (sectionItemList.get(sectionPosition).getSectionText().equals("Favourites")){
            childViewHolder.star.setImageDrawable(context.getResources().getDrawable(R.drawable.star));
        }
        else {
            childViewHolder.star.setImageDrawable(context.getResources().getDrawable(R.drawable.unstar));
        }
        childViewHolder.star.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnItemClickListener(child.getId(),v);
            }
        });
        childViewHolder.rel_whole_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    UtilsDefault.updateSharedPreference(Constants.WEATHER_REPORT,null);
                    UtilsDefault.updateSharedPreference(Constants.AIRPORT_LATITUDE,sectionItemList.get(sectionPosition).getChildItems().get(childPosition).getLatitude());
                    UtilsDefault.updateSharedPreference(Constants.AIRPORT_LONGITUDE,sectionItemList.get(sectionPosition).getChildItems().get(childPosition).getLongitude());
                    UtilsDefault.updateSharedPreference(Constants.AIRPORT_NAME,sectionItemList.get(sectionPosition).getChildItems().get(childPosition).getName());
                    UtilsDefault.updateSharedPreference(Constants.AIRPORT_ID,String.valueOf(sectionItemList.get(sectionPosition).getChildItems().get(childPosition).getId()));
                    UtilsDefault.updateSharedPreference(Constants.AIRPORT_CODE,String.valueOf(sectionItemList.get(sectionPosition).getChildItems().get(childPosition).getCode()));

                    UtilsDefault.updateSharedPreference(Constants.AIRPORT_TYPE,null);

                    Intent intent=new Intent(context, ActivityHome.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(intent);
                    ((ActivitySearch)context).finish();

                Log.d("airportid" , "onClick: "+String.valueOf(sectionItemList.get(sectionPosition).getChildItems().get(childPosition).getId()));

            }
        });



    }
    public void updateUserAirport(ModelSearchResult.Airport data) {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(context,
                    R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;

        }
        shwProgress();
        //  Log.d("aviparms", "search: "+UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)+search);
        String userid = "0";
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
            userid = UtilsDefault.getSharedPreferenceValue(Constants.USER_ID);
        }

        Log.d("uid",
                "updateProfile: " + UtilsDefault.getSharedPreferenceValue(Constants.USER_ID));
        aviapi.updateUserAirport( UtilsDefault.getSharedPreferenceValue(Constants.USER_ID),data.getId()).
                enqueue(new Callback<CommonMsgModel>() {
                    @Override
                    public void onResponse(Call<CommonMsgModel> call, Response<CommonMsgModel> response) {
                        hideprogress();
                        if (response.body() != null) {
                            CommonMsgModel datas = response.body();
                            String header = response.headers().get(context.getString(R.string.header_find_key));
                            UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                            if (datas.getStatus() == Constants.STATUS_200) {
                                UtilsDefault.updateSharedPreference(Constants.WEATHER_REPORT,null);
                                UtilsDefault.updateSharedPreference(Constants.AIRPORT_LATITUDE,data.getLatitude());
                                UtilsDefault.updateSharedPreference(Constants.AIRPORT_LONGITUDE,data.getLongitude());
                                UtilsDefault.updateSharedPreference(Constants.AIRPORT_NAME,data.getName());
                                UtilsDefault.updateSharedPreference(Constants.AIRPORT_ID,String.valueOf(data.getId()));
                                UtilsDefault.updateSharedPreference(Constants.AIRPORT_CODE,String.valueOf(data.getCode()));

                                UtilsDefault.updateSharedPreference(Constants.AIRPORT_TYPE,null);

                                Intent intent=new Intent(context, ActivityHome.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                context.startActivity(intent);
                                ((ActivitySearch)context).finish();

                                //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                            } else if (datas.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                                updateUserAirport(data);
                                //countDownStart();
                            } else {
                                Toast.makeText(context, datas.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(context, R.string.server_error, Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<CommonMsgModel> call, Throwable t) {
                        hideprogress();
                        Log.d("failure", "onFailure: " + t.getMessage());
                        Toast.makeText(context, R.string.server_error, Toast.LENGTH_SHORT).show();
                    }
                });
    }
    public class ChildViewHolder extends RecyclerView.ViewHolder {

        TextView name,tv_numberof_item,tv_cityname;
        ImageView star;
        LinearLayout rel_whole_lay;
        public ChildViewHolder(View itemView) {
            super(itemView);
            name =  itemView.findViewById(R.id.tv_headername);
            star=  itemView.findViewById(R.id.img_fav);
            tv_numberof_item=  itemView.findViewById(R.id.tv_numberof_item);
            rel_whole_lay=  itemView.findViewById(R.id.rel_whole_lay);
            tv_cityname=  itemView.findViewById(R.id.tv_cityname);
        }
    }
    public class SectionViewHolder extends RecyclerView.ViewHolder {

        TextView name,tv_numberof_item;
        ImageView star;
        public SectionViewHolder(View itemView) {
            super(itemView);
            name =  itemView.findViewById(R.id.tv_headername);
            tv_numberof_item=  itemView.findViewById(R.id.tv_numberof_item);

        }
    }
}