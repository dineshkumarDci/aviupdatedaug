package com.dci.avicustomer.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dci.avicustomer.R;
import com.dci.avicustomer.activities.ActivityEatAndDrink;
import com.dci.avicustomer.activities.ActivityRestaurantDetails;
import com.dci.avicustomer.activities.ActivityShopDetails;
import com.dci.avicustomer.activities.ActivityShopList;
import com.dci.avicustomer.activities.ActivityUserReviews;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.ModelEatAndDrink;
import com.dci.avicustomer.models.ModelShopList;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AdapterEatDrinks extends RecyclerView.Adapter<AdapterEatDrinks.DrawerViewHolder> {
    int type;
    OnItemClickListener onItemClickListener;
    private List<ModelEatAndDrink.Datum> home_data_list;
    private Context mContext;
    boolean isCurrentDayAvailable = false;
    boolean isshopOpen = false;
    boolean isRestaurantAvailable = false;

    public AdapterEatDrinks(Context mContext, List<ModelEatAndDrink.Datum> home_data_list, int type) {
        this.mContext = mContext;
        this.home_data_list = home_data_list;
        this.type = type;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public AdapterEatDrinks.DrawerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_eat_drink, parent, false);
        return new AdapterEatDrinks.DrawerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterEatDrinks.DrawerViewHolder holder, final int position) {
        isCurrentDayAvailable = false;
        ModelEatAndDrink.Datum modelShopList = home_data_list.get(position);
        holder.tv_shopname.setText(UtilsDefault.checkNull(modelShopList.getName()));

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(modelShopList.getTerminal().getName());

        if (modelShopList.getAirport_type() != null && modelShopList.getAirport_type() == 1) {
            stringBuilder.append("-" + "Domestic");
        } else {
            stringBuilder.append("-" + "International");
        }

        if (modelShopList.getArea_type() != null && modelShopList.getArea_type() == Constants.AREA_DEP) {
            stringBuilder.append("-" + "Departure");
        } else {
            stringBuilder.append("-" + "Arrival");
        }


        holder.tv_terminal_name.setText(stringBuilder);

        try {
            holder.ratingBar.setRating(modelShopList.getAverage_rating());
        } catch (Exception e) {

        }
        if (modelShopList.getRating_count() != null) {
            holder.tv_rating_value.setText("(" + modelShopList.getRating_count() + ")");
        }
        if (modelShopList.getOffer() != null && modelShopList.getOffer().size() != 0 &&
                modelShopList.getOffer().get(0).getOffer_code() != null) {
            holder.tv_offer_code.setVisibility(View.VISIBLE);
            holder.tv_offer_code.setText(modelShopList.getOffer().get(0).getOffer_code());
        } else {
            holder.tv_offer_code.setVisibility(View.INVISIBLE);
        }
        if (home_data_list.get(position).getRestaurant_type() != null) {
            holder.tv_rest_type.setVisibility(View.VISIBLE);
            holder.tv_rest_type.setText(UtilsDefault.checkNull(home_data_list.get(position).getRestaurant_type().
                    getName()));
        } else {
            holder.tv_rest_type.setVisibility(View.GONE);
        }

        //  holder.tv_address.setText(modelShopList.getAddress());


        String baseurl = ((ActivityEatAndDrink) mContext).getBaseUrl();
        if (home_data_list.get(position).getShop_image().contains(",")) {
            String[] image = home_data_list.get(position).getShop_image().split(",");
            Glide.with(mContext).load(baseurl + image[0]).into(holder.img_shop);
        } else {
            Glide.with(mContext).load(baseurl + home_data_list.get(position).getShop_image()).into(holder.img_shop);

        }
        if (home_data_list.get(position).getOpenClose() != null &&
                home_data_list.get(position).getOpenClose() == 1) {
            holder.tv_open_status.setText("Open");
            holder.tv_open_status.setTextColor(mContext.getResources().getColor(R.color.dark_green));


            if (modelShopList.getRestaurant_times() != null &&
                    modelShopList.getRestaurant_times().size() != 0) {

                SimpleDateFormat sdf = new SimpleDateFormat("EEE");
                Date d = new Date();
                String dayOfTheWeek = sdf.format(d);
                Log.d("day", "onBindViewHolder: " + dayOfTheWeek);


                if (modelShopList.getRestaurant_times().get(0).
                        getOpen_24_hrs().
                        equalsIgnoreCase("yes")) {
                    holder.tv_open_time.setText(mContext.getString(R.string.hrs_open));
                    holder.tv_open_status.setTextColor(mContext.getResources().getColor(R.color.dark_green));
                    String[] Days = modelShopList.
                            getRestaurant_times().get(0).
                            getOpen_days().split(",");

                    for (String day : Days) {
                        Log.d("apiday", "onBindViewHolder: " + day);
                        if (dayOfTheWeek.toLowerCase().equals(day.toLowerCase())) {
                            Log.d("apiday", "onBindViewHolder: " + day);
                            if (dayOfTheWeek.toLowerCase().equals(day.toLowerCase())) {
                                isCurrentDayAvailable = true;
                            }
                        }

                    }
                    if (isCurrentDayAvailable) {
                        holder.tv_open_status.setText(mContext.getString(R.string.open));
                        holder.tv_open_status.setTextColor(mContext.getResources().
                                getColor(R.color.dark_green));


                    } else {
                        holder.tv_open_status.setText(mContext.getString(R.string.closed));
                        holder.tv_open_status.setTextColor(mContext.getResources().
                                getColor(R.color.red));
                    }


                } else if (modelShopList.getRestaurant_times().get(0).getOpen_days().contains(",")) {
                    String[] Days = modelShopList.getRestaurant_times().get(0).
                            getOpen_days().split(",");
                    boolean isshopOpen = false;
                    boolean isCurrentDayAvailable = false;
                    holder.tv_open_time.setText(modelShopList.getRestaurant_times().get(0).
                            getFrom_time()
                            + modelShopList.getRestaurant_times().get(0).getFrom_am_pm()
                            + " To " +
                            modelShopList.getRestaurant_times().get(0).getTo_time() +
                            modelShopList.getRestaurant_times().get(0).getTo_am_pm());
                    for (String day : Days) {
                        Log.d("apiday", "onBindViewHolder: " + day);
                        if (dayOfTheWeek.toLowerCase().equals(day.toLowerCase())) {
                            isCurrentDayAvailable = true;
                        }

                    }
                    if (isCurrentDayAvailable) {

                        try {
                            isshopOpen = checkTimeIntervel(modelShopList.getRestaurant_times().get(0).
                                            getFrom_time()
                                            + " " + modelShopList.getRestaurant_times().get(0).getFrom_am_pm().toUpperCase(),
                                    modelShopList.getRestaurant_times().get(0).getTo_time() + " " +
                                            modelShopList.getRestaurant_times().get(0).getTo_am_pm().toUpperCase());
                        } catch (Exception e) {

                        }


                    }

                    if (isshopOpen) {
                        holder.tv_open_status.setText(mContext.getString(R.string.open));
                        holder.tv_open_status.setTextColor(mContext.getResources().
                                getColor(R.color.dark_green));
                    } else {
                        holder.tv_open_status.setText(mContext.getString(R.string.closed));
                        holder.tv_open_status.setTextColor(mContext.getResources().
                                getColor(R.color.red));
                    }


                } else {
                    if (home_data_list.get(position).getRestaurant_times().get(0).
                            getOpen_days().toLowerCase().contains(dayOfTheWeek)) {
                        boolean isshopOpen = false;
                        holder.tv_open_time.setText(modelShopList.getRestaurant_times().get(0).
                                getFrom_time()
                                + modelShopList.getRestaurant_times().get(0).getFrom_am_pm()
                                + " To " +
                                modelShopList.getRestaurant_times().get(0).getTo_time() +
                                modelShopList.getRestaurant_times().get(0).getTo_am_pm());
                        isshopOpen = checkTimeIntervel(modelShopList.getRestaurant_times().get(0).
                                        getFrom_time()
                                        + " " + modelShopList.getRestaurant_times().get(0).getFrom_am_pm().toUpperCase(),
                                modelShopList.getRestaurant_times().get(0).getTo_time() + " " +
                                        modelShopList.getRestaurant_times().get(0).getTo_am_pm().toUpperCase());
                        if (isshopOpen) {

                            holder.tv_open_status.setText(mContext.getString(R.string.open));
                            holder.tv_open_status.setTextColor(mContext.getResources().
                                    getColor(R.color.dark_green));
                        } else {

                            holder.tv_open_status.setText(mContext.getString(R.string.closed));
                            holder.tv_open_status.setTextColor(mContext.getResources().
                                    getColor(R.color.red));
                        }


                    }
                }


            }


        } else {
            holder.tv_open_status.setText("Closed");
            holder.tv_open_status.setTextColor(mContext.getResources().getColor(R.color.red));
            try {
                if (modelShopList.getRestaurant_times().get(0).
                        getOpen_24_hrs().
                        equalsIgnoreCase("yes")) {
                    holder.tv_open_time.setText(mContext.getString(R.string.hrs_open));
                    // holder.tv_open_status.setTextColor(mContext.getResources().getColor(R.color.dark_green));

                } else {

                    holder.tv_open_time.setText(modelShopList.getRestaurant_times().get(0).
                            getFrom_time()
                            + modelShopList.getRestaurant_times().get(0).getFrom_am_pm()
                            + " To " +
                            modelShopList.getRestaurant_times().get(0).getTo_time() +
                            modelShopList.getRestaurant_times().get(0).getTo_am_pm());


                }
            } catch (Exception e) {

            }


        }

        if (home_data_list.get(position).getETA() != null && !
                home_data_list.get(position).getETA().equals("")) {
            if (home_data_list.get(position).getETA().contains(":")) {
                String time[] = home_data_list.get(position).getETA().split(":");

                StringBuffer stringBuffer = new StringBuffer();
                if (!time[0].equals("00")) {
                    stringBuffer.append(time[0] + " Hrs ");
                }
                if (!time[1].equals("00")) {
                    stringBuffer.append(time[1] + " Mins");
                }
                holder.tv_eta.setText("Average response time:- " + stringBuffer);
            }
        }
        holder.img_shop.setClipToOutline(true);
        //  Log.d("imgg", "onBindViewHolder: " + baseurl + home_data_list.get(position).getProfile_img());
    }

    public void updateRowIndex(int pos) {
        type = pos;
        notifyDataSetChanged();
    }

    public Boolean checkTimeIntervel(String startTime, String endTime) {
        try {
            // String string1 = "20:11:13";
            Date c = Calendar.getInstance().getTime();
            System.out.println("Current time => " + c);

            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
            String formattedDate = df.format(c);
            String fromDate = formattedDate + " " + startTime;
            Log.d("times", "checkTimeIntervel: " + startTime + "end" + endTime);
            Date time1 = new SimpleDateFormat("dd-MMM-yyyy hh:mm aa").parse(fromDate);
            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTime(time1);
            //  calendar1.add(Calendar.DATE, 1);


            //String string2 = "14:49:00";

            String todate = formattedDate + " " + endTime;
            Date time2 = new SimpleDateFormat("dd-MMM-yyyy hh:mm aa").parse(todate);
            Calendar calendar2 = Calendar.getInstance();
            calendar2.setTime(time2);
            calendar2.add(Calendar.DATE, 1);

            // String someRandomTime = "01:00:00";
            //  Date d = new SimpleDateFormat("hh:mm aa").parse(someRandomTime);
            Calendar calendar3 = Calendar.getInstance();
            Date x = calendar3.getTime();

            if (x.after(calendar1.getTime()) && x.before(calendar2.getTime())) {


                System.out.println(true);
                return true;
            } else {

                return false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
            Log.d("timer", "checkTimeIntervel: " + "parseerror");
            return false;
        }

    }

    @Override
    public int getItemCount() {
        return home_data_list.size();
    }

    public void setonItemClick(OnItemClickListener listener) {
        this.onItemClickListener = listener;

    }

    public void updateindex(int index) {
        this.type = index;
        notifyDataSetChanged();
    }

    class DrawerViewHolder extends RecyclerView.ViewHolder {
        TextView tv_shopname, tv_eta, tv_rating_value, tv_open_time, tv_rest_type, tv_offer_code, tv_terminal_name, tv_open_status;
        ImageView img_shop, img_locate, img_phn_num, img_email;
        LinearLayout lin_address_lay;
        RatingBar ratingBar;
        RelativeLayout lin_offer_lay;
        CardView main_lay;


        public DrawerViewHolder(View itemView) {
            super(itemView);
            //  tv_offer_des = itemView.findViewById(R.id.tv_offer_des);
            tv_shopname = itemView.findViewById(R.id.tv_shopname);
            lin_offer_lay = itemView.findViewById(R.id.lin_offer_lay);
            tv_eta = itemView.findViewById(R.id.tv_eta);
            tv_rest_type = itemView.findViewById(R.id.tv_rest_type);
            tv_open_status = itemView.findViewById(R.id.tv_open_status);
            tv_offer_code = itemView.findViewById(R.id.tv_offer_code);
            img_shop = itemView.findViewById(R.id.img_shop);
            tv_open_time = itemView.findViewById(R.id.tv_open_time);
            lin_address_lay = itemView.findViewById(R.id.lin_address_lay);
            //  tv_offer_percentage = itemView.findViewById(R.id.tv_offer_percentage);
            tv_terminal_name = itemView.findViewById(R.id.tv_terminal_name);
            ratingBar = itemView.findViewById(R.id.rating);
            ratingBar = itemView.findViewById(R.id.rating);
            tv_rating_value = itemView.findViewById(R.id.tv_rating_value);


            img_locate = itemView.findViewById(R.id.img_locate);
            img_phn_num = itemView.findViewById(R.id.img_phn_num);
            img_email = itemView.findViewById(R.id.img_email);
            lin_offer_lay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (home_data_list.get(getAdapterPosition()).getId() != null) {
                        Intent intent = new Intent(mContext, ActivityUserReviews.class);
                        intent.putExtra("id", String.valueOf(home_data_list.get(getAdapterPosition()).getId()));
                        mContext.startActivity(intent);
                    } else {
                        Toast.makeText(mContext, mContext.getString(R.string.review_notavailble), Toast.LENGTH_SHORT).show();
                    }

                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
                        Intent intent = new Intent(mContext, ActivityRestaurantDetails.class);
                        intent.putExtra("id", String.valueOf(home_data_list.get(getAdapterPosition()).getId()));
                        //  intent.putExtra("imgbase", ((ActivityShopList) mContext).getBaseUrl());
                        mContext.startActivity(intent);
                    } else {
                        Toast.makeText(mContext, R.string.login_messge, Toast.LENGTH_SHORT).show();
                    }


                    // item_lay.setBackgroundColor(mContext.getResources().getColor(R.color.orrange));

//                    int position = getAdapterPosition();
//                    ((MainActivity) mContext).showMenuItems(position);
                }
            });
            img_email.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (home_data_list.get(getAdapterPosition()).getEmail() != null && !
                            home_data_list.get(getAdapterPosition()).getEmail().equals("")) {
                        try {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + home_data_list.get(getAdapterPosition()).getEmail().trim()));
                            intent.putExtra(Intent.EXTRA_SUBJECT, "Test");
                            intent.putExtra(Intent.EXTRA_TEXT, "Test");
                            mContext.startActivity(intent);
                        } catch (Exception e) {
                            Toast.makeText(mContext, "Sorry...You don't have any mail app", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(mContext, R.string.no_email, Toast.LENGTH_SHORT).show();
                    }

                }
            });
            img_locate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (home_data_list.get(getAdapterPosition()).getLatitude() != null &&
                            home_data_list.get(getAdapterPosition()).getLongitude() != null) {
                        Uri gmmIntentUri = Uri.parse("geo:<" + home_data_list.get(getAdapterPosition()).getLatitude() + ">,<" +
                                home_data_list.get(getAdapterPosition()).getLongitude() + ">?q=<" +
                                home_data_list.get(getAdapterPosition()).getLatitude() + ">,<" +
                                home_data_list.get(getAdapterPosition()).getLongitude() + ">(" +
                                home_data_list.get(getAdapterPosition()).getName() + ")");
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps");
                        mContext.startActivity(mapIntent);
                    } else {
                        Toast.makeText(mContext, "" +
                                mContext.getString(R.string.locate_detailsnot), Toast.LENGTH_SHORT).show();
                    }
                }
            });
            img_phn_num.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (home_data_list.get(getAdapterPosition()).getPhone() != null && !
                            home_data_list.get(getAdapterPosition()).getPhone().equals("")) {
                        Uri u = Uri.parse("tel:" + home_data_list.get(getAdapterPosition()).getPhone().trim());
                        Intent i = new Intent(Intent.ACTION_DIAL, u);
                        mContext.startActivity(i);
                    } else {
                        Toast.makeText(mContext, R.string.no_phone_num, Toast.LENGTH_SHORT).show();
                    }
                }

            });
        }
    }
//    public int setActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.activehome;
//                break;
//            case 1:
//                id=  R.drawable.activeaccount;
//
//                break;
//            case 2:
//                id= R.drawable.activelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.activewallet;
//                break;
//            case 4:
//                id=  R.drawable.activeorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.activewishlish;
//                break;
//            case 6:
//                id=  R.drawable.activereceipe;
//                break;
//            case 7:
//                id=  R.drawable.activelogout;
//                break;
//            default:
//                id=  R.drawable.activelogout;
//
//        }
//        return id;
//
//
//    }
//    public int setInActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.inactivehome;
//                break;
//            case 1:
//                id=  R.drawable.inactiveaccount;
//
//                break;
//            case 2:
//                id= R.drawable.inactivelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.inactivewallet;
//                break;
//            case 4:
//                id=  R.drawable.inactiveorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.inactivewishlish;
//                break;
//            case 6:
//                id=  R.drawable.inactivereceipe;
//                break;
//            case 7:
//                id=  R.drawable.inactivelogout;
//                break;
//            default:
//                id=  R.drawable.inactivelogout;
//
//        }
//        return id;
//
//
//    }
}
