package com.dci.avicustomer.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dci.avicustomer.R;
import com.dci.avicustomer.activities.ActivityLatestNews;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.ModelHome;

import java.util.List;

public class AdapterLatestNews extends RecyclerView.Adapter<AdapterLatestNews.DrawerViewHolder> {
    int type;
    OnItemClickListener onItemClickListener;
    private List<ModelHome.ANews> home_data_list;
    private Context mContext;

    public AdapterLatestNews(Context mContext, List<ModelHome.ANews> home_data_list, int type) {
        this.mContext = mContext;
        this.home_data_list = home_data_list;
        this.type = type;
    }

    @Override
    public AdapterLatestNews.DrawerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_latestnews, parent, false);
        return new AdapterLatestNews.DrawerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterLatestNews.DrawerViewHolder holder, final int position) {
        if (home_data_list.get(position).getId()!=null){
            holder.main_lay.setVisibility(View.VISIBLE);
            holder.seemore_lay.setVisibility(View.GONE);
            holder.tv_newsHeading.setText(home_data_list.get(position).getTitle());

            Glide.with(mContext).load(home_data_list.get(position).getImage()).into(holder.img_news);

            //Glide.get(mContext).with(holder.img_news).load(home_data_list.get(position).getImage());
        }
        else {
                holder.main_lay.setVisibility(View.GONE);
                holder.seemore_lay.setVisibility(View.VISIBLE);
            }




    }

    public void updateRowIndex(int pos) {
        type = pos;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return home_data_list.size();
    }

    public void setonItemClick(OnItemClickListener listener) {
        this.onItemClickListener = listener;

    }

    public void updateindex(int index) {
        this.type = index;
        notifyDataSetChanged();
    }

    class DrawerViewHolder extends RecyclerView.ViewHolder {
        TextView tv_newsHeading,tv_offer_des,tv_offer_percentage;
        ImageView img_news;
        LinearLayout seemore_lay;
        CardView main_lay;


        public DrawerViewHolder(View itemView) {
            super(itemView);
          //  tv_offer_des = itemView.findViewById(R.id.tv_offer_des);
            tv_newsHeading = itemView.findViewById(R.id.tv_newsHeading);
            img_news = itemView.findViewById(R.id.img_news);
            main_lay= itemView.findViewById(R.id.main_lay);
            seemore_lay= itemView.findViewById(R.id.seemore_lay);
            //tv_offer_percentage= itemView.findViewById(R.id.tv_offer_percentage);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getAdapterPosition()!=home_data_list.size()-1){
                        Intent intent=new Intent(mContext, ActivityLatestNews.class);
                        intent.putExtra("news_id",String.valueOf(home_data_list.get(getAdapterPosition()).getId()));
                        mContext.startActivity(intent);
                    }
                    else {
                        Intent intent=new Intent(mContext, ActivityLatestNews.class);
                        intent.putExtra("news_id","");
                        mContext.startActivity(intent);
                    }


                    // item_lay.setBackgroundColor(mContext.getResources().getColor(R.color.orrange));

//                    int position = getAdapterPosition();
//                    ((MainActivity) mContext).showMenuItems(position);
                }
            });
        }
    }
//    public int setActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.activehome;
//                break;
//            case 1:
//                id=  R.drawable.activeaccount;
//
//                break;
//            case 2:
//                id= R.drawable.activelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.activewallet;
//                break;
//            case 4:
//                id=  R.drawable.activeorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.activewishlish;
//                break;
//            case 6:
//                id=  R.drawable.activereceipe;
//                break;
//            case 7:
//                id=  R.drawable.activelogout;
//                break;
//            default:
//                id=  R.drawable.activelogout;
//
//        }
//        return id;
//
//
//    }
//    public int setInActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.inactivehome;
//                break;
//            case 1:
//                id=  R.drawable.inactiveaccount;
//
//                break;
//            case 2:
//                id= R.drawable.inactivelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.inactivewallet;
//                break;
//            case 4:
//                id=  R.drawable.inactiveorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.inactivewishlish;
//                break;
//            case 6:
//                id=  R.drawable.inactivereceipe;
//                break;
//            case 7:
//                id=  R.drawable.inactivelogout;
//                break;
//            default:
//                id=  R.drawable.inactivelogout;
//
//        }
//        return id;
//
//
//    }
}
