//package com.dci.avi.adapter;
//
//import android.app.ProgressDialog;
//import android.content.Context;
//import android.graphics.Typeface;
//import android.support.v4.content.ContextCompat;
//import android.support.v4.content.res.ResourcesCompat;
//import android.support.v7.widget.RecyclerView;
//import android.util.Log;
//import android.view.Gravity;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.lab.livigro.R;
//import com.lab.livigro.activity.ActivityChartRaiseTest;
//import com.lab.livigro.model.ChartTestModel;
//import com.lab.livigro.model.CommonMsgStatusModel;
//import com.lab.livigro.model.DummyParentDataItem;
//import com.lab.livigro.retrofit.ApiInterface;
//import com.lab.livigro.retrofit.ServiceGenerator;
//import com.lab.livigro.utils.Constants;
//import com.lab.livigro.utils.UtilsDefault;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//
//public class RecyclerDataAdapter extends RecyclerView.Adapter<RecyclerDataAdapter.MyViewHolder> {
//   // public ArrayList<DummyParentDataItem> dummyParentDataItems;
//    List<ChartTestModel.Datum> dummyParentDataItems;
//    Context context;
//    ProgressDialog progressDialog;
//
//    public RecyclerDataAdapter(List<ChartTestModel.Datum> dummyParentDataItems,Context context) {
//        this.dummyParentDataItems = dummyParentDataItems;
//        this.context=context;
//    }
//
//    @Override
//    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_selected_test, parent, false);
//        return new MyViewHolder(itemView);
//    }
//
//    @Override
//    public void onBindViewHolder(MyViewHolder holder, int position) {
//        ChartTestModel.Datum dummyParentDataItem = dummyParentDataItems.get(position);
//        holder.textView_parentName.setText(dummyParentDataItem.getName());
//        //
//        int noOfChildTextViews = holder.linearLayout_childItems.getChildCount();
//        if (dummyParentDataItem.getGroup_package_array()!=null&&dummyParentDataItem.getGroup_package_array().size()!=0){
//            holder.droparrow.setVisibility(View.VISIBLE);
//            int noOfChild = dummyParentDataItem.getGroup_package_array().size();
//            if (noOfChild < noOfChildTextViews) {
//                for (int index = noOfChild; index < noOfChildTextViews; index++) {
//                    TextView currentTextView = (TextView) holder.linearLayout_childItems.getChildAt(index);
//                    currentTextView.setVisibility(View.GONE);
//                }
//            }
//            for (int textViewIndex = 0; textViewIndex < noOfChild; textViewIndex++) {
//                TextView currentTextView = (TextView) holder.linearLayout_childItems.getChildAt(textViewIndex);
//                currentTextView.setText(dummyParentDataItem.getGroup_package_array().get(textViewIndex).getName());
//                /*currentTextView.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Toast.makeText(mContext, "" + ((TextView) view).getText().toString(), Toast.LENGTH_SHORT).show();
//                    }
//                });*/
//            }
//        }
//        else {
//            holder.droparrow.setVisibility(View.INVISIBLE);
//        }
//
//
//    }
//
//    @Override
//    public int getItemCount() {
//        return dummyParentDataItems.size();
//    }
//
//    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
//        private Context context;
//        private TextView textView_parentName;
//        RelativeLayout parent_lay;
//        private LinearLayout linearLayout_childItems;
//        ImageView remove,droparrow;
//
//        MyViewHolder(View itemView) {
//            super(itemView);
//            context = itemView.getContext();
//            textView_parentName = itemView.findViewById(R.id.testname);
//            parent_lay = itemView.findViewById(R.id.parent_lay);
//            remove= itemView.findViewById(R.id.remove);
//            droparrow= itemView.findViewById(R.id.droparrow);
//            linearLayout_childItems = itemView.findViewById(R.id.ll_child_items);
//            linearLayout_childItems.setVisibility(View.GONE);
//            int intMaxNoOfChild = 0;
//            for (int index = 0; index < dummyParentDataItems.size(); index++) {
//                if(dummyParentDataItems.get(index).getGroup_package_array()!=null){
//                    int intMaxSizeTemp = dummyParentDataItems.get(index).getGroup_package_array().size();
//                    if (intMaxSizeTemp > intMaxNoOfChild) intMaxNoOfChild = intMaxSizeTemp;
//                }
//
//            }
//            for (int indexView = 0; indexView < intMaxNoOfChild; indexView++) {
//                TextView textView = new TextView(context);
//                textView.setId(indexView);
//                textView.setPadding(0, 20, 0, 20);
//                textView.setGravity(Gravity.CENTER);
//                Typeface typeface = ResourcesCompat.getFont(context, R.font.sfregular);
//                textView.setTypeface(typeface);
//
//                textView.setTextColor(ContextCompat.getColor(context,R.color.colorPrimary));
//                textView.setBackground(ContextCompat.getDrawable(context, R.drawable.background_sub_module_text));
//                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//                textView.setOnClickListener(this);
//                linearLayout_childItems.addView(textView, layoutParams);
//            }
//           // textView_parentName.setOnClickListener(this);
//            parent_lay.setOnClickListener(this);
//            remove.setOnClickListener(this);
//        }
//
//        @Override
//        public void onClick(View view) {
//            if (view==parent_lay) {
//                if (dummyParentDataItems.get(getAdapterPosition()).getGroup_package_array()!=null&&
//                        dummyParentDataItems.get(getAdapterPosition()).getGroup_package_array().size()!=0){
//                    if (linearLayout_childItems.getVisibility() == View.VISIBLE) {
//                        linearLayout_childItems.setVisibility(View.GONE);
//                        droparrow.setImageResource(R.drawable.down_arrow);
//                    } else {
//                        droparrow.setImageResource(R.drawable.up_arrow);
//                        linearLayout_childItems.setVisibility(View.VISIBLE);
//                    }
//                }
//                else {
//                    droparrow.setVisibility(View.INVISIBLE);
//                }
//
//            }
////            } else {
////                TextView textViewClicked = (TextView) view;
////                Toast.makeText(context, "" + textViewClicked.getText().toString(), Toast.LENGTH_SHORT).show();
////            }
//            if (view==remove){
//
//                insertChart(String.valueOf(dummyParentDataItems.get(getAdapterPosition()).getId()),getAdapterPosition(),dummyParentDataItems.get(getAdapterPosition()).getType());
//            }
//        }
//    }
//    public void insertChart(final String testid, final int pos,String type) {
//        if (UtilsDefault.isOnline()) {
//            progressDialog = new ProgressDialog(context);
//
//            progressDialog.setMessage((context.getString(R.string.pleasewait)));
//            progressDialog.setCancelable(false);
//            progressDialog.show();
//            //  }
//
//            Log.d("testtype", "insertChart: "+type);
//
//
////            ApiInterface apiService =
////                    APIClient.getClient().create(ApiInterface.class);
//
//            ApiInterface apiService =
//                    ServiceGenerator.createService(ApiInterface.class);
//            Call<CommonMsgStatusModel> call = apiService.insertchart(type,((ActivityChartRaiseTest)context).getPid(), testid,
//                    "1"
//            );
//            call.enqueue(new Callback<CommonMsgStatusModel>() {
//                @Override
//                public void onResponse(Call<CommonMsgStatusModel> call, Response<CommonMsgStatusModel> response) {
//                    //  hiderefresh();
//
//                    if (progressDialog != null) {
//                        progressDialog.dismiss();
//                    }
//
//
//                    if (response.body() != null) {
//                        Log.d("status", "onResponse: "+response.body().getStatus());
//                        if (response.body().getStatus() == Constants.STATUS_200) {
//                            // Toast.makeText(context, R.string.addedsuccessfully, Toast.LENGTH_SHORT).show();
//                            //  UtilsDefault.updateSharedPreference(Constants.chartcount,"1");
//                            try {
//                                UtilsDefault.updateSharedPreference(Constants.USER_BLOCK,String.valueOf(response.body().getUserIsBlocked()));
//                            }
//                            catch (Exception e){
//
//                            }
//                            String header = response.headers().get(context.getString(R.string.header_find_key));
//                            UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
//                            //updateBadgeView(1);
//                            // Log.e("responsesucces", "" + (response.body().toString()));
//
//                        } else if (response.body().getStatus() == Constants.STATUS_415) {
//                            String header = response.headers().get(context.getString(R.string.header_find_key));
//                            UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
//                            insertChart(testid, pos,"");
//                            // Loadspinner();
//                            //  Toast.makeText(context, R.string.authorizationfailed, Toast.LENGTH_SHORT).show();
//                        }
//                        //414 token invalid
//                        else if (response.body().getStatus() == Constants.STATUS_414) {
//
//                            Toast.makeText(context, R.string.authorizationfailed, Toast.LENGTH_SHORT).show();
//
//
//                        } else if (response.body().getStatus() == Constants.STATUS_416) {
//
//
//                            Toast.makeText(context, R.string.usernotfound, Toast.LENGTH_SHORT).show();
//                        } else if (response.body().getStatus() == Constants.STATUS_201) {
//                            String header = response.headers().get(context.getString(R.string.header_find_key));
//                            UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
//                            ((ActivityChartRaiseTest)context).getRaisedTest();
//                           // removeAt(pos);
//                            Toast.makeText(context, R.string.removedsuccessfully, Toast.LENGTH_SHORT).show();
//
//
//                        } else {
//                            // Toast.makeText(ActivityRaiseTest.this, R.string.nodata, Toast.LENGTH_SHORT).show();
//                            String header = response.headers().get(context.getString(R.string.header_find_key));
//                            UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
//
//
//                        }
//
//
//                    } else {
//                        Toast.makeText(context, R.string.api_error, Toast.LENGTH_SHORT).show();
//                    }
//
//
//                    //Testorderform_cl cl=new Testorderform_cl(response.body())
//                    // Log.e("name",call.)
//                }
//
//                @Override
//                public void onFailure(Call<CommonMsgStatusModel> call, Throwable t) {
//                    Log.e("responsefailure", "" + call.toString());
//                    if (progressDialog != null) {
//                        progressDialog.dismiss();
//                    }
//
//                    Toast.makeText(context, R.string.server_error, Toast.LENGTH_SHORT).show();
//
//                }
//            });
//        } else {
//            if (progressDialog != null) {
//                progressDialog.dismiss();
//            }
//
//
//            Toast.makeText(context, R.string.no_net, Toast.LENGTH_SHORT).show();
//        }
//
//    }
//    public void removeAt(int position) {
//        dummyParentDataItems.remove(position);
//        notifyItemRemoved(position);
//       // notifyItemRangeChanged(position, dummyParentDataItems.size());
//       // ((ActivityChartRaiseTest)c).updateamount(child);
//    }
//
//
//}