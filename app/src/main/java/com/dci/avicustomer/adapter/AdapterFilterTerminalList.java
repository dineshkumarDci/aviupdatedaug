package com.dci.avicustomer.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dci.avicustomer.R;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.ModelShopList;
import com.dci.avicustomer.models.ModelUserTipList;

import java.util.List;

public class AdapterFilterTerminalList extends RecyclerView.Adapter<AdapterFilterTerminalList.DrawerViewHolder> {
    int row_index;
    OnItemClickListener onItemClickListener;
    private List<ModelShopList.Terminal> terminalList;
    private Context mContext;
    int selectedpos=-1;

    public AdapterFilterTerminalList(Context mContext, List<ModelShopList.Terminal> terminalList, int row_index) {
        this.mContext = mContext;
        this.terminalList = terminalList;
        selectedpos=row_index;
    }

    @Override
    public AdapterFilterTerminalList.DrawerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_filter_terminal_list, parent,
                false);
        return new AdapterFilterTerminalList.DrawerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterFilterTerminalList.DrawerViewHolder holder, final int position) {
        holder.tv_terminal_name.setBackground(mContext.getDrawable(R.drawable.shop_filter_unselect));
        holder.tv_terminal_name.setTextColor(mContext.getResources().getColor(R.color.filter_grey));
        holder.tv_terminal_name.setText(terminalList.get(position).getName());
        holder.tv_terminal_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedpos=position;
                onItemClickListener.OnItemClickListener(position,v);
                notifyDataSetChanged();

            }
        });
        if (position==selectedpos){
            holder.tv_terminal_name.setBackground(mContext.getDrawable(R.drawable.shop_filter_select));
            holder.tv_terminal_name.setTextColor(mContext.getResources().getColor(R.color.white));
        }

//        holder.item_lay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onItemClickListener.OnItemClickListener(position, view);
//                if (position!=12){
//                    row_index = position;
//                    notifyDataSetChanged();
//                }
//
//            }
//        });

    }
    public void updateRowIndex(int pos){
        selectedpos=pos;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return terminalList.size();
    }

    public void setonItemClick(OnItemClickListener listener) {
        this.onItemClickListener = listener;

    }

    public void updateindex(int index) {
        this.selectedpos = index;
        notifyDataSetChanged();
    }

    class DrawerViewHolder extends RecyclerView.ViewHolder {
        TextView tv_terminal_name,tv_user_tipdate;



        public DrawerViewHolder(View itemView) {
            super(itemView);
            tv_terminal_name = itemView.findViewById(R.id.tv_terminal_name);
          //  tv_user_tipdate = itemView.findViewById(R.id.tv_user_tipdate);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    // item_lay.setBackgroundColor(mContext.getResources().getColor(R.color.orrange));

//                    int position = getAdapterPosition();
//                    ((MainActivity) mContext).showMenuItems(position);
                }
            });
        }
    }
//    public int setActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.activehome;
//                break;
//            case 1:
//                id=  R.drawable.activeaccount;
//
//                break;
//            case 2:
//                id= R.drawable.activelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.activewallet;
//                break;
//            case 4:
//                id=  R.drawable.activeorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.activewishlish;
//                break;
//            case 6:
//                id=  R.drawable.activereceipe;
//                break;
//            case 7:
//                id=  R.drawable.activelogout;
//                break;
//            default:
//                id=  R.drawable.activelogout;
//
//        }
//        return id;
//
//
//    }
//    public int setInActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.inactivehome;
//                break;
//            case 1:
//                id=  R.drawable.inactiveaccount;
//
//                break;
//            case 2:
//                id= R.drawable.inactivelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.inactivewallet;
//                break;
//            case 4:
//                id=  R.drawable.inactiveorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.inactivewishlish;
//                break;
//            case 6:
//                id=  R.drawable.inactivereceipe;
//                break;
//            case 7:
//                id=  R.drawable.inactivelogout;
//                break;
//            default:
//                id=  R.drawable.inactivelogout;
//
//        }
//        return id;
//
//
//    }
}
