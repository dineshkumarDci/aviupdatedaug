package com.dci.avicustomer.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dci.avicustomer.R;
import com.dci.avicustomer.activities.ActivityOfferPage;
import com.dci.avicustomer.activities.ActivityRestaurantDetails;
import com.dci.avicustomer.activities.ActivityShopDetails;
import com.dci.avicustomer.activities.ActivityShopList;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.ModelHome;
import com.dci.avicustomer.models.ModelOfferList;

import java.util.ArrayList;
import java.util.List;

public class AdapterOfferList extends RecyclerView.Adapter<AdapterOfferList.DrawerViewHolder> {
    String type;
    OnItemClickListener onItemClickListener;
    private List<ModelOfferList.Datum> home_data_list = new ArrayList<>();
    private Context mContext;
    String imagepath;
    ModelOfferList offerData = new ModelOfferList();
    ModelOfferList.ImagePath imagePath = new ModelOfferList().new ImagePath();

    public AdapterOfferList(Context mContext, List<ModelOfferList.Datum> home_data_list) {
        this.mContext = mContext;
        this.type = type;
        this.home_data_list = home_data_list;

        Log.d("onBindVieswHolder", "onBindViewHolder: " + home_data_list.size());
    }

    @Override
    public AdapterOfferList.DrawerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_offers, parent, false);
        return new AdapterOfferList.DrawerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterOfferList.DrawerViewHolder holder, final int position) {
        ModelOfferList.Datum datum = home_data_list.get(position);
        try {
            if (datum.getRestaurants() != null) {
                holder.tv_restro_name.setText(datum.getRestaurants().getName());
//                if (datum.getRestaurants().getShop_image()!=null){
//
//                    if (!datum.getRestaurants().getShop_image().contains(",")){
//                        Glide.with(mContext).load(imagePath.getRestaurant()+datum.getRestaurants().getShop_image()).into(holder.imag_offericon);
//
//                    }
//                    else {
//                        String[]data=datum.getRestaurants().getShop_image().split(",");
//                        Glide.with(mContext).load(imagePath.getRestaurant()+data[0]).into(holder.imag_offericon);
//
//                    }
//                }
                if (datum.getRestaurants().getAverage_rating() != null &&
                        !datum.getRestaurants().getAverage_rating().equals("")) {

                    holder.rating.setVisibility(View.VISIBLE);
                    holder.rating.setRating(Float.valueOf(datum.getRestaurants().getAverage_rating()));
                } else {
                    holder.rating.setVisibility(View.GONE);
                }

//                Glide.with(mContext).load(offerData.getImagePath().getRestaurant()+
//                        datum.getRestaurants().getShop_image()).into(holder.imag_offericon);

            }
            if (datum.getRetail__shop() != null) {
                holder.tv_restro_name.setText(datum.getRetail__shop().getName());
//                if (datum.getRetail__shop().getProfile_img()!=null){
//
//                    if (!datum.getRetail__shop().getProfile_img().contains(",")){
//                        Glide.with(mContext).load(imagePath.getCoupons()+datum.getRetail__shop().getProfile_img()).into(holder.imag_offericon);
//
//                    }
//                    else {
//                        String[]data=datum.getRetail__shop().getProfile_img().split(",");
//                        Glide.with(mContext).load(imagePath.getCoupons()+data[0]).into(holder.imag_offericon);
//
//                    }
//                }

            }
            Glide.with(mContext).load(imagePath.getCoupons() + datum.getImage()).into(holder.imag_offericon);

            if (datum.getOffer_type_id()!=null&&datum.getOffer_type_id()==1){
                holder.tv_offer_percentage.setText(datum.getDiscount_percentage());
                holder.lin_offer_lay.setVisibility(View.VISIBLE);
                holder.tv_flat.setVisibility(View.GONE);

            }
            else if (datum.getOffer_type_id()!=null&&datum.getOffer_type_id()==2){
                holder.tv_flat.setText("Flat "+datum.getFlat_rate());
                holder.lin_offer_lay.setVisibility(View.GONE);
                holder.tv_flat.setVisibility(View.VISIBLE);
            }


            try {
                byte[] base64 = Base64.decode(datum.getDescription(), Base64.DEFAULT);
                String html = new String(base64, "UTF-8");

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    holder.tv_offer_des.setText(Html.fromHtml(html, Html.FROM_HTML_MODE_COMPACT));
                } else {
                    holder.tv_offer_des.setText(Html.fromHtml(html));
                }
            } catch (Exception e) {
                e.printStackTrace();
                holder.tv_offer_des.setText(datum.getDescription());


            }
            holder.tv_offer_des.setText(UtilsDefault.checkNull(datum.getTitle()));
            // holder.tv_offer_des.setText(datum.getTitle());
            holder.imag_offericon.setClipToOutline(true);

        } catch (Exception e) {

        }


    }

    public void setImagePath(String pathRestro, String path,String shopsImage) {
        imagePath.setCoupons(path);
        imagePath.setRestaurant(pathRestro);
        imagePath.setShopimage(shopsImage);
    }


    @Override
    public int getItemCount() {
        return home_data_list.size();
    }

    public void setonItemClick(OnItemClickListener listener) {
        this.onItemClickListener = listener;

    }


    class DrawerViewHolder extends RecyclerView.ViewHolder {
        TextView tv_restro_name, tv_offer_des, tv_offer_percentage,tv_flat;
        ImageView imag_offericon;
        LinearLayout lin_offer_lay;
        RatingBar rating;


        public DrawerViewHolder(View itemView) {
            super(itemView);
            rating = itemView.findViewById(R.id.rating);
            tv_offer_des = itemView.findViewById(R.id.tv_offer_des);
            tv_flat = itemView.findViewById(R.id.tv_flat);
            tv_restro_name = itemView.findViewById(R.id.tv_restro_name);
            imag_offericon = itemView.findViewById(R.id.imag_offericon);
            tv_offer_percentage = itemView.findViewById(R.id.tv_offer_percentage);
            lin_offer_lay= itemView.findViewById(R.id.lin_offer_lay);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
                        if (home_data_list.get(getAdapterPosition()).getRestaurants() != null) {
                            Intent intent = new Intent(mContext, ActivityRestaurantDetails.class);
                            intent.putExtra("id",
                                    String.valueOf(home_data_list.get(getAdapterPosition()).getRestaurants().getId()));
                            mContext.startActivity(intent);
                        } else if (home_data_list.get(getAdapterPosition()).getRetail__shop() != null) {
                          //  Toast.makeText(mContext, R.string.restro_not_avialable, Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(mContext, ActivityShopDetails.class);
                            intent.putExtra("data", home_data_list.get(getAdapterPosition()).getRetail__shop());
                            intent.putExtra("imgbase",imagePath.getShopimage());
                            mContext.startActivity(intent);

                        } else {
                            Toast.makeText(mContext, R.string.restro_not_avialable, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(mContext, R.string.login_messge, Toast.LENGTH_SHORT).show();
                    }

                    // item_lay.setBackgroundColor(mContext.getResources().getColor(R.color.orrange));

//                    int position = getAdapterPosition();
//                    ((MainActivity) mContext).showMenuItems(position);
                }
            });
        }
    }
//    public int setActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.activehome;
//                break;
//            case 1:
//                id=  R.drawable.activeaccount;
//
//                break;
//            case 2:
//                id= R.drawable.activelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.activewallet;
//                break;
//            case 4:
//                id=  R.drawable.activeorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.activewishlish;
//                break;
//            case 6:
//                id=  R.drawable.activereceipe;
//                break;
//            case 7:
//                id=  R.drawable.activelogout;
//                break;
//            default:
//                id=  R.drawable.activelogout;
//
//        }
//        return id;
//
//
//    }
//    public int setInActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.inactivehome;
//                break;
//            case 1:
//                id=  R.drawable.inactiveaccount;
//
//                break;
//            case 2:
//                id= R.drawable.inactivelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.inactivewallet;
//                break;
//            case 4:
//                id=  R.drawable.inactiveorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.inactivewishlish;
//                break;
//            case 6:
//                id=  R.drawable.inactivereceipe;
//                break;
//            case 7:
//                id=  R.drawable.inactivelogout;
//                break;
//            default:
//                id=  R.drawable.inactivelogout;
//
//        }
//        return id;
//
//
//    }
}
