package com.dci.avicustomer.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.avicustomer.R;
import com.dci.avicustomer.models.ModelFacilities;

import java.util.List;

public class AdapterFacility extends RecyclerView.Adapter<AdapterFacility.MyViewHolder> {
   // public ArrayList<DummyParentDataItem> dummyParentDataItems;
    List<ModelFacilities.Datum> dummyParentDataItems;
    Context context;
    ProgressDialog progressDialog;

    public AdapterFacility(List<ModelFacilities.Datum> dummyParentDataItems, Context context) {
        this.dummyParentDataItems = dummyParentDataItems;
        this.context=context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_facility_parent,
                parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ModelFacilities.Datum dummyParentDataItem = dummyParentDataItems.get(position);
        holder.tv_title.setText(dummyParentDataItem.getName());
        //
        int noOfChildTextViews = holder.linearLayout_childItems.getChildCount();
        if (dummyParentDataItem.getLoungefacility()!=null&&dummyParentDataItem.getLoungefacility().size()!=0)
        {
            int noOfChild = dummyParentDataItem.getLoungefacility().size();
//            if (noOfChild < noOfChildTextViews) {
//                for (int index = noOfChild; index < noOfChildTextViews; index++) {
//                    TextView currentTextView = (TextView) holder.linearLayout_childItems.getChildAt(index);
//                    currentTextView.setVisibility(View.GONE);
//                }
//            }
            for (int textViewIndex = 0; textViewIndex < noOfChild; textViewIndex++) {
                View view = (View) holder.linearLayout_childItems.getChildAt(textViewIndex);
                view.setVisibility(View.VISIBLE);
                TextView textView=(TextView) view.findViewById(R.id.tv_facility_child);
                textView.setText(dummyParentDataItem.getLoungefacility().get(textViewIndex).getFacility().getName());

                //currentTextView.setText(dummyParentDataItem.getLoungefacility().get(textViewIndex).getFacility().getName());
                /*currentTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(mContext, "" + ((TextView) view).getText().toString(), Toast.LENGTH_SHORT).show();
                    }
                });*/
            }
        }



    }

    @Override
    public int getItemCount() {
        return dummyParentDataItems.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private Context context;
        private TextView tv_title;

        private LinearLayout linearLayout_childItems;
        private TextView tv_facility_child;



        MyViewHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();
            tv_title = itemView.findViewById(R.id.tv_title);


            linearLayout_childItems = itemView.findViewById(R.id.linear_child);
            linearLayout_childItems.setVisibility(View.VISIBLE);
            int intMaxNoOfChild = 0;
            for (int index = 0; index < dummyParentDataItems.size(); index++) {
                if (dummyParentDataItems.get(index).getLoungefacility() != null) {
                    int intMaxSizeTemp = dummyParentDataItems.get(index).getLoungefacility().size();
                    if (intMaxSizeTemp > intMaxNoOfChild) {
                        intMaxNoOfChild = intMaxSizeTemp;
                    }
                }
            }
            for (int indexView = 0; indexView < intMaxNoOfChild; indexView++) {
                View myView = LayoutInflater.from(context).inflate(R.layout.list_item_facility_child, null);
                tv_facility_child=myView.findViewById(R.id.tv_facility_child);
                tv_facility_child.setTag(indexView);
                myView.setTag(indexView);

                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//                textView.setOnClickListener(this);
                linearLayout_childItems.setTag(indexView);
                linearLayout_childItems.addView(myView, layoutParams);
            }
            Log.d("viewhold", "MyViewHolder: "+intMaxNoOfChild+"index"+getAdapterPosition());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (v.getId()==R.id.tv_facility_child){
                        int pos= (int) v.getTag();
                        Toast.makeText(context, dummyParentDataItems.get(getAdapterPosition()).getLoungefacility().get(pos).getFacility().getName(),
                                Toast.LENGTH_SHORT).show();
                    }
                }
            });

           // textView_parentName.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {

//            } else {
//                TextView textViewClicked = (TextView) view;
//                Toast.makeText(context, "" + textViewClicked.getText().toString(), Toast.LENGTH_SHORT).show();
//            }

        }
    }

    public void removeAt(int position) {
        dummyParentDataItems.remove(position);
        notifyItemRemoved(position);
       // notifyItemRangeChanged(position, dummyParentDataItems.size());
       // ((ActivityChartRaiseTest)c).updateamount(child);
    }


}