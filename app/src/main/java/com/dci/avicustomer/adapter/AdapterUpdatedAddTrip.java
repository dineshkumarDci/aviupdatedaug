package com.dci.avicustomer.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.avicustomer.R;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.interfaces.OnImageRemoveListener;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.ModelUpdatedFlightStatus;
import com.jaiselrahman.filepicker.model.MediaFile;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AdapterUpdatedAddTrip extends RecyclerView.Adapter<AdapterUpdatedAddTrip.ViewHolder> {
    int type;
    OnItemClickListener onItemClickListener;
    private List<ModelUpdatedFlightStatus.Data> home_data_list;
    private Context mContext;

    public AdapterUpdatedAddTrip(Context mContext,
                                 List<ModelUpdatedFlightStatus.Data> home_data_list, int type) {
        this.mContext = mContext;
        this.home_data_list = home_data_list;
        this.type = type;
    }

    @Override
    public AdapterUpdatedAddTrip.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_add_trip, parent, false);
        return new AdapterUpdatedAddTrip.ViewHolder(view);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(AdapterUpdatedAddTrip.ViewHolder holder, final int position) {
        ModelUpdatedFlightStatus.Data data = home_data_list.get(position);
        holder.tv_airlinenames.setText(UtilsDefault.checkNull(data.getAirline()));
        holder.tv_airlinename.setText(UtilsDefault.checkNull(data.getAirline()));
        holder.flight_code.setSelected(true);
        if (data.getFlightStatus() != null && !
                data.getFlightStatus().equals("")) {
            holder.tv_dept_status.setText(data.getFlightStatus());
        }
        if (data.getArrCity() != null &&
                data.getArrCountry() != null) {
            holder.tv_arrival_city.setText(data.getArrCity() + "," +
                    data.getArrCountry());
            holder.tv_arrival_city1.setText(data.getArrCity() + "," +
                    data.getArrCountry());
            holder. tv_arr_airport_name.setText(data.getArrCity() + "," +
                    data.getArrCountry());

        } else {
            holder.tv_arrival_city1.setText(mContext.getString(R.string.nill));
            holder.tv_arrival_city.setText(mContext.getString(R.string.nill));
            holder.tv_arr_airport_name.setText(mContext.getString(R.string.nill));
        }

        if (data.getArrCode() != null) {
            holder.tv_arrive_code.setText(data.getArrCode());
            holder.tv_arrive_code1.setText(data.getArrCode());
        } else {
            holder. tv_arrive_code.setText(mContext.getString(R.string.nill));
            holder. tv_arrive_code1.setText(mContext.getString(R.string.nill));
        }
        if (data.getAirlineCode() != null) {
            holder.tv_flight_code.setText(data.getAirlineCode() +
                    data.getFlightNumber());
            holder.flight_code.setText(data.getAirlineCode() +
                    data.getFlightNumber());
        } else {
            holder.flight_code.setText(mContext.getString(R.string.nill));
            holder.tv_flight_code.setText(mContext.getString(R.string.nill));
        }


        if (data.getArrTime_actual() != null && data.getArrdate_actual() != null) {
            holder.tv_arr_actualtime.setText(UtilsDefault.timeFormat(data.getArrTime_actual()) + " , " +
                    UtilsDefault.dateFormatWithoutYear(
                            data.getArrdate_actual() + " 01:34:01"));
        } else {
            holder.tv_arr_actualtime.setText(mContext.getString(R.string.nill));
        }

        if (data.getDepTime_actual() != null && data.getDepdate_actual() != null) {
            holder. tv_dep_actualtime.setText(UtilsDefault.timeFormat(data.getDepTime_actual()) + " , " +
                    UtilsDefault.dateFormatWithoutYear(
                            data.getDepdate_actual() + " 01:34:01"));
        } else {
            holder.  tv_dep_actualtime.setText(mContext.getString(R.string.nill));
        }

        if (data.getArrTime() != null) {
            holder.  tv_arrive_time.setText(UtilsDefault.timeFormat(data.getArrTime()));
            holder.  tv_arrive_time1.setText(UtilsDefault.timeFormat(data.getArrTime()));
            holder. tv_arr_schedule_time.setText(UtilsDefault.timeFormat(data.getArrTime()) + " , " +
                    UtilsDefault.dateFormatWithoutYear(
                            data.getArrdate() + " 01:34:01"));

        } else {
            holder.tv_arrive_time.setText(mContext.getString(R.string.nill));
            holder.tv_arrive_time1.setText(mContext.getString(R.string.nill));
            holder.tv_arr_schedule_time.setText(mContext.getString(R.string.nill));
        }
        if (data.getArrdate() != null) {
            holder.tv_arrive_date.setText(UtilsDefault.dateFormat(
                    data.getArrdate() + " 01:34:01"));
            holder.tv_arrive_date1.setText(UtilsDefault.dateFormat(
                    data.getArrdate() + " 01:34:01"));

        } else {
            holder.tv_arrive_date1.setText(mContext.getString(R.string.nill));
            holder.tv_arrive_date.setText(mContext.getString(R.string.nill));
        }


        if (data.getArrTerminal() != null && !data.getArrTerminal().equals("")) {
            holder.tv_arrival_terminal.setText(mContext.getString(R.string.text_terminal) + " " +
                    UtilsDefault.checkNull(data.getArrTerminal())
            );
            holder.tv_arr_terminal_name.setText(mContext.getString(R.string.text_terminal) + " " +
                    UtilsDefault.checkNull(data.getArrTerminal()) + " "
                    + " , " + UtilsDefault.checkNull(data.getArrGate()));
        }




        // tv_dept_terminal.setText(R.string.text_terminal+" "+data.getData().getDeparture().getAirport().getAirportId().getAirportCode());
        //   tv_dept_terminal.setText(R.string.text_terminal+" "+data.getData().getDeparture().getAirport().getTerminal());
        //   UtilsDefault.updateSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));


        if (data.getDepTerminal() != null && !data.getDepTerminal().equals("")) {
            holder.tv_dept_terminal.setText(mContext.getString(R.string.text_terminal) + " " +
                    UtilsDefault.checkNull(data.getDepTerminal()));
            holder.tv_dep_terminal_name.setText(mContext.getString(R.string.text_terminal) + " " +
                    UtilsDefault.checkNull(data.getDepTerminal()) + " , "
                    + UtilsDefault.checkNull(data.getDepGate()));
        }


        if (data.getDepdate() != null) {
            holder.tv_dept_date.setText(UtilsDefault.dateFormat(data.getDepdate()
                    + " 01:34:01"));
            holder.tv_dept_date1.setText(UtilsDefault.dateFormat(data.getDepdate()
                    + " 01:34:01"));

        } else {
            holder.tv_dept_date1.setText(mContext.getString(R.string.nill));
            holder.tv_dept_date.setText(mContext.getString(R.string.nill));

        }
        if (data.getDepTime() != null) {
            holder. tv_dept_time.setText(UtilsDefault.timeFormat(data.getDepTime()));
            holder. tv_dept_time1.setText(UtilsDefault.timeFormat(data.getDepTime()));

            holder. tv_dep_schedule_time.setText(UtilsDefault.timeFormat(data.getDepTime()) + " , " +
                    UtilsDefault.dateFormatWithoutYear(data.getDepdate() + " 01:34:01"));
        } else {
            holder.tv_dept_time.setText(mContext.getString(R.string.nill));
            holder. tv_dept_time1.setText(UtilsDefault.timeFormat(data.getDepTime()));
            holder.tv_dep_schedule_time.setText(mContext.getString(R.string.nill));
        }
        if (data.getDepCity() != null &&
                data.getDeptCountry() != null) {
            holder. tv_dept_city.setText(data.getDepCity() + "," +
                    data.getDeptCountry());
            holder. tv_dept_city1.setText(data.getDepCity() + "," +
                    data.getDeptCountry());
            holder.tv_dep_airport_name.setText(data.getDepCity() + "," +
                    data.getDeptCountry());

        } else {
            holder. tv_dept_city1.setText(mContext.getString(R.string.nill));
            holder. tv_dept_city.setText(mContext.getString(R.string.nill));
            holder.tv_dep_airport_name.setText(mContext.getString(R.string.nill));
        }
        if (data.getDepCode() != null) {
            holder. tv_dept_code.setText(data.getDepCode());
            holder. tv_dept_code1.setText(data.getDepCode());
        } else {
            holder. tv_dept_code.setText(mContext.getString(R.string.nill));
            holder. tv_dept_code1.setText(mContext.getString(R.string.nill));
        }

        if (data.getDepTime() != null && data.getArrTime() != null) {
            try {
                if (data.getDepTime() != null &&
                        data.getArrTime() != null) {
                    holder.tv_travel_time.setText(UtilsDefault.differentBetweenTime(data.getDepdate()+" "+data.getDepTime(),
                            data.getArrdate()+" "+ data.getArrTime()));
                    holder.tv_travel_time1.setText(UtilsDefault.differentBetweenTime(data.getDepdate()+" "+data.getDepTime(),
                            data.getArrdate()+" "+ data.getArrTime()));
                } else {
                    holder.tv_travel_time.setText(mContext.getString(R.string.nill));
                    holder.tv_travel_time1.setText(mContext.getString(R.string.nill));
                }
            } catch (Exception e) {
                Toast.makeText(mContext, R.string.server_error, Toast.LENGTH_SHORT).show();
            }

        } else {
            holder.tv_travel_time1.setText(mContext.getString(R.string.nill));
            holder. tv_travel_time.setText(mContext.getString(R.string.nill));
        }


        holder. tv_dept_status.setBackground(mContext.getDrawable(R.drawable.bg_flight_dept_status));


        if (data!= null && data.getFlightStatus() != null &&
                data.getFlightStatus().toLowerCase().equals(Constants.DELAYED)) {
            holder.tv_dept_status.setBackground(mContext.getDrawable(R.drawable.bg_flight_red_status));
        } else {
            holder. tv_dept_status.setBackground(mContext.getDrawable(R.drawable.bg_flight_dept_status));
        }
        boolean isdeparted = false;
//        if (data != null && data.getDep_delay() != null &&
//                !data.getDep_delay().equals("0")) {
//            holder.dep_status.setVisibility(View.VISIBLE);
//            holder. tv_dep_delay.setVisibility(View.VISIBLE);


        if (data.getDepdate_actual() != null &&
                data.getDepTime_actual() != null &&
                !data.getDepdate_actual().equals("") &&
                !data.getDepTime_actual().equals("")) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date newDate = null;
            try {
                newDate = format.parse(data.getDepdate_actual() + " " +
                        data.getDepTime_actual());
                if (Calendar.getInstance().getTime().after(newDate)) {
                    isdeparted=true;
                    if (data == null ||data.getDep_delay() == null) {
                        //   holder.dep_status.setVisibility(View.VISIBLE);
                        //   holder.tv_dep_delay.setVisibility(View.VISIBLE);
                    }
                    else {
                        if ( !data.getDep_delay().equals("0")){
                            holder.tv_dept_status.setText(mContext.getString(R.string.dep_delayed) + " " +
                                    data.getDep_delay());
                            holder. tv_dept_status.setBackground(mContext.getDrawable(R.drawable.bg_flight_red_status));
                        }

                    }

                } else {
                    isdeparted=false;
                    if (data == null ||data.getDep_delay() == null
                           ) {
                        holder.dep_status.setVisibility(View.VISIBLE);
                        //holder.tv_dep_delay.setVisibility(View.VISIBLE);
                    }
                    else {
                        if( !data.getDep_delay().equals("0")){
                            holder. tv_dept_status.setText(mContext.getString(R.string.dep_delay) + " " +
                                    data.getDep_delay());
                            holder.  tv_dept_status.setBackground(mContext.getDrawable(R.drawable.bg_flight_red_status));
                        }

                        holder.  tv_dep_actualtime_head.setText(mContext.getString(R.string.estimated_time));
                    }

                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if (data.getDep_delay()!=null&&!data.getDep_delay().equals("0")){
            holder. tv_dep_delay.setText(mContext.getString(R.string.delayed_by) + " " + data.getDep_delay());
            holder.tv_dep_delay.setTextColor(mContext.getResources().getColor(R.color.red_status));

        }
        // holder.tv_dep_delay.setText(mContext.getString(R.string.delayed_by) + " " + data.getDep_delay());

//
//        if (data!= null && data.getArr_delay() != null && !
//                data.getArr_delay().equals("0")) {
//            holder.tv_arr_status.setVisibility(View.VISIBLE);
//            holder.tv_arr_delay.setVisibility(View.VISIBLE);
        if (data.getArrdate_actual() != null &&
                data.getArrTime_actual() != null &&
                !data.getArrdate_actual().equals("") &&
                !data.getArrTime_actual().equals("")) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date newDate = null;




            try {
                newDate = format.parse(data.getArrdate_actual() + " " +
                        data.getArrTime_actual());
                if(!Calendar.getInstance().getTime().after(newDate)){
                    holder.tv_arr_actualtime_head.setText(mContext.getString(R.string.estimated_time));
                }
                if (Calendar.getInstance().getTime().after(newDate)) {
                    if (data== null || data.getArr_delay() ==  null) {
                        holder.tv_arr_status.setVisibility(View.VISIBLE);
                        holder.tv_arr_delay.setVisibility(View.VISIBLE);
                    }
                    else {
                        if ( !data.getArr_delay().equals("0")){
                            holder.tv_dept_status.setText(mContext.getString(R.string.arr_delayed) + " " +
                                    data.getArr_delay());
                            holder.tv_dept_status.setBackground(mContext.getDrawable(R.drawable.bg_flight_red_status));
                        }

                    }

                } else {
                    if (data== null || data.getArr_delay() ==  null) {
                        //holder.tv_arr_status.setVisibility(View.VISIBLE);
                        // holder.tv_arr_delay.setVisibility(View.VISIBLE);
                    }
                    else {
                        if(isdeparted){
                            if ( !data.getArr_delay().equals("0")){
                                holder.tv_dept_status.setText(mContext.getString(R.string.arr_delay) + " " +
                                        data.getArr_delay());
                                holder. tv_dept_status.setBackground(mContext.getDrawable(R.drawable.bg_flight_red_status));
                            }

                        }

                        holder.tv_arr_actualtime_head.setText(mContext.getString(R.string.estimated_time));
                    }

                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }



        if (data.getArr_delay()!=null&&!data.getArr_delay().equals("0")){
            holder. tv_arr_delay.setText(mContext.getString(R.string.delayed_by) + " " + data.getArr_delay());
            holder.tv_arr_delay.setTextColor(mContext.getResources().getColor(R.color.red_status));

        }


//        }
//        else {
//            holder. tv_arr_status.setVisibility(View.GONE);
//            holder. tv_arr_delay.setVisibility(View.GONE);
//        }

        if (data.getFlightStatus().equals("Scheduled")) {
            holder. tv_dep_actualtime_head.setText(mContext.getString(R.string.estimate_time));
        }

        if (data.getListBoarding()!=null){
            holder.recycle_boarding_images.setVisibility(View.VISIBLE);
            AdapterSelectedFiles adapterSelectedFiles=new AdapterSelectedFiles(
                    mContext,data.getListBoarding(),1);
            adapterSelectedFiles.setonItemClick(new OnImageRemoveListener() {
                @Override
                public void OnIMageremove(int pos, View view, int Type) {
                        data.getListBoarding().remove(pos);
                        notifyDataSetChanged();
                }
            });
            holder.recycle_boarding_images.setAdapter(adapterSelectedFiles);
            adapterSelectedFiles.notifyDataSetChanged();
            holder.recycle_boarding_images.setLayoutManager( new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL,
                    false));
        }
        else {
            holder.recycle_boarding_images.setVisibility(View.GONE);
            List<MediaFile>list=new ArrayList<>();
            AdapterSelectedFiles adapterSelectedFiles=new AdapterSelectedFiles(mContext,list,1);
            holder.recycle_boarding_images.setAdapter(adapterSelectedFiles);
            adapterSelectedFiles.notifyDataSetChanged();
            holder.recycle_boarding_images.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL,
                    false));
        }
        if (data.getListEticket()!=null){
            holder.recycle_eticket_images.setVisibility(View.VISIBLE);
            AdapterSelectedFiles adapterSelectedFiles=new AdapterSelectedFiles(mContext,data.getListEticket(),1);
            holder.recycle_eticket_images.setAdapter(adapterSelectedFiles);
            adapterSelectedFiles.notifyDataSetChanged();
            holder.recycle_eticket_images.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL,
                    false));
            adapterSelectedFiles.setonItemClick(new OnImageRemoveListener() {
                @Override
                public void OnIMageremove(int pos, View view, int Type) {
                        data.getListEticket().remove(pos);
                        notifyDataSetChanged();
                }
            });
        }
        else {
            holder.recycle_eticket_images.setVisibility(View.GONE);
            List<MediaFile>list=new ArrayList<>();
            AdapterSelectedFiles adapterSelectedFiles=new AdapterSelectedFiles(mContext,list,1);
            holder.recycle_eticket_images.setAdapter(adapterSelectedFiles);
            adapterSelectedFiles.notifyDataSetChanged();
            holder.recycle_eticket_images.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL,
                    false));
        }
        if (data.isOpen()){
            holder.lin_second_lay.setVisibility(View.VISIBLE);
            holder.card_mainlay1.setVisibility(View.GONE);
        }
        else {
            holder.lin_second_lay.setVisibility(View.GONE);
            holder.card_mainlay1.setVisibility(View.VISIBLE);
        }
        holder.btn_add_trip.setEnabled(true);
        holder.img_shop.setClipToOutline(true);
        holder.user_login.setClipToOutline(true);
        try {
            if (data.getFlightStatus().toLowerCase().equals(mContext.getResources().getString(R.string.cancelled))){
                holder.tv_dept_status.setBackground(mContext.getResources().getDrawable(R.drawable.bg_flight_red_status));
                holder.tv_dept_status.setText(data.getFlightStatus());
                holder.tv_dep_delay.setText(data.getFlightStatus());
                holder.tv_arr_delay.setText(data.getFlightStatus());
            }
        }
        catch (Exception e){

        }
    }

    public void updateRowIndex(int pos) {
        type = pos;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return home_data_list.size();
    }

    public void setonItemClick(OnItemClickListener listener) {
        this.onItemClickListener = listener;

    }

    public void updateindex(int index) {
        this.type = index;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_dept_time1,tv_dept_date1,tv_arrive_time1,tv_arrive_date1,tv_dept_code1,
                tv_arrive_code1,tv_dept_city1,tv_arrival_city1,tv_travel_time1,
                tv_dept_time,tv_arrive_time,tv_dept_date,tv_arrive_date,tv_dept_code,
                tv_arrive_code,tv_dept_city,tv_arrival_city,tv_dept_terminal,tv_arrival_terminal,tv_travel_time,
                tv_dep_actualtime_head,tv_arr_actualtime_head,tv_dep_airport_name,tv_dep_schedule_time,tv_dep_actualtime,
                tv_dep_terminal_name,tv_dep_delay,dep_status,tv_arr_airport_name,tv_arr_schedule_time,tv_arr_actualtime,
                tv_arr_terminal_name,tv_arr_delay,tv_arr_status,tv_airlinenames,tv_flight_code,flight_code,tv_airlinename;
        Button tv_dept_status;
        CardView card_mainlay1;
        ImageView img_add_Bording,img_add_etick;
        LinearLayout lin_second_lay;
        RecyclerView recycle_boarding_images,recycle_eticket_images;
        Button btn_add_trip;
        ImageView img_shop,user_login;

        public ViewHolder(View itemView) {
            super(itemView);
            //  tv_offer_des = itemView.findViewById(R.id.tv_offer_des);
            tv_dept_status=itemView.findViewById(R.id.tv_dept_status);
            recycle_boarding_images=itemView.findViewById(R.id.recycle_boarding_images);
            recycle_eticket_images=itemView.findViewById(R.id.recycle_eticket_images);
            btn_add_trip=itemView.findViewById(R.id.btn_add_trip);
            img_shop=itemView.findViewById(R.id.img_shop);
            user_login=itemView.findViewById(R.id.user_login);
            img_add_Bording=itemView.findViewById(R.id.img_add_Bording);
            img_add_etick=itemView.findViewById(R.id.img_add_etick);
            card_mainlay1=itemView.findViewById(R.id.card_mainlay1);
            lin_second_lay=itemView.findViewById(R.id.lin_second_lay);
            tv_airlinename=itemView.findViewById(R.id.tv_airlinename);
            flight_code=itemView.findViewById(R.id.flight_code);
            tv_dept_time1=itemView.findViewById(R.id.tv_dept_time1);
            tv_flight_code=itemView.findViewById(R.id.tv_flight_code);
            tv_airlinenames=itemView.findViewById(R.id.tv_airlinenames);
            tv_dept_date1=itemView.findViewById(R.id.tv_dept_date1);
            tv_arrive_time1=itemView.findViewById(R.id.tv_arrive_time1);
            tv_arrive_date1=itemView.findViewById(R.id.tv_arrive_date1);
            tv_dept_code1=itemView.findViewById(R.id.tv_dept_code1);
            tv_arrive_code1=itemView.findViewById(R.id.tv_arrive_code1);
            tv_dept_city1=itemView.findViewById(R.id.tv_dept_city1);
            tv_arrival_city1=itemView.findViewById(R.id.tv_arrival_city1);
            tv_travel_time1=itemView.findViewById(R.id.tv_travel_time1);
            tv_dept_time=itemView.findViewById(R.id.tv_dept_time);
            tv_arrive_time=itemView.findViewById(R.id.tv_arrive_time);
            tv_dept_date=itemView.findViewById(R.id.tv_dept_date);
            tv_arrive_date=itemView.findViewById(R.id.tv_arrive_date);
            tv_dept_code=itemView.findViewById(R.id.tv_dept_code);
            tv_arrive_code=itemView.findViewById(R.id.tv_arrive_code);
            tv_dept_city=itemView.findViewById(R.id.tv_dept_city);
            tv_arrival_city=itemView.findViewById(R.id.tv_arrival_city);
            tv_dept_terminal=itemView.findViewById(R.id.tv_dept_terminal);
            tv_arrival_terminal=itemView.findViewById(R.id.tv_arrival_terminal);
            tv_travel_time=itemView.findViewById(R.id.tv_travel_time);
            tv_dep_actualtime_head=itemView.findViewById(R.id.tv_dep_actualtime_head);
            tv_arr_actualtime_head=itemView.findViewById(R.id.tv_arr_actualtime_head);
            tv_dep_airport_name=itemView.findViewById(R.id.tv_dep_airport_name);
            tv_dep_schedule_time=itemView.findViewById(R.id.tv_dep_schedule_time);
            tv_dep_actualtime=itemView.findViewById(R.id.tv_dep_actualtime);
            tv_dep_terminal_name=itemView.findViewById(R.id.tv_dep_terminal_name);
            tv_dep_delay=itemView.findViewById(R.id.tv_dep_delay);
            dep_status=itemView.findViewById(R.id.dep_status);
            tv_arr_airport_name=itemView.findViewById(R.id.tv_arr_airport_name);
            tv_arr_schedule_time=itemView.findViewById(R.id.tv_arr_schedule_time);
            tv_arr_actualtime=itemView.findViewById(R.id.tv_arr_actualtime);
            tv_arr_terminal_name=itemView.findViewById(R.id.tv_arr_terminal_name);
            tv_arr_delay=itemView.findViewById(R.id.tv_arr_delay);
            tv_arr_status=itemView.findViewById(R.id.tv_arr_status);
            img_add_Bording.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.OnItemClickListener(getAdapterPosition(),v);
                }
            });
            img_add_etick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.OnItemClickListener(getAdapterPosition(),v);
                }
            });
            btn_add_trip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.OnItemClickListener(getAdapterPosition(),v);
                   // btn_add_trip.setEnabled(false);
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (card_mainlay1.getVisibility()==View.VISIBLE){
                        lin_second_lay.setVisibility(View.VISIBLE);
                        card_mainlay1.setVisibility(View.GONE);
                    }
                    else if (lin_second_lay.getVisibility()==View.VISIBLE){
                        lin_second_lay.setVisibility(View.GONE);
                        card_mainlay1.setVisibility(View.VISIBLE);
                    }
                }
            });

        }

    }

}