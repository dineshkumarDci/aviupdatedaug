package com.dci.avicustomer.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestFutureTarget;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.dci.avicustomer.R;
import com.dci.avicustomer.interfaces.OnImageRemoveListener;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.ModelTripDetails;
import com.jaiselrahman.filepicker.model.MediaFile;

import java.io.File;
import java.util.List;

public class AdapterSelectedFiles extends RecyclerView.Adapter<AdapterSelectedFiles.DrawerViewHolder> {
    int type;
    OnImageRemoveListener onImageRemoveListener;
    private List<MediaFile> boardinglist;
    private Context mContext;

    public AdapterSelectedFiles(Context mContext, List<MediaFile> boardingList,int type) {
        this.mContext = mContext;
        this.boardinglist = boardingList;
        this.type = type;
    }

    @Override
    public AdapterSelectedFiles.DrawerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_boarding_image, parent, false);
        return new AdapterSelectedFiles.DrawerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterSelectedFiles.DrawerViewHolder holder, final int position) {
        holder.file_name.setVisibility(View.GONE);
        File file=new File(boardinglist.get(position).getPath());
            Glide.with(mContext).load(file).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable final GlideException e,
                                            final Object model, final Target<Drawable> target,
                                            final boolean isFirstResource) {
                    try{
                        holder.file_name.setVisibility(View.VISIBLE);
                        holder.file_name.setText(boardinglist.get(position).getName());
                        holder.img_boarding.setBackground(mContext.getResources().getDrawable(R.drawable.backround_file_thum));
                        holder.file_name.setSelected(true);
                    }
                    catch (Exception ex){

                    }

                    return false;
                }

                @Override
                public boolean onResourceReady(final Drawable resource,
                                               final Object model,
                                               final Target<Drawable> target,
                                               final DataSource dataSource,
                                               final boolean isFirstResource) {


                    return false;
                }
            }).into(holder.img_boarding);
            holder.img_cancel.setVisibility(View.VISIBLE);
            holder.img_boarding.setClipToOutline(true);
            if (type==1){
                holder.img_cancel.setVisibility(View.VISIBLE);
            }
    }

    public void updateRowIndex(int pos) {
        type = pos;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
            return boardinglist.size();
    }

    public void setonItemClick(OnImageRemoveListener listener) {
        this.onImageRemoveListener = listener;

    }

    public void updateindex(int index) {
        this.type = index;
        notifyDataSetChanged();
    }

    class DrawerViewHolder extends RecyclerView.ViewHolder {

        ImageView img_boarding,img_cancel;
        TextView file_name;



        public DrawerViewHolder(View itemView) {
            super(itemView);
            //  tv_offer_des = itemView.findViewById(R.id.tv_offer_des);

            img_boarding = itemView.findViewById(R.id.img_boarding);
            img_cancel= itemView.findViewById(R.id.img_cancel);
            file_name=itemView.findViewById(R.id.file_name);

            //tv_offer_percentage= itemView.findViewById(R.id.tv_offer_percentage);

            img_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onImageRemoveListener.OnIMageremove(getAdapterPosition(),view,type);



                    // item_lay.setBackgroundColor(mContext.getResources().getColor(R.color.orrange));

//                    int position = getAdapterPosition();
//                    ((MainActivity) mContext).showMenuItems(position);
                }
            });
        }
    }
//    public int setActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.activehome;
//                break;
//            case 1:
//                id=  R.drawable.activeaccount;
//
//                break;
//            case 2:
//                id= R.drawable.activelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.activewallet;
//                break;
//            case 4:
//                id=  R.drawable.activeorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.activewishlish;
//                break;
//            case 6:
//                id=  R.drawable.activereceipe;
//                break;
//            case 7:
//                id=  R.drawable.activelogout;
//                break;
//            default:
//                id=  R.drawable.activelogout;
//
//        }
//        return id;
//
//
//    }
//    public int setInActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.inactivehome;
//                break;
//            case 1:
//                id=  R.drawable.inactiveaccount;
//
//                break;
//            case 2:
//                id= R.drawable.inactivelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.inactivewallet;
//                break;
//            case 4:
//                id=  R.drawable.inactiveorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.inactivewishlish;
//                break;
//            case 6:
//                id=  R.drawable.inactivereceipe;
//                break;
//            case 7:
//                id=  R.drawable.inactivelogout;
//                break;
//            default:
//                id=  R.drawable.inactivelogout;
//
//        }
//        return id;
//
//
//    }
}
