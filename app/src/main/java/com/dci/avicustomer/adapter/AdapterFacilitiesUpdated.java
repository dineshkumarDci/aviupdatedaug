package com.dci.avicustomer.adapter;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dci.avicustomer.R;
import com.dci.avicustomer.activities.ActivityFacilitiesDetails;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.ModelFacilities;
import java.util.List;
public class AdapterFacilitiesUpdated extends RecyclerView.Adapter<AdapterFacilitiesUpdated.MyViewHolder> {
    // public ArrayList<DummyParentDataItem> dummyParentDataItems;
    List<ModelFacilities.Datum> dummyParentDataItems;
    Context context;
    ProgressDialog progressDialog;

    public AdapterFacilitiesUpdated(List<ModelFacilities.Datum> dummyParentDataItems, Context context) {
        this.dummyParentDataItems = dummyParentDataItems;
        this.context=context;
    }

    @Override
    public AdapterFacilitiesUpdated.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_child_facilities, parent, false);
        return new AdapterFacilitiesUpdated.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AdapterFacilitiesUpdated.MyViewHolder holder, int position) {
        ModelFacilities.Datum dummyParentDataItem = dummyParentDataItems.get(position);
        holder.tv_facilitiesname.setText(dummyParentDataItem.getName());

        if (dummyParentDataItem.getConditions()!=null&&!dummyParentDataItem.getConditions().equals("")){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                holder.tv_describtion.setText(Html.fromHtml(dummyParentDataItem.getConditions(), Html.FROM_HTML_MODE_COMPACT));
            } else {
                holder.tv_describtion.setText(Html.fromHtml(dummyParentDataItem.getConditions()));
            }
        }
        holder.tv_terminal_details.setText(dummyParentDataItem.getAirport_details());
        if (dummyParentDataItems.get(position).getImages()!=null&&!dummyParentDataItems.get(position).getImages().equals("")) {
            Glide.with(context).load(dummyParentDataItems.get(position).getUrl() +
                    dummyParentDataItems.get(position).getImages()).into(holder.img_shop);
        }

        holder.img_shop.setClipToOutline(true);
       // holder.tv_describtion.setText(dummyParentDataItem.);

        //

    }

    @Override
    public int getItemCount() {
        return dummyParentDataItems.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView tv_facilitiesname,tv_terminal_details,tv_describtion;
        public ImageView img_shop,img_phn_num,img_email;




        MyViewHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();
            img_phn_num= itemView.findViewById(R.id.img_phn_num);
            img_email= itemView.findViewById(R.id.img_email);
            tv_facilitiesname = itemView.findViewById(R.id.tv_facilitiesname);
            tv_terminal_details = itemView.findViewById(R.id.tv_terminal_details);
            tv_describtion= itemView.findViewById(R.id.tv_describtion);
            img_shop= itemView.findViewById(R.id.img_shop);
            img_phn_num.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dummyParentDataItems.get(getAdapterPosition()).getPhone()!=null&&!
                            dummyParentDataItems.get(getAdapterPosition()).getPhone().equals("")){
                        Uri u = Uri.parse("tel:" + dummyParentDataItems.get(getAdapterPosition()).getPhone().trim());
                        Intent i = new Intent(Intent.ACTION_DIAL, u);
                        context.startActivity(i);
                    }
                    else {
                        Toast.makeText(context, R.string.no_phone_num, Toast.LENGTH_SHORT).show();
                    }
                }
            });
            img_email.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dummyParentDataItems.get(getAdapterPosition()).getEmail()!=null&&!
                            dummyParentDataItems.get(getAdapterPosition()).getEmail().equals("")){
                        Uri u = Uri.parse("tel:" + dummyParentDataItems.get(getAdapterPosition()).getEmail().trim());
                        Intent i = new Intent(Intent.ACTION_DIAL, u);
                        context.startActivity(i);
                    }
                    else {
                        Toast.makeText(context, R.string.no_email, Toast.LENGTH_SHORT).show();

                    }
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ModelFacilities.Datum dummyParentDataItem=dummyParentDataItems.get(getAdapterPosition());
                    if (dummyParentDataItem!=null){
                        Intent intent =new Intent(context, ActivityFacilitiesDetails.class);
                        intent.putExtra("data",dummyParentDataItem);
                        context.startActivity(intent);
                    }
                    else {
                        Toast.makeText(context, context.getString(R.string.detail_not_available), Toast.LENGTH_SHORT).show();
                    }



                }
            });

            // textView_parentName.setOnClickListener(this);

        }

    }

    public void removeAt(int position) {
        dummyParentDataItems.remove(position);
        notifyItemRemoved(position);
        // notifyItemRangeChanged(position, dummyParentDataItems.size());
        // ((ActivityChartRaiseTest)c).updateamount(child);
    }


}