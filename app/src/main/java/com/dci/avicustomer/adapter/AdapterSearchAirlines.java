package com.dci.avicustomer.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dci.avicustomer.R;
import com.dci.avicustomer.activities.ActivityAirlineSearch;
import com.dci.avicustomer.activities.ActivityOrderTracking;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.ModelMyOrders;
import com.dci.avicustomer.models.ModelSearchAirline;

import java.util.List;

public class AdapterSearchAirlines extends RecyclerView.Adapter<AdapterSearchAirlines.DrawerViewHolder> {
    int type;
    OnItemClickListener onItemClickListener;
    private List<ModelSearchAirline.Datum> home_data_list;
    private Context mContext;

    public AdapterSearchAirlines(Context mContext, List<ModelSearchAirline.Datum> home_data_list) {
        this.mContext = mContext;
        this.home_data_list = home_data_list;
        this.type = type;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public AdapterSearchAirlines.DrawerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_search_airline, parent,
                false);
        return new AdapterSearchAirlines.DrawerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterSearchAirlines.DrawerViewHolder holder, final int position) {
        ModelSearchAirline.Datum modelShopList = home_data_list.get(position);
        if (modelShopList.getAirline_display_name()!=null){
            holder.tv_headername.setText(modelShopList.getAirline_display_name());
        }

        //Log.d("imgg", "onBindViewHolder: " + baseurl + home_data_list.get(position).getProfile_img());
    }

    public void updateRowIndex(int pos) {
        type = pos;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return home_data_list.size();
    }

    public void setonItemClick(OnItemClickListener listener) {
        this.onItemClickListener = listener;

    }

    public void updateindex(int index) {
        this.type = index;
        notifyDataSetChanged();
    }

    class DrawerViewHolder extends RecyclerView.ViewHolder {
        TextView tv_headername;



        public DrawerViewHolder(View itemView) {
            super(itemView);
             tv_headername = itemView.findViewById(R.id.tv_headername);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    UtilsDefault.updateSharedPreference(Constants.SELECTED_AIRLINECODE,home_data_list.get(getAdapterPosition()).getAirline_code());
                    UtilsDefault.updateSharedPreference(Constants.SELECTED_AIRLINENAME,home_data_list.get(getAdapterPosition()).
                            getAirline_display_name());
                    ((ActivityAirlineSearch)mContext).finish();
                }
            });

        }
    }
//    public int setActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.activehome;
//                break;
//            case 1:
//                id=  R.drawable.activeaccount;
//
//                break;
//            case 2:
//                id= R.drawable.activelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.activewallet;
//                break;
//            case 4:
//                id=  R.drawable.activeorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.activewishlish;
//                break;
//            case 6:
//                id=  R.drawable.activereceipe;
//                break;
//            case 7:
//                id=  R.drawable.activelogout;
//                break;
//            default:
//                id=  R.drawable.activelogout;
//
//        }
//        return id;
//
//
//    }
//    public int setInActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.inactivehome;
//                break;
//            case 1:
//                id=  R.drawable.inactiveaccount;
//
//                break;
//            case 2:
//                id= R.drawable.inactivelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.inactivewallet;
//                break;
//            case 4:
//                id=  R.drawable.inactiveorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.inactivewishlish;
//                break;
//            case 6:
//                id=  R.drawable.inactivereceipe;
//                break;
//            case 7:
//                id=  R.drawable.inactivelogout;
//                break;
//            default:
//                id=  R.drawable.inactivelogout;
//
//        }
//        return id;
//
//
//    }
}
