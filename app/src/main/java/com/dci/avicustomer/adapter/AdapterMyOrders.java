package com.dci.avicustomer.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dci.avicustomer.R;
import com.dci.avicustomer.activities.ActivityOrderTracking;
import com.dci.avicustomer.activities.ActivityShopDetails;
import com.dci.avicustomer.activities.ActivityShopList;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.ModelMyOrders;
import com.dci.avicustomer.models.ModelShopList;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class AdapterMyOrders extends RecyclerView.Adapter<AdapterMyOrders.DrawerViewHolder> {
    int type;
    OnItemClickListener onItemClickListener;
    private List<ModelMyOrders.Order> home_data_list;
    private Context mContext;

    public AdapterMyOrders(Context mContext, List<ModelMyOrders.Order> home_data_list, int type) {
        this.mContext = mContext;
        this.home_data_list = home_data_list;
        this.type = type;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public AdapterMyOrders.DrawerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_myorders, parent,
                false);
        return new AdapterMyOrders.DrawerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterMyOrders.DrawerViewHolder holder, final int position) {
        ModelMyOrders.Order modelShopList = home_data_list.get(position);
        holder.tv_shopname.setText(UtilsDefault.checkNull(modelShopList.getRestaurantName()) + "," +
                UtilsDefault.checkNull(modelShopList.getTerminal()));
        // String baseurl = ((ActivityShopList) mContext).getBaseUrl();
        holder.tv_order_date.setText(UtilsDefault.dateFormat(home_data_list.get(position).getOrderDate()));
        holder.tv_order_id.setText(home_data_list.get(position).getOrderID());
        holder.tv_order_status.setText(home_data_list.get(position).getOrderStatus());
        holder.tv_price.setText(modelShopList.getCurrency() + " " + modelShopList.getTotal());
        Glide.with(mContext).load(home_data_list.get(position).getImage()).into(holder.img_shop);
        holder.img_shop.setClipToOutline(true);
        if (modelShopList.getCancelMsg() != null && !modelShopList.getCancelMsg().equals("")) {
            holder.tv_cancel_reason.setText("Reason:-"+modelShopList.getCancelMsg());
        }
        try {
            holder.rating.setRating(modelShopList.getRating());
        } catch (Exception e) {

        }
        if (modelShopList.getOrderStatusId() == 7) {
            holder.tv_order_status.setBackground(mContext.getDrawable(R.drawable.bg_completed));
        } else {
            holder.tv_order_status.setBackground(mContext.getDrawable(R.drawable.bg_ordered));
        }
        //Log.d("imgg", "onBindViewHolder: " + baseurl + home_data_list.get(position).getProfile_img());
    }

    public void updateRowIndex(int pos) {
        type = pos;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return home_data_list.size();
    }

    public void setonItemClick(OnItemClickListener listener) {
        this.onItemClickListener = listener;

    }

    public void updateindex(int index) {
        this.type = index;
        notifyDataSetChanged();
    }

    class DrawerViewHolder extends RecyclerView.ViewHolder {
        TextView tv_shopname, tv_order_date, tv_price, tv_order_status, tv_order_id, tv_cancel_reason;
        ImageView img_shop;
        RatingBar rating;


        public DrawerViewHolder(View itemView) {
            super(itemView);
            //  tv_offer_des = itemView.findViewById(R.id.tv_offer_des);
            rating = itemView.findViewById(R.id.rating);
            tv_shopname = itemView.findViewById(R.id.tv_shopname);
            tv_order_date = itemView.findViewById(R.id.tv_order_date);
            tv_price = itemView.findViewById(R.id.tv_price);
            tv_order_status = itemView.findViewById(R.id.tv_order_status);
            img_shop = itemView.findViewById(R.id.img_shop);
            tv_order_id = itemView.findViewById(R.id.tv_order_id);
            tv_cancel_reason = itemView.findViewById(R.id.tv_cancel_reason);
            rating = itemView.findViewById(R.id.rating);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (home_data_list.get(getAdapterPosition()).getOrderStatusId() == 7) {
                        Toast.makeText(mContext, mContext.getString(R.string.order_completed), Toast.LENGTH_SHORT).show();
                    } else {
                        Intent intent = new Intent(mContext, ActivityOrderTracking.class);
                        intent.putExtra("id", home_data_list.get(getAdapterPosition()).getOrderID());
                        mContext.startActivity(intent);
                    }
//                    if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)!=null){
//                        Intent intent = new Intent(mContext, ActivityShopDetails.class);
//                        intent.putExtra("data", home_data_list.get(getAdapterPosition()));
//                        intent.putExtra("imgbase", ((ActivityShopList) mContext).getBaseUrl());
//                        mContext.startActivity(intent);
//                    }
//                    else {
//                        Toast.makeText(mContext, R.string.login_messge, Toast.LENGTH_SHORT).show();
//                    }


                    // item_lay.setBackgroundColor(mContext.getResources().getColor(R.color.orrange));

//                    int position = getAdapterPosition();
//                    ((MainActivity) mContext).showMenuItems(position);
                }
            });

        }
    }
//    public int setActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.activehome;
//                break;
//            case 1:
//                id=  R.drawable.activeaccount;
//
//                break;
//            case 2:
//                id= R.drawable.activelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.activewallet;
//                break;
//            case 4:
//                id=  R.drawable.activeorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.activewishlish;
//                break;
//            case 6:
//                id=  R.drawable.activereceipe;
//                break;
//            case 7:
//                id=  R.drawable.activelogout;
//                break;
//            default:
//                id=  R.drawable.activelogout;
//
//        }
//        return id;
//
//
//    }
//    public int setInActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.inactivehome;
//                break;
//            case 1:
//                id=  R.drawable.inactiveaccount;
//
//                break;
//            case 2:
//                id= R.drawable.inactivelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.inactivewallet;
//                break;
//            case 4:
//                id=  R.drawable.inactiveorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.inactivewishlish;
//                break;
//            case 6:
//                id=  R.drawable.inactivereceipe;
//                break;
//            case 7:
//                id=  R.drawable.inactivelogout;
//                break;
//            default:
//                id=  R.drawable.inactivelogout;
//
//        }
//        return id;
//
//
//    }
}
