package com.dci.avicustomer.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dci.avicustomer.R;
import com.dci.avicustomer.activities.ActivityShopDetails;
import com.dci.avicustomer.activities.ActivityShopList;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.ModelShopList;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class AdapterShopList extends RecyclerView.Adapter<AdapterShopList.DrawerViewHolder> {
    int type;
    OnItemClickListener onItemClickListener;
    private List<ModelShopList.Datum> home_data_list;
    private Context mContext;

    public AdapterShopList(Context mContext, List<ModelShopList.Datum> home_data_list, int type) {
        this.mContext = mContext;
        this.home_data_list = home_data_list;
        this.type = type;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public AdapterShopList.DrawerViewHolder onCreateViewHolder(ViewGroup parent,
                                                               int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.list_item_shoplist, parent, false);
        return new AdapterShopList.DrawerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterShopList.DrawerViewHolder holder, final int position) {
        ModelShopList.Datum modelShopList = home_data_list.get(position);
        holder.tv_shopname.setText(UtilsDefault.checkNull(modelShopList.getName()));
        StringBuilder stringBuilder=new StringBuilder();
        stringBuilder.append(UtilsDefault.checkNull(modelShopList.getTerminal().getName()));

        if (modelShopList.getWorking_hours()!=null){
            holder.tv_open_time.setText(modelShopList.getWorking_hours());
        }
        if (modelShopList.getArea_type()!=null&&modelShopList.getArea_type()==Constants.AREA_DEP){
            stringBuilder.append("-"+"Departure");
        }
        else {
            stringBuilder.append("-"+"Arrival");
        }

        holder.tv_terminal_name.setText(stringBuilder);

//        holder.tv_terminal_name.setText("Terminal " + UtilsDefault.checkNull(modelShopList.getTerminal().getName()) + "-" + UtilsDefault.checkNull(modelShopList.getAirport_type()) + "-" +
//                modelShopList.getArea_type());
        if (modelShopList.getRetail_times() != null && modelShopList.getRetail_times().size() != 0) {
            SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
            Date d = new Date();
            String dayOfTheWeek = sdf.format(d);
            for (ModelShopList.Datum.Retail_time data : modelShopList.getRetail_times()) {
                if (data.getDay() != null && data.getDay().toLowerCase().equals(dayOfTheWeek.toLowerCase())) {
                    holder.tv_open_time.setText(data.getFrom_time() + " to " + data.getTo_time());
                }
            }
        }
        if (home_data_list.get(position).getOpenClose()!=null&&
                home_data_list.get(position).getOpenClose()==1){
            holder.tv_open_status.setText("Open");
            holder.tv_open_status.setTextColor(mContext.getResources().getColor(R.color.dark_green));
        }
        else {
            holder.tv_open_status.setText("Closed");
            holder.tv_open_status.setTextColor(mContext.getResources().getColor(R.color.red));
        }
        holder.tv_address.setText(modelShopList.getAddress());
        if (modelShopList.getCoupons() != null && modelShopList.getCoupons().size() != 0) {
            if(modelShopList.getCoupons().get(0).getDiscount_percentage()!=null&&!
                    modelShopList.getCoupons().get(0).getDiscount_percentage().equals("")){
                holder.lin_offer_lay.setVisibility(View.VISIBLE);
                holder.tv_offer_percentage.setText(UtilsDefault.checkNull
                        (modelShopList.getCoupons().get(0).getDiscount_percentage()));
            }
            else {
                holder.lin_offer_lay.setVisibility(View.GONE);
            }

        }
        String baseurl = ((ActivityShopList) mContext).getBaseUrl();
        Glide.with(mContext).load(baseurl  +
                home_data_list.get(position).getProfile_img()).into(holder.img_shop);
        holder.img_shop.setClipToOutline(true);
        Log.d("imgg", "onBindViewHolder: " + baseurl + home_data_list.get(position).getProfile_img());
    }

    public void updateRowIndex(int pos) {
        type = pos;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return home_data_list.size();
    }

    public void setonItemClick(OnItemClickListener listener) {
        this.onItemClickListener = listener;

    }

    public void updateindex(int index) {
        this.type = index;
        notifyDataSetChanged();
    }

    class DrawerViewHolder extends RecyclerView.ViewHolder {
        TextView tv_shopname, tv_address, tv_open_time, tv_offer_percentage, tv_terminal_name,tv_open_status;
        ImageView img_shop, img_locate, img_phn_num, img_email;
        LinearLayout lin_address_lay,lin_offer_lay;

        CardView main_lay;


        public DrawerViewHolder(View itemView) {
            super(itemView);
            //  tv_offer_des = itemView.findViewById(R.id.tv_offer_des);
            tv_shopname = itemView.findViewById(R.id.tv_shopname);
            tv_open_status= itemView.findViewById(R.id.tv_open_status);
            img_shop = itemView.findViewById(R.id.img_shop);
            tv_open_time = itemView.findViewById(R.id.tv_open_time);
            tv_address = itemView.findViewById(R.id.tv_address);
            lin_address_lay = itemView.findViewById(R.id.lin_address_lay);
            tv_offer_percentage = itemView.findViewById(R.id.tv_offer_percentage);
            lin_offer_lay= itemView.findViewById(R.id.lin_offer_lay);
            tv_terminal_name = itemView.findViewById(R.id.tv_terminal_name);

            img_locate = itemView.findViewById(R.id.img_locate);
            img_phn_num = itemView.findViewById(R.id.img_phn_num);
            img_email = itemView.findViewById(R.id.img_email);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)!=null){
                        Intent intent = new Intent(mContext, ActivityShopDetails.class);
                        intent.putExtra("data", home_data_list.get(getAdapterPosition()));
                        intent.putExtra("imgbase", ((ActivityShopList) mContext).getBaseUrl());
                        mContext.startActivity(intent);
                    }
                    else {
                        Toast.makeText(mContext, R.string.login_messge, Toast.LENGTH_SHORT).show();
                    }



                    // item_lay.setBackgroundColor(mContext.getResources().getColor(R.color.orrange));

//                    int position = getAdapterPosition();
//                    ((MainActivity) mContext).showMenuItems(position);
                }
            });
            img_email.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (home_data_list.get(getAdapterPosition()).getEmail() != null && !
                            home_data_list.get(getAdapterPosition()).getEmail().equals("")) {
                        try {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + home_data_list.get(getAdapterPosition()).getEmail().trim()));
                            intent.putExtra(Intent.EXTRA_SUBJECT, "Test");
                            intent.putExtra(Intent.EXTRA_TEXT, "Test");
                            mContext.startActivity(intent);
                        } catch (Exception e) {
                            Toast.makeText(mContext, "Sorry...You don't have any mail app", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(mContext, R.string.no_email, Toast.LENGTH_SHORT).show();
                    }

                }
            });
            img_locate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (home_data_list.get(getAdapterPosition()).getLatitude() != null &&
                            home_data_list.get(getAdapterPosition()).getLongitude() != null) {
                        Uri gmmIntentUri = Uri.parse("geo:<" + home_data_list.get(getAdapterPosition()).getLatitude() + ">,<" +
                                home_data_list.get(getAdapterPosition()).getLongitude() + ">?q=<" +
                                home_data_list.get(getAdapterPosition()).getLatitude() + ">,<" +
                                home_data_list.get(getAdapterPosition()).getLongitude() + ">(" +
                                home_data_list.get(getAdapterPosition()).getName() + ")");
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps");
                        mContext.startActivity(mapIntent);
                    } else {
                        Toast.makeText(mContext, "" +
                                mContext.getString(R.string.locate_detailsnot), Toast.LENGTH_SHORT).show();
                    }


//                    if (lin_address_lay.getVisibility()== View.VISIBLE){
//                        lin_address_lay.setVisibility(View.GONE);
//                    }
//                    else {
//                        lin_address_lay.setVisibility(View.VISIBLE);
//
//                    }
                }
            });
            img_phn_num.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (home_data_list.get(getAdapterPosition()).getPhone() != null && !
                            home_data_list.get(getAdapterPosition()).getPhone().equals("")) {
                        Uri u = Uri.parse("tel:" + home_data_list.get(getAdapterPosition()).getPhone().trim());
                        Intent i = new Intent(Intent.ACTION_DIAL, u);
                        mContext.startActivity(i);
                    } else {
                        Toast.makeText(mContext, R.string.no_phone_num, Toast.LENGTH_SHORT).show();
                    }
                }

            });
        }
    }
//    public int setActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.activehome;
//                break;
//            case 1:
//                id=  R.drawable.activeaccount;
//
//                break;
//            case 2:
//                id= R.drawable.activelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.activewallet;
//                break;
//            case 4:
//                id=  R.drawable.activeorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.activewishlish;
//                break;
//            case 6:
//                id=  R.drawable.activereceipe;
//                break;
//            case 7:
//                id=  R.drawable.activelogout;
//                break;
//            default:
//                id=  R.drawable.activelogout;
//
//        }
//        return id;
//
//
//    }
//    public int setInActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.inactivehome;
//                break;
//            case 1:
//                id=  R.drawable.inactiveaccount;
//
//                break;
//            case 2:
//                id= R.drawable.inactivelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.inactivewallet;
//                break;
//            case 4:
//                id=  R.drawable.inactiveorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.inactivewishlish;
//                break;
//            case 6:
//                id=  R.drawable.inactivereceipe;
//                break;
//            case 7:
//                id=  R.drawable.inactivelogout;
//                break;
//            default:
//                id=  R.drawable.inactivelogout;
//
//        }
//        return id;
//
//
//    }
}
