package com.dci.avicustomer.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dci.avicustomer.R;
import com.dci.avicustomer.activities.ActivityLatestNews;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.ModelHome;
import com.dci.avicustomer.models.ModelTransport;

import java.util.List;

public class AdapterTransport extends RecyclerView.Adapter<AdapterTransport.DrawerViewHolder> {
    int type;
    OnItemClickListener onItemClickListener;
    private List<ModelTransport.Datum> home_data_list;
    private Context mContext;
    String imgurl;

    public AdapterTransport(Context mContext, List<ModelTransport.Datum> home_data_list, int type,String imgurl) {
        this.mContext = mContext;
        this.home_data_list = home_data_list;
        this.type = type;
        this.imgurl=imgurl;
    }

    @Override
    public AdapterTransport.DrawerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_transport, parent, false);
        return new AdapterTransport.DrawerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterTransport.DrawerViewHolder holder, final int position) {
            holder.tv_servicename.setText(home_data_list.get(position).getName());
            holder.tv_mobile_number.setText("Mobile number: "+home_data_list.get(position).getContact());
            holder.tv_play_link.setText(home_data_list.get(position).getLink());
            holder.tv_emailid.setText(home_data_list.get(position).getEmail());
            Glide.with(mContext).load(imgurl+home_data_list.get(position).getProfile_img()).
                    into(holder.img_trans_img);
            //Glide.get(mContext).with(holder.img_news).load(home_data_list.get(position).getImage());
    }

    public void updateRowIndex(int pos) {
        type = pos;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return home_data_list.size();
    }

    public void setonItemClick(OnItemClickListener listener) {
        this.onItemClickListener = listener;

    }

    public void updateindex(int index) {
        this.type = index;
        notifyDataSetChanged();
    }

    class DrawerViewHolder extends RecyclerView.ViewHolder {
        TextView tv_servicename,tv_mobile_number,tv_emailid,tv_play_link;
        ImageView img_trans_img;
        LinearLayout lin_mail,lin_call;



        public DrawerViewHolder(View itemView) {
            super(itemView);
            //  tv_offer_des = itemView.findViewById(R.id.tv_offer_des);
            tv_servicename = itemView.findViewById(R.id.tv_servicename);
            lin_mail= itemView.findViewById(R.id.lin_mail);
            lin_call= itemView.findViewById(R.id.lin_call);
            img_trans_img = itemView.findViewById(R.id.img_trans_img);
            tv_mobile_number= itemView.findViewById(R.id.tv_mobile_number);
            tv_emailid= itemView.findViewById(R.id.tv_emailid);
            //tv_offer_percentage= itemView.findViewById(R.id.tv_offer_percentage);
            tv_play_link= itemView.findViewById(R.id.tv_play_link);
            lin_call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (home_data_list.get(getAdapterPosition()).getContact()!=null&&!
                            home_data_list.get(getAdapterPosition()).getContact().equals("")){
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse("tel:"+home_data_list.get(getAdapterPosition()).getContact()));
                        mContext.startActivity(intent);
                    }
                    else {
                        Toast.makeText(mContext, R.string.no_number, Toast.LENGTH_SHORT).show();
                    }

                    // item_lay.setBackgroundColor(mContext.getResources().getColor(R.color.orrange));

//                    int position = getAdapterPosition();
//                    ((MainActivity) mContext).showMenuItems(position);
                }
            });
            lin_mail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (home_data_list.get(getAdapterPosition()).getEmail()!=null&&!
                            home_data_list.get(getAdapterPosition()).getEmail().equals("")){
                        try {
                            Intent intent = new Intent (Intent.ACTION_VIEW , Uri.parse("mailto:" +
                                    home_data_list.get(getAdapterPosition()).getEmail().trim()));
                            intent.putExtra(Intent.EXTRA_SUBJECT, "Test");
                            intent.putExtra(Intent.EXTRA_TEXT, "Test");
                            mContext.startActivity(intent);
                        } catch(Exception e) {
                            Toast.makeText(mContext, "Sorry...You don't have any mail app", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }
                    else {
                        Toast.makeText(mContext, R.string.noemail, Toast.LENGTH_SHORT).show();
                    }

                    // item_lay.setBackgroundColor(mContext.getResources().getColor(R.color.orrange));

//                    int position = getAdapterPosition();
//                    ((MainActivity) mContext).showMenuItems(position);
                }
            });
        }
    }
//    public int setActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.activehome;
//                break;
//            case 1:
//                id=  R.drawable.activeaccount;
//
//                break;
//            case 2:
//                id= R.drawable.activelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.activewallet;
//                break;
//            case 4:
//                id=  R.drawable.activeorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.activewishlish;
//                break;
//            case 6:
//                id=  R.drawable.activereceipe;
//                break;
//            case 7:
//                id=  R.drawable.activelogout;
//                break;
//            default:
//                id=  R.drawable.activelogout;
//
//        }
//        return id;
//
//
//    }
//    public int setInActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.inactivehome;
//                break;
//            case 1:
//                id=  R.drawable.inactiveaccount;
//
//                break;
//            case 2:
//                id= R.drawable.inactivelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.inactivewallet;
//                break;
//            case 4:
//                id=  R.drawable.inactiveorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.inactivewishlish;
//                break;
//            case 6:
//                id=  R.drawable.inactivereceipe;
//                break;
//            case 7:
//                id=  R.drawable.inactivelogout;
//                break;
//            default:
//                id=  R.drawable.inactivelogout;
//
//        }
//        return id;
//
//
//    }
}
