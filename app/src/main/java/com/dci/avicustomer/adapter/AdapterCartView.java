package com.dci.avicustomer.adapter;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dci.avicustomer.R;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.ModelCartView;
import com.dci.avicustomer.models.ModelRestaurant;

import java.util.ArrayList;
import java.util.List;

public class AdapterCartView extends RecyclerView.Adapter<AdapterCartView.DrawerViewHolder> {
    String type;
    OnItemClickListener onItemClickListener;
    private List<ModelCartView.Cart> home_data_list=new ArrayList<>();
    private Context mContext;
    String imagepath;


    public AdapterCartView(Context mContext,List<ModelCartView.Cart> home_data_list,String imagepath) {
        this.mContext = mContext;
        this.type = type;
        this.home_data_list=home_data_list;
        this.imagepath=imagepath;

        Log.d("onBindVieswHolder", "onBindViewHolder: "+home_data_list.size());
    }

    @Override
    public AdapterCartView.DrawerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.list_item_restro_items,
                        parent, false);
        return new AdapterCartView.DrawerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterCartView.DrawerViewHolder holder, final int position) {
        holder.rel_whole_lay.setBackground(mContext.getDrawable(R.drawable.card_bg));
        ModelCartView.Cart datum=home_data_list.get(position);
        holder.img_delete_icon.setVisibility(View.VISIBLE);
        holder.tv_item_name.setText(datum.getRestaurantmenucategory().getName());
        holder.tv_price.setText(datum.getRestaurantmenucategory().getCurrency().getCode()+" "+datum.getRestaurantmenucategory().getPrice());
        holder.tv_qty.setText(""+datum.getQuantity());
        holder.imag_item.setClipToOutline(true);
        try {
            byte[]base64= Base64.decode(datum.getRestaurantmenucategory().getDescription(), Base64.DEFAULT);
            String html = new String(base64, "UTF-8");

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                holder.tv_describtion.setText(Html.fromHtml(html, Html.FROM_HTML_MODE_COMPACT));
            } else {
                holder.tv_describtion.setText(Html.fromHtml(html));
            }
        } catch (Exception e) {
            e.printStackTrace();
            holder.tv_describtion.setText(datum.getRestaurantmenucategory().getDescription());




        }
        Glide.with(mContext).load(imagepath+datum.getRestaurantmenucategory().getImages()).into(holder.imag_item);
    }



    @Override
    public int getItemCount() {
        return home_data_list.size();
    }

    public void setonItemClick(OnItemClickListener listener) {
        this.onItemClickListener = listener;
    }



    class DrawerViewHolder extends RecyclerView.ViewHolder {
        TextView tv_item_name,tv_price,tv_describtion,tv_qty;
        ImageView imag_item,img_plus,img_minus,img_delete_icon;
        RelativeLayout rel_whole_lay;




        public DrawerViewHolder(View itemView) {
            super(itemView);
            rel_whole_lay= itemView.findViewById(R.id.rel_whole_lay);
            tv_price = itemView.findViewById(R.id.tv_price);
            imag_item = itemView.findViewById(R.id.imag_item);
            tv_qty= itemView.findViewById(R.id.tv_qty);
            tv_item_name = itemView.findViewById(R.id.tv_item_name);
            tv_describtion = itemView.findViewById(R.id.tv_describtion);
            img_plus= itemView.findViewById(R.id.img_plus);
            img_minus= itemView.findViewById(R.id.img_minus);
            img_delete_icon= itemView.findViewById(R.id.img_delete_icon);
            img_plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                        onItemClickListener.OnItemClickListener(getAdapterPosition(),view);
                   // }
//                    ((MainActivity) mContext).showMenuItems(position);
                }
            });
            img_minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (home_data_list.get(getAdapterPosition()).getQuantity()!=0){
                        onItemClickListener.OnItemClickListener(getAdapterPosition(),view);
                    }


//                    ((MainActivity) mContext).showMenuItems(position);
                }
            });
            img_delete_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (home_data_list.get(getAdapterPosition()).getQuantity()!=0){
                        onItemClickListener.OnItemClickListener(getAdapterPosition(),v);
                    }
                }
            });
        }
    }
//    public int setActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.activehome;
//                break;
//            case 1:
//                id=  R.drawable.activeaccount;
//
//                break;
//            case 2:
//                id= R.drawable.activelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.activewallet;
//                break;
//            case 4:
//                id=  R.drawable.activeorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.activewishlish;
//                break;
//            case 6:
//                id=  R.drawable.activereceipe;
//                break;
//            case 7:
//                id=  R.drawable.activelogout;
//                break;
//            default:
//                id=  R.drawable.activelogout;
//
//        }
//        return id;
//
//
//    }
//    public int setInActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.inactivehome;
//                break;
//            case 1:
//                id=  R.drawable.inactiveaccount;
//
//                break;
//            case 2:
//                id= R.drawable.inactivelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.inactivewallet;
//                break;
//            case 4:
//                id=  R.drawable.inactiveorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.inactivewishlish;
//                break;
//            case 6:
//                id=  R.drawable.inactivereceipe;
//                break;
//            case 7:
//                id=  R.drawable.inactivelogout;
//                break;
//            default:
//                id=  R.drawable.inactivelogout;
//
//        }
//        return id;
//
//
//    }
}

