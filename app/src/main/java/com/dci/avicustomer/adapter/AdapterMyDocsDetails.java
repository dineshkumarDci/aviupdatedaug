package com.dci.avicustomer.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dci.avicustomer.R;
import com.dci.avicustomer.activities.ActivityCommonWebLoader;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.ModelDocumentDetailsList;
import com.dci.avicustomer.models.ModelTripDetails;

import java.util.List;

public class AdapterMyDocsDetails extends RecyclerView.Adapter<AdapterMyDocsDetails.DrawerViewHolder> {
    int type;
    OnItemClickListener onItemClickListener;
    private List<ModelDocumentDetailsList.Document> docsList;
    private Context mContext;
    String imagePath;
    public AdapterMyDocsDetails(Context mContext, List<ModelDocumentDetailsList.Document> boardingList,
                               String imagepath) {
        this.mContext = mContext;
        this.docsList = boardingList;
        this.imagePath=imagepath;
    }

    @Override
    public AdapterMyDocsDetails.DrawerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_docsdetails, parent, false);
        return new AdapterMyDocsDetails.DrawerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterMyDocsDetails.DrawerViewHolder holder, final int position) {
            Glide.with(mContext).load(imagePath+docsList.get(position).getImage()).
                    placeholder(R.drawable.folder).into(holder.image_docs);
            holder.tv_image_name.setText(docsList.get(position).getImage());
            if (docsList.get(position).getUpdated_at()!=null&&!docsList.get(position).getUpdated_at().equals("")){
                holder.tv_update_time.setText(UtilsDefault.dateFormat(docsList.get(position).getUpdated_at()));
            }
            holder.image_docs.setClipToOutline(true);
            holder.tv_update_time.setVisibility(View.VISIBLE);
//            holder.image_docs.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    String MimeType=getMimeType(imagePath+docsList.get(position).getImage());
//                    if (MimeType.contains("image")){
//                        showImage(holder.image_docs.getDrawable(),docsList.get(position).getImage());
//                    }
//                    else {
//                        Intent intent=new Intent(mContext, ActivityCommonWebLoader.class);
//                        intent.putExtra("data",
//                                "https://drive.google.com/viewerng/viewer?embedded=true&url="+
//                                        imagePath+docsList.get(position).getImage());
//                        intent.putExtra("title",docsList.get(position).getImage());
//                        mContext.startActivity(intent);
//                       // https://drive.google.com/viewerng/viewer?embedded=true&url=
//                    }
//                }
//            });

    }

    public void updateRowIndex(int pos) {
        type = pos;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {

            return docsList.size();
    }

    public void setonItemClick(OnItemClickListener listener) {
        this.onItemClickListener = listener;

    }



    public void updateindex(int index) {
        this.type = index;
        notifyDataSetChanged();
    }

    class DrawerViewHolder extends RecyclerView.ViewHolder {

        ImageView image_docs,image_delete;
        TextView tv_image_name,tv_update_time;



        public DrawerViewHolder(View itemView) {
            super(itemView);
            //  tv_offer_des = itemView.findViewById(R.id.tv_offer_des);

            image_docs = itemView.findViewById(R.id.image_docs);
            image_delete = itemView.findViewById(R.id.image_delete);
            tv_image_name=itemView.findViewById(R.id.tv_image_name);
            tv_update_time=itemView.findViewById(R.id.tv_update_time);
            image_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.OnItemClickListener(getAdapterPosition(),v);
                }
            });



            //tv_offer_percentage= itemView.findViewById(R.id.tv_offer_percentage);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String MimeType=getMimeType(imagePath+docsList.get(getAdapterPosition()).getImage());
                    if (MimeType.contains("image")){
                        showImage(docsList.get(getAdapterPosition()).getImage());
                    }
                    else {
                        Intent intent=new Intent(mContext, ActivityCommonWebLoader.class);
                        intent.putExtra("data",
                                "https://drive.google.com/viewerng/viewer?embedded=true&url="+
                                        imagePath+docsList.get(getAdapterPosition()).getImage());
                        intent.putExtra("title",docsList.get(getAdapterPosition()).getImage());
                        mContext.startActivity(intent);
                        // https://drive.google.com/viewerng/viewer?embedded=true&url=
                    }

                    // item_lay.setBackgroundColor(mContext.getResources().getColor(R.color.orrange));

//                    int position = getAdapterPosition();
//                    ((MainActivity) mContext).showMenuItems(position);
                }
            });
        }
    }

//    public int setActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.activehome;
//                break;
//            case 1:
//                id=  R.drawable.activeaccount;
//
//                break;
//            case 2:
//                id= R.drawable.activelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.activewallet;
//                break;
//            case 4:
//                id=  R.drawable.activeorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.activewishlish;
//                break;
//            case 6:
//                id=  R.drawable.activereceipe;
//                break;
//            case 7:
//                id=  R.drawable.activelogout;
//                break;
//            default:
//                id=  R.drawable.activelogout;
//
//        }
//        return id;
//
//
//    }
//    public int setInActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.inactivehome;
//                break;
//            case 1:
//                id=  R.drawable.inactiveaccount;
//
//                break;
//            case 2:
//                id= R.drawable.inactivelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.inactivewallet;
//                break;
//            case 4:
//                id=  R.drawable.inactiveorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.inactivewishlish;
//                break;
//            case 6:
//                id=  R.drawable.inactivereceipe;
//                break;
//            case 7:
//                id=  R.drawable.inactivelogout;
//                break;
//            default:
//                id=  R.drawable.inactivelogout;
//
//        }
//        return id;
//
//
//    }
public static String getMimeType(String url) {
    String type = null;
    String extension = MimeTypeMap.getFileExtensionFromUrl(url);
    if (extension != null) {
        type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
    }
    return type;
}
public void showImage(String imgUrl) {
    Dialog builder = new Dialog(mContext);
    builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
    builder.getWindow().setBackgroundDrawable(
            new ColorDrawable(android.graphics.Color.TRANSPARENT));
    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
        @Override
        public void onDismiss(DialogInterface dialogInterface) {

            //nothing;
        }
    });

    ImageView imageView = new ImageView(mContext);
    //imageView.setImageDrawable(drawable);

    builder.addContentView(imageView, new RelativeLayout.LayoutParams(
           1000,
            1000));

    Glide.with(mContext).load(imagePath+imgUrl).
            placeholder(R.drawable.folder).into(imageView);

    builder.show();
}
}
