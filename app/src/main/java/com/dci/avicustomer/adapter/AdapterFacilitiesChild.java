package com.dci.avicustomer.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dci.avicustomer.R;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.ModelFacilities;

import java.util.List;

public class AdapterFacilitiesChild extends RecyclerView.Adapter<AdapterFacilitiesChild.MyViewHolder> {
    // public ArrayList<DummyParentDataItem> dummyParentDataItems;
    List<ModelFacilities.Loungefacility> dummyParentDataItems;
    Context context;
    ProgressDialog progressDialog;
    OnItemClickListener onItemClickListener;

    public AdapterFacilitiesChild(List<ModelFacilities.Loungefacility> dummyParentDataItems, Context context) {
        this.dummyParentDataItems = dummyParentDataItems;
        this.context=context;
    }

    @Override
    public AdapterFacilitiesChild.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_child_facilities, parent, false);
        return new AdapterFacilitiesChild.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AdapterFacilitiesChild.MyViewHolder holder, int position) {
        ModelFacilities.Loungefacility dummyParentDataItem = dummyParentDataItems.get(position);
        holder.tv_facilitiesname.setText(dummyParentDataItem.getFacility().getName());
        //




    }

    @Override
    public int getItemCount() {
        return dummyParentDataItems.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tv_facilitiesname,tv_terminal_details,tv_describtion;
        public ImageView img_shop;

        MyViewHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();
            tv_facilitiesname = itemView.findViewById(R.id.tv_facilitiesname);
            tv_terminal_details = itemView.findViewById(R.id.tv_terminal_details);
            tv_describtion= itemView.findViewById(R.id.tv_describtion);
            img_shop= itemView.findViewById(R.id.img_shop);
            itemView.setOnClickListener(this);

            // textView_parentName.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            onItemClickListener.OnItemClickListener(getAdapterPosition(),view);

//            } else {
//                TextView textViewClicked = (TextView) view;
//                Toast.makeText(context, "" + textViewClicked.getText().toString(), Toast.LENGTH_SHORT).show();
//            }

        }
    }
    public void setonItemClick(OnItemClickListener listener) {
        this.onItemClickListener = listener;

    }

    public void removeAt(int position) {
        dummyParentDataItems.remove(position);
        notifyItemRemoved(position);
        // notifyItemRangeChanged(position, dummyParentDataItems.size());
        // ((ActivityChartRaiseTest)c).updateamount(child);
    }


}
