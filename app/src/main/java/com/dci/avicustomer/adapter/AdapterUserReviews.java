package com.dci.avicustomer.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dci.avicustomer.R;
import com.dci.avicustomer.activities.ActivityUserReviews;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.DrawerItem;
import com.dci.avicustomer.models.ModelUserReviews;
import com.github.siyamed.shapeimageview.CircularImageView;

import java.util.List;

public class AdapterUserReviews extends RecyclerView.Adapter<AdapterUserReviews.DrawerViewHolder> {
    int row_index;
    OnItemClickListener onItemClickListener;
    private List<ModelUserReviews.Review> reviewList;
    private Context mContext;
    String baseUrl="";

    public AdapterUserReviews(Context mContext, List<ModelUserReviews.Review> drawerMenuList) {
        this.mContext = mContext;
        this.reviewList = drawerMenuList;
    }

    @Override
    public AdapterUserReviews.DrawerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_user_review, parent, false);
        return new AdapterUserReviews.DrawerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterUserReviews.DrawerViewHolder holder, final int position) {
//        holder.title.setText(drawerMenuList.get(position).getTitle());
//        holder.icon.setImageResource(drawerMenuList.get(position).getIcon());
        if (reviewList.get(position).getDate()!=null&&!reviewList.get(position).getDate().equals(""))
        {
            holder.tv_date.setText(UtilsDefault.dateFormat(reviewList.get(position).getDate()));

        }
        holder.tv_user_name.setText(reviewList.get(position).getName());
        holder.tv_reviewComments.setText(reviewList.get(position).getComment());
        String baseurl=((ActivityUserReviews)mContext).getImageBase();


        Glide.with(mContext).load(
                baseurl+reviewList.get(position).getImage()).
                placeholder(R.drawable.placeholder_icon).into(holder.img_userimage);
        Log.d("image", "onBindViewHolder: "+baseurl
                +reviewList.get(position).getImage());
        try {
            holder.rating.setRating(reviewList.get(position).getRating());
        }
        catch (Exception e){

        }





    }
    public void updateRowIndex(int pos){
        row_index=pos;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return reviewList.size();
    }

    public void setonItemClick(OnItemClickListener listener) {
        this.onItemClickListener = listener;

    }

    public void updateindex(int index) {
        this.row_index = index;
        notifyDataSetChanged();
    }

    class DrawerViewHolder extends RecyclerView.ViewHolder {
        TextView tv_user_name,tv_date,tv_reviewComments;
        CircularImageView img_userimage;
        RatingBar rating;



        public DrawerViewHolder(View itemView) {
            super(itemView);
            tv_user_name = itemView.findViewById(R.id.tv_user_name);
            tv_date = itemView.findViewById(R.id.tv_date);
            img_userimage = itemView.findViewById(R.id.img_userimage);
            tv_reviewComments= itemView.findViewById(R.id.tv_reviewComments);
            rating= itemView.findViewById(R.id.rating);
            //item_lay = itemView.findViewById(R.id.item_lay);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    // item_lay.setBackgroundColor(mContext.getResources().getColor(R.color.orrange));

//                    int position = getAdapterPosition();
//                    ((MainActivity) mContext).showMenuItems(position);
                }
            });
        }
    }
//    public int setActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.activehome;
//                break;
//            case 1:
//                id=  R.drawable.activeaccount;
//
//                break;
//            case 2:
//                id= R.drawable.activelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.activewallet;
//                break;
//            case 4:
//                id=  R.drawable.activeorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.activewishlish;
//                break;
//            case 6:
//                id=  R.drawable.activereceipe;
//                break;
//            case 7:
//                id=  R.drawable.activelogout;
//                break;
//            default:
//                id=  R.drawable.activelogout;
//
//        }
//        return id;
//
//
//    }
//    public int setInActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.inactivehome;
//                break;
//            case 1:
//                id=  R.drawable.inactiveaccount;
//
//                break;
//            case 2:
//                id= R.drawable.inactivelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.inactivewallet;
//                break;
//            case 4:
//                id=  R.drawable.inactiveorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.inactivewishlish;
//                break;
//            case 6:
//                id=  R.drawable.inactivereceipe;
//                break;
//            case 7:
//                id=  R.drawable.inactivelogout;
//                break;
//            default:
//                id=  R.drawable.inactivelogout;
//
//        }
//        return id;
//
//
//    }
}
