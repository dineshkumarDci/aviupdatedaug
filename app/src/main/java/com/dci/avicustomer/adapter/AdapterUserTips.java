package com.dci.avicustomer.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dci.avicustomer.R;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.ModelUserTipList.Datum;
import com.dci.avicustomer.models.ModelUserTipList;

import java.util.List;

public class AdapterUserTips extends RecyclerView.Adapter<AdapterUserTips.DrawerViewHolder> {
    int row_index;
    OnItemClickListener onItemClickListener;
    private List<ModelUserTipList.Datum> userTipsList;
    private Context mContext;

    public AdapterUserTips(Context mContext, List<ModelUserTipList.Datum> userTipsList, int row_index) {
        this.mContext = mContext;
        this.userTipsList = userTipsList;
        this.row_index = row_index;
    }

    @Override
    public AdapterUserTips.DrawerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_user_tips, parent, false);
        return new AdapterUserTips.DrawerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterUserTips.DrawerViewHolder holder, final int position) {
        holder.tv_user_tipdes.setText(userTipsList.get(position).getDescription());
        holder.tv_user_tipdate.setText(UtilsDefault.dateFormat(userTipsList.get(position).getUpdated_at()));
        if (userTipsList.get(position).getUsersFname()!=null){
            holder.tv_user_name.setText(userTipsList.get(position).getUsersFname());
        }


//        holder.item_lay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onItemClickListener.OnItemClickListener(position, view);
//                if (position!=12){
//                    row_index = position;
//                    notifyDataSetChanged();
//                }
//
//            }
//        });

    }
    public void updateRowIndex(int pos){
        row_index=pos;
        notifyDataSetChanged();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return userTipsList.size();
    }

    public void setonItemClick(OnItemClickListener listener) {
        this.onItemClickListener = listener;

    }

    public void updateindex(int index) {
        this.row_index = index;
        notifyDataSetChanged();
    }

    class DrawerViewHolder extends RecyclerView.ViewHolder {
        TextView tv_user_tipdes,tv_user_tipdate,tv_user_name;
       


        public DrawerViewHolder(View itemView) {
            super(itemView);
            tv_user_tipdes = itemView.findViewById(R.id.tv_user_tipdes);
            tv_user_tipdate = itemView.findViewById(R.id.tv_user_tipdate);
            tv_user_name = itemView.findViewById(R.id.tv_user_name);
         

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    // item_lay.setBackgroundColor(mContext.getResources().getColor(R.color.orrange));

//                    int position = getAdapterPosition();
//                    ((MainActivity) mContext).showMenuItems(position);
                }
            });
        }
    }
//    public int setActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.activehome;
//                break;
//            case 1:
//                id=  R.drawable.activeaccount;
//
//                break;
//            case 2:
//                id= R.drawable.activelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.activewallet;
//                break;
//            case 4:
//                id=  R.drawable.activeorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.activewishlish;
//                break;
//            case 6:
//                id=  R.drawable.activereceipe;
//                break;
//            case 7:
//                id=  R.drawable.activelogout;
//                break;
//            default:
//                id=  R.drawable.activelogout;
//
//        }
//        return id;
//
//
//    }
//    public int setInActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.inactivehome;
//                break;
//            case 1:
//                id=  R.drawable.inactiveaccount;
//
//                break;
//            case 2:
//                id= R.drawable.inactivelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.inactivewallet;
//                break;
//            case 4:
//                id=  R.drawable.inactiveorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.inactivewishlish;
//                break;
//            case 6:
//                id=  R.drawable.inactivereceipe;
//                break;
//            case 7:
//                id=  R.drawable.inactivelogout;
//                break;
//            default:
//                id=  R.drawable.inactivelogout;
//
//        }
//        return id;
//
//
//    }
}
