package com.dci.avicustomer.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dci.avicustomer.R;
import com.dci.avicustomer.activities.ActivityShopDetails;
import com.dci.avicustomer.activities.ActivityShopList;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.ModelFlightSchedule;
import com.dci.avicustomer.models.ModelShopList;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class AdapterFlightSchedule extends RecyclerView.Adapter<AdapterFlightSchedule.ViewHolder> {
    int type;
    OnItemClickListener onItemClickListener;
    private List<ModelFlightSchedule.Datum> home_data_list;
    private Context mContext;

    public AdapterFlightSchedule(Context mContext,
                                 List<ModelFlightSchedule.Datum> home_data_list, int type) {
        this.mContext = mContext;
        this.home_data_list = home_data_list;
        this.type = type;
    }

    @Override
    public AdapterFlightSchedule.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_flight_schedule, parent, false);
        return new AdapterFlightSchedule.ViewHolder(view);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(AdapterFlightSchedule.ViewHolder holder, final int position) {

        ModelFlightSchedule.Datum modelShopList = home_data_list.get(position);
        holder.flight_code.setText(modelShopList.getAirlineCode()+" "+modelShopList.getFlightNumber());
        holder.tv_arrival_city.setText(modelShopList.getArrival_city()+" , "+modelShopList.getArrival_countryid());
        holder.tv_dept_city.setText(modelShopList.getDepature_city()+" , "+modelShopList.getDepature_countryid());
        holder.tv_arrive_code.setText(modelShopList.getArrival_airportcode());
        holder.tv_arrive_date.setText(UtilsDefault.dateFormat(modelShopList.getArrival_date()+" 12:33:00"));
        holder.tv_dept_date.setText(UtilsDefault.dateFormat(modelShopList.getDepature_date()+" 12:33:00"));
        holder.tv_dept_code.setText(modelShopList.getDepature_airportcode());
        holder.tv_arrive_time.setText(UtilsDefault.get24HoursTimeFormat(modelShopList.getArrival_time()));
        holder.tv_dept_time.setText(UtilsDefault.get24HoursTimeFormat(modelShopList.getDepature_time()));
        if (modelShopList.getArrival_terminal()==null||modelShopList.getArrival_terminal().equals("")){
// holder.tv_arrval_terminal
        }
        else {
            holder.tv_arrval_terminal.setText(mContext.getString(R.string.text_terminal)+" "+
                    String.valueOf(modelShopList.getArrival_terminal()));
        }
        if (modelShopList.getDepature_terminal()==null||modelShopList.getDepature_terminal().equals("")){
// holder.tv_arrval_terminal
        }
        else {
            holder.tv_dept_terminal.setText(mContext.getString(R.string.text_terminal)+" "+
                    String.valueOf(modelShopList.getDepature_terminal()));
        }

        if (modelShopList.getDepGate()==null||modelShopList.getDepGate().equals("")){
// holder.tv_arrval_terminal
        }
        else {
            holder.tv_dept_terminal.append(
                    String.valueOf(" , "+modelShopList.getDepGate()));
        }

        if (modelShopList.getArrGate()==null||modelShopList.getArrGate().equals("")){
// holder.tv_arrval_terminal
        }
        else {
            holder.tv_arrval_terminal.append(
                    String.valueOf(" , "+modelShopList.getArrGate()));
        }
        holder.flight_status.setText(modelShopList.getFlightStatus());



        holder.tv_travel_time.setText(UtilsDefault.differentBetweenTime(modelShopList.getDepature_date()+" "+
                modelShopList.getDepature_time(),modelShopList.getArrival_date()+" "+modelShopList.getArrival_time()));
        holder.tv_arrival_city.setSelected(true);
        holder.tv_dept_city.setSelected(true);
        holder.rel_bottom2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.OnItemClickListener(position,v);

            }
        });
        holder.img_shop.setClipToOutline(true);
        holder.flight_code.setSelected(true);
        if (modelShopList.getAirlineName()!=null){
            holder.tv_airlinenames.setText(modelShopList.getAirlineName());

        }
        else {
            holder.tv_airlinenames.setText(mContext.getString(R.string.nill));
        }

//        holder.tv_dept_time.setText(UtilsDefault.timeFormat(modelShopList.getDepature_time()));
//        holder.tv_dept_time.setText(UtilsDefault.timeFormat(modelShopList.getDepature_time()));
//        holder.tv_dept_time.setText(UtilsDefault.timeFormat(modelShopList.getDepature_time()));


    }

    public void updateRowIndex(int pos) {
        type = pos;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return home_data_list.size();
    }

    public void setonItemClick(OnItemClickListener listener) {
        this.onItemClickListener = listener;

    }

    public void updateindex(int index) {
        this.type = index;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_dept_time,tv_address, tv_dept_date,
                tv_arrive_time, tv_arrive_date,tv_dept_code,
                tv_dept_city,tv_arrive_code,tv_arrival_city,tv_airlinenames,
                flight_status,flight_code,tv_travel_time,tv_dept_terminal,tv_arrval_terminal,tv_airlinename;
        ImageView img_shop, img_locate, img_phn_num, img_email;
        LinearLayout lin_bottom1,lin_bottom3;
        CardView card_test;
        RelativeLayout rel_bottom2;
        public ViewHolder(View itemView) {
            super(itemView);
            //  tv_offer_des = itemView.findViewById(R.id.tv_offer_des);
            tv_dept_time = itemView.findViewById(R.id.tv_dept_time);
            card_test = itemView.findViewById(R.id.card_test);
            tv_airlinenames= itemView.findViewById(R.id.tv_airlinenames);
            tv_dept_terminal = itemView.findViewById(R.id.tv_dept_terminal);
            tv_arrval_terminal = itemView.findViewById(R.id.tv_arrval_terminal);
            tv_travel_time = itemView.findViewById(R.id.tv_travel_time);
            tv_airlinename=itemView.findViewById(R.id.tv_airlinename);
            lin_bottom3= itemView.findViewById(R.id.lin_bottom3);
            tv_address = itemView.findViewById(R.id.tv_address);
            tv_dept_date = itemView.findViewById(R.id.tv_dept_date);
            tv_address = itemView.findViewById(R.id.tv_address);
            tv_arrive_time=itemView.findViewById(R.id.tv_arrive_time);
            tv_arrive_date = itemView.findViewById(R.id.tv_arrive_date);
            tv_dept_code = itemView.findViewById(R.id.tv_dept_code);
            tv_dept_city = itemView.findViewById(R.id.tv_dept_city);
            tv_arrive_code = itemView.findViewById(R.id.tv_arrive_code);
            tv_arrival_city = itemView.findViewById(R.id.tv_arrival_city);
            flight_status = itemView.findViewById(R.id.flight_status);
            flight_code = itemView.findViewById(R.id.flight_code);

            img_locate = itemView.findViewById(R.id.img_locate);
            img_phn_num = itemView.findViewById(R.id.img_phn_num);
            img_shop= itemView.findViewById(R.id.img_shop);
            img_email = itemView.findViewById(R.id.img_email);
            lin_bottom1 = itemView.findViewById(R.id.lin_bottom1);
            rel_bottom2 = itemView.findViewById(R.id.rel_bottom2);
            tv_dept_city.setSelected(true);
            tv_arrival_city.setSelected(true);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (lin_bottom1.getVisibility()==View.VISIBLE){
                        lin_bottom1.setVisibility(View.GONE);
                        rel_bottom2.setVisibility(View.GONE);
                        lin_bottom3.setVisibility(View.GONE);
                        card_test.setVisibility(View.GONE);
                    }
                    else {
                        lin_bottom3.setVisibility(View.VISIBLE);
                        lin_bottom1.setVisibility(View.VISIBLE);
                        rel_bottom2.setVisibility(View.VISIBLE);
                        card_test.setVisibility(View.INVISIBLE);

                    }
                    onItemClickListener.OnItemClickListener(getAdapterPosition(),v);
                }
            });
        }
    }
//    public int setActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.activehome;
//                break;
//            case 1:
//                id=  R.drawable.activeaccount;
//
//                break;
//            case 2:
//                id= R.drawable.activelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.activewallet;
//                break;
//            case 4:
//                id=  R.drawable.activeorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.activewishlish;
//                break;
//            case 6:
//                id=  R.drawable.activereceipe;
//                break;
//            case 7:
//                id=  R.drawable.activelogout;
//                break;
//            default:
//                id=  R.drawable.activelogout;
//
//        }
//        return id;
//
//
//    }
//    public int setInActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.inactivehome;
//                break;
//            case 1:
//                id=  R.drawable.inactiveaccount;
//
//                break;
//            case 2:
//                id= R.drawable.inactivelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.inactivewallet;
//                break;
//            case 4:
//                id=  R.drawable.inactiveorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.inactivewishlish;
//                break;
//            case 6:
//                id=  R.drawable.inactivereceipe;
//                break;
//            case 7:
//                id=  R.drawable.inactivelogout;
//                break;
//            default:
//                id=  R.drawable.inactivelogout;
//
//        }
//        return id;
//
//
//    }
}

