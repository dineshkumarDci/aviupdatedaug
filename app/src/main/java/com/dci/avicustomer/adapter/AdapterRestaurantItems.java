package com.dci.avicustomer.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dci.avicustomer.R;
import com.dci.avicustomer.activities.ActivityRestaurantDetails;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.ModelOfferList;
import com.dci.avicustomer.models.ModelRestaurant;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class AdapterRestaurantItems extends RecyclerView.Adapter<AdapterRestaurantItems.DrawerViewHolder> {
    String type;
    OnItemClickListener onItemClickListener;
    private List<ModelRestaurant.Restaurantmenu> home_data_list=new ArrayList<>();
    private Context mContext;
    String imagepath;


    public AdapterRestaurantItems(Context mContext,List<ModelRestaurant.Restaurantmenu> home_data_list,
                                  String imagepath) {
        this.mContext = mContext;
        this.type = type;
        this.home_data_list=home_data_list;
        this.imagepath=imagepath;

        Log.d("onBindVieswHolder", "onBindViewHolder: "+home_data_list.size());
    }

    @Override
    public AdapterRestaurantItems.DrawerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_restro_items, parent, false);
        return new AdapterRestaurantItems.DrawerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterRestaurantItems.DrawerViewHolder holder, final int position) {
        ModelRestaurant.Restaurantmenu datum=home_data_list.get(position);
        holder.tv_item_name.setText(datum.getName());
        try {


            byte[]base64= Base64.decode(datum.getDescription(), Base64.DEFAULT);
            String html = new String(base64, "UTF-8");

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                holder.tv_describtion.setText(Html.fromHtml(html, Html.FROM_HTML_MODE_COMPACT));
            } else {
                holder.tv_describtion.setText(Html.fromHtml(html));
            }
        } catch (Exception e) {
            e.printStackTrace();
            holder.tv_describtion.setText(datum.getDescription());


        }
        holder.tv_price.setText(datum.getCurrencyCode()+" "+datum.getPrice());
        holder.tv_qty.setText(""+datum.getQuantity());
        holder.imag_item.setClipToOutline(true);
        Glide.with(mContext).load(imagepath+datum.getImages()).into(holder.imag_item);

    }



    @Override
    public int getItemCount() {
        return home_data_list.size();
    }

    public void setonItemClick(OnItemClickListener listener) {
        this.onItemClickListener = listener;

    }



    class DrawerViewHolder extends RecyclerView.ViewHolder {
        TextView tv_item_name,tv_price,tv_describtion,tv_qty;
        ImageView imag_item,img_plus,img_minus;




        public DrawerViewHolder(View itemView) {
            super(itemView);

            tv_price = itemView.findViewById(R.id.tv_price);
            imag_item = itemView.findViewById(R.id.imag_item);
            tv_qty= itemView.findViewById(R.id.tv_qty);
            tv_item_name = itemView.findViewById(R.id.tv_item_name);
            tv_describtion = itemView.findViewById(R.id.tv_describtion);
            img_plus= itemView.findViewById(R.id.img_plus);
            img_minus= itemView.findViewById(R.id.img_minus);
            img_plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                  //  if (home_data_list.get(getAdapterPosition()).getQuantity()<50){
                        onItemClickListener.OnItemClickListener(getAdapterPosition(),view);
                   // }

//                    ((MainActivity) mContext).showMenuItems(position);
                }
            });
            img_minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (home_data_list.get(getAdapterPosition()).getQuantity()!=0){
                        onItemClickListener.OnItemClickListener(getAdapterPosition(),view);
                    }


//                    ((MainActivity) mContext).showMenuItems(position);
                }
            });
        }
    }
//    public int setActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.activehome;
//                break;
//            case 1:
//                id=  R.drawable.activeaccount;
//
//                break;
//            case 2:
//                id= R.drawable.activelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.activewallet;
//                break;
//            case 4:
//                id=  R.drawable.activeorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.activewishlish;
//                break;
//            case 6:
//                id=  R.drawable.activereceipe;
//                break;
//            case 7:
//                id=  R.drawable.activelogout;
//                break;
//            default:
//                id=  R.drawable.activelogout;
//
//        }
//        return id;
//
//
//    }
//    public int setInActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.inactivehome;
//                break;
//            case 1:
//                id=  R.drawable.inactiveaccount;
//
//                break;
//            case 2:
//                id= R.drawable.inactivelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.inactivewallet;
//                break;
//            case 4:
//                id=  R.drawable.inactiveorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.inactivewishlish;
//                break;
//            case 6:
//                id=  R.drawable.inactivereceipe;
//                break;
//            case 7:
//                id=  R.drawable.inactivelogout;
//                break;
//            default:
//                id=  R.drawable.inactivelogout;
//
//        }
//        return id;
//
//
//    }
}

