package com.dci.avicustomer.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dci.avicustomer.R;
import com.dci.avicustomer.activities.ActivityOfferPage;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.ModelHome;

import java.util.List;

public class AdapterHomeOffers extends RecyclerView.Adapter<AdapterHomeOffers.DrawerViewHolder> {
    int type;
    OnItemClickListener onItemClickListener;
    String imagepath;
    private List<ModelHome.Offers> home_data_list;
    private Context mContext;

    public AdapterHomeOffers(Context mContext, List<ModelHome.Offers> home_data_list, int type, String imagepath) {
        this.mContext = mContext;
        this.home_data_list = home_data_list;
        this.type = type;
        this.imagepath = imagepath;
        Log.d("onBindVieswHolder", "onBindViewHolder: " + home_data_list.size());
    }

    @Override
    public DrawerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_home_offers, parent, false);
        return new DrawerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DrawerViewHolder holder, final int position) {

        if (home_data_list.get(position).getId() != 0) {
            holder.main_lay.setVisibility(View.VISIBLE);
            holder.seemore_lay.setVisibility(View.GONE);
            holder.tv_offer_name.setText(home_data_list.get(position).getName());
            holder.tv_offer_percentage.setVisibility(View.VISIBLE);
            holder.lin_offer_lay.bringToFront();

            holder.tv_offer_des.setText(home_data_list.get(position).getOfferTitle());

            Glide.with(mContext).load(imagepath + home_data_list.get(position).getImage())
                    .into(holder.offerimage);
            if ( home_data_list.get(position).getOffer_type_id() != null&&
                    home_data_list.get(position).getOffer_type_id()==1) {
                holder.tv_flat.setVisibility(View.GONE);
                holder.lin_offer_lay.setVisibility(View.VISIBLE);
                holder.tv_offer_percentage.setText(home_data_list.get(position).getPercentage());
            } else {
                holder.lin_offer_lay.setVisibility(View.GONE);

            }

            if (home_data_list.get(position).getOffer_type_id() != null&&
                    home_data_list.get(position).getOffer_type_id()==2){
                holder.lin_offer_lay.setVisibility(View.GONE);
                holder.tv_flat.setVisibility(View.VISIBLE);
                holder.tv_flat.bringToFront();
                holder.tv_flat.setText("Flat "+home_data_list.get(position).getFlat_rate());

            }
        } else {
            holder.main_lay.setVisibility(View.GONE);
            holder.seemore_lay.setVisibility(View.VISIBLE);
        }
    }


    public void updateRowIndex(int pos) {
        type = pos;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return home_data_list.size();
    }

    public void setonItemClick(OnItemClickListener listener) {
        this.onItemClickListener = listener;

    }

    public void updateindex(int index) {
        this.type = index;
        notifyDataSetChanged();
    }

    class DrawerViewHolder extends RecyclerView.ViewHolder {
        TextView tv_offer_name, tv_offer_des, tv_offer_percentage,tv_flat;
        ImageView offerimage;
        LinearLayout seemore_lay, lin_offer_lay;
        CardView main_lay;


        public DrawerViewHolder(View itemView) {
            super(itemView);
            tv_offer_des = itemView.findViewById(R.id.tv_offer_des);
            tv_flat= itemView.findViewById(R.id.tv_flat);
            lin_offer_lay = itemView.findViewById(R.id.lin_offer_lay);
            tv_offer_name = itemView.findViewById(R.id.tv_offer_name);
            offerimage = itemView.findViewById(R.id.offerimage);
            seemore_lay = itemView.findViewById(R.id.seemore_lay);
            main_lay = itemView.findViewById(R.id.main_lay);
            tv_offer_percentage = itemView.findViewById(R.id.tv_offer_percentage);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mContext, ActivityOfferPage.class);
                    mContext.startActivity(intent);


                    // item_lay.setBackgroundColor(mContext.getResources().getColor(R.color.orrange));

//                    int position = getAdapterPosition();
//                    ((MainActivity) mContext).showMenuItems(position);
                }
            });
        }
    }
//    public int setActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.activehome;
//                break;
//            case 1:
//                id=  R.drawable.activeaccount;
//
//                break;
//            case 2:
//                id= R.drawable.activelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.activewallet;
//                break;
//            case 4:
//                id=  R.drawable.activeorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.activewishlish;
//                break;
//            case 6:
//                id=  R.drawable.activereceipe;
//                break;
//            case 7:
//                id=  R.drawable.activelogout;
//                break;
//            default:
//                id=  R.drawable.activelogout;
//
//        }
//        return id;
//
//
//    }
//    public int setInActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.inactivehome;
//                break;
//            case 1:
//                id=  R.drawable.inactiveaccount;
//
//                break;
//            case 2:
//                id= R.drawable.inactivelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.inactivewallet;
//                break;
//            case 4:
//                id=  R.drawable.inactiveorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.inactivewishlish;
//                break;
//            case 6:
//                id=  R.drawable.inactivereceipe;
//                break;
//            case 7:
//                id=  R.drawable.inactivelogout;
//                break;
//            default:
//                id=  R.drawable.inactivelogout;
//
//        }
//        return id;
//
//
//    }
}
