package com.dci.avicustomer.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dci.avicustomer.R;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.ModelRestaurant;

import java.util.ArrayList;
import java.util.List;

public class AdapterRestroTitle extends RecyclerView.Adapter<AdapterRestroTitle.DrawerViewHolder> {
    String type;
    OnItemClickListener onItemClickListener;
    String imagepath;
    int pos=-1;

    private List<ModelRestaurant.MenuCatg> home_data_list = new ArrayList<>();
    private Context mContext;

    public AdapterRestroTitle(Context mContext, List<ModelRestaurant.MenuCatg> home_data_list,int pos) {
        this.mContext = mContext;
        this.type = type;
        this.home_data_list = home_data_list;
        this.pos=pos;



        Log.d("onBindVieswHolder", "onBindViewHolder: " + home_data_list.size());
    }

    @Override
    public AdapterRestroTitle.DrawerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_restro_header_items, parent, false);
        return new AdapterRestroTitle.DrawerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterRestroTitle.DrawerViewHolder holder, final int position) {
        holder.tv_headername.setTextColor(mContext.getResources().getColor(R.color.view_line));
        holder.tv_selected_view.setVisibility(View.INVISIBLE);
        ModelRestaurant.MenuCatg datum = home_data_list.get(position);
        holder.tv_headername.setText(datum.getName());
        if (pos==-1&&position==0){
            holder.tv_headername.setTextColor(mContext.getResources().getColor(R.color.high_dark));
            holder.tv_selected_view.setVisibility(View.VISIBLE);

        }
        else if (pos==position){
            holder.tv_headername.setTextColor(mContext.getResources().getColor(R.color.high_dark));
            holder.tv_selected_view.setVisibility(View.VISIBLE);
        }
    }

    public void setImagePath(String path) {
        imagepath=path;
    }


    @Override
    public int getItemCount() {
        return home_data_list.size();
    }

    public void setonItemClick(OnItemClickListener listener) {
        this.onItemClickListener = listener;

    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    class DrawerViewHolder extends RecyclerView.ViewHolder {
        TextView tv_headername, tv_price, tv_describtion, tv_qty;
        ImageView imag_item, img_plus, img_minus;
        View tv_selected_view;


        public DrawerViewHolder(View itemView) {
            super(itemView);

            tv_selected_view= itemView.findViewById(R.id.tv_selected_view);

            tv_headername = itemView.findViewById(R.id.tv_headername);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pos=getAdapterPosition();
                    onItemClickListener.OnItemClickListener(getAdapterPosition(),v);
                }
            });

        }
    }
//    public int setActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.activehome;
//                break;
//            case 1:
//                id=  R.drawable.activeaccount;
//
//                break;
//            case 2:
//                id= R.drawable.activelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.activewallet;
//                break;
//            case 4:
//                id=  R.drawable.activeorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.activewishlish;
//                break;
//            case 6:
//                id=  R.drawable.activereceipe;
//                break;
//            case 7:
//                id=  R.drawable.activelogout;
//                break;
//            default:
//                id=  R.drawable.activelogout;
//
//        }
//        return id;
//
//
//    }
//    public int setInActiveIcon(int pos) {
//        int id;
//        switch (pos) {
//            case 0:
//                id= R.drawable.inactivehome;
//                break;
//            case 1:
//                id=  R.drawable.inactiveaccount;
//
//                break;
//            case 2:
//                id= R.drawable.inactivelocation;
//                break;
//
//            case 3:
//                id=  R.drawable.inactivewallet;
//                break;
//            case 4:
//                id=  R.drawable.inactiveorderstatus;
//                break;
//            case 5:
//                id=  R.drawable.inactivewishlish;
//                break;
//            case 6:
//                id=  R.drawable.inactivereceipe;
//                break;
//            case 7:
//                id=  R.drawable.inactivelogout;
//                break;
//            default:
//                id=  R.drawable.inactivelogout;
//
//        }
//        return id;
//
//
//    }
}

