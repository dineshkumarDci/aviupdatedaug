package com.dci.avicustomer.models;

import java.util.List;

public class ModelMyOrders {
    public Integer status;
    public String message;
    public List<Order> orders = null;
    public int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public class Order {

        public Integer subTotal;
        public String total;
        public String adminCommission;
        public String discount;
        public String restaurantName;
        public String userName;
        public String orderDate;
        public String orderStatus;
        public String orderID;
        public String currency;
        public Float rating;
        public Integer place;
        public String terminal;
        public String image;
        public Integer orderStatusId;
        public String cancelMsg;

        public String getCancelMsg() {
            return cancelMsg;
        }

        public void setCancelMsg(String cancelMsg) {
            this.cancelMsg = cancelMsg;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public void setRating(Float rating) {
            this.rating = rating;
        }

        public Integer getOrderStatusId() {
            return orderStatusId;
        }

        public void setOrderStatusId(Integer orderStatusId) {
            this.orderStatusId = orderStatusId;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public float getRating() {
            return rating;
        }

        public void setRating(float rating) {
            this.rating = rating;
        }

        public Integer getSubTotal() {
            return subTotal;
        }

        public void setSubTotal(Integer subTotal) {
            this.subTotal = subTotal;
        }

        public String getAdminCommission() {
            return adminCommission;
        }

        public void setAdminCommission(String adminCommission) {
            this.adminCommission = adminCommission;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public String getRestaurantName() {
            return restaurantName;
        }

        public void setRestaurantName(String restaurantName) {
            this.restaurantName = restaurantName;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getOrderDate() {
            return orderDate;
        }

        public void setOrderDate(String orderDate) {
            this.orderDate = orderDate;
        }

        public String getOrderStatus() {
            return orderStatus;
        }

        public void setOrderStatus(String orderStatus) {
            this.orderStatus = orderStatus;
        }

        public String getOrderID() {
            return orderID;
        }

        public void setOrderID(String orderID) {
            this.orderID = orderID;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }


        public Integer getPlace() {
            return place;
        }

        public void setPlace(Integer place) {
            this.place = place;
        }

        public String getTerminal() {
            return terminal;
        }

        public void setTerminal(String terminal) {
            this.terminal = terminal;
        }
    }
}
