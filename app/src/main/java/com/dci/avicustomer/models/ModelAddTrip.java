package com.dci.avicustomer.models;

public class ModelAddTrip {

    public Integer status;
    public String message;
   // public Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public class Data {

        public Integer id;
        public Integer user_id;
        public Integer airport_id;
        public String flight_code;
        public String dep_airport_code;
        public String arr_airport_code;
        public String dep_airport_city;
        public String arr_airport_city;
        public String dep_airport_country;
        public String arr_airport_country;
        public Integer dep_terminal;
        public Integer arr_terminal;
        public String dep_date;
        public String arr_date;
        public String dep_time;
        public String arr_time;
        public String flight_status;
        public Object boardingPass;
        public Object eTickets;
        public Integer status;
        public String created_at;
        public String updated_at;

    }
}
