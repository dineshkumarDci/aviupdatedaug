package com.dci.avicustomer.models;

import java.util.List;

public class ModelSearchAirline {


    public Integer status;
    public String message;
    public List<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        public Integer id;
        public String airline_name;
        public String airline_code;
        public String airline_display_name;
       // public Integer status;
       // public Integer created_by;
       // public String created_at;
        //public String updated_at;

        public String getAirline_name() {
            return airline_name;
        }

        public void setAirline_name(String airline_name) {
            this.airline_name = airline_name;
        }

        public String getAirline_code() {
            return airline_code;
        }

        public void setAirline_code(String airline_code) {
            this.airline_code = airline_code;
        }

        public String getAirline_display_name() {
            return airline_display_name;
        }

        public void setAirline_display_name(String airline_display_name) {
            this.airline_display_name = airline_display_name;
        }
    }
}
