package com.dci.avicustomer.models;

import java.io.Serializable;
import java.util.List;

public class ModelFacilities implements Serializable {

    public Integer status;
    public String message;
    public List<Datum> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public class Datum implements Serializable {

        public Integer id;
        public String name;
        public String images;
        public String phone;
        public String email;
        public String airport_details;
        public String url;
       // public Integer airportid;
     //   public Integer terminalid;
     //   public String images;
     //   public String phone;
     //   public String email;
//        public String loungeid;
//        public String landmark;
        public String conditions;
//        public Integer created_by;
//        public String created_at;
//        public String updated_at;
//        public Integer status;


        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getAirport_details() {
            return airport_details;
        }

        public void setAirport_details(String airport_details) {
            this.airport_details = airport_details;
        }

        public String getConditions() {
            return conditions;
        }

        public void setConditions(String conditions) {
            this.conditions = conditions;
        }

        public String getImages() {
            return images;
        }

        public void setImages(String images) {
            this.images = images;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public List<Loungefacility> loungefacility = null;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<Loungefacility> getLoungefacility() {
            return loungefacility;
        }

        public void setLoungefacility(List<Loungefacility> loungefacility) {
            this.loungefacility = loungefacility;
        }


    }
    public class Loungefacility implements Serializable {

        //            public Integer id;
//            public Integer loungeid;
//            public Integer facilityid;
//            public String created_at;
//            public String updated_at;
        public Facility facility;

        public Facility getFacility() {
            return facility;
        }

        public void setFacility(Facility facility) {
            this.facility = facility;
        }
    }
    public class Facility implements Serializable {

     //   public Integer id;
        public String name;
//        public String description;
//        public String created_at;
//        public String updated_at;
//        public Integer created_by;
//        public Integer status;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
