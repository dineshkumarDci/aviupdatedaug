package com.dci.avicustomer.models;

import com.dci.avicustomer.SectionedRecycleView.Section;

import java.util.List;

public class SectionedDataModel implements Section<ModelSearchResult.Airport> {

    private String headerTitle;
    private List<ModelSearchResult.Airport> allItemsInSection;

    public SectionedDataModel(List<ModelSearchResult.Airport> childList, String sectionText) {
        this.allItemsInSection = childList;
        this.headerTitle = sectionText;
    }

    public String getSectionText() {
        return headerTitle;
    }

    @Override
    public List<ModelSearchResult.Airport> getChildItems() {
        return allItemsInSection;
    }
}
