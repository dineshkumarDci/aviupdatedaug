package com.dci.avicustomer.models;

import com.microsoft.appcenter.ingestion.Ingestion;
import com.ybs.countrypicker.Country;

import java.util.List;

public class ModelHome {
    public Integer status;
    public String message;
    public Data data;
    public String jwt_token;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getJwt_token() {
        return jwt_token;
    }

    public void setJwt_token(String jwt_token) {
        this.jwt_token = jwt_token;
    }

    public class Data {

        public aAirport aAirport;
        public List<ATerminal> aTerminal = null;
        public String upTripTime;
        public String tripDateTimeType;
        public Integer user_count;

        public List<AviTip> aviTips = null;
        public List<UserTip> userTips = null;
        public List<ANews> aNews = null;
        public List<Offers>offers;
        public Imagepath imagePath;

        public OrderRating orderRating;
        public TripsRating tripsRating;
        public String timezone;

        public Integer getUser_count() {
            return user_count;
        }

        public void setUser_count(Integer user_count) {
            this.user_count = user_count;
        }

        public String getTripDateTimeType() {
            return tripDateTimeType;
        }

        public void setTripDateTimeType(String tripDateTimeType) {
            this.tripDateTimeType = tripDateTimeType;
        }

        public String getUpTripTime() {
            return upTripTime;
        }

        public void setUpTripTime(String upTripTime) {
            this.upTripTime = upTripTime;
        }

        public String getTimezone() {
            return timezone;
        }

        public void setTimezone(String timezone) {
            this.timezone = timezone;
        }

        public OrderRating getOrderRating() {
            return orderRating;
        }

        public void setOrderRating(OrderRating orderRating) {
            this.orderRating = orderRating;
        }

        public TripsRating getTripsRating() {
            return tripsRating;
        }

        public void setTripsRating(TripsRating tripsRating) {
            this.tripsRating = tripsRating;
        }

        public List<Offers> getOffers() {
            return offers;
        }

        public void setOffers(List<Offers> offers) {
            this.offers = offers;
        }

        public Imagepath getImagePath() {
            return imagePath;
        }

        public void setImagePath(Imagepath imagePath) {
            this.imagePath = imagePath;
        }

        public ModelHome.aAirport getaAirport() {
            return aAirport;
        }

        public void setaAirport(ModelHome.aAirport aAirport) {
            this.aAirport = aAirport;
        }

        public List<ATerminal> getaTerminal() {
            return aTerminal;
        }

        public void setaTerminal(List<ATerminal> aTerminal) {
            this.aTerminal = aTerminal;
        }



        public List<AviTip> getAviTips() {
            return aviTips;
        }

        public void setAviTips(List<AviTip> aviTips) {
            this.aviTips = aviTips;
        }

        public List<UserTip> getUserTips() {
            return userTips;
        }

        public void setUserTips(List<UserTip> userTips) {
            this.userTips = userTips;
        }

        public List<ANews> getaNews() {
            return aNews;
        }

        public void setaNews(List<ANews> aNews) {
            this.aNews = aNews;
        }
    }
    public class TripsRating {

        public Integer id;
        public Integer user_id;
        public Integer airport_id;
        public String flight_code;
       // public String dep_airport_code;
       // public String arr_airport_code;
        public String dep_airport_city;
        public String arr_airport_city;
        public String dep_airport_name;
        public String arr_airport_name;
        public String airline;
       // public String dep_airport_country;
       // public String arr_airport_country;
       // public String dep_terminal;
      //  public String arr_terminal;
//        public String dep_date_time;
//        public String arr_date_time;
//        public String dep_date;
//        public String arr_date;
//        public String dep_time;
//        public String arr_time;
//        public String flight_status;
       // public Object boardingPass;
        //public Object eTickets;


        public String getDep_airport_name() {
            return dep_airport_name;
        }

        public void setDep_airport_name(String dep_airport_name) {
            this.dep_airport_name = dep_airport_name;
        }

        public String getArr_airport_name() {
            return arr_airport_name;
        }

        public void setArr_airport_name(String arr_airport_name) {
            this.arr_airport_name = arr_airport_name;
        }

        public Integer status;
        public Integer review;

        public String getAirline() {
            return airline;
        }

        public void setAirline(String airline) {
            this.airline = airline;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getUser_id() {
            return user_id;
        }

        public void setUser_id(Integer user_id) {
            this.user_id = user_id;
        }

        public Integer getAirport_id() {
            return airport_id;
        }

        public void setAirport_id(Integer airport_id) {
            this.airport_id = airport_id;
        }

        public String getFlight_code() {
            return flight_code;
        }

        public void setFlight_code(String flight_code) {
            this.flight_code = flight_code;
        }

        public String getDep_airport_city() {
            return dep_airport_city;
        }

        public void setDep_airport_city(String dep_airport_city) {
            this.dep_airport_city = dep_airport_city;
        }

        public String getArr_airport_city() {
            return arr_airport_city;
        }

        public void setArr_airport_city(String arr_airport_city) {
            this.arr_airport_city = arr_airport_city;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public Integer getReview() {
            return review;
        }

        public void setReview(Integer review) {
            this.review = review;
        }
        //public String created_at;
        //public String updated_at;



    }
    public class OrderRating {

      //  public Integer subTotal;
//        public String adminCommission;
//        public String discount;
        public String restaurantName;
//        public String userName;
//        public String orderDate;
//        public String orderStatus;
        public String orderID;
        public String currency;
        public int restaurantID;

      //  public Object rating;
       // public Integer place;
       // public String terminal;
        public String image;

        public String getRestaurantName() {
            return restaurantName;
        }

        public void setRestaurantName(String restaurantName) {
            this.restaurantName = restaurantName;
        }

        public int getRestaurant_id() {
            return restaurantID;
        }

        public void setRestaurant_id(int restaurant_id) {
            this.restaurantID = restaurant_id;
        }

        public String getOrderID() {
            return orderID;
        }

        public void setOrderID(String orderID) {
            this.orderID = orderID;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
        // public Integer orderStatusId;

    }



    public class Imagepath{
        String restaurant;
        String coupons;

        public String getRestaurant() {
            return restaurant;
        }

        public void setRestaurant(String restaurant) {
            this.restaurant = restaurant;
        }

        public String getCoupons() {
            return coupons;
        }

        public void setCoupons(String coupons) {
            this.coupons = coupons;
        }
    }
    public class Offers{
        String name;
        Integer offer_type_id;

        String image;
        String percentage;
        String offerTitle;
        int id;
        String flat_rate;

        public String getFlat_rate() {
            return flat_rate;
        }

        public void setFlat_rate(String flat_rate) {
            this.flat_rate = flat_rate;
        }

        public Integer getOffer_type_id() {
            return offer_type_id;
        }

        public void setOffer_type_id(Integer offer_type_id) {
            this.offer_type_id = offer_type_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getPercentage() {
            return percentage;
        }

        public void setPercentage(String percentage) {
            this.percentage = percentage;
        }

        public String getOfferTitle() {
            return offerTitle;
        }

        public void setOfferTitle(String offerTitle) {
            this.offerTitle = offerTitle;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
    public class UserTip {

        public Integer id;
      //  public Integer airport_id;
        public String description;
        public String usersFname;
      //  public Integer status;
      //  public Integer created_by;
       // public String created_at;
        public String updated_at;
        public Users users;

        public String getUsersFname() {
            return usersFname;
        }

        public void setUsersFname(String usersFname) {
            this.usersFname = usersFname;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public class Users{

//            public Integer id;
//            public String first_name;
//            public Object last_name;
//            public String phone;
//            public Object mobile;
//            public Object gender;
//            public String dob;
//            public String email;
//            public Object profile_img;
//            public Object personal_notes;
//            public Integer user_type_id;
//            public String password;
//            public String user_type;
//            public String remember_token;
//            public Integer status;
//            public Integer is_admin;
//            public Integer login_type;
//            public String fb_id;
//            public String twitter_id;
//            public String gmail_id;
//            public Integer created_by;
//            public String created_at;
//            public String updated_at;
//            public String forgetpassword_at;

        }

    }
    public class aAirport {

        public Integer id;
        public String name;
        public String code;
     //   public String phone;
      //  public String email;
        public String address;
//        public String transport_id;
//        public Integer country_id;
//        public Integer state_id;
        public Integer city_id;
        public String city_name;
        public String latitude;
        public String longitude;
        public city city;
        public country country;

        public country getCountry() {
            return country;
        }

        public void setCountry(country country) {
            this.country = country;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public aAirport.city getCity() {
            return city;
        }

        public void setCity(aAirport.city city) {
            this.city = city;
        }
        //        public Integer status;
//        public Integer created_by;
//        public String created_at;
//        public String updated_at;


        public Integer getCity_id() {
            return city_id;
        }

        public void setCity_id(Integer city_id) {
            this.city_id = city_id;
        }

        public String getCity_name() {
            return city_name;
        }

        public void setCity_name(String city_name) {
            this.city_name = city_name;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public class city{
            String name;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
        public class country{
            String name;
            int id;
            String sortname;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getSortname() {
                return sortname;
            }

            public void setSortname(String sortname) {
                this.sortname = sortname;
            }
        }
    }
    public class ANews {

        public Integer id;
        public String title;
        public String description;
        public String image;
//        public Integer status;
//        public Integer created_by;
//        public String created_at;
//        public String updated_at;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }
//    public class ARestrant {
//
//          public Integer id;
////        public Integer airport_id;
////        public Integer airport_terminal_id;
////        public Integer service_type_id;
////        public String shop_no;
//        public String name;
////        public String phone;
//        public String shop_image;
//        public String email;
//      //  public String address;
//      //  public String acc_no;
//       // public String ifsc_code;
//       // public String landmark;
//       // public String commission;
//       // public String description;
//       // public String created_at;
//       // public String updated_at;
//       // public Integer status;
//       // public Integer created_by;
//
//
//        public Integer getId() {
//            return id;
//        }
//
//        public void setId(Integer id) {
//            this.id = id;
//        }
//
//        public List<coupons> offer;
//
//        public List<coupons> getCoupons() {
//            return offer;
//        }
//
//        public void setCoupons(List<coupons> coupons) {
//            this.offer = coupons;
//        }
//
//       public class coupons{
//            public String discount_percentage;
//           public String title;
//           public String image;
//
//           public String getImage() {
//               return image;
//           }
//
//           public void setImage(String image) {
//               this.image = image;
//           }
//
//           public String getTitle() {
//               return title;
//           }
//
//           public void setTitle(String title) {
//               this.title = title;
//           }
//
//           public String getDiscount_percentage() {
//                return discount_percentage;
//            }
//
//            public void setDiscount_percentage(String discount_percentage) {
//                this.discount_percentage = discount_percentage;
//            }
//        }
//
//        public String getName() {
//            return name;
//        }
//
//        public void setName(String name) {
//            this.name = name;
//        }
//
//        public String getShop_image() {
//            return shop_image;
//        }
//
//        public void setShop_image(String shop_image) {
//            this.shop_image = shop_image;
//        }
//    }
    public class ATerminal {

//        public Integer id;
//        public Integer airport_id;
        public Integer terminal_id;
//        public String created_at;
//        public String updated_at;
//        public Integer status;
        public String name;
//        public Object description;
//        public Integer created_by;


        public Integer getTerminal_id() {
            return terminal_id;
        }

        public void setTerminal_id(Integer terminal_id) {
            this.terminal_id = terminal_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }
    public class AviTip {

       // public Integer id;
        public Integer airport_id;
        public String description;
//        public Integer status;
//        public Integer created_by;
//        public String created_at;
       public String updated_at;
//        public Users users;


        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

//        public class Users {
//
//            public Integer id;
//            public String first_name;
//            public String last_name;
//            public String phone;
//            public String mobile;
//            public String gender;
//            public String dob;
//            public String email;
//            public String profile_img;
//            public String personal_notes;
//            public Integer user_type_id;
//            public String password;
//            public String user_type;
//            public String remember_token;
//            public Integer status;
//            public Integer is_admin;
//            public Integer login_type;
//            public String fb_id;
//            public String twitter_id;
//            public String gmail_id;
//            public Integer created_by;
//            public String created_at;
//            public String updated_at;
//            public String forgetpassword_at;
//
//        }

    }

}
