package com.dci.avicustomer.models;

import java.util.List;

public class ModelSearchResult {
    public Integer status;
    public String message;
    public Data data;
    public String jwt_token;

    public String getJwt_token() {
        return jwt_token;
    }

    public void setJwt_token(String jwt_token) {
        this.jwt_token = jwt_token;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        public List<Airport> favAirports = null;
        public List<Airport> airports = null;

        public List<Airport> getFavAirports() {
            return favAirports;
        }

        public void setFavAirports(List<Airport> favAirports) {
            this.favAirports = favAirports;
        }

        public List<Airport> getAirports() {
            return airports;
        }

        public void setAirports(List<Airport> airports) {
            this.airports = airports;
        }
    }
    public class Airport {

        public Integer id;
        public String name;
        public String code;
        public String phone;
        public String city_name;
        public String email;
        public String address;
        public String transport_id;
        public Integer country_id;
        public Integer state_id;
        public Integer city_id;
        public String latitude;
        public String longitude;
        public Integer status;
        public Integer created_by;
        public String created_at;
        public String updated_at;

        public String getCity_name() {
            return city_name;
        }

        public void setCity_name(String city_name) {
            this.city_name = city_name;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getTransport_id() {
            return transport_id;
        }

        public void setTransport_id(String transport_id) {
            this.transport_id = transport_id;
        }

        public Integer getCountry_id() {
            return country_id;
        }

        public void setCountry_id(Integer country_id) {
            this.country_id = country_id;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }
    }
}
