package com.dci.avicustomer.models;

import java.io.Serializable;
import java.util.List;

public class ModelRestaurant {
    public Integer status;
    public String message;
    public Data data;
    public Images imagePath;
    public int totalCart;

    public int getTotalCart() {
        return totalCart;
    }

    public void setTotalCart(int totalCart) {
        this.totalCart = totalCart;
    }

    public Images getImagePath() {
        return imagePath;
    }

    public void setImagePath(Images imagePath) {
        this.imagePath = imagePath;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Images {
        public String restaurant;
        public String menu;

        public String getRestaurant() {
            return restaurant;
        }

        public void setRestaurant(String restaurant) {
            this.restaurant = restaurant;
        }

        public String getMenu() {
            return menu;
        }

        public void setMenu(String menu) {
            this.menu = menu;
        }
    }

    public class Data {

        public List<MenuCatg> menuCatg = null;
        public Restaurent restaurent;

        public List<MenuCatg> getMenuCatg() {
            return menuCatg;
        }

        public void setMenuCatg(List<MenuCatg> menuCatg) {
            this.menuCatg = menuCatg;
        }

        public Restaurent getRestaurent() {
            return restaurent;
        }

        public void setRestaurent(Restaurent restaurent) {
            this.restaurent = restaurent;
        }
    }

    public class MenuCatg {

        public Integer id;
        public String name;
        //  public String description;
        public List<Restaurantmenu> restaurantmenu = null;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<Restaurantmenu> getRestaurantmenu() {
            return restaurantmenu;
        }

        public void setRestaurantmenu(List<Restaurantmenu> restaurantmenu) {
            this.restaurantmenu = restaurantmenu;
        }
    }

    public class Restaurantmenu {

        public Integer id;
        //  public Integer airport_id;
        //   public Integer airport_terminal_id;
        public String name;
        public String description;
        public String price;
        //   public Integer restaurant_id;
        //   public Integer currency_id;
        //   public Integer food_type_id;
        //   public Integer restaurant_menu_category_id;
        public String images;
        public Integer quantity;
        public String currencyCode;



        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getCurrencyCode() {
            return currencyCode;
        }

        public void setCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getImages() {
            return images;
        }

        public void setImages(String images) {
            this.images = images;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }
    }

    public class Restaurent {

        public Integer id;
        public Integer user_id;
        public Integer airport_id;
        //  public Integer airport_terminal_id;
        //  public Integer service_type_id;
        //  public Integer restauranttype_id;
        // public String shop_no;
        public String name;
        public Integer openClose;
        public String phone;
        public String shop_image;
        public String email;
        public String address;
        public String average_rating;
        public Integer rating_count;
        public String offer_code;
        public Terminal terminal;
        public String ETA;
        public Integer area_type;
        public String latitude;
        public String longitude;
        public restaurant_type restaurant_type;

        public restaurant_type getRestaurant_type() {
            return restaurant_type;
        }

        public void setRestaurant_type(restaurant_type restaurant_type) {
            this.restaurant_type = restaurant_type;
        }

        public class restaurant_type{
            String name;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getETA() {
            return ETA;
        }

        public void setETA(String ETA) {
            this.ETA = ETA;
        }

        public Integer getArea_type() {
            return area_type;
        }

        public void setArea_type(Integer area_type) {
            this.area_type = area_type;
        }

        public List<Retail_time>restaurant_times;

        public List<Retail_time> getRestaurant_times() {
            return restaurant_times;
        }

        public void setRestaurant_times(List<Retail_time> restaurant_times) {
            this.restaurant_times = restaurant_times;
        }

        public class Retail_time {

            //  public Integer id;
            public Integer shop_id;
            public String day;
            public String from_time;
            public String to_time;
            public String from_am_pm;
            public String to_am_pm;
            public String open_24_hrs;
            public String open_days;

            public String getFrom_am_pm() {
                return from_am_pm;
            }

            public void setFrom_am_pm(String from_am_pm) {
                this.from_am_pm = from_am_pm;
            }

            public String getTo_am_pm() {
                return to_am_pm;
            }

            public void setTo_am_pm(String to_am_pm) {
                this.to_am_pm = to_am_pm;
            }

            public String getOpen_24_hrs() {
                return open_24_hrs;
            }

            public void setOpen_24_hrs(String open_24_hrs) {
                this.open_24_hrs = open_24_hrs;
            }

            public String getOpen_days() {
                return open_days;
            }

            public void setOpen_days(String open_days) {
                this.open_days = open_days;
            }
            // public String created_at;
            //  public String updated_at;

            public Integer getShop_id() {
                return shop_id;
            }

            public void setShop_id(Integer shop_id) {
                this.shop_id = shop_id;
            }

            public String getDay() {
                return day;
            }

            public void setDay(String day) {
                this.day = day;
            }

            public String getFrom_time() {
                return from_time;
            }

            public void setFrom_time(String from_time) {
                this.from_time = from_time;
            }

            public String getTo_time() {
                return to_time;
            }

            public void setTo_time(String to_time) {
                this.to_time = to_time;
            }
        }

        public Integer getOpenClose() {
            return openClose;
        }

        public void setOpenClose(Integer openClose) {
            this.openClose = openClose;
        }


        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public Terminal getTerminal() {
            return terminal;
        }

        public void setTerminal(Terminal terminal) {
            this.terminal = terminal;
        }
        //  public List<Object> restaurant_times = null;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getShop_image() {
            return shop_image;
        }

        public void setShop_image(String shop_image) {
            this.shop_image = shop_image;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getAverage_rating() {
            return average_rating;
        }

        public void setAverage_rating(String average_rating) {
            this.average_rating = average_rating;
        }

        public Integer getRating_count() {
            return rating_count;
        }

        public void setRating_count(Integer rating_count) {
            this.rating_count = rating_count;
        }

        public String getOffer_code() {
            return offer_code;
        }

        public void setOffer_code(String offer_code) {
            this.offer_code = offer_code;
        }
    }

    public class Terminal {

        public Integer id;
        public String name;
        public String description;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }

}

