package com.dci.avicustomer.models;

public class ModelSocialLogin {
    public Integer status;
    public String message;
    public Data data;
    public String jwt_token;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getJwt_token() {
        return jwt_token;
    }

    public void setJwt_token(String jwt_token) {
        this.jwt_token = jwt_token;
    }

    public class Data {

        public Integer id;
        public String first_name;
        public String last_name;
        public String phone;
        public String mobile;
        public String gender;
        public String dob;
        public String email;
        public String profile_img;
       // public String personal_notes;
        public Integer user_type_id;
        public String user_type;
       // public Integer status;
       // public Integer is_admin;
        public Integer login_type;
        public String fb_id;
        public String twitter_id;
        public String gmail_id;
        public Integer created_by;
      //  public String created_at;
     //   public String updated_at;
        public String forgetpassword_at;
        public Integer airport_id;
        public Integer terminal_id;
        public Integer airport_type;


        public Integer getAirport_id() {
            return airport_id;
        }

        public void setAirport_id(Integer airport_id) {
            this.airport_id = airport_id;
        }

        public Integer getTerminal_id() {
            return terminal_id;
        }

        public void setTerminal_id(Integer terminal_id) {
            this.terminal_id = terminal_id;
        }

        public Integer getAirport_type() {
            return airport_type;
        }

        public void setAirport_type(Integer airport_type) {
            this.airport_type = airport_type;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getProfile_img() {
            return profile_img;
        }

        public void setProfile_img(String profile_img) {
            this.profile_img = profile_img;
        }

        public String getFb_id() {
            return fb_id;
        }

        public void setFb_id(String fb_id) {
            this.fb_id = fb_id;
        }

        public String getTwitter_id() {
            return twitter_id;
        }

        public void setTwitter_id(String twitter_id) {
            this.twitter_id = twitter_id;
        }

        public String getGmail_id() {
            return gmail_id;
        }

        public void setGmail_id(String gmail_id) {
            this.gmail_id = gmail_id;
        }

        public String getForgetpassword_at() {
            return forgetpassword_at;
        }

        public void setForgetpassword_at(String forgetpassword_at) {
            this.forgetpassword_at = forgetpassword_at;
        }
    }
}
