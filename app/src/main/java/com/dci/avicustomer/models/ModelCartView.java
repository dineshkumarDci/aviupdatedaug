package com.dci.avicustomer.models;

import java.util.List;

public class ModelCartView {
    public Integer status;
    public Integer serviceTypeID;
    public String message;
    public List<Cart> cart = null;
    public imagePath imagePath;
    public Calculation calculation;
    public String eta;

    public String getEta() {
        return eta;
    }

    public void setEta(String eta) {
        this.eta = eta;
    }

    public Calculation getCalculation() {
        return calculation;
    }
    public void setCalculation(Calculation calculation) {
        this.calculation = calculation;
    }

    public Integer getServiceTypeID() {
        return serviceTypeID;
    }

    public void setServiceTypeID(Integer serviceTypeID) {
        this.serviceTypeID = serviceTypeID;
    }

    public class Calculation {
        String currencyCode;
        String menuTotal;
        String adminCommission;
        String coupon;
        String subtotal;
        String convenience_fee;
        String handling_fee;
        String delivery_fee;
        String gst_perct;
        String gst_amt;

        public String getConvenience_fee() {
            return convenience_fee;
        }

        public void setConvenience_fee(String convenience_fee) {
            this.convenience_fee = convenience_fee;
        }

        public String getHandling_fee() {
            return handling_fee;
        }

        public void setHandling_fee(String handling_fee) {
            this.handling_fee = handling_fee;
        }

        public String getDelivery_fee() {
            return delivery_fee;
        }

        public void setDelivery_fee(String delivery_fee) {
            this.delivery_fee = delivery_fee;
        }

        public String getGst_perct() {
            return gst_perct;
        }

        public void setGst_perct(String gst_perct) {
            this.gst_perct = gst_perct;
        }

        public String getGst_amt() {
            return gst_amt;
        }

        public void setGst_amt(String gst_amt) {
            this.gst_amt = gst_amt;
        }

        public String getCurrencyCode() {
            return currencyCode;
        }

        public void setCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
        }

        public String getMenuTotal() {
            return menuTotal;
        }

        public void setMenuTotal(String menuTotal) {
            this.menuTotal = menuTotal;
        }

        public String getAdminCommission() {
            return adminCommission;
        }

        public void setAdminCommission(String adminCommission) {
            this.adminCommission = adminCommission;
        }

        public String getCoupon() {
            return coupon;
        }

        public void setCoupon(String coupon) {
            this.coupon = coupon;
        }

        public String getSubtotal() {
            return subtotal;
        }

        public void setSubtotal(String subtotal) {
            this.subtotal = subtotal;
        }
    }

    public ModelCartView.imagePath getImagePath() {
        return imagePath;
    }

    public void setImagePath(ModelCartView.imagePath imagePath) {
        this.imagePath = imagePath;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Cart> getCart() {
        return cart;
    }

    public void setCart(List<Cart> cart) {
        this.cart = cart;
    }

    public class imagePath {
        String menu;

        public String getMenu() {
            return menu;
        }

        public void setMenu(String menu) {
            this.menu = menu;
        }
    }

    public class Cart {
        public Integer id;
        public Integer menu;
        //  public Integer user_id;
        //  public Integer airport_id;
        //  public Integer terminal_id;
        public Integer quantity;
        // public Integer status;
        // public String created_at;
        // public String updated_at;
        public Restaurantmenucategory restaurantmenucategory;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

        public Restaurantmenucategory getRestaurantmenucategory() {
            return restaurantmenucategory;
        }

        public void setRestaurantmenucategory(Restaurantmenucategory restaurantmenucategory) {
            this.restaurantmenucategory = restaurantmenucategory;
        }

        public class Restaurantmenucategory {
            public Integer id;
            public Integer airport_id;
            //  public Integer airport_terminal_id;
            public String name;
            public String description;
            public String price;
            public Integer restaurant_id;
            //  public Integer currency_id;
            //  public Integer food_type_id;
            public Integer restaurant_menu_category_id;
            public String images;
            // public String created_at;
            // public String updated_at;
            public Integer status;
            //  public Integer created_by;
           // public Restaurant restaurant;
            public Currency currency;

            public Integer getRestaurant_id() {
                return restaurant_id;
            }

            public void setRestaurant_id(Integer restaurant_id) {
                this.restaurant_id = restaurant_id;
            }

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getImages() {
                return images;
            }

            public void setImages(String images) {
                this.images = images;
            }

            public Currency getCurrency() {
                return currency;
            }

            public void setCurrency(Currency currency) {
                this.currency = currency;
            }

            public class Currency {
                public Integer id;
                public String name;
                public String code;
                public String symbol;
                public Integer status;

                public Integer getId() {
                    return id;
                }

                public void setId(Integer id) {
                    this.id = id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getSymbol() {
                    return symbol;
                }

                public void setSymbol(String symbol) {
                    this.symbol = symbol;
                }

                public String getCode() {
                    return code;
                }

                public void setCode(String code) {
                    this.code = code;
                }
                //   public Object created_at;
                //    public Object updated_at;
            }
        }

    }
//    public class Restaurant{
//        public String ETA;
//
//        public String getETA() {
//            return ETA;
//        }
//
//        public void setETA(String ETA) {
//            this.ETA = ETA;
//        }
//    }
}
