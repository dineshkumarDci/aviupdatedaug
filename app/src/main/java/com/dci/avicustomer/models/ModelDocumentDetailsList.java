package com.dci.avicustomer.models;

import android.content.Intent;

import java.util.List;

public class ModelDocumentDetailsList {
    public Integer status;
    public String message;
    public Data data;
    public String jwt_token;
    public Count count;

    public Count getCount() {
        return count;
    }

    public void setCount(Count count) {
        this.count = count;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getJwt_token() {
        return jwt_token;
    }

    public void setJwt_token(String jwt_token) {
        this.jwt_token = jwt_token;
    }
    public class Count{
       Integer boardPass;
        Integer presDoce;
        Integer visaDoce;
        Integer eticket;

        public Integer getEticket() {
            return eticket;
        }

        public void setEticket(Integer eticket) {
            this.eticket = eticket;
        }

        public Integer getBoardPass() {
            return boardPass;
        }

        public void setBoardPass(Integer boardPass) {
            this.boardPass = boardPass;
        }

        public Integer getPresDoce() {
            return presDoce;
        }

        public void setPresDoce(Integer presDoce) {
            this.presDoce = presDoce;
        }

        public Integer getVisaDoce() {
            return visaDoce;
        }

        public void setVisaDoce(Integer visaDoce) {
            this.visaDoce = visaDoce;
        }
    }

    public class Data {

        public List<Document> document = null;
        public String imagePath;

        public List<Document> getDocument() {
            return document;
        }

        public void setDocument(List<Document> document) {
            this.document = document;
        }

        public String getImagePath() {
            return imagePath;
        }

        public void setImagePath(String imagePath) {
            this.imagePath = imagePath;
        }
    }

    public class Document {

        public Integer id;
        public Integer user_id;
        public Integer type;
        public String image;
       // public Integer status;
        public String updated_at;
       // public String created_at;


        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getUser_id() {
            return user_id;
        }

        public void setUser_id(Integer user_id) {
            this.user_id = user_id;
        }

        public Integer getType() {
            return type;
        }

        public void setType(Integer type) {
            this.type = type;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }
}
