package com.dci.avicustomer.models;

import java.util.List;

public class ModelFlightSchedule {
    public Integer status;
    public String message;
    public List<Datum> data = null;
    public Integer totalcount;

    public Integer getTotalcount() {
        return totalcount;
    }

    public void setTotalcount(int totalcount) {
        this.totalcount = totalcount;
    }
    //public List<String> flight = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        public String FlightNumber;
        public String AirlineName;
        public String FlightStatus;
        public String AirlineCode;
        public String schedule_date;
        public String depature_airportcode;
        public String depature_airportname;
        public String depature_city;
        public String depature_countryid;
        public String depature_terminal;
        public String depature_date;
        public String depature_time;
        public String arrival_airportcode;
        public String arrival_airportname;
        public String arrival_city;
        public String arrival_countryid;
        public String arrival_terminal;
        public String arrival_date;
        public String arrival_time;

        public String depGate;
        public String arrGate;

        public String getDepGate() {
            return depGate;
        }

        public void setDepGate(String depGate) {
            this.depGate = depGate;
        }

        public String getArrGate() {
            return arrGate;
        }

        public void setArrGate(String arrGate) {
            this.arrGate = arrGate;
        }

        public String getFlightStatus() {
            return FlightStatus;
        }

        public void setFlightStatus(String flightStatus) {
            FlightStatus = flightStatus;
        }

        public String getFlightNumber() {
            return FlightNumber;
        }

        public void setFlightNumber(String flightNumber) {
            FlightNumber = flightNumber;
        }

        public String getAirlineName() {
            return AirlineName;
        }

        public void setAirlineName(String airlineName) {
            AirlineName = airlineName;
        }

        public String getAirlineCode() {
            return AirlineCode;
        }

        public void setAirlineCode(String airlineCode) {
            AirlineCode = airlineCode;
        }

        public String getSchedule_date() {
            return schedule_date;
        }

        public void setSchedule_date(String schedule_date) {
            this.schedule_date = schedule_date;
        }

        public String getDepature_airportcode() {
            return depature_airportcode;
        }

        public void setDepature_airportcode(String depature_airportcode) {
            this.depature_airportcode = depature_airportcode;
        }

        public String getDepature_airportname() {
            return depature_airportname;
        }

        public void setDepature_airportname(String depature_airportname) {
            this.depature_airportname = depature_airportname;
        }

        public String getDepature_city() {
            return depature_city;
        }

        public void setDepature_city(String depature_city) {
            this.depature_city = depature_city;
        }

        public String getDepature_countryid() {
            return depature_countryid;
        }

        public void setDepature_countryid(String depature_countryid) {
            this.depature_countryid = depature_countryid;
        }

        public String getDepature_terminal() {
            return depature_terminal;
        }

        public void setDepature_terminal(String depature_terminal) {
            this.depature_terminal = depature_terminal;
        }

        public String getDepature_date() {
            return depature_date;
        }

        public void setDepature_date(String depature_date) {
            this.depature_date = depature_date;
        }

        public String getDepature_time() {
            return depature_time;
        }

        public void setDepature_time(String depature_time) {
            this.depature_time = depature_time;
        }

        public String getArrival_airportcode() {
            return arrival_airportcode;
        }

        public void setArrival_airportcode(String arrival_airportcode) {
            this.arrival_airportcode = arrival_airportcode;
        }

        public String getArrival_airportname() {
            return arrival_airportname;
        }

        public void setArrival_airportname(String arrival_airportname) {
            this.arrival_airportname = arrival_airportname;
        }

        public String getArrival_city() {
            return arrival_city;
        }

        public void setArrival_city(String arrival_city) {
            this.arrival_city = arrival_city;
        }

        public String getArrival_countryid() {
            return arrival_countryid;
        }

        public void setArrival_countryid(String arrival_countryid) {
            this.arrival_countryid = arrival_countryid;
        }

        public String getArrival_terminal() {
            return arrival_terminal;
        }

        public void setArrival_terminal(String arrival_terminal) {
            this.arrival_terminal = arrival_terminal;
        }

        public String getArrival_date() {
            return arrival_date;
        }

        public void setArrival_date(String arrival_date) {
            this.arrival_date = arrival_date;
        }

        public String getArrival_time() {
            return arrival_time;
        }

        public void setArrival_time(String arrival_time) {
            this.arrival_time = arrival_time;
        }
    }
}
