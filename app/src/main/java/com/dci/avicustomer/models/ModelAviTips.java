package com.dci.avicustomer.models;

import java.util.List;

public class ModelAviTips {

    public Integer status;
    public String message;
    public List<Datum> data = null;
    public String jwt_token;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getJwt_token() {
        return jwt_token;
    }

    public void setJwt_token(String jwt_token) {
        this.jwt_token = jwt_token;
    }

    public class Datum {

        public Integer id;
        public Integer airport_id;
        public String description;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getAirport_id() {
            return airport_id;
        }

        public void setAirport_id(Integer airport_id) {
            this.airport_id = airport_id;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
        //        public Integer status;
//        public Integer created_by;
//        public String created_at;
//        public String updated_at;
//        public Users users;

    }
//    public class Users {
//
//        public Integer id;
//        public String first_name;
//        public String last_name;
//        public String phone;
//        public String mobile;
//        public String gender;
//        public String dob;
//        public String email;
//        public String profile_img;
//        public String personal_notes;
//        public Integer user_type_id;
//        public String password;
//        public String user_type;
//        public String remember_token;
//        public Integer status;
//        public Integer is_admin;
//        public Integer login_type;
//        public String fb_id;
//        public String twitter_id;
//        public String gmail_id;
//        public Integer created_by;
//        public String created_at;
//        public String updated_at;
//        public String forgetpassword_at;
//
//    }
}
