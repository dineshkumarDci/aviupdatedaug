package com.dci.avicustomer.models;

import android.content.Intent;

import java.io.Serializable;
import java.util.List;

public class ModelOfferList implements Serializable {

    public Integer status;
    public String message;
    public Offer offer;
    public List<TopOffer> topOffer = null;
    public ImagePath imagePath;


    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Offer getOffer() {
        return offer;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }

    public List<TopOffer> getTopOffer() {
        return topOffer;
    }

    public void setTopOffer(List<TopOffer> topOffer) {
        this.topOffer = topOffer;
    }

    public ImagePath getImagePath() {
        return imagePath;
    }

    public void setImagePath(ImagePath imagePath) {
        this.imagePath = imagePath;
    }

    public class ImagePath implements Serializable {
        public String restaurant;
        public String coupons;
        public String shopimage;

        public String getShopimage() {
            return shopimage;
        }

        public void setShopimage(String shopimage) {
            this.shopimage = shopimage;
        }

        public String getRestaurant() {
            return restaurant;
        }

        public void setRestaurant(String restaurant) {
            this.restaurant = restaurant;
        }

        public String getCoupons() {
            return coupons;
        }

        public void setCoupons(String coupons) {
            this.coupons = coupons;
        }
    }
    public class TopOffer implements Serializable {

       // public Integer id;
       // public Integer offer_type_id;
       // public Object offer_type;
       public Integer offer_type_id;
        public String flat_rate;
        public String offer_code;
        public String min_purchase_value;
        public String discount_percentage;
        public String max_discount;
        //public String offer_start_date;
        //public String offer_end_date;
        //public String conditions;
        public String description;
       // public Integer service_types;
       // public Integer notify_to_app;
        public String title;
       // public String flat_rate;
        public String image;
       // public Integer service_id;
       // public Integer type_id;
//        public String created_at;
//        public String updated_at;
//        public Integer status;
//        public Integer created_by;
        public Restaurants restaurants;
        public ModelShopList.Datum retail__shop;

        public Integer getOffer_type_id() {
            return offer_type_id;
        }

        public void setOffer_type_id(Integer offer_type_id) {
            this.offer_type_id = offer_type_id;
        }

        public String getFlat_rate() {
            return flat_rate;
        }

        public void setFlat_rate(String flat_rate) {
            this.flat_rate = flat_rate;
        }

        public String getOffer_code() {
            return offer_code;
        }

        public void setOffer_code(String offer_code) {
            this.offer_code = offer_code;
        }

        public String getMin_purchase_value() {
            return min_purchase_value;
        }

        public void setMin_purchase_value(String min_purchase_value) {
            this.min_purchase_value = min_purchase_value;
        }

        public String getDiscount_percentage() {
            return discount_percentage;
        }

        public void setDiscount_percentage(String discount_percentage) {
            this.discount_percentage = discount_percentage;
        }

        public String getMax_discount() {
            return max_discount;
        }

        public void setMax_discount(String max_discount) {
            this.max_discount = max_discount;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public Restaurants getRestaurants() {
            return restaurants;
        }

        public void setRestaurants(Restaurants restaurants) {
            this.restaurants = restaurants;
        }

        public ModelShopList.Datum getRetail__shop() {
            return retail__shop;
        }

        public void setRetail__shop(ModelShopList.Datum retail__shop) {
            this.retail__shop = retail__shop;
        }

        public class Restaurants implements Serializable {

            public Integer id;
          //  public Integer user_id;
          //  public Integer airport_id;
          //  public Integer airport_terminal_id;
         //   public Integer service_type_id;
         //   public Integer restauranttype_id;
          //  public String shop_no;
            public String name;
          //  public String phone;
            public String shop_image;
          //  public String email;
          //  public String address;
          //  public String acc_no;
          //  public String ifsc_code;
          //  public String landmark;
          //  public String commission;
            public String description;
          //  public Object airport_type;
         //   public Object area_type;
         //   public String created_at;
         //   public String updated_at;
            public Integer status;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getShop_image() {
                return shop_image;
            }

            public void setShop_image(String shop_image) {
                this.shop_image = shop_image;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public Integer getStatus() {
                return status;
            }

            public void setStatus(Integer status) {
                this.status = status;
            }
            //   public Integer created_by;

        }

    }
    public class Offer implements Serializable {

        public Integer current_page;
        public List<Datum> data = null;
      //  public String first_page_url;
       // public Integer from;
        public Integer last_page;
      //  public String last_page_url;
       // public Object next_page_url;
        public String path;
       // public Integer per_page;
      //  public Object prev_page_url;
        public Integer to;
        public Integer total;

        public Integer getTotal() {
            return total;
        }

        public void setTotal(Integer total) {
            this.total = total;
        }

        public List<Datum> getData() {
            return data;
        }

        public void setData(List<Datum> data) {
            this.data = data;
        }
    }
    public class Datum implements Serializable {

        public Integer id;
        public Integer offer_type_id;
        public String flat_rate;
      //  public Object offer_type;
       // public String offer_code;
     //   public String min_purchase_value;
        public String discount_percentage;
      //  public String max_discount;
      //  public String offer_start_date;
      //  public String offer_end_date;
      //  public String conditions;
        public String description;
       // public Integer service_types;
       // public Integer notify_to_app;
        public String title;

        public String image;
       // public Integer service_id;
       // public Integer type_id;
     //   public String created_at;
     //   public String updated_at;
       // public Integer status;
       // public Integer created_by;
        public Restaurants restaurants;
        public ModelShopList.Datum retail__shop;

        public Integer getOffer_type_id() {
            return offer_type_id;
        }

        public void setOffer_type_id(Integer offer_type_id) {
            this.offer_type_id = offer_type_id;
        }

        public String getFlat_rate() {
            return flat_rate;
        }

        public void setFlat_rate(String flat_rate) {
            this.flat_rate = flat_rate;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getDiscount_percentage() {
            return discount_percentage;
        }

        public void setDiscount_percentage(String discount_percentage) {
            this.discount_percentage = discount_percentage;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public Restaurants getRestaurants() {
            return restaurants;
        }

        public void setRestaurants(Restaurants restaurants) {
            this.restaurants = restaurants;
        }

        public ModelShopList.Datum getRetail__shop() {
            return retail__shop;
        }

        public void setRetail__shop(ModelShopList.Datum retail__shop) {
            this.retail__shop = retail__shop;
        }
    }
    public class Retail__shop implements Serializable {

       // public Integer id;
        public String name;
      //  public String phone;
      //  public String email;
        public Integer airport_id;
        public String shop_no;
        public Integer airport_terminal_id;
        public Integer retail_type_id;
      //  public String address;
      //  public String landmark;
        public String description;
      //  public Integer airport_type;
      //  public Integer area_type;
      //  public String latitude;
      //  public String longitude;
      //  public Integer status;
        public String profile_img;
      //  public Integer created_by;
      //  public String created_at;
      //  public String updated_at;


        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getAirport_id() {
            return airport_id;
        }

        public void setAirport_id(Integer airport_id) {
            this.airport_id = airport_id;
        }

        public String getShop_no() {
            return shop_no;
        }

        public void setShop_no(String shop_no) {
            this.shop_no = shop_no;
        }

        public Integer getAirport_terminal_id() {
            return airport_terminal_id;
        }

        public void setAirport_terminal_id(Integer airport_terminal_id) {
            this.airport_terminal_id = airport_terminal_id;
        }

        public Integer getRetail_type_id() {
            return retail_type_id;
        }

        public void setRetail_type_id(Integer retail_type_id) {
            this.retail_type_id = retail_type_id;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getProfile_img() {
            return profile_img;
        }

        public void setProfile_img(String profile_img) {
            this.profile_img = profile_img;
        }
    }
    public class Restaurants implements Serializable {

        public Integer id;
        //public Integer user_id;
      //  public Integer airport_id;
      //  public Integer airport_terminal_id;
      //  public Integer service_type_id;
      //  public Integer restauranttype_id;
      //  public String shop_no;
        public String name;
        public String phone;
        public String shop_image;
        // public String email;
      //  public String address;
      //  public String acc_no;
      //  public String ifsc_code;
       // public String landmark;
       // public String commission;
       // public String description;
       // public Object airport_type;
       // public Object area_type;
       // public String created_at;
       // public String updated_at;
     //   public Integer status;
     //   public Integer created_by;
        public String average_rating;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getAverage_rating() {
            return average_rating;
        }

        public void setAverage_rating(String average_rating) {
            this.average_rating = average_rating;
        }

        public String getShop_image() {
            return shop_image;
        }

        public void setShop_image(String shop_image) {
            this.shop_image = shop_image;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }
    }
}
