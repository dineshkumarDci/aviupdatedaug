package com.dci.avicustomer.models;

import java.util.List;

public class ModelUserTipList {
    public Integer status;
    public String message;
    public List<Datum> data = null;
    public String jwt_token;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getJwt_token() {
        return jwt_token;
    }

    public void setJwt_token(String jwt_token) {
        this.jwt_token = jwt_token;
    }

    public class Datum {

        public Integer id;
        public Integer airport_id;
        public String description;
        public Integer status;
        public Integer created_by;
        public String created_at;
        public String updated_at;
        public String usersFname;
        public String usersLname;

        public String getUsersFname() {
            return usersFname;
        }

        public void setUsersFname(String usersFname) {
            this.usersFname = usersFname;
        }

        public String getUsersLname() {
            return usersLname;
        }

        public void setUsersLname(String usersLname) {
            this.usersLname = usersLname;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getAirport_id() {
            return airport_id;
        }

        public void setAirport_id(Integer airport_id) {
            this.airport_id = airport_id;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
        // public Users users;

    }
}
