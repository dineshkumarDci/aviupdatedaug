package com.dci.avicustomer.models;

import java.io.Serializable;
import java.util.List;

public class ModelShopList implements Serializable {
    public Integer status;
    public String message;
    public List<Datum> data = null;
    public List<Terminal> terminal = null;
    public Integer total;
    public String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<Terminal> getTerminals() {
        return terminal;
    }

    public void setTerminals(List<Terminal> terminals) {
        this.terminal = terminals;
    }

    // public String jwt_token;
   public class Terminal implements Serializable {

       public Integer id;
     //  public Integer airport_id;
       public Integer terminal_id;
       //public String created_at;
       //public String updated_at;
       //public Integer status;
       public String name;
       public boolean isSelect;

        public boolean isSelect() {
            return isSelect;
        }

        public void setSelect(boolean select) {
            isSelect = select;
        }

        public Integer getId() {
           return id;
       }

       public void setId(Integer id) {
           this.id = id;
       }

       public Integer getTerminal_id() {
           return terminal_id;
       }

       public void setTerminal_id(Integer terminal_id) {
           this.terminal_id = terminal_id;
       }

       public String getName() {
           return name;
       }

       public void setName(String name) {
           this.name = name;
       }
       //public Object description;
       //public Integer created_by;

   }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum implements Serializable {

        public Integer id;
        public String name;
        public String phone;
        public String email;
        public String working_hours;
      //  public Integer airport_id;
        public String shop_no;
      //  public Integer airport_terminal_id;
       // public Integer retail_type_id;
        public String address;
        public String landmark;
        public String description;
        public String airport_type;
        public Integer area_type;
        public Integer status;
        public String profile_img;
        public Integer openClose;

       // public Integer created_by;
       // public String created_at;
       // public String updated_at;
        public Retailtype retailtype;
        public Terminal terminal;
        public List<Retail_time> retail_times = null;
        public List<Coupons> offer;
        public String latitude;
        public String longitude;

        public String getWorking_hours() {
            return working_hours;
        }

        public void setWorking_hours(String working_hours) {
            this.working_hours = working_hours;
        }

        public Integer getOpenClose() {
            return openClose;
        }

        public void setOpenClose(Integer openClose) {
            this.openClose = openClose;
        }

        public String getProfile_img() {
            return profile_img;
        }

        public void setProfile_img(String profile_img) {
            this.profile_img = profile_img;
        }

        public List<Retail_time> getRetail_times() {
            return retail_times;
        }

        public void setRetail_times(List<Retail_time> retail_times) {
            this.retail_times = retail_times;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public  List<Coupons> getCoupons() {
            return offer;
        }

        public void setCoupons( List<Coupons> coupons) {
            this.offer = coupons;
        }

        public String getAirport_type() {
            return airport_type;
        }

        public void setAirport_type(String airport_type) {
            this.airport_type = airport_type;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getShop_no() {
            return shop_no;
        }

        public void setShop_no(String shop_no) {
            this.shop_no = shop_no;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getLandmark() {
            return landmark;
        }

        public void setLandmark(String landmark) {
            this.landmark = landmark;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Integer getArea_type() {
            return area_type;
        }

        public void setArea_type(Integer area_type) {
            this.area_type = area_type;
        }

        public Retailtype getRetailtype() {
            return retailtype;
        }

        public void setRetailtype(Retailtype retailtype) {
            this.retailtype = retailtype;
        }

        public Terminal getTerminal() {
            return terminal;
        }

        public void setTerminal(Terminal terminal) {
            this.terminal = terminal;
        }

        public class Terminal implements Serializable {

           // public Integer id;
            public String name;
          //  public Object description;
           // public String created_at;
           // public String updated_at;
          //  public Integer status;
         //   public Integer created_by;


            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
        public class Retail_time implements Serializable {

          //  public Integer id;
            public Integer shop_id;
            public String day;
            public String from_time;
            public String to_time;
           // public String created_at;
          //  public String updated_at;

            public Integer getShop_id() {
                return shop_id;
            }

            public void setShop_id(Integer shop_id) {
                this.shop_id = shop_id;
            }

            public String getDay() {
                return day;
            }

            public void setDay(String day) {
                this.day = day;
            }

            public String getFrom_time() {
                return from_time;
            }

            public void setFrom_time(String from_time) {
                this.from_time = from_time;
            }

            public String getTo_time() {
                return to_time;
            }

            public void setTo_time(String to_time) {
                this.to_time = to_time;
            }
        }
        public class Coupons implements Serializable {

            public Integer id;
          //  public Integer offer_type_id;
            public String offer_code;
            public String min_purchase_value;
            public String discount_percentage;
            public String max_discount;
            public String offer_start_date;
            public String offer_end_date;
            public String conditions;
            public String description;
           // public Integer service_types;
           // public Integer notify_to_app;
            public String title;
           // public String flat_rate;
            public String image;
           // public Integer service_id;
           // public Integer type_id;
           // public String created_at;
            //public String updated_at;
            public Integer status;
            //public Integer created_by;

            public String getOffer_code() {
                return offer_code;
            }

            public void setOffer_code(String offer_code) {
                this.offer_code = offer_code;
            }

            public String getMin_purchase_value() {
                return min_purchase_value;
            }

            public void setMin_purchase_value(String min_purchase_value) {
                this.min_purchase_value = min_purchase_value;
            }

            public String getDiscount_percentage() {
                return discount_percentage;
            }

            public void setDiscount_percentage(String discount_percentage) {
                this.discount_percentage = discount_percentage;
            }

            public String getMax_discount() {
                return max_discount;
            }

            public void setMax_discount(String max_discount) {
                this.max_discount = max_discount;
            }

            public String getOffer_start_date() {
                return offer_start_date;
            }

            public void setOffer_start_date(String offer_start_date) {
                this.offer_start_date = offer_start_date;
            }

            public String getOffer_end_date() {
                return offer_end_date;
            }

            public void setOffer_end_date(String offer_end_date) {
                this.offer_end_date = offer_end_date;
            }

            public String getConditions() {
                return conditions;
            }

            public void setConditions(String conditions) {
                this.conditions = conditions;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }
        }
        public class Retailtype implements Serializable {

         //   public Integer id;
            public String name;
          //  public Integer status;
         //   public String created_at;
         //   public String updated_at;
         //   public Integer created_by;


            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

    }
}
