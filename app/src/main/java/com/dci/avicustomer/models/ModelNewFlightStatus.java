package com.dci.avicustomer.models;

public class ModelNewFlightStatus {
    public Integer status;
    public String message;
    public Data data;
    public String jwt_token;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getJwt_token() {
        return jwt_token;
    }

    public void setJwt_token(String jwt_token) {
        this.jwt_token = jwt_token;
    }

    public class Data {



        public String depdate_actual;
        public String arrdate_actual;
        public String depTime_actual;
        public String arrTime_actual;
        public String dep_delay;
        public String arr_delay;


        public String getDep_delay() {
            return dep_delay;
        }

        public void setDep_delay(String dep_delay) {
            this.dep_delay = dep_delay;
        }

        public String getArr_delay() {
            return arr_delay;
        }

        public void setArr_delay(String arr_delay) {
            this.arr_delay = arr_delay;
        }

        public String getDepdate_actual() {
            return depdate_actual;
        }

        public void setDepdate_actual(String depdate_actual) {
            this.depdate_actual = depdate_actual;
        }

        public String getArrdate_actual() {
            return arrdate_actual;
        }

        public void setArrdate_actual(String arrdate_actual) {
            this.arrdate_actual = arrdate_actual;
        }

        public String getDepTime_actual() {
            return depTime_actual;
        }

        public void setDepTime_actual(String depTime_actual) {
            this.depTime_actual = depTime_actual;
        }

        public String getArrTime_actual() {
            return arrTime_actual;
        }

        public void setArrTime_actual(String arrTime_actual) {
            this.arrTime_actual = arrTime_actual;
        }

        public String airline;
        public String FlightStatus;
        public String airlineCode;
        public String FlightNumber;
        public String depCode;
        public String arrCode;
        public String depCity;
        public String arrCity;
        public String depAirport;
        public String arrAirport;
        public String depdate;
        public String arrdate;
        public String depTime;
        public String arrTime;
        public String depTerminal;
        public String arrTerminal;
        public String deptCountry;
        public String arrCountry;
        public String depAirportName;
        public String arrAirportName;
        public String depGate;
        public String arrGate;


        public String getDepGate() {
            return depGate;
        }

        public void setDepGate(String depGate) {
            this.depGate = depGate;
        }

        public String getArrGate() {
            return arrGate;
        }

        public void setArrGate(String arrGate) {
            this.arrGate = arrGate;
        }

        public String getFlightStatus() {
            return FlightStatus;
        }

        public void setFlightStatus(String flightStatus) {
            FlightStatus = flightStatus;
        }

        public String getDepAirportName() {
            return depAirportName;
        }

        public void setDepAirportName(String depAirportName) {
            this.depAirportName = depAirportName;
        }

        public String getArrAirportName() {
            return arrAirportName;
        }

        public void setArrAirportName(String arrAirportName) {
            this.arrAirportName = arrAirportName;
        }

        public String getDeptCountry() {
            return deptCountry;
        }

        public void setDeptCountry(String deptCountry) {
            this.deptCountry = deptCountry;
        }

        public String getArrCountry() {
            return arrCountry;
        }

        public void setArrCountry(String arrCountry) {
            this.arrCountry = arrCountry;
        }

        public String getAirline() {
            return airline;
        }

        public void setAirline(String airline) {
            this.airline = airline;
        }

        public String getAirlineCode() {
            return airlineCode;
        }

        public void setAirlineCode(String airlineCode) {
            this.airlineCode = airlineCode;
        }

        public String getFlightNumber() {
            return FlightNumber;
        }

        public void setFlightNumber(String flightNumber) {
            this.FlightNumber = flightNumber;
        }

        public String getDepCode() {
            return depCode;
        }

        public void setDepCode(String depCode) {
            this.depCode = depCode;
        }

        public String getArrCode() {
            return arrCode;
        }

        public void setArrCode(String arrCode) {
            this.arrCode = arrCode;
        }

        public String getDepCity() {
            return depCity;
        }

        public void setDepCity(String depCity) {
            this.depCity = depCity;
        }

        public String getArrCity() {
            return arrCity;
        }

        public void setArrCity(String arrCity) {
            this.arrCity = arrCity;
        }

        public String getDepAirport() {
            return depAirport;
        }

        public void setDepAirport(String depAirport) {
            this.depAirport = depAirport;
        }

        public String getArrAirport() {
            return arrAirport;
        }

        public void setArrAirport(String arrAirport) {
            this.arrAirport = arrAirport;
        }

        public String getDepdate() {
            return depdate;
        }

        public void setDepdate(String depdate) {
            this.depdate = depdate;
        }

        public String getArrdate() {
            return arrdate;
        }

        public void setArrdate(String arrdate) {
            this.arrdate = arrdate;
        }

        public String getDepTime() {
            return depTime;
        }

        public void setDepTime(String depTime) {
            this.depTime = depTime;
        }

        public String getArrTime() {
            return arrTime;
        }

        public void setArrTime(String arrTime) {
            this.arrTime = arrTime;
        }

        public String getDepTerminal() {
            return depTerminal;
        }

        public void setDepTerminal(String depTerminal) {
            this.depTerminal = depTerminal;
        }

        public String getArrTerminal() {
            return arrTerminal;
        }

        public void setArrTerminal(String arrTerminal) {
            this.arrTerminal = arrTerminal;
        }
    }
}
