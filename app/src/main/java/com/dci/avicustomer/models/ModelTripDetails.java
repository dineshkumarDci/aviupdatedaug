package com.dci.avicustomer.models;

import java.io.Serializable;
import java.util.List;

public class ModelTripDetails implements Serializable {
    public Integer status;
    public String message;
    public Data data;
    public List<BoardImg> boardImg = null;
    public List<ETicketImg> eTicketImg = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public List<BoardImg> getBoardImg() {
        return boardImg;
    }

    public void setBoardImg(List<BoardImg> boardImg) {
        this.boardImg = boardImg;
    }

    public List<ETicketImg> geteTicketImg() {
        return eTicketImg;
    }

    public void seteTicketImg(List<ETicketImg> eTicketImg) {
        this.eTicketImg = eTicketImg;
    }

    public class Data implements Serializable {

        public Integer id;
        public String airline;
        public Integer user_id;
        public Integer airport_id;
        public String flight_status;
        public String flight_code;
        public String dep_airport_code;
        public String arr_airport_code;
        public String dep_airport_city;
        public String arr_airport_city;
        public String dep_airport_country;
        public String arr_airport_country;
        public String dep_terminal;
        public String arr_terminal;
        public String dep_date_time;
        public String arr_date_time;
        public String dep_date;
        public String arr_date;
        public String dep_time;
        public String arr_time;

        public String boardingPass;
        public String eTickets;
        public Integer status;
        public String created_at;
        public String updated_at;
        public String dep_delay;
        public String arr_delay;
        public String act_dep_date;
        public String act_dep_time;
        public String act_arr_date;
        public String act_arr_time;

        public String depGate;
        public String arrGate;


        public String getAct_dep_date() {
            return act_dep_date;
        }

        public void setAct_dep_date(String act_dep_date) {
            this.act_dep_date = act_dep_date;
        }

        public String getAct_dep_time() {
            return act_dep_time;
        }

        public void setAct_dep_time(String act_dep_time) {
            this.act_dep_time = act_dep_time;
        }

        public String getAct_arr_date() {
            return act_arr_date;
        }

        public void setAct_arr_date(String act_arr_date) {
            this.act_arr_date = act_arr_date;
        }

        public String getAct_arr_time() {
            return act_arr_time;
        }

        public void setAct_arr_time(String act_arr_time) {
            this.act_arr_time = act_arr_time;
        }

        public String getDep_delay() {
            return dep_delay;
        }

        public void setDep_delay(String dep_delay) {
            this.dep_delay = dep_delay;
        }

        public String getArr_delay() {
            return arr_delay;
        }

        public void setArr_delay(String arr_delay) {
            this.arr_delay = arr_delay;
        }

        public String getDepGate() {
            return depGate;
        }

        public void setDepGate(String depGate) {
            this.depGate = depGate;
        }

        public String getArrGate() {
            return arrGate;
        }

        public void setArrGate(String arrGate) {
            this.arrGate = arrGate;
        }

        public String getAirline() {
            return airline;
        }

        public void setAirline(String airline) {
            this.airline = airline;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getAirport_id() {
            return airport_id;
        }

        public void setAirport_id(Integer airport_id) {
            this.airport_id = airport_id;
        }

        public String getFlight_code() {
            return flight_code;
        }

        public void setFlight_code(String flight_code) {
            this.flight_code = flight_code;
        }

        public String getDep_airport_code() {
            return dep_airport_code;
        }

        public void setDep_airport_code(String dep_airport_code) {
            this.dep_airport_code = dep_airport_code;
        }

        public String getArr_airport_code() {
            return arr_airport_code;
        }

        public void setArr_airport_code(String arr_airport_code) {
            this.arr_airport_code = arr_airport_code;
        }

        public String getDep_airport_city() {
            return dep_airport_city;
        }

        public void setDep_airport_city(String dep_airport_city) {
            this.dep_airport_city = dep_airport_city;
        }

        public String getArr_airport_city() {
            return arr_airport_city;
        }

        public void setArr_airport_city(String arr_airport_city) {
            this.arr_airport_city = arr_airport_city;
        }

        public String getDep_airport_country() {
            return dep_airport_country;
        }

        public void setDep_airport_country(String dep_airport_country) {
            this.dep_airport_country = dep_airport_country;
        }

        public String getArr_airport_country() {
            return arr_airport_country;
        }

        public void setArr_airport_country(String arr_airport_country) {
            this.arr_airport_country = arr_airport_country;
        }

        public String getDep_terminal() {
            return dep_terminal;
        }

        public void setDep_terminal(String dep_terminal) {
            this.dep_terminal = dep_terminal;
        }

        public String getArr_terminal() {
            return arr_terminal;
        }

        public void setArr_terminal(String arr_terminal) {
            this.arr_terminal = arr_terminal;
        }

        public String getDep_date_time() {
            return dep_date_time;
        }

        public void setDep_date_time(String dep_date_time) {
            this.dep_date_time = dep_date_time;
        }

        public String getArr_date_time() {
            return arr_date_time;
        }

        public void setArr_date_time(String arr_date_time) {
            this.arr_date_time = arr_date_time;
        }

        public String getDep_date() {
            return dep_date;
        }

        public void setDep_date(String dep_date) {
            this.dep_date = dep_date;
        }

        public String getArr_date() {
            return arr_date;
        }

        public void setArr_date(String arr_date) {
            this.arr_date = arr_date;
        }

        public String getDep_time() {
            return dep_time;
        }

        public void setDep_time(String dep_time) {
            this.dep_time = dep_time;
        }

        public String getArr_time() {
            return arr_time;
        }

        public void setArr_time(String arr_time) {
            this.arr_time = arr_time;
        }

        public String getFlight_status() {
            return flight_status;
        }

        public void setFlight_status(String flight_status) {
            this.flight_status = flight_status;
        }

        public String getBoardingPass() {
            return boardingPass;
        }

        public void setBoardingPass(String boardingPass) {
            this.boardingPass = boardingPass;
        }

        public String geteTickets() {
            return eTickets;
        }

        public void seteTickets(String eTickets) {
            this.eTickets = eTickets;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }
    }
    public class BoardImg implements Serializable {

        public Integer id;
        public Integer user_id;
       // public Integer type;
        public String image;
        String ImageDataType;
        String mimetype;

        public String getMimetype() {
            return mimetype;
        }

        public void setMimetype(String mimetype) {
            this.mimetype = mimetype;
        }

        public String getType() {
            return ImageDataType;
        }
        public long selectImageid;

        public long getSelectImageid() {
            return selectImageid;
        }

        public void setSelectImageid(long selectImageid) {
            this.selectImageid = selectImageid;
        }
        public void setType(String type) {
            this.ImageDataType = type;
        }
        //  public Integer status;
      //  public String updated_at;
      //  public String created_at;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }
    public class ETicketImg implements Serializable {

        public Integer id;
        public Integer user_id;
      //  public Integer type;
        public String image;
        public String ImageDataType;
        public long selectImageid;
        public String mimetype;

        public String getMimetype() {
            return mimetype;
        }

        public void setMimetype(String mimetype) {
            this.mimetype = mimetype;
        }

        public long getSelectImageid() {
            return selectImageid;
        }

        public void setSelectImageid(long selectImageid) {
            this.selectImageid = selectImageid;
        }

        public String getType() {
            return ImageDataType;
        }

        public void setType(String type) {
            this.ImageDataType = type;
        }
        //        public Integer status;
//        public String updated_at;
//        public String created_at;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }
}
