package com.dci.avicustomer.models;

import java.util.List;

public class ModelOrderTracking {
    public Integer status;
    public String message;
    public Order order;
    public List<Orderstatus> orderStatus = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public List<Orderstatus> getOrderStatus() {
        return orderStatus;
    }

    public void
    setOrderStatus(List<Orderstatus> orderStatus) {
        this.orderStatus = orderStatus;
    }

    public class Menu {
        public Integer subTotal;
        public String commission;
        public String discount;
        public String currency;
        public String menu;
        public Integer quantity;
        public String price;

        public Integer getSubTotal() {
            return subTotal;
        }

        public void setSubTotal(Integer subTotal) {
            this.subTotal = subTotal;
        }

        public String getCommission() {
            return commission;
        }

        public void setCommission(String commission) {
            this.commission = commission;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getMenu() {
            return menu;
        }

        public void setMenu(String menu) {
            this.menu = menu;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }
    }
    public class Orderstatus {

        public Integer id;
        public String name;
        public Integer status;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }
    }
    public class Order {
        public String restaurant;
        public String orderStatus;
        public Integer orderStatusId;
        public String airport;
        public String terminal;
        public String order_id;
        public Integer place;
        public Object dine_in_count;
        public String date;
        public String ETATime;
        public List<Menu> menu = null;
        public String currency;
        public Integer coupon;
        public String commission;
        public String total;

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getRestaurant() {
            return restaurant;
        }

        public void setRestaurant(String restaurant) {
            this.restaurant = restaurant;
        }

        public String getOrderStatus() {
            return orderStatus;
        }

        public void setOrderStatus(String orderStatus) {
            this.orderStatus = orderStatus;
        }

        public Integer getOrderStatusId() {
            return orderStatusId;
        }

        public void setOrderStatusId(Integer orderStatusId) {
            this.orderStatusId = orderStatusId;
        }

        public String getAirport() {
            return airport;
        }

        public void setAirport(String airport) {
            this.airport = airport;
        }

        public String getTerminal() {
            return terminal;
        }

        public void setTerminal(String terminal) {
            this.terminal = terminal;
        }

        public Integer getPlace() {
            return place;
        }

        public void setPlace(Integer place) {
            this.place = place;
        }

        public Object getDine_in_count() {
            return dine_in_count;
        }

        public void setDine_in_count(Object dine_in_count) {
            this.dine_in_count = dine_in_count;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String geteTATime() {
            return ETATime;
        }

        public void seteTATime(String eTATime) {
            this.ETATime = eTATime;
        }

        public List<Menu> getMenu() {
            return menu;
        }

        public void setMenu(List<Menu> menu) {
            this.menu = menu;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public Integer getCoupon() {
            return coupon;
        }

        public void setCoupon(Integer coupon) {
            this.coupon = coupon;
        }

        public String getCommission() {
            return commission;
        }

        public void setCommission(String commission) {
            this.commission = commission;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }
    }
}
