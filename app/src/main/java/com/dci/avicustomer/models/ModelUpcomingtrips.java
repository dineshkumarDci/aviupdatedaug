package com.dci.avicustomer.models;

public class ModelUpcomingtrips{
    public Integer status;
    public String message;
    public Data data;
    public String upTripTime;
    public String upTripTimeType;

    public String getUpTripTimeType() {
        return upTripTimeType;
    }

    public void setUpTripTimeType(String upTripTimeType) {
        this.upTripTimeType = upTripTimeType;
    }

    public String getUpTripTime() {
        return upTripTime;
    }

    public void setUpTripTime(String upTripTime) {
        this.upTripTime = upTripTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

//        public Integer id;
//        public Integer user_id;
//        public Integer airport_id;
//        public String flight_code;
//        public String dep_airport_code;
//        public String arr_airport_code;
//        public String dep_airport_city;
//        public String arr_airport_city;
//        public String dep_airport_country;
//        public String arr_airport_country;
//        public String dep_terminal;
//        public String arr_terminal;
          public String dep_date_time;
//        public String arr_date_time;
//        public String dep_date;
//        public String arr_date;
       // public String dep_time;
//        public String arr_time;
//        public String flight_status;
//        public Object boardingPass;
//        public Object eTickets;
//        public Integer status;
//        public String created_at;
//        public String updated_at;

        public String airport_type;

        public String getAirport_type() {
            return airport_type;
        }

        public void setAirport_type(String airport_type) {
            this.airport_type = airport_type;
        }

        public String getDep_date_time() {
            return dep_date_time;
        }

        public void setDep_date_time(String dep_date_time) {
            this.dep_date_time = dep_date_time;
        }
    }
}
