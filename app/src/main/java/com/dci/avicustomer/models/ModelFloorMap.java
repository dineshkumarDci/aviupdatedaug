package com.dci.avicustomer.models;

import java.util.List;

public class ModelFloorMap {
    public Integer status;
    public String message;
    public List<Datum> data = null;
    public String url;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public class Datum {

        public Integer id;
        public Integer airport_id;
        public String floormap;
        public String storey;
        public String created_at;
        public String updated_at;
        public Integer created_by;
        public Integer status;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getAirport_id() {
            return airport_id;
        }

        public void setAirport_id(Integer airport_id) {
            this.airport_id = airport_id;
        }

        public String getFloormap() {
            return floormap;
        }

        public void setFloormap(String floormap) {
            this.floormap = floormap;
        }

        public String getStorey() {
            return storey;
        }

        public void setStorey(String storey) {
            this.storey = storey;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public Integer getCreated_by() {
            return created_by;
        }

        public void setCreated_by(Integer created_by) {
            this.created_by = created_by;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }
    }
}
