package com.dci.avicustomer.models;

public class ModelInputAddtrip {


    int user_id;
    int airport_id;
    String time_zone;
    String flight_code;
    String dep_airport_code;
    String arr_airport_code;
    String dep_airport_name;
    String arr_airport_name;
    String dep_airport_city;
    String arr_airport_city;
    String dep_airport_country;
    String arr_airport_country;
    String dep_terminal;
    String arr_terminal;
    String dep_date;
    String arr_date;
    String dep_time;
    String arr_time;
    String flight_status;
    StringBuilder eTickets;
    StringBuilder boardingPass;
    String airline;
    int id;
    String depGate;
    String arrGate;
    public String getDepGate() {
        return depGate;
    }

    public void setDepGate(String depGate) {
        this.depGate = depGate;
    }

    public String getArrGate() {
        return arrGate;
    }

    public void setArrGate(String arrGate) {
        this.arrGate = arrGate;
    }

    public String getTime_zone() {
        return time_zone;
    }

    public void setTime_zone(String time_zone) {
        this.time_zone = time_zone;
    }

    public String getDep_airport_name() {
        return dep_airport_name;
    }

    public void setDep_airport_name(String dep_airport_name) {
        this.dep_airport_name = dep_airport_name;
    }

    public String getArr_airport_name() {
        return arr_airport_name;
    }

    public void setArr_airport_name(String arr_airport_name) {
        this.arr_airport_name = arr_airport_name;
    }

    public String getAirlineName() {
        return airline;
    }

    public void setAirlineName(String airlineName) {
        airline = airlineName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public StringBuilder geteTickets() {
        return eTickets;
    }

    public void seteTickets(StringBuilder eTickets) {
        this.eTickets = eTickets;
    }

    public StringBuilder getBoardingPass() {
        return boardingPass;
    }

    public void setBoardingPass(StringBuilder boardingPass) {
        this.boardingPass = boardingPass;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getAirport_id() {
        return airport_id;
    }

    public void setAirport_id(int airport_id) {
        this.airport_id = airport_id;
    }

    public String getFlight_code() {
        return flight_code;
    }

    public void setFlight_code(String flight_code) {
        this.flight_code = flight_code;
    }

    public String getDep_airport_code() {
        return dep_airport_code;
    }

    public void setDep_airport_code(String dep_airport_code) {
        this.dep_airport_code = dep_airport_code;
    }

    public String getArr_airport_code() {
        return arr_airport_code;
    }

    public void setArr_airport_code(String arr_airport_code) {
        this.arr_airport_code = arr_airport_code;
    }

    public String getDep_airport_city() {
        return dep_airport_city;
    }

    public void setDep_airport_city(String dep_airport_city) {
        this.dep_airport_city = dep_airport_city;
    }

    public String getArr_airport_city() {
        return arr_airport_city;
    }

    public void setArr_airport_city(String arr_airport_city) {
        this.arr_airport_city = arr_airport_city;
    }

    public String getDep_airport_country() {
        return dep_airport_country;
    }

    public void setDep_airport_country(String dep_airport_country) {
        this.dep_airport_country = dep_airport_country;
    }

    public String getArr_airport_country() {
        return arr_airport_country;
    }

    public void setArr_airport_country(String arr_airport_country) {
        this.arr_airport_country = arr_airport_country;
    }

    public String getDep_terminal() {
        return dep_terminal;
    }

    public void setDep_terminal(String dep_terminal) {
        this.dep_terminal = dep_terminal;
    }

    public String getArr_terminal() {
        return arr_terminal;
    }

    public void setArr_terminal(String arr_terminal) {
        this.arr_terminal = arr_terminal;
    }

    public String getDep_date() {
        return dep_date;
    }

    public void setDep_date(String dep_date) {
        this.dep_date = dep_date;
    }

    public String getArr_date() {
        return arr_date;
    }

    public void setArr_date(String arr_date) {
        this.arr_date = arr_date;
    }

    public String getDep_time() {
        return dep_time;
    }

    public void setDep_time(String dep_time) {
        this.dep_time = dep_time;
    }

    public String getArr_time() {
        return arr_time;
    }

    public void setArr_time(String arr_time) {
        this.arr_time = arr_time;
    }

    public String getFlight_status() {
        return flight_status;
    }

    public void setFlight_status(String flight_status) {
        this.flight_status = flight_status;
    }
}
