package com.dci.avicustomer.models;

/**
 * Created by dineshkumars on 4/23/2018.
 */

public class DrawerItem {
    String title;
    int icon;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
