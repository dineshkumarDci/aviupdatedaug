package com.dci.avicustomer.models;

import java.util.List;

public class ModelEatAndDrink {

    public Integer status;
    public String message;
    public List<Datum> data = null;
    public List<Terminal> terminal = null;
    public Integer total;
    public ImagePath imagePath;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public List<Terminal> getTerminal() {
        return terminal;
    }

    public void setTerminal(List<Terminal> terminal) {
        this.terminal = terminal;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public ImagePath getImagePath() {
        return imagePath;
    }

    public void setImagePath(ImagePath imagePath) {
        this.imagePath = imagePath;
    }

    public class Terminal {

        public Integer id;
       // public Integer airport_id;
        public Integer terminal_id;
       // public String created_at;
      //  public String updated_at;
       // public Integer status;
        public String name;
       // public String description;
       // public Integer created_by;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getTerminal_id() {
            return terminal_id;
        }

        public void setTerminal_id(Integer terminal_id) {
            this.terminal_id = terminal_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
    public class Datum {

        public Integer id;
        public Integer user_id;
        public Integer airport_id;
        public Integer openClose;
        //public Integer airport_terminal_id;
      //  public Integer service_type_id;
      //  public Integer restauranttype_id;
      //  public String shop_no;
        public String name;
        public String phone;
        public String shop_image;
        public String email;
        public String longitude;
        public String latitude;

       // public String address;
        //public String acc_no;
        //public String ifsc_code;
       // public String landmark;
       // public String commission;
        public String description;
        public Integer airport_type;
        public Integer area_type;
        public String ETA;

        //public String created_at;
        //public String updated_at;
       // public Integer status;
       // public Integer created_by;
        public float average_rating;
        public Integer rating_count;
        public List<Offer> offer = null;
        public Terminal terminal;
        public restaurant_type restaurant_type;

        public String getETA() {
            return ETA;
        }

        public void setETA(String ETA) {
            this.ETA = ETA;
        }

        public restaurant_type getRestaurant_type() {
            return restaurant_type;
        }

        public void setRestaurant_type(restaurant_type restaurant_type) {
            this.restaurant_type = restaurant_type;
        }

        public class restaurant_type{
            String name;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
        public List<Restaurant_time> restaurant_times = null;

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public Integer getOpenClose() {
            return openClose;
        }

        public void setOpenClose(Integer openClose) {
            this.openClose = openClose;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getAirport_type() {
            return airport_type;
        }

        public void setAirport_type(Integer airport_type) {
            this.airport_type = airport_type;
        }

        public Integer getArea_type() {
            return area_type;
        }

        public void setArea_type(Integer area_type) {
            this.area_type = area_type;
        }

        public float getAverage_rating() {
            return average_rating;
        }

        public void setAverage_rating(float average_rating) {
            this.average_rating = average_rating;
        }

        public Integer getRating_count() {
            return rating_count;
        }

        public void setRating_count(Integer rating_count) {
            this.rating_count = rating_count;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getShop_image() {
            return shop_image;
        }

        public void setShop_image(String shop_image) {
            this.shop_image = shop_image;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public List<Offer> getOffer() {
            return offer;
        }

        public void setOffer(List<Offer> offer) {
            this.offer = offer;
        }

        public Terminal getTerminal() {
            return terminal;
        }

        public void setTerminal(Terminal terminal) {
            this.terminal = terminal;
        }

        public List<Restaurant_time> getRestaurant_times() {
            return restaurant_times;
        }

        public void setRestaurant_times(List<Restaurant_time> restaurant_times) {
            this.restaurant_times = restaurant_times;
        }

        public class Terminal {

            public Integer id;
            public String name;
//            public String description;
//            public String created_at;
//            public String updated_at;
//            public Integer status;
//            public Integer created_by;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

    }

    public class ImagePath {

        public String restaurant;
        public String menu;

        public String getRestaurant() {
            return restaurant;
        }

        public void setRestaurant(String restaurant) {
            this.restaurant = restaurant;
        }

        public String getMenu() {
            return menu;
        }

        public void setMenu(String menu) {
            this.menu = menu;
        }
    }
    public class Offer {

        public Integer id;
//        public Integer offer_type_id;
//        public Object offer_type;
        public String offer_code;
//        public String min_purchase_value;
//        public String discount_percentage;
//        public String max_discount;
//        public String offer_start_date;
//        public String offer_end_date;
//        public String conditions;
//        public String description;
//        public Integer service_types;
//        public Object notify_to_app;
//        public String title;
//        public String flat_rate;
//        public String image;
//        public Integer service_id;
//        public Integer type_id;
//        public String created_at;
//        public String updated_at;
//        public Integer status;
//        public Integer created_by;

        public String getOffer_code() {
            return offer_code;
        }

        public void setOffer_code(String offer_code) {
            this.offer_code = offer_code;
        }
    }

    public class Restaurant_time {

        public Integer id;
        public Integer rest_id;
        public String day;
        public String from_time;
        public String to_time;
        public String from_am_pm;
        public String to_am_pm;
        public String open_24_hrs;
        public String open_days;
      //  public String created_at;
       // public String updated_at;


        public String getFrom_am_pm() {
            return from_am_pm;
        }

        public void setFrom_am_pm(String from_am_pm) {
            this.from_am_pm = from_am_pm;
        }

        public String getTo_am_pm() {
            return to_am_pm;
        }

        public void setTo_am_pm(String to_am_pm) {
            this.to_am_pm = to_am_pm;
        }

        public String getOpen_24_hrs() {
            return open_24_hrs;
        }

        public void setOpen_24_hrs(String open_24_hrs) {
            this.open_24_hrs = open_24_hrs;
        }

        public String getOpen_days() {
            return open_days;
        }

        public void setOpen_days(String open_days) {
            this.open_days = open_days;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getRest_id() {
            return rest_id;
        }

        public void setRest_id(Integer rest_id) {
            this.rest_id = rest_id;
        }

        public String getDay() {
            return day;
        }

        public void setDay(String day) {
            this.day = day;
        }

        public String getFrom_time() {
            return from_time;
        }

        public void setFrom_time(String from_time) {
            this.from_time = from_time;
        }

        public String getTo_time() {
            return to_time;
        }

        public void setTo_time(String to_time) {
            this.to_time = to_time;
        }
    }
}
