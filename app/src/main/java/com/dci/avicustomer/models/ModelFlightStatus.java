package com.dci.avicustomer.models;

public class ModelFlightStatus {

    public Integer status;
    public String message;
    public Data data;
    public String jwt_token;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getJwt_token() {
        return jwt_token;
    }

    public void setJwt_token(String jwt_token) {
        this.jwt_token = jwt_token;
    }

    public class Data {

        public Flight flight;
        public Departure departure;
        public Arrival Arrival;

        public Flight getFlight() {
            return flight;
        }

        public void setFlight(Flight flight) {
            this.flight = flight;
        }

        public Departure getDeparture() {
            return departure;
        }

        public void setDeparture(Departure departure) {
            this.departure = departure;
        }

        public Data.Arrival getArrival() {
            return Arrival;
        }

        public void setArrival(Data.Arrival arrival) {
            Arrival = arrival;
        }

        public class Flight {

            public String FlightNumber;
            public CommercialAirline CommercialAirline;
            public String OrigDate;

            public String getFlightNumber() {
                return FlightNumber;
            }

            public void setFlightNumber(String flightNumber) {
                FlightNumber = flightNumber;
            }

            public Flight.CommercialAirline getCommercialAirline() {
                return CommercialAirline;
            }

            public void setCommercialAirline(Flight.CommercialAirline commercialAirline) {
                CommercialAirline = commercialAirline;
            }

            public String getOrigDate() {
                return OrigDate;
            }

            public void setOrigDate(String origDate) {
                OrigDate = origDate;
            }

            // public OperatingFlight operatingFlight;
           // public List<CodeShare> CodeShare = null;
           public class CommercialAirline {

               public AirportId AirlineId;
               public String AirlineName;

                public AirportId getAirlineId() {
                    return AirlineId;
                }

                public void setAirlineId(AirportId airlineId) {
                    AirlineId = airlineId;
                }

                public String getAirlineName() {
                    return AirlineName;
                }

                public void setAirlineName(String airlineName) {
                    AirlineName = airlineName;
                }
            }
           public class AirportId {

               public String AirlineCode;

               public String getAirlineCode() {
                   return AirlineCode;
               }

               public void setAirlineCode(String airlineCode) {
                   AirlineCode = airlineCode;
               }
               // public IATACode___ iATACode;

           }
        }


        public class Departure {

            public Airport Airport;
            public DateTime DateTime;
            //  public Original original;
            public DelayReason DelayReason;

            public Departure.Airport getAirport() {
                return Airport;
            }

            public void setAirport(Departure.Airport airport) {
                Airport = airport;
            }

            public Departure.DateTime getDateTime() {
                return DateTime;
            }

            public void setDateTime(Departure.DateTime dateTime) {
                DateTime = dateTime;
            }

            public Departure.DelayReason getDelayReason() {
                return DelayReason;
            }

            public void setDelayReason(Departure.DelayReason delayReason) {
                DelayReason = delayReason;
            }

            public class Airport {

                public AirportId AirportId;
                public String AirportName;
                public AirportLocation AirportLocation;
                public String Terminal;

                public Departure.AirportId getAirportId() {
                    return AirportId;
                }

                public void setAirportId(Departure.AirportId airportId) {
                    AirportId = airportId;
                }

                public String getAirportName() {
                    return AirportName;
                }

                public void setAirportName(String airportName) {
                    AirportName = airportName;
                }

                public Departure.AirportLocation getAirportLocation() {
                    return AirportLocation;
                }

                public void setAirportLocation(Departure.AirportLocation airportLocation) {
                    AirportLocation = airportLocation;
                }

                public String getTerminal() {
                    return Terminal;
                }

                public void setTerminal(String terminal) {
                    Terminal = terminal;
                }
            }
            public class AirportLocation {

                public String CityName;
                // public String stateId;
                public String CountryId;

                public String getCityName() {
                    return CityName;
                }

                public void setCityName(String cityName) {
                    CityName = cityName;
                }

                public String getCountryId() {
                    return CountryId;
                }

                public void setCountryId(String countryId) {
                    CountryId = countryId;
                }
            }
            public class DateTime {

                public String Date;
                public String Time;

                public String getDate() {
                    return Date;
                }

                public void setDate(String date) {
                    Date = date;
                }

                public String getTime() {
                    return Time;
                }

                public void setTime(String time) {
                    Time = time;
                }
                //                public Local_ local;
//                public Scheduled_ scheduled;
//                public GateTime_ gateTime;
//                public ScheduleData_ scheduleData;

            }
            public class DelayReason {

                public String Code;

                public String getCode() {
                    return Code;
                }

                public void setCode(String code) {
                    Code = code;
                }
            }
            public class AirportId {

                public String AirportCode;

                public String getAirportCode() {
                    return AirportCode;
                }

                public void setAirportCode(String airportCode) {
                    AirportCode = airportCode;
                }
                // public IATACode___ iATACode;

            }

        }
        public class Arrival {
            public Airport Airport;
            public DateTime DateTime;
            //  public Original original;
            public DelayReason DelayReason;

            public Arrival.Airport getAirport() {
                return Airport;
            }

            public void setAirport(Arrival.Airport airport) {
                Airport = airport;
            }

            public Arrival.DateTime getDateTime() {
                return DateTime;
            }

            public void setDateTime(Arrival.DateTime dateTime) {
                DateTime = dateTime;
            }

            public Arrival.DelayReason getDelayReason() {
                return DelayReason;
            }

            public void setDelayReason(Arrival.DelayReason delayReason) {
                DelayReason = delayReason;
            }

            public class Airport {

                public AirportId AirportId;
                public String AirportName;
                public AirportLocation AirportLocation;
                public String Terminal;

                public Arrival.AirportId getAirportId() {
                    return AirportId;
                }

                public void setAirportId(Arrival.AirportId airportId) {
                    AirportId = airportId;
                }

                public String getAirportName() {
                    return AirportName;
                }

                public void setAirportName(String airportName) {
                    AirportName = airportName;
                }

                public Arrival.AirportLocation getAirportLocation() {
                    return AirportLocation;
                }

                public void setAirportLocation(Arrival.AirportLocation airportLocation) {
                    AirportLocation = airportLocation;
                }

                public String getTerminal() {
                    return Terminal;
                }

                public void setTerminal(String terminal) {
                    Terminal = terminal;
                }
            }
            public class AirportLocation {

                public String CityName;
               // public String stateId;
                public String CountryId;

                public String getCityName() {
                    return CityName;
                }

                public void setCityName(String cityName) {
                    CityName = cityName;
                }

                public String getCountryId() {
                    return CountryId;
                }

                public void setCountryId(String countryId) {
                    CountryId = countryId;
                }
            }
            public class DateTime {

                public String Date;
                public String Time;

                public String getDate() {
                    return Date;
                }

                public void setDate(String date) {
                    Date = date;
                }

                public String getTime() {
                    return Time;
                }

                public void setTime(String time) {
                    Time = time;
                }
                //                public Local_ local;
//                public Scheduled_ scheduled;
//                public GateTime_ gateTime;
//                public ScheduleData_ scheduleData;

            }
            public class DelayReason {

                public String Code;

                public String getCode() {
                    return Code;
                }

                public void setCode(String code) {
                    Code = code;
                }
            }
            public class AirportId {

                public String AirportCode;

                public String getAirportCode() {
                    return AirportCode;
                }

                public void setAirportCode(String airportCode) {
                    AirportCode = airportCode;
                }
                // public IATACode___ iATACode;

            }

        }
//        public class CommercialAirline {
//
//            public AirlineId_ airlineId;
//            public String airlineName;
//
//        }
//        public class OperatingFlight {
//
//
//        }


    }
}
