package com.dci.avicustomer.firebase;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.text.Html;
import android.util.Log;
import android.widget.Toast;

import com.dci.avicustomer.R;
import com.dci.avicustomer.activities.ActivityHome;
import com.dci.avicustomer.activities.ActivityOfferPage;
import com.dci.avicustomer.activities.ActivityOrderTracking;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.models.NotificationID;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;


import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;


/**
 * Created by Dinesh on 11/13/2017.
 */

public class FirebaseMessaging extends FirebaseMessagingService {
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    String TAG = "FirebaseMessagings";
    Bitmap bitmap;
    String message;
    String title;
    String imageUri = "";
    String type = "";
    String orderid="";
    String productid="";

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        Log.w(TAG, "getInstanceId success"+token);
                        UtilsDefault.updateFcmSharedPreference(Constants.FCM_KEY,token);
                        // Log and toast

                    }
                });
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        //Calling method to generate icon_notification
        Log.d("FirebaseMessagings", "onMessageReceived: " + remoteMessage.getNotification());
        //sendNotification(remoteMessage.getNotification());

        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        //The message which i send will have keys named [message, image, AnotherActivity] and corresponding values.
        //You can change as per the requirement.

        //message will contain the Push Message
        orderid="";
        type="";
        message="";
        imageUri=null;
        title="";
        productid="";
        bitmap=null;
        if (remoteMessage.getData().get("message") != null) {
            message = remoteMessage.getData().get("message");
        }
        if (remoteMessage.getData().get("image") != null) {
            imageUri = remoteMessage.getData().get("image");
        }
        if (remoteMessage.getData().get("title") != null) {
            title = remoteMessage.getData().get("title");
        }
        if (remoteMessage.getData().get("type") != null) {
            type = remoteMessage.getData().get("type");
        }
        if (remoteMessage.getData().get("order_id") != null) {
            orderid = remoteMessage.getData().get("order_id");
        }
        if (remoteMessage.getData().get("product_id") != null) {
            productid = remoteMessage.getData().get("product_id");
        }


        //imageUri will contain URL of the image to be displayed with Notification

        //If the key AnotherActivity has  value as True then when the user taps on notification, in the app AnotherActivity will be opened.
        //If the key AnotherActivity has  value as False then when the user taps on notification, in the app MainActivity will be opened.
        // String TrueOrFlase = remoteMessage.getData().get("AnotherActivity");
        bitmap = getBitmapfromUrl(imageUri);
//        if (UtilsDefault.getFcmSharedPreferenceValue(Constants.NOTIFICATION_STATUS)!=null&&
//                UtilsDefault.getFcmSharedPreferenceValue(Constants.NOTIFICATION_STATUS).equals("2")){
//            return;
//
//        }
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)==null){
            return;
        }
        if (remoteMessage.getData().get("image") ==null||
                remoteMessage.getData().get("image").equals("")){
            sendNotification(message, bitmap, title);
        }
        else {
            showBigNotification(bitmap,title,message);
        }




    }

    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(""+imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;

        }
    }


    //This method is only generating push icon_notification
    //It is same as we did in earlier posts
    private void sendNotification(String msg, Bitmap bitmap, String title) {

//        Intent intentNo = new Intent("notificationListener");
//        intentNo.putExtra("notify", true);
//        sendBroadcast(intentNo);
        Intent intent;
        if(orderid!=null&&!orderid.equals("")){
            intent= new Intent(this, ActivityOrderTracking.class);
            intent.putExtra("id",orderid);
        }
        else if (type.equals("1")){
            intent= new Intent(this, ActivityOfferPage.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("isFromNotification", "yes");
        }
        else {
            intent= new Intent(this, ActivityHome.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }
//        if (type.equals(Constants.NOTIFY_WALLET)){
//            intent= new Intent(this, HomePageActivity.class);
//            intent.putExtra("data","2");
//
//        }
//        else if (type.equals(Constants.NOTIFY_PRODUCT)){
//            intent = new Intent(this, ActivityOrderDetails.class);
//            intent.putExtra("id",orderid);
//        }
//        else if (type.equals(Constants.NOTIFY_PRODUCT_PRODUCT_DETAIL)){
//            intent = new Intent(this, ProductDescribtionActivity.class);
//            intent.putExtra("id", productid);
//           // intent.putExtra("")
//        }
//        else if(UtilsDefault.getSharedPreferenceValue(Constants.usernam)==null){
//            intent = new Intent(this, LoginActvity.class);
//           // intent.putExtra("id", productid);
//        }
//        else {
//            intent = new Intent(this, NotificationActivity.class);
//        }
      //  Intent intent = new Intent(this, NotificationActivity.class);

       // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        // Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        int requestID = (int) System.currentTimeMillis();
        PendingIntent pendingIntent = PendingIntent.getActivity(this, requestID, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
////        RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.notification_item);
////        contentView.setTextViewText(R.id.text_notification_title,messageBody.getTitle());
////        contentView.setTextViewText(R.id.text_notification_subtitle,messageBody.getBody());
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "")
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.appicons))
                .setSmallIcon(R.drawable.alarm_notify)
                .setContentTitle(UtilsDefault.checkNull(title))
                .setContentText(UtilsDefault.checkNull(msg))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            NotificationChannel channel = new NotificationChannel(channelId,
//                    "Channel human readable title",
//                    NotificationManager.IMPORTANCE_DEFAULT);
//            notificationManager.createNotificationChannel(channel);
//        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelId = getString(R.string.default_notification_channel_id);
            String name = getString(R.string.default_notification_channel_id);
            NotificationChannel channel = new NotificationChannel(channelId, name,
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(msg);
            notificationManager.createNotificationChannel(channel);
            notificationBuilder.setChannelId(channelId);
        }
        notificationManager.notify(requestID, notificationBuilder.build());
    }
    private void showBigNotification(Bitmap bitmap, String title, String message) {
        Intent intent;
        if(orderid!=null&&!orderid.equals("")){
            intent= new Intent(this, ActivityOrderTracking.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("id",orderid);
        }
        else if (type.equals("1")){
            intent= new Intent(this, ActivityOfferPage.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("isFromNotification", "yes");
        }
        else {
            intent= new Intent(this, ActivityHome.class);
        }
//        if (type.equals(Constants.NOTIFY_WALLET)){
//            intent= new Intent(this, HomePageActivity.class);
//            intent.putExtra("data","2");
//
//        }
//        else if (type.equals(Constants.NOTIFY_PRODUCT)){
//            intent = new Intent(this, ActivityOrderDetails.class);
//            intent.putExtra("id",orderid);
//        }
//        else if (type.equals(Constants.NOTIFY_PRODUCT_PRODUCT_DETAIL)){
//            intent = new Intent(this, ProductDescribtionActivity.class);
//            intent.putExtra("id", productid);
//            // intent.putExtra("")
//        }
//        else if(UtilsDefault.getSharedPreferenceValue(Constants.usernam)==null){
//            intent = new Intent(this, LoginActvity.class);
//            // intent.putExtra("id", productid);
//        }
//        else {
//            intent = new Intent(this, NotificationActivity.class);
//        }
        //  Intent intent = new Intent(this, NotificationActivity.class);

        // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        // Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        int requestID = (int) System.currentTimeMillis();
        PendingIntent pendingIntent = PendingIntent.getActivity(this, requestID, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();
        bigPictureStyle.setBigContentTitle(title);
        bigPictureStyle.setSummaryText(Html.fromHtml(message).toString());
        bigPictureStyle.bigPicture(bitmap);

        NotificationManager notificationManager = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, "")
                .setTicker(title)
                .setWhen(0)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentIntent(pendingIntent)
                .setSound(defaultSoundUri)
                .setStyle(bigPictureStyle)
                .setSmallIcon(R.drawable.alarm_notify)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.appicons))
                .setContentText(message);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelId = getString(R.string.default_notification_channel_id);
            String name = getString(R.string.default_notification_channel_id);
            NotificationChannel channel = new NotificationChannel(channelId, name,
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription("Avi");
            notificationManager.createNotificationChannel(channel);
            mBuilder.setChannelId(channelId);

        }


        notificationManager.notify(requestID, mBuilder.build());
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }
}
