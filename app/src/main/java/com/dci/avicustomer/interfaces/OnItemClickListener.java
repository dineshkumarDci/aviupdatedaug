package com.dci.avicustomer.interfaces;

import android.view.View;

public interface OnItemClickListener {
    public void OnItemClickListener(int pos, View view);
}
