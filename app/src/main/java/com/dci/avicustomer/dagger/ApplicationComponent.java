package com.dci.avicustomer.dagger;

import android.content.SharedPreferences;


import com.dci.avicustomer.activities.ActivityAddTrip;
import com.dci.avicustomer.activities.ActivityAirlineSearch;
import com.dci.avicustomer.activities.ActivityAppFeedBack;
import com.dci.avicustomer.activities.ActivityAviTips;
import com.dci.avicustomer.activities.ActivityCartView;
import com.dci.avicustomer.activities.ActivityChangePassword;
import com.dci.avicustomer.activities.ActivityCommonWebLoader;
import com.dci.avicustomer.activities.ActivityEatAndDrink;
import com.dci.avicustomer.activities.ActivityEditTrip;
import com.dci.avicustomer.activities.ActivityFacilities;
import com.dci.avicustomer.activities.ActivityFlightSchedule;
import com.dci.avicustomer.activities.ActivityFlightStatus;
import com.dci.avicustomer.activities.ActivityForgotPassword;
import com.dci.avicustomer.activities.ActivityHome;
import com.dci.avicustomer.activities.ActivityLatestNews;
import com.dci.avicustomer.activities.ActivityLogin;
import com.dci.avicustomer.activities.ActivityMyDocsDetails;
import com.dci.avicustomer.activities.ActivityMyDocument;
import com.dci.avicustomer.activities.ActivityMyTripDetails;
import com.dci.avicustomer.activities.ActivityMyTrips;
import com.dci.avicustomer.activities.ActivityOrderTracking;
import com.dci.avicustomer.activities.ActivityRestaurantDetails;
import com.dci.avicustomer.activities.ActivitySearch;
import com.dci.avicustomer.activities.ActivitySetting;
import com.dci.avicustomer.activities.ActivityShopList;
import com.dci.avicustomer.activities.ActivitySignup;
import com.dci.avicustomer.activities.ActivityTransport;
import com.dci.avicustomer.activities.ActivityUpdatedFlightStatus;
import com.dci.avicustomer.activities.ActivityUserReviews;
import com.dci.avicustomer.activities.ActivityUserTips;
import com.dci.avicustomer.activities.WebViewActivity;
import com.dci.avicustomer.adapter.AdapterSectionRecycler;
import com.dci.avicustomer.fragments.FragmentHome;
import com.dci.avicustomer.fragments.FragmentMyCompletedTrips;
import com.dci.avicustomer.fragments.FragmentMyOrders;
import com.dci.avicustomer.fragments.FragmentMyProfile;
import com.dci.avicustomer.fragments.FragmentOfferList;
import com.dci.avicustomer.helpers.ShoppingApplication;

import com.dci.avicustomer.retrofit.RetrofitModule;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

@Singleton
@Component(modules = {AppModule.class, RetrofitModule.class})
public interface ApplicationComponent {
    void inject(ShoppingApplication application);
    Retrofit retrofit();
    void inject(ActivityLogin activityLogin);
    void inject(ActivitySignup activityLogin);
    void inject(ActivityForgotPassword activityLogin);
    void inject(ActivitySearch activityLogin);
    void inject(FragmentHome fragmentHome);
    void inject(ActivityAviTips aviTips);
    void inject(ActivityLatestNews latestNews);
    void inject(ActivityUserTips activityUserTips);
    void inject(ActivityAppFeedBack activityAppFeedBack);
    void inject(ActivityChangePassword activityChangePassword);
    void inject(ActivityFlightStatus activityFlightStatus);
    void inject(ActivityShopList activityShopList);
    void inject(ActivityFacilities facilities);
    void inject(ActivityFlightSchedule flightSchedule);
    void inject(ActivityMyTrips activityMyTrips);
    void inject(ActivityMyTripDetails activityMyTripDetails);
    void inject(ActivityAddTrip activityAddTrip);
    void inject(ActivityTransport activityTransport);
    void inject(ActivityMyDocument activityMyDocument);
    void inject(ActivityMyDocsDetails activityMyDocsDetails);
    void inject(ActivityCommonWebLoader activityCommonWebLoader);
    void inject(ActivityEditTrip activityEditTrip);
    void inject(FragmentOfferList fragmentOfferList);
    void inject(ActivityCartView activityCartView);
    void inject(ActivityRestaurantDetails activityRestaurantDetails);
    void inject(ActivityEatAndDrink activityEatAndDrink);
    void inject(FragmentMyCompletedTrips fragmentMyCompletedTrips);
    void inject(FragmentMyOrders fragmentMyOrders);
    void inject(ActivityOrderTracking activityOrderTracking);
    void inject(FragmentMyProfile fragmentMyProfile);
    void inject(WebViewActivity webViewActivity);
    void inject(ActivityUserReviews activityUserReviews);
    void inject(ActivitySetting activitySetting);
    void inject(AdapterSectionRecycler adapterSectionRecycler);
    void inject(ActivityHome activityHome);
    void inject(ActivityAirlineSearch activityAirlineSearch);
    void inject(ActivityUpdatedFlightStatus activityUpdatedFlightStatus);

}
