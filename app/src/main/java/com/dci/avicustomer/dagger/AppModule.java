package com.dci.avicustomer.dagger;

import android.content.Context;
import android.content.SharedPreferences;

import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.retrofit.AVIAPI;


import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

import static android.content.Context.MODE_PRIVATE;

@Module
public class AppModule {




    private final ShoppingApplication aviapp;

    public AppModule(ShoppingApplication app) {
        this.aviapp = app;
    }
    //provides dependencies (return application class objects)
    @Provides
    @Singleton
    Context provideApplicationContext() {
        return aviapp;
    }

    //provides dependencies (return sharedprference objects)


    //provides dependencies (return API class objects)
    @Provides
    public AVIAPI provideViduApiInterface(Retrofit retrofit) {
        return retrofit.create(AVIAPI.class);
    }
}
