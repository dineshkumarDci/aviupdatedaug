package com.dci.avicustomer.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.avicustomer.R;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;

import com.dci.avicustomer.models.ModelSocialLogin;
import com.dci.avicustomer.models.RegisterModel;
import com.dci.avicustomer.retrofit.AVIAPI;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.TwitterAuthProvider;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityLogin extends BaseActivity implements View.OnClickListener {
    private static final int RC_SIGN_IN = 9001;
    @BindView(R.id.sign_up)
    TextView sign_up;
    @BindView(R.id.btn_signin)
    Button btn_signin;
    @BindView(R.id.tv_forgotPassword)
    TextView tv_forgotPassword;
    @BindView(R.id.img_gmail)
    ImageView img_gmail;
    @BindView(R.id.ed_username)
    EditText ed_username;
    @BindView(R.id.ed_password)
    EditText ed_password;
    @BindView(R.id.img_facebook)
    ImageView img_facebook;
    String TAG = "ActivityLogin";
    @Inject
    public AVIAPI aviapi;
    @BindView(R.id.img_twitter)
    ImageView img_twitter;
    private GoogleSignInClient mGoogleSignInClient;
    private FirebaseAuth mAuth;
    TwitterAuthClient client;
    CallbackManager callbackManager;

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        ShoppingApplication.getContext().getComponent().inject(this);
        sign_up.setOnClickListener(this);
        tv_forgotPassword.setOnClickListener(this);
        img_gmail.setOnClickListener(this);
        btn_signin.setOnClickListener(this);
        img_twitter.setOnClickListener(this);
        img_facebook.setOnClickListener(this);
        callbackManager = CallbackManager.Factory.create();

        configGmail();
        configTwitter();
//        try {
//            PackageInfo info = getPackageManager().getPackageInfo("com.dci.avi",
//                    PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
//            }
//        } catch (PackageManager.NameNotFoundException e) {
//
//        } catch (NoSuchAlgorithmException e) {
//
//        }

    }
    public void configTwitter(){
        TwitterAuthConfig authConfig =  new TwitterAuthConfig(
                getString(R.string.twitter_consumer_key),
                getString(R.string.twitter_consumer_secret));

        TwitterConfig twitterConfig = new TwitterConfig.Builder(this)
                .twitterAuthConfig(authConfig)
                .build();
        Twitter.initialize(twitterConfig);
        client = new TwitterAuthClient();
    }

    @Override
    protected int getlayout() {
        return R.layout.activity_login;
    }

    public void configGmail() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_id))
                .requestEmail()
                .build();
        // [END config_signin]
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mAuth = FirebaseAuth.getInstance();
    }
    public void showAlertEmail(FirebaseUser user,String loginType){

        Gson gson = new Gson();
        String json = gson.toJson(user);

        String email="";


        String img="";

       // Log.d(TAG, "showAlertEmail: "+user.getPhotoUrl());

        try {
            JSONObject jsonObject=new JSONObject(json);
            JSONArray jsonArray=jsonObject.getJSONArray("zztl");
            JSONObject jsonObject1=jsonArray.getJSONObject(1);
            email=jsonObject1.getString("zzif");
           // Log.d(TAG, "showAlertEmail: "+email);



        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!email.equals("")){
            SocialLogin(user.getDisplayName(),email,loginType,
                    user.getUid(),"");
        }

        else if (user.getEmail()==null||user.getEmail().equals("")){
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
// ...Irrelevant code for customizing the buttons and title
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.layout_alert_enter_emailid, null);
            dialogBuilder.setView(dialogView);
            EditText editText=dialogView.findViewById(R.id.ed_emailid);
            Button btn_submit=dialogView.findViewById(R.id.btn_submit);
            btn_submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String mailid=editText.getText().toString().trim();
                    if (mailid.equals("")){
                        editText.setError(getResources().getString(R.string.error_emailid));
                        editText.requestFocus();
                    }
                    else if (!UtilsDefault.isEmailValid(mailid)){
                        editText.setError(getResources().getString(R.string.error_valid_email));
                        editText.requestFocus();
                    }
                    else {
                        SocialLogin(user.getDisplayName(),mailid,loginType,
                                user.getUid(),"");
                    }
                }
            });
            AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            alertDialog.show();
        }
        else {


            SocialLogin(user.getDisplayName(),user.getEmail(),loginType,
                    user.getUid(),"");
        }

    }

    private void signIn() {
        signOut();
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    private void signOut() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        // ...
                    }
                });
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d("data", "firebaseAuthWithGoogle:" + acct.getId());
        // [START_EXCLUDE silent]
        shwProgress();
        // [END_EXCLUDE]

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            Gson gson = new Gson();
                            String json = gson.toJson(user);

                            showAlertEmail(user,"gmail");
//                            SocialLogin(user.getDisplayName(),user.getEmail(),"gmail",
//                                    user.getUid(),"");
                            Log.d(TAG, "onComplete: "+user.getDisplayName()+user.getEmail()+"gmail"+
                                    user.getUid()+"-"+json);
                          //  updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(ActivityLogin.this, "Authentication Failed.", Toast.LENGTH_SHORT).show();
                            // Snackbar.make(findViewById(R.id.main_layout), "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
                            //  updateUI(null);
                        }

                        // [START_EXCLUDE]
                        hideprogress();
                        // hideProgressDialog();
                        // [END_EXCLUDE]
                    }
                });
    }

    private void updateUI(FirebaseUser user) {
        hideprogress();
        if (user != null) {

            Toast.makeText(this, user.getDisplayName() + user.getPhoneNumber(), Toast.LENGTH_SHORT).show();

//            mStatusTextView.setText(getString(R.string.google_status_fmt, user.getEmail()));
//            mDetailTextView.setText(getString(R.string.firebase_status_fmt, user.getUid()));

//            findViewById(R.id.signInButton).setVisibility(View.GONE);
//            findViewById(R.id.signOutAndDisconnect).setVisibility(View.VISIBLE);
        } else {
//            mStatusTextView.setText(R.string.signed_out);
//            mDetailTextView.setText(null);
//
//            findViewById(R.id.signInButton).setVisibility(View.VISIBLE);
//            findViewById(R.id.signOutAndDisconnect).setVisibility(View.GONE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed"+e.getMessage());
                // ...
            }
        }
        callbackManager.onActivityResult(requestCode, resultCode, data);
        (new TwitterLoginButton(this)).onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        if (v == sign_up) {
            finish();
            startActivity(new Intent(ActivityLogin.this, ActivitySignup.class));
        }
        if (v == tv_forgotPassword) {
            finish();
            startActivity(new Intent(ActivityLogin.this, ActivityForgotPassword.class));
        }
        if (v == img_gmail) {
            if(!UtilsDefault.isOnline()){
                Toast.makeText(this, R.string.error_no_net, Toast.LENGTH_SHORT).show();
                return;
            }
            signIn();
        }
        if (v==img_facebook){
            if(!UtilsDefault.isOnline()){
                Toast.makeText(this, R.string.error_no_net, Toast.LENGTH_SHORT).show();
                return;
            }
            LoginManager.getInstance().setLoginBehavior(LoginBehavior.NATIVE_WITH_FALLBACK);

            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));

            LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    handleFacebookAccessToken(loginResult.getAccessToken());
                }

                @Override
                public void onCancel() {
                    Toast.makeText(ActivityLogin.this, "Cancelled", Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onError(FacebookException error) {
                    Toast.makeText(ActivityLogin.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
        if(v==img_twitter){
            if(!UtilsDefault.isOnline()){
                Toast.makeText(this, R.string.error_no_net, Toast.LENGTH_SHORT).show();
                return;
            }
            client.authorize(ActivityLogin.this, new com.twitter.sdk.android.core.Callback<TwitterSession>() {
                @Override
                public void success(Result<TwitterSession> twitterSessionResult) {
                   handleTwitterSession(twitterSessionResult.data);
                }

                @Override
                public void failure(TwitterException e) {

                      Toast.makeText(ActivityLogin.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
        if (v == btn_signin) {

            String uname = ed_username.getText().toString().trim();
            String password = ed_password.getText().toString().trim();
            if (!UtilsDefault.isOnline()){
                Toast.makeText(this, R.string.error_no_net, Toast.LENGTH_SHORT).show();
                return;
            }
            if (uname.equals("")) {
                ed_username.requestFocus();
                ed_username.setError(getString(R.string.error_enter_email));

            } else if (password.equals("")) {
                ed_password.requestFocus();
                ed_password.setError(getString(R.string.error_pass),null);
            }

//            else if(uname.contains("[a-zA-Z]+") == true&&!UtilsDefault.isEmailValid(uname)){
//                ed_username.requestFocus();
//                ed_username.setError(getString(R.string.error_valid_emailor_mobile));
//            }
//
//            else if (uname.matches("[0-9]+")==true&&!UtilsDefault.isValidMobile(uname)){
//                ed_username.requestFocus();
//                ed_username.setError(getString(R.string.error_valid_emailor_mobile));
//            }
            else {
                login(uname,password);


            }
        }

    }
    public void riseSearchActivity(){

    }
    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);
        shwProgress();
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            Gson gson = new Gson();
                            String json = gson.toJson(user);
                            Log.d(TAG, "onComplete: "+user.getDisplayName()+user.getEmail()+"facebook"+
                                    user.getUid()+json);
                            showAlertEmail(user,"facebook");
//                            SocialLogin(user.getDisplayName(),user.getEmail(),"facebook",
//                                    user.getUid(),"");
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(ActivityLogin.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                           // updateUI(null);
                        }
                        hideprogress();

                        // ...
                    }
                });
    }
    private void handleTwitterSession(TwitterSession session) {
        Log.d(TAG, "handleTwitterSession:" + session);
        // [START_EXCLUDE silent]
        shwProgress();
        // [END_EXCLUDE]

        AuthCredential credential = TwitterAuthProvider.getCredential(
                session.getAuthToken().token,
                session.getAuthToken().secret);

        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            showAlertEmail(user,"twitter");
                            Gson gson = new Gson();
                            String json = gson.toJson(user);
//                            SocialLogin(user.getDisplayName(),user.getEmail(),"twitter",
//                                    user.getUid(),"");
                            Log.d(TAG, "onComplete: "+user.getDisplayName()+user.getEmail()+"facebook"+
                                    user.getUid()+json);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(ActivityLogin.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                           // updateUI(null);
                        }

                        // [START_EXCLUDE]
                        hideprogress();
                        // [END_EXCLUDE]
                    }
                });
    }


    public void SocialLogin(String username, String email, String type, String id, String img){
        shwProgress();
        if (!UtilsDefault.isOnline()){
            Toast.makeText(this, R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;
        }

        Log.d(TAG, "SocialLogin: "+username+email+"type"+type+"id"+id+"Image"+img);
        aviapi.socailLogin(email,username,type,id,img,
                UtilsDefault.getFcmSharedPreferenceValue(Constants.FCM_KEY),Constants.DEV_ANDROID).enqueue(new Callback<ModelSocialLogin>() {
            @Override
            public void onResponse(Call<ModelSocialLogin> call, Response<ModelSocialLogin> response) {
                hideprogress();
                if (response.body()!=null){
                    ModelSocialLogin data=response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    UtilsDefault.updateFcmSharedPreference(Constants.API_KEY,data.getJwt_token());
                    if (data.getStatus()== Constants.STATUS_200){
                        try {
                            LoginManager.getInstance().logOut();

                        }
                        catch (Exception e){
                        }

                        UtilsDefault.updateSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                        UtilsDefault.updateSharedPreference(Constants.FIRST_NAME,data.getData().getFirst_name());
                        UtilsDefault.updateSharedPreference(Constants.LAST_NAME,data.getData().getLast_name());
                        UtilsDefault.updateSharedPreference(Constants.MOBILE_NUMBER,data.getData().getMobile());
                        UtilsDefault.updateSharedPreference(Constants.GENDER,data.getData().getGender());
                        UtilsDefault.updateSharedPreference(Constants.EMAILID,data.getData().getEmail());
                        UtilsDefault.updateSharedPreference(Constants.PROFILE_IMG,data.getData().getProfile_img());
                        if (data.getData()!=null&&data.getData().getAirport_id()!=null&&data.getData().getAirport_id()!=0){
                            UtilsDefault.updateSharedPreference(Constants.AIRPORT_ID,String.valueOf(data.getData().getAirport_id()));
                        }
//                        if (data.getData()!=null&&data.getData().getTerminal_id()!=null&&data.getData().getTerminal_id()!=0){
//                            UtilsDefault.updateSharedPreference(Constants.TERMINAL_ID,
//                                    String.valueOf(data.getData().getTerminal_id()));
//                        }
//                        if (data.getData()!=null&&data.getData().getAirport_type()!=null&&data.getData().getAirport_type()!=0){
//                            UtilsDefault.updateSharedPreference(Constants.AIRPORT_ID,String.valueOf(data.getData().getAirport_type()));
//                        }
                        //Toast.makeText(ActivityLogin.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                        riseupToNextscreen();
                        //   UtilsDefault.updateSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));



                    }
                    else if (data.getStatus()==Constants.API_TOKENEXPIRY_STATUS){
                        SocialLogin(username,email,type,id,img);
                    }
                    else {
                        Toast.makeText(ActivityLogin.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Toast.makeText(ActivityLogin.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ModelSocialLogin> call, Throwable t) {
                hideprogress();
                Log.d("failures", "onFailure: "+t.getMessage());

                Toast.makeText(ActivityLogin.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void login(String uname,String password){
        if (!UtilsDefault.isOnline()){
            Toast.makeText(ActivityLogin.this, R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;

        }
        shwProgress();
        aviapi.login(uname,password,Constants.USER_ROLE,
                UtilsDefault.getFcmSharedPreferenceValue(Constants.FCM_KEY),Constants.DEV_ANDROID).enqueue(new Callback<RegisterModel>() {
            @Override
            public void onResponse(Call<RegisterModel> call, Response<RegisterModel> response) {
                hideprogress();


                if (response.body()!=null){
                    RegisterModel data=response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    UtilsDefault.updateSharedPreference(Constants.API_KEY,data.getJwt_token());
                    if (data.getStatus()== Constants.STATUS_200){
                        UtilsDefault.updateSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                        UtilsDefault.updateSharedPreference(Constants.FIRST_NAME,data.getData().getFirst_name());
                        UtilsDefault.updateSharedPreference(Constants.LAST_NAME,data.getData().getLast_name());
                        UtilsDefault.updateSharedPreference(Constants.MOBILE_NUMBER,data.getData().getMobile());
                        UtilsDefault.updateSharedPreference(Constants.GENDER,data.getData().getGender());
                        UtilsDefault.updateSharedPreference(Constants.EMAILID,data.getData().getEmail());
                        UtilsDefault.updateSharedPreference(Constants.USER_IMAGE_PATH,data.getPath());
                        UtilsDefault.updateSharedPreference(Constants.PROFILE_IMG,data.getData().getProfile_img());
                        if (data.getData()!=null&&data.getData().getAirport_id()!=null&&data.getData().getAirport_id()!=0){
                            UtilsDefault.updateSharedPreference(Constants.AIRPORT_ID,
                                    String.valueOf(data.getData().getAirport_id()));
                        }

//                        if (data.getData()!=null&&data.getData().getTerminal_id()!=null&&data.getData().getTerminal_id()!=0){
//                            UtilsDefault.updateSharedPreference(Constants.TERMINAL_ID,
//                                    String.valueOf(data.getData().getTerminal_id()));
//                        }
//                        if (data.getData()!=null&&data.getData().getAirport_type()!=null&&data.getData().getAirport_type()!=0){
//                            UtilsDefault.updateSharedPreference(Constants.AIRPORT_ID,String.valueOf(data.getData().getAirport_type()));
//                        }

                        Toast.makeText(ActivityLogin.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                        riseupToNextscreen();
                     //   UtilsDefault.updateSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));



                    }
                    else {
                        Toast.makeText(ActivityLogin.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Toast.makeText(ActivityLogin.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<RegisterModel> call, Throwable t) {
                hideprogress();

                Toast.makeText(ActivityLogin.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void riseupToNextscreen(){
        if (UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID)!=null&&!
                UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID).equals("")&&
                !UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID).equals("0")){
            Intent intent=new Intent(ActivityLogin.this,ActivityHome.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
        else {
            Intent intent=new Intent(ActivityLogin.this,ActivitySearch.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }


    }
}
