package com.dci.avicustomer.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.dci.avicustomer.BuildConfig;
import com.dci.avicustomer.R;
import com.dci.avicustomer.adapter.AdapterFilterTerminalList;
import com.dci.avicustomer.adapter.AdapterSelectedFiles;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.interfaces.OnImageRemoveListener;
import com.dci.avicustomer.models.CommonMsgModel;
import com.dci.avicustomer.models.ModelAddDocuments;
import com.dci.avicustomer.models.ModelAddTrip;
import com.dci.avicustomer.models.ModelInputAddtrip;
import com.dci.avicustomer.models.ModelNewFlightStatus;
import com.dci.avicustomer.models.ModelSelectedImage;
import com.dci.avicustomer.retrofit.AVIAPI;
import com.github.angads25.filepicker.controller.DialogSelectionListener;
import com.github.angads25.filepicker.model.DialogConfigs;
import com.github.angads25.filepicker.model.DialogProperties;
import com.github.angads25.filepicker.view.FilePickerDialog;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.dynamic.IFragmentWrapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jaiselrahman.filepicker.activity.FilePickerActivity;
import com.jaiselrahman.filepicker.config.Configurations;
import com.jaiselrahman.filepicker.model.MediaFile;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * I have extends this class from Flight status to reuse Flight Status Api
 */

public class ActivityAddTrip extends ActivityFlightStatus implements View.OnClickListener {
    @BindView(R.id.nest_result_lay)
    RelativeLayout nest_result_lay;
    @BindView(R.id.img_add_Bording)
    ImageView img_add_Bording;
    @BindView(R.id.img_add_etick)
    ImageView img_add_etick;
    @BindView(R.id.recycle_boarding_images)
    RecyclerView recycle_boarding_images;
    @BindView(R.id.recycle_eticket_images)
    RecyclerView recycle_eticket_images;
    @BindView(R.id.btn_add_trip)
    Button btn_add_trip;
    @Inject
    public AVIAPI aviapi;
    Dialog mBottomSheetDialog;
    @BindView(R.id.animation_view)
    LottieAnimationView animation_view;
    @BindView(R.id.rel_progress)
    RelativeLayout rel_progress;
    List<MediaFile> listBoarding = new ArrayList<>();
    List<MediaFile> listEticket = new ArrayList<>();
    int sectionType = Integer.parseInt(Constants.SECTION_BOARDING);
    private AdapterSelectedFiles adapterSelectedFiles, adapterETicket;
    StringBuilder selectedBordingPass = new StringBuilder();
    StringBuilder selectedEticket = new StringBuilder();
    ModelInputAddtrip modelInputAddtrip;
    boolean issearchResultSucess = false;
//    @Override
//    public void shwProgress() {
//        rel_progress.setVisibility(View.VISIBLE);
//
//
//    }
//
//    @Override
//    public void hideprogress() {
//        rel_progress.setVisibility(View.GONE);
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        ShoppingApplication.getContext().getComponent().
                inject(this);
        tv_title.setText(getString(R.string.add_trip));
        img_add_Bording.setOnClickListener(this);
        img_add_etick.setOnClickListener(this);
        btn_add_trip.setOnClickListener(this);
        tv_clickhere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ActivityAddTrip.this);
                    alertDialogBuilder.setCancelable(true);
                    LayoutInflater inflater = ActivityAddTrip.this.getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.alert_common_alert_dialog, null);
                    alertDialogBuilder.setView(dialogView);
                    TextView titletv = dialogView.findViewById(R.id.tv_alert_title);
                    titletv.setText(getString(R.string.avi_trips));
                    TextView tv_content = dialogView.findViewById(R.id.tv_content);
                    tv_content.setText(getString(R.string.no_trip_alert_message));
                    btn_ok = dialogView.findViewById(R.id.btn_ok);
                    btn_ok.setText(getString(R.string.add_trip));
                    btn_ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                           if (commonalert!=null){
                               commonalert.dismiss();
                           }
                        }
                    });
                    commonalert = alertDialogBuilder.create();
                    commonalert.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    commonalert.show();

                } else {
                    alertShowLogin(getString(R.string.login_messge));
                }
            }
        });
        intizializeView();
    }

    public void intizializeView() {
        adapterSelectedFiles = new AdapterSelectedFiles(ActivityAddTrip.this,
                listBoarding, 0);
        adapterSelectedFiles.setonItemClick(new OnImageRemoveListener() {
            @Override
            public void OnIMageremove(int pos, View view, int Type) {
                recycle_boarding_images.setVisibility(View.VISIBLE);
                // deletelistBoarding(listBoarding.get(pos).getPath());
                listBoarding.remove(pos);
                adapterSelectedFiles.notifyItemRemoved(pos);

                if (listBoarding.size() == 0) {
                    recycle_boarding_images.setVisibility(View.GONE);
                }
            }
        });

        recycle_boarding_images.setLayoutManager(
                new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,
                        false));
        recycle_boarding_images.setAdapter(adapterSelectedFiles);
        adapterETicket = new AdapterSelectedFiles(ActivityAddTrip.this, listEticket, 1);
        adapterETicket.setonItemClick(new OnImageRemoveListener() {
            @Override
            public void OnIMageremove(int pos, View view, int Type) {
                recycle_eticket_images.setVisibility(View.VISIBLE);
                // deletelistEticket(listEticket.get(pos).getPath());
                listEticket.remove(pos);
                adapterETicket.notifyItemRemoved(pos);
                if (listEticket.size() == 0) {
                    recycle_eticket_images.setVisibility(View.GONE);
                }
            }
        });
        recycle_eticket_images.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.HORIZONTAL, false));
        recycle_eticket_images.setAdapter(adapterETicket);
    }

    public void handleImageSlection(int requestCode, int resultCode,
                                    @Nullable Intent data) {
        try {
            if (requestCode == FILE_REQUEST_CODE && resultCode == RESULT_OK
                    && data != null) {
                if (mBottomSheetDialog != null) {
                    mBottomSheetDialog.cancel();
                }
                if (sectionType == Integer.parseInt(Constants.SECTION_BOARDING)) {
                    int totalcount = listBoarding.size() + data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES).size();
                    if (totalcount > Constants.MAX_FILE_SELECT) {
                        alertcommon(getString(R.string.add_trip),  getString(R.string.maximum_five_files));
                        return;
                    }
                    recycle_boarding_images.setVisibility(View.VISIBLE);
                    listBoarding.addAll(data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES));
                    adapterSelectedFiles.notifyDataSetChanged();
                } else {
                    int totalcount = listEticket.size() + data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES).size();
                    if (totalcount > Constants.MAX_FILE_SELECT) {
                        alertcommon(getString(R.string.add_trip), getString(R.string.maximum_five_files));
                        return;
                    }
                    recycle_eticket_images.setVisibility(View.VISIBLE);
                    listEticket.addAll(data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES));
                    adapterETicket.notifyDataSetChanged();
                }

            } else if (requestCode == IMAGE_REQUEST_CODE && resultCode == RESULT_OK
                    && data != null) {
                if (mBottomSheetDialog != null) {
                    mBottomSheetDialog.cancel();
                }
                if (sectionType == Integer.parseInt(Constants.SECTION_BOARDING)) {
                    int totalcount = listBoarding.size() + data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES).size();
                    if (totalcount > Constants.MAX_FILE_SELECT) {
                        alertcommon(getString(R.string.add_trip), getString(R.string.maximum_five_files));
                        return;
                    }
                    recycle_boarding_images.setVisibility(View.VISIBLE);
                    listBoarding.addAll(data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES));
                    adapterSelectedFiles.notifyDataSetChanged();
                } else {
                    int totalcount = listEticket.size() + data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES).size();
                    if (totalcount > Constants.MAX_FILE_SELECT) {
                        alertcommon(getString(R.string.add_trip), getString(R.string.maximum_five_files));
                        return;
                    }
                    recycle_eticket_images.setVisibility(View.VISIBLE);
                    listEticket.addAll(data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES));
                    adapterETicket.notifyDataSetChanged();
                }

            } else if (requestCode == CAMERA_REQUEST_CODE && resultCode == RESULT_OK
                    && data != null) {
                if (mBottomSheetDialog != null) {
                    mBottomSheetDialog.cancel();
                }
                if (sectionType == Integer.parseInt(Constants.SECTION_BOARDING)) {
                    int totalcount = listBoarding.size() + data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES).size();
                    if (totalcount > Constants.MAX_FILE_SELECT) {
                        alertcommon(getString(R.string.add_trip), getString(R.string.maximum_five_files));
                        return;
                    }
                    recycle_boarding_images.setVisibility(View.VISIBLE);
                    listBoarding.addAll(data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES));
                    adapterSelectedFiles.notifyDataSetChanged();
                } else {
                    int totalcount = listEticket.size() + data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES).size();
                    if (totalcount > Constants.MAX_FILE_SELECT) {
                        alertcommon(getString(R.string.add_trip), getString(R.string.maximum_five_files));
                        return;
                    }
                    recycle_eticket_images.setVisibility(View.VISIBLE);
                    listEticket.addAll(data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES));
                    adapterETicket.notifyDataSetChanged();
                }
            }
        } catch (Exception e) {
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        handleImageSlection(requestCode, resultCode, data);


    }

    public void showBottomFilePIcker(int maxSelect) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {
                    //openSettings();

                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                android.Manifest.permission.CAMERA},
                        101);
                Toast.makeText(this, R.string.enable_camera, Toast.LENGTH_SHORT).show();
                if (!shouldShowRequestPermissionRationale( Manifest.permission.CAMERA)||
                        !shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                    openSettings();
                    Toast.makeText(this, R.string.enable_camera, Toast.LENGTH_SHORT).show();
                    return;
                }
                return;
            }
        }

        mBottomSheetDialog = new Dialog(this, R.style.PickerMaterialDialogSheet);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_bottom_picker, null);
        mBottomSheetDialog.setContentView(dialogView); // your custom view.
        mBottomSheetDialog.setCancelable(true);
        LinearLayout lin_docs_lay = dialogView.findViewById(R.id.lin_docs_lay);
        LinearLayout lin_gallery_lay = dialogView.findViewById(R.id.lin_gallery_lay);
        LinearLayout lin_camera_lay = dialogView.findViewById(R.id.lin_camera_lay);
        LinearLayout lin_scan_lay = dialogView.findViewById(R.id.lin_scan_lay);
        CardView card_cancel = dialogView.findViewById(R.id.card_cancel);
        card_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.cancel();
            }
        });
        lin_gallery_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setImagePicker(maxSelect);
            }
        });
        lin_docs_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFilePicker(maxSelect);
            }
        });
        lin_camera_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setcameraPicker(maxSelect);
            }
        });

        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //Log.d(TAG, "onActivityResulttPermissionsResult: "+requestCode);
        if (requestCode == 101) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                //  Log.d(TAG, "onActivityResulttPermissionsResult: "+PackageManager.PERMISSION_GRANTED);
                showBottomFilePIcker(Constants.MAX_FILE_SELECT);

            } else if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_DENIED ||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                            == PackageManager.PERMISSION_DENIED) {
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                    boolean isdenied = false;
//
//                    for (String permision : permissions) {
//                        isdenied = shouldShowRequestPermissionRationale(permision);
//
//
//                    }
//                    if (!isdenied) {
//                        openSettings();
//                    }
//
//
//                }
               // Toast.makeText(this, R.string.enable_camera, Toast.LENGTH_SHORT).show();

                } else {
                    showBottomFilePIcker(Constants.MAX_FILE_SELECT);
                }
            }
        }

        @Override
        public void closeCommonAlert () {
            commonalert.dismiss();
            // finish();
        }

        public void addTrip (ModelInputAddtrip modelInputAddtrip){
            if (!UtilsDefault.isOnline()) {
                Toast.makeText(ActivityAddTrip.this,
                        R.string.error_no_net, Toast.LENGTH_SHORT).show();
                return;
            }
            shwProgress();
            modelInputAddtrip.setTime_zone(timeZone.getID());
            aviapi.addtrip(modelInputAddtrip).enqueue(new Callback<ModelAddTrip>() {
                @Override
                public void onResponse(Call<ModelAddTrip> call, Response<ModelAddTrip> response) {
                    hideprogress();
                    hideKeyboard();

                    if (response.body() != null) {
                        ModelAddTrip data = response.body();
                        String header = response.headers().get(getString(R.string.header_find_key));
                        UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                        if (data.getStatus() == Constants.STATUS_200) {
                            btn_add_trip.setEnabled(true);
                            Toast.makeText(ActivityAddTrip.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(ActivityAddTrip.this, ActivityHome.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                            //alertcommon(getString(R.string.add_my_tips),data.getMessage());

                            //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                        } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                            addTrip(modelInputAddtrip);
                        } else {
                            btn_add_trip.setEnabled(true);
                            alert(data.getMessage());
                            //alertcommon(getString(R.string.add_my_tips),data.getMessage());
                            //  Toast.makeText(ActivityFlightSchedule.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        btn_add_trip.setEnabled(true);
                        Toast.makeText(ActivityAddTrip.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<ModelAddTrip> call, Throwable t) {
                    btn_add_trip.setEnabled(true);

                    hideprogress();
                    hideKeyboard();
                    Log.d("failure", "onFailure: " + t.getMessage());
                    Toast.makeText(ActivityAddTrip.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }
            });
        }


        @Override
        protected int getlayout () {
            return R.layout.activity_add_trip;
        }

    @Override
    public void showDetailsScreen(ModelNewFlightStatus data) {
        if (data!=null&&data.getData()!=null&&data.getData().getAirline()!=null){
            tv_flight_code.setText(data.getData().getAirline());
            tv_flight_code.setSelected(true);
        }
        if (data!=null&&data.getData()!=null&&data.getData().getAirlineCode()!=null&&
                data!=null&&data.getData()!=null&&data.getData().getFlightNumber()!=null){
            tv_airlinename.setText(data.getData().getAirlineCode() + data.getData().getFlightNumber());

        }

        if (data.getData()!=null&&data.getData().getFlightStatus()!=null&&
                data.getData().getFlightStatus().toLowerCase().equals(Constants.DELAYED)){
            tv_dept_status.setBackground(getDrawable(R.drawable.bg_flight_red_status));
        }
        else {
            tv_dept_status.setBackground(getDrawable(R.drawable.bg_flight_dept_status));
        }
        if (data.getData().getDepGate()==null||data.getData().getDepGate().equals("")){
// holder.tv_arrval_terminal
        }
        else {
            tv_dept_terminal.append(
                    String.valueOf(" , "+data.getData().getDepGate()));
        }

        if (data.getData().getArrGate()==null||data.getData().getArrGate().equals("")){
// holder.tv_arrval_terminal
        }
        else {
            tv_arrival_terminal.append(
                    String.valueOf(" , "+data.getData().getArrGate()));
        }
        if (data.getData()!=null&&data.getData().getDep_delay()!=null&&
                !data.getData().getDep_delay().equals("0")){
//            dep_status.setVisibility(View.VISIBLE);
//            tv_dep_delay.setVisibility(View.VISIBLE);
            tv_dep_delay.setText(getString(R.string.delayed_by)+" "+data.getData().getDep_delay());
            tv_dept_status.setText(getString(R.string.dep_delay)+" "+data.getData().getDep_delay());
            tv_dept_status.setBackground(getDrawable(R.drawable.bg_flight_red_status));

        }
        if (data.getData()!=null&&data.getData().getArr_delay()!=null&&!
                data.getData().getArr_delay().equals("0")){
//            tv_arr_status.setVisibility(View.VISIBLE);
//            tv_arr_delay.setVisibility(View.VISIBLE);
            tv_arr_delay.setText(getString(R.string.delayed_by)+" "+data.getData().getArr_delay());
            tv_dept_status.setText(getString(R.string.arr_delay)+" "+data.getData().getArr_delay());
            tv_dept_status.setBackground(getDrawable(R.drawable.bg_flight_red_status));

        }

    }

    @Override
        public void updateUi (ModelNewFlightStatus flightData){
            super.updateUi(flightData);
            if (flightData != null && flightData.getData() != null && flightData.getStatus() == Constants.STATUS_200) {
                issearchResultSucess = true;
                modelInputAddtrip = new ModelInputAddtrip();
                modelInputAddtrip.setFlight_status(UtilsDefault.checkNull(flightData.getData().getFlightStatus()));
                modelInputAddtrip.setAirlineName(UtilsDefault.checkNull(flightData.getData().getAirline()));
                modelInputAddtrip.setAirport_id(Integer.parseInt(UtilsDefault.checkNull(UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID))));
                modelInputAddtrip.setUser_id(Integer.parseInt(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)));
                modelInputAddtrip.setFlight_code(UtilsDefault.checkNull(flightData.getData().getAirlineCode()) + UtilsDefault.checkNull(flightData.getData().getFlightNumber()));
                modelInputAddtrip.setDep_airport_code(UtilsDefault.checkNull(flightData.getData().getDepCode()));
                modelInputAddtrip.setArr_airport_code(UtilsDefault.checkNull(flightData.getData().getArrCode()));
                modelInputAddtrip.setArr_airport_city(UtilsDefault.checkNull(flightData.getData().getArrCity()));
                modelInputAddtrip.setDep_airport_city(UtilsDefault.checkNull(flightData.getData().getDepCity()));
                modelInputAddtrip.setArr_airport_country(UtilsDefault.checkNull(flightData.getData().getArrCountry()));
                modelInputAddtrip.setDep_airport_country(UtilsDefault.checkNull(flightData.getData().getDeptCountry()));
                if (flightData.getData().getDepTerminal() != null && !flightData.getData().getDepTerminal().equals("")) {
                    modelInputAddtrip.setDep_terminal(flightData.getData().getDepTerminal());
                }
                if (flightData.getData().getArrTerminal() != null && !flightData.getData().getArrTerminal().equals("")) {
                    modelInputAddtrip.setArr_terminal(flightData.getData().getArrTerminal());
                }
                modelInputAddtrip.setDep_date(UtilsDefault.checkNull(flightData.getData().getDepdate()));
                modelInputAddtrip.setArr_date(UtilsDefault.checkNull(flightData.getData().getArrdate()));
                modelInputAddtrip.setDep_time(UtilsDefault.checkNull(flightData.getData().getDepTime()));
                modelInputAddtrip.setArr_time(UtilsDefault.checkNull(flightData.getData().getArrTime()));
                modelInputAddtrip.setDep_airport_name(UtilsDefault.checkNull(flightData.getData().
                        getDepAirportName()));
                modelInputAddtrip.setArr_airport_name(UtilsDefault.checkNull(flightData.getData().
                        getArrAirportName()));
                if (flightData.getData().getArrGate()!=null){
                    modelInputAddtrip.setArrGate(flightData.getData().getArrGate());

                }
                if (flightData.getData().getDepGate()!=null){
                    modelInputAddtrip.setDepGate(flightData.getData().getDepGate());
                }

                search_lay.setVisibility(View.VISIBLE);
                nest_result_lay.setVisibility(View.VISIBLE);
            } else {
                issearchResultSucess = false;
            }

        }
        public void uploadMultiFile (List < MediaFile > filelist, String section, ModelInputAddtrip
        modelInputAddtrip){
            if (!UtilsDefault.isOnline()) {
                Toast.makeText(ActivityAddTrip.this, R.string.error_no_net, Toast.LENGTH_SHORT).show();
                return;
            }
            shwProgress();

            RequestBody sections = RequestBody.create(
                    MediaType.parse("text/plain"),
                    section);

            RequestBody uid = RequestBody.create(MediaType.parse("text/plain"),
                    UtilsDefault.getSharedPreferenceValue(Constants.USER_ID));
            //Log.d("section", "uploadMultiFile: "+section);

            aviapi.uploadMultiFile(getMultiart(filelist), sections, uid).enqueue(new Callback<ModelAddDocuments>() {
                @Override
                public void onResponse(Call<ModelAddDocuments> call, Response<ModelAddDocuments> response) {
                    hideprogress();
                    ModelAddDocuments data = response.body();
                    if (data.getStatus() == Constants.STATUS_200) {
                        if (section.equals(Constants.SECTION_BOARDING)) {
                            selectedBordingPass = new StringBuilder();
                            for (ModelAddDocuments.Datum documents : data.getData()) {
                                if (data.getData().indexOf(documents) == data.getData().size() - 1) {
                                    selectedBordingPass.append(documents.getId());
                                } else {
                                    selectedBordingPass.append(documents.getId() + ",");
                                }
                            }
                            modelInputAddtrip.setBoardingPass(selectedBordingPass);
                            if (listEticket.size() != 0) {

                                uploadMultiFile(listEticket, Constants.SECTION_ETICKET, modelInputAddtrip);
                            } else {


                                addTrip(modelInputAddtrip);
                            }


                        } else {
                            selectedEticket = new StringBuilder();
                            selectedEticket.setLength(0);
                            for (ModelAddDocuments.Datum documents : data.getData()) {
                                if (data.getData().indexOf(documents) == data.getData().size() - 1) {
                                    selectedEticket.append(documents.getId());
                                } else {
                                    selectedEticket.append(documents.getId() + ",");
                                }

                            }
                            modelInputAddtrip.seteTickets(selectedEticket);
                            addTrip(modelInputAddtrip);
                        }

                    }
                    else if (data.getStatus()==Constants.API_TOKENEXPIRY_STATUS){
                        uploadMultiFile(filelist,section,modelInputAddtrip);
                    }


                    else {
                        btn_add_trip.setEnabled(true);
                        alert(data.getMessage());
                        //  Toast.makeText(ActivityMyDocsDetails.this, "Success " + data.getMessage(), Toast.LENGTH_LONG).show();
                        Log.d("failure", "onResponse: " + data.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<ModelAddDocuments> call,
                                      Throwable t) {
                    hideprogress();
                    btn_add_trip.setEnabled(true);
                    alert(getString(R.string.server_error));
                }
            });


        }

        public List<MultipartBody.Part> getMultiart (List < MediaFile > upFileList) {
            List<MultipartBody.Part> parts = new ArrayList<>();
            for (int i = 0; i < upFileList.size(); i++) {
                File file = new File(upFileList.get(i).getPath());
                Log.d("filepath", "uploadMultiFile: " + upFileList.get(i).getPath());
//            Uri uri;
//            if (Build.VERSION.SDK_INT >= 23) {
//                uri = FileProvider.getUriForFile(getApplicationContext(),
//                        BuildConfig.APPLICATION_ID + ".provider", file);
//            } else {
//                uri = Uri.fromFile(file);
//            }
//            byte[] bytes = new byte[0];
//            try {
//                bytes =compress(uri);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }

                MultipartBody.Part fileParts;
                String fileNames = new Date().getTime() +
                        file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("."));
                if (upFileList.get(i).getMimeType().contains("image")) {
                    Uri uri;
                    if (Build.VERSION.SDK_INT >= 23) {
                        uri = FileProvider.getUriForFile(getApplicationContext(),
                                BuildConfig.APPLICATION_ID + ".provider", file);
                    } else {
                        uri = Uri.fromFile(file);
                    }
                    byte[] bytes = new byte[0];
                    try {
                        bytes = compress(uri,file);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    RequestBody fileBody = null;
                    //fileBody = RequestBody.create(MediaType.parse(getContentResolver().getType(uri)),bytes);
                    fileBody = RequestBody.create(MediaType.parse(upFileList.get(i).getMimeType()),
                            bytes);
                    fileParts = MultipartBody.Part.createFormData("file[]", fileNames, fileBody);
                } else {
                    RequestBody fileBody = null;
                    //fileBody = RequestBody.create(MediaType.parse(getContentResolver().getType(uri)),file);
                    fileBody = RequestBody.create(MediaType.parse(upFileList.get(i).getMimeType()),
                            file);
                    fileParts = MultipartBody.Part.createFormData("file[]",
                            fileNames, fileBody);

                }

                parts.add(fileParts);
            }
            return parts;
        }

        @Override
        public void onClick (View v){
            super.onClick(v);
            if (v == img_add_Bording) {
                if (listBoarding.size() >= Constants.MAX_FILE_SELECT) {
                    alertcommon(getString(R.string.add_trip), getString(R.string.maximum_five_files));
                    return;
                }

                sectionType = Integer.parseInt(Constants.SECTION_BOARDING);
                showBottomFilePIcker(Constants.MAX_FILE_SELECT);
            }
            if (v == img_add_etick) {
                if (listEticket.size() >= Constants.MAX_FILE_SELECT) {
                    alertcommon(getString(R.string.add_trip), getString(R.string.maximum_five_files));
                    return;
                }
                sectionType = Integer.parseInt(Constants.SECTION_ETICKET);
                showBottomFilePIcker(Constants.MAX_FILE_SELECT);
            }
            if (v == btn_add_trip && btn_add_trip.getText() == getString(R.string.add_to_my_trips)) {
                if (!issearchResultSucess) {
                    alert(getString(R.string.please_select_trip));
                    return;
                }
                if (listBoarding.size() != 0) {
                    btn_add_trip.setEnabled(false);
                    Toast.makeText(this, getString(R.string.uploading), Toast.LENGTH_SHORT).show();
                    selectedBordingPass.setLength(0);
                    uploadMultiFile(listBoarding, Constants.SECTION_BOARDING, modelInputAddtrip);

                } else if (listEticket.size() != 0) {
                    btn_add_trip.setEnabled(false);
                    Toast.makeText(this, getString(R.string.uploading), Toast.LENGTH_SHORT).show();
                    selectedEticket.setLength(0);
                    uploadMultiFile(listEticket, Constants.SECTION_ETICKET, modelInputAddtrip);
                } else {
                    addTrip(modelInputAddtrip);

                }


            }
        }
        public void deletelistBoarding(String path){
            for (MediaFile file : listBoarding) {
                if (file.getPath().equals(path)) {
                    listBoarding.remove(file);
                }
            }
        }
        public void deletelistEticket (String path){
            for (MediaFile file : listEticket) {
                if (file.getPath().equals(path)) {
                    listEticket.remove(file);
                }
            }
        }

        @Override
        public void onBackPressed () {
            if (!btn_add_trip.isEnabled()) {
                Toast.makeText(this, getString(R.string.processing_image), Toast.LENGTH_SHORT).show();
            } else {
                super.onBackPressed();
            }
        }

        public byte[] compress (Uri selectedImage,File file) throws FileNotFoundException {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage),
                    null, o);
            final int REQUIRED_SIZE = 400;
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
                    break;
                }
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            Bitmap bitmap = BitmapFactory.decodeStream(
                    getContentResolver().openInputStream(selectedImage), null, o2);

            ExifInterface exif = null;
            try {

                exif = new ExifInterface(file.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
            }

            int orientation = ExifInterface.ORIENTATION_NORMAL;

            if (exif != null)
                orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    bitmap = rotateBitmap(bitmap, 90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    bitmap = rotateBitmap(bitmap, 180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    bitmap = rotateBitmap(bitmap, 270);
                    break;
            }

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            return byteArray;
        }
    public static Bitmap rotateBitmap(Bitmap bitmap, int degrees) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }
        public Bitmap getBitmap (String filePath){
            File sd = Environment.getExternalStorageDirectory();
            File imageFile = new File(sd + filePath);
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath(), bmOptions);
            return bitmap;
        }


    }
