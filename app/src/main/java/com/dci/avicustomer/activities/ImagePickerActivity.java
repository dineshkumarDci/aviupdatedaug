package com.dci.avicustomer.activities;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;


import com.dci.avicustomer.BuildConfig;
import com.dci.avicustomer.R;
import com.dci.avicustomer.helpers.MarshMallowPermission;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URISyntaxException;


public abstract class ImagePickerActivity extends BaseActivity {
    private static final int SELECT_PICTURE = 0;
    private static final int REQUEST_CAMERA = 1;
    private static final int PERMISSION_REQUEST_CODE = 200;
    private boolean isCameraOrGallery;

    public void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Gallery",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    isCameraOrGallery = true;
                    if (Build.VERSION.SDK_INT >= 23) {
                        checkCameraPermissionAboveMarshmallow();
                    } else {
                        openCamera();
                    }
                } else if (items[item].equals("Choose from Gallery")) {
                    isCameraOrGallery = false;
                    if (Build.VERSION.SDK_INT >= 23) {
                        checkGalleryPermissionAboveMarshmallow();
                    } else {
                        openGallery();
                    }
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");

        startActivityForResult(
                Intent.createChooser(intent, "Select File"),
                SELECT_PICTURE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                File f = new File(Environment.getExternalStorageDirectory()
                        .toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
                try {
                    Bitmap bm;
                    BitmapFactory.Options btmapOptions = new BitmapFactory.Options();
                    btmapOptions.inSampleSize = 2;
                    bm = BitmapFactory.decodeFile(f.getAbsolutePath(),
                            btmapOptions);

                    if (Build.VERSION.SDK_INT >= 23) {
                        Uri photoURI = FileProvider.getUriForFile(getApplicationContext(),
                                BuildConfig.APPLICATION_ID + ".provider", f);
                        onImageSelect(bitmapToBase64(compress(photoURI)), decodeUri(photoURI),photoURI,f);
                    } else {
                        // onImageSelect(bitmapToBase64(bm), decodeUri(Uri.fromFile(f)));
                        onImageSelect(bitmapToBase64(decodeUri(Uri.fromFile(f))),
                                decodeUri(Uri.fromFile(f)),Uri.fromFile(f),f);
                    }
                    String path = Environment
                            .getExternalStorageDirectory()
                            + File.separator
                            + "test";
                   // f.delete();

                    OutputStream fOut = null;
                    File file = new File(path, String.valueOf(System
                            .currentTimeMillis()) + ".jpg");
                    fOut = new FileOutputStream(file);
                    bm.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
                    fOut.flush();
                    fOut.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == SELECT_PICTURE) {
                try {
                    Uri uri = data.getData();
                    // String path = uri.getPath();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String filePath = cursor.getString(columnIndex);
                    cursor.close();
                    File file = new File(filePath);

                    // Get length of file in bytes
                    long fileSizeInBytes = file.length();
                    // Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
                    long fileSizeInKB = fileSizeInBytes / 1024;
                    //  Convert the KB to MegaBytes (1 MB = 1024 KBytes)
                    long fileSizeInMB = fileSizeInKB / 1024;
                    Log.d("image", "onActivityResult: "+fileSizeInMB+uri.getPath()+"size-"+fileSizeInBytes);
                    if (fileSizeInMB > 12) {
                        Log.d("image", "onActivityResult: "+fileSizeInMB);
                        Toast.makeText(this, "File size is to large..", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    onSelectFromGalleryResult(data);
                }
                catch (Exception e){
                    //Toast.makeText(this, R.string.image_error, Toast.LENGTH_SHORT).show();
                }


               /* Uri selectedImageUri = data.getData();
                String tempPath = getPath(selectedImageUri);
                Bitmap bm;
                BitmapFactory.Options btmapOptions = new BitmapFactory.Options();
                btmapOptions.inSampleSize = 2;
                bm = BitmapFactory.decodeFile(tempPath, btmapOptions);
                onImageSelect(bitmapToBase64(bm), bm);*/
            }
        }

    }


    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            //   try {
            Uri selectedImage = data.getData();
            // bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                /*ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 90, bytes);*/
            /*} catch (IOException e) {
                e.printStackTrace();
            }*/
            try {

                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);

                onImageSelect(bitmapToBase64(compress(selectedImage)), decodeUri(selectedImage),selectedImage,null);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //ivImage.setImageBitmap(bm);

    }


    private Bitmap compress(Uri selectedImage) throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o);
        final int REQUIRED_SIZE = 400;
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(
                getContentResolver().openInputStream(selectedImage), null, o2);
    }

    private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(
                getContentResolver().openInputStream(selectedImage), null, o);

        final int REQUIRED_SIZE = 400;

        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(
                getContentResolver().openInputStream(selectedImage), null, o2);
    }


    void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File f = new File(Environment
                .getExternalStorageDirectory(), "temp.jpg");

        if (Build.VERSION.SDK_INT >= 23) {
            Uri photoURI = FileProvider.getUriForFile(getApplicationContext(),
                    BuildConfig.APPLICATION_ID + ".provider",
                    f);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            startActivityForResult(intent, REQUEST_CAMERA);
        } else {
            Uri photoURI = Uri.fromFile(f);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            startActivityForResult(intent, REQUEST_CAMERA);
        }
//        Uri photoURI = FileProvider.getUriForFile(getApplicationContext(),
//                BuildConfig.APPLICATION_ID + ".provider",
//               f);
        //intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));

    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        cursor.moveToFirst();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        return cursor.getString(column_index);
    }

    public String bitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
        //  Log.d("encodedBitmap", encoded);
        return encoded;
    }

    public Bitmap base64ToBitmap(String encodedImage) {
        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }


    protected abstract void onImageSelect(String Base64, Bitmap bmp,Uri uri,File file);

    private void checkCameraPermissionAboveMarshmallow() {
        if (MarshMallowPermission.checkPermissionForCamera(this) && MarshMallowPermission.checkPermissionForExternalStorage(this)) {
            openCamera();
        } else {
            showPermissionDialog();

            Toast.makeText(this, R.string.enable_camera, Toast.LENGTH_SHORT).show();


        }
    }

    private void checkGalleryPermissionAboveMarshmallow() {
        if (MarshMallowPermission.checkPermissionForExternalStorage(this)) {
            openGallery();
        } else {
            showPermissionDialog();
            Toast.makeText(this, R.string.enable_camera, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Method for display the permission dialog for marshmallow
     */
    @TargetApi(Build.VERSION_CODES.M)
    void showPermissionDialog() {
        try {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    android.Manifest.permission.CAMERA}, PERMISSION_REQUEST_CODE);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE && (grantResults.length > 0) && (grantResults[0] +
                grantResults[1]) == PackageManager.PERMISSION_GRANTED) {
            if (isCameraOrGallery) {
                openCamera();
            } else {
                openGallery();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }


}
