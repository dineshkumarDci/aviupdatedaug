package com.dci.avicustomer.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.avicustomer.R;
import com.dci.avicustomer.adapter.AdapterCartView;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.models.ModelCartView;
import com.dci.avicustomer.models.ModelOrderTracking;
import com.dci.avicustomer.retrofit.AVIAPI;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityOrderTracking extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.img_order_placed)
    ImageView img_order_placed;
    @BindView(R.id.img_order_confirm)
    ImageView img_order_confirm;
    @BindView(R.id.im_order_proccess)
    ImageView im_order_proccess;
    @BindView(R.id.img_order_ready)
    ImageView img_order_ready;
    @BindView(R.id.view_order_placed)
    View view_order_placed;
    @BindView(R.id.view_line_order_confirm)
    View view_line_order_confirm;
    @BindView(R.id.view_order_proccess)
    View view_order_proccess;
    @BindView(R.id.tv_order_placed)
    TextView tv_order_placed;
    @BindView(R.id.tv_receive_order)
    TextView tv_receive_order;
    @BindView(R.id.tvorder_confirm)
    TextView tvorder_confirm;
    @BindView(R.id.tv_order_confirm_msg)
    TextView tv_order_confirm_msg;
    @BindView(R.id.tv_order_proccess)
    TextView tv_order_proccess;
    @BindView(R.id.tv_order_proccess_msg)
    TextView tv_order_proccess_msg;
    @BindView(R.id.tv_order_ready)
    TextView tv_order_ready;
    @BindView(R.id.tv_order_ready_msg)
    TextView tv_order_ready_msg;
    @BindView(R.id.tv_view)
    TextView tv_view;
    @BindView(R.id.order_main_msg)
    TextView order_main_msg;
    @BindView(R.id.tv_order_time)
    TextView tv_order_time;
    @BindView(R.id.rel_bottom_lay)
    RelativeLayout rel_bottom_lay;
    @BindView(R.id.tv_order_id)
    TextView tv_order_id;
    @BindView(R.id.btn_track_other_orders)
    Button btn_track_other_orders;

    private Handler delayHandler = new Handler();
    private Runnable delayrunnable;
    @Inject
    public AVIAPI aviapi;
    String type="";

    @Override
    public void onBackPressed() {
        if (type.equals("")){
            super.onBackPressed();
        }
        else {
            Intent intent=new Intent(ActivityOrderTracking.this,ActivityHome.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        ShoppingApplication.getContext().getComponent().inject(this);
        tv_title.setText(getResources().getString(R.string.title_order_track));
        tv_view.setOnClickListener(this);
        if (getIntent().getStringExtra("id") != null) {
            getOrderDetails(getIntent().getStringExtra("id"));
            tv_order_id.setText(getIntent().getStringExtra("id"));

        }
        if (getIntent().getStringExtra("type")!=null){
            type=getIntent().getStringExtra("type");

        }
        btn_track_other_orders.setOnClickListener(this);
       // getOrderDetails("AVI518392368");

    }

    public void getOrderDetails(String orderId) {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityOrderTracking.this, R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;
        }
        shwProgress();
        //  Log.d("aviparms", "search: "+UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)+search);
        String userid = "0";
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
            userid = UtilsDefault.getSharedPreferenceValue(Constants.USER_ID);
        }
        aviapi.getOrderDetails(orderId,UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)).
                enqueue(new Callback<ModelOrderTracking>() {
            @Override
            public void onResponse(Call<ModelOrderTracking> call, Response<ModelOrderTracking> response) {
                hideprogress();
                hideKeyboard();
                if (response.body() != null) {
                    ModelOrderTracking data = response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus() == Constants.STATUS_200) {
                        FetchOrder(data.getOrder().getOrderStatusId(),data.getOrder().getOrderStatus());
                        setWaitingTime(data.getOrder().geteTATime(),data.getOrder().getDate());
                        tv_order_id.setText(data.getOrder().getOrder_id());
                        //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                    } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                        getOrderDetails(orderId);

                    } else {
                        alertcommon(getResources().getString(R.string.title_order_track),
                               data.getMessage());
                    }
                } else {
                    alertcommon(getResources().getString(R.string.title_order_track),
                            getString(R.string.nodata));
                    //   Toast.makeText(ActivityMyTrips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ModelOrderTracking> call, Throwable t) {
                hideprogress();
                hideKeyboard();
                alertcommon(getResources().getString(R.string.title_order_track), getString(R.string.nodata));
                Log.d("failure", "onFailure: " + t.getMessage());
                //Toast.makeText(ActivityMyTrips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void closeCommonAlert() {
        finish();
    }

    public void setWaitingTime(String delaytime, String orderTime){
        delayrunnable = new Runnable() {
            @Override
            public void run() {
                try {
                    delayHandler.postDelayed(this, 1000);
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    Calendar curentcal = Calendar.getInstance();
                    if (UtilsDefault.getSharedPreferenceValue(Constants.TIME_ZONE) != null) {
                        TimeZone tz = TimeZone.getTimeZone(UtilsDefault.
                                getSharedPreferenceValue(Constants.TIME_ZONE));
                      //  curentcal.setTimeZone(tz);
                       // format.setTimeZone(tz);
                    }
                    Date event_date = format.parse(orderTime);
                    Date current_date = curentcal.getTime();
                    Calendar eventCal=Calendar.getInstance();
                    eventCal.setTime(event_date);
                    int hr,min;
                    if (orderTime!=null&&orderTime.contains(":")){
                        String time[]=delaytime.split(":");
                        hr=Integer.parseInt(time[0]);
                        min=Integer.parseInt(time[1]);
                        eventCal.add(Calendar.HOUR_OF_DAY, hr);
                        eventCal.add(Calendar.MINUTE, min);

                    }


                    if (!current_date.after(eventCal.getTime())) {
                        Date date=eventCal.getTime();

                        long diff =date.getTime()- current_date.getTime() ;
                       // long Days = diff / (24 * 60 * 60 * 1000);
                        long Hours = diff / (60 * 60 * 1000) % 24;
                        long Minutes = diff / (60 * 1000) % 60;
                        long Seconds = diff / 1000 % 60;
                        //
                        //  tv_days.setText(String.format("%02d", Days));
                        //   Log.d("coundown", "countDownStart: "+Hours);

                        //tv_hrs.setText(String.format("%02d", Hours));
                        StringBuffer stringBuffer=new StringBuffer();
                        if (!String.format("%02d", Hours).equals("00")){
                            stringBuffer.append(String.format("%02d", Hours)+" Hrs ");
                        }
                        if (!String.format("%02d", Minutes).equals("00")){
                            stringBuffer.append(String.format("%02d", Minutes)+" Mins");
                        }
                        tv_order_time.setVisibility(View.VISIBLE);
                        tv_order_time.setText(stringBuffer);
                        Log.d("coundown", "countDownStart: "+"timePassed"+String.format("%02d", Hours)+"MIns"+
                                String.format("%02d", Minutes)+"Sec"+String.format("%02d", Minutes));
                       // tv_secs.setText(String.format("%02d", Seconds));

                    } else {
                        tv_order_time.setVisibility(View.GONE);
                        Log.d("coundown", "countDownStart: "+"timePassed");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                     Log.d("coundown", "countDownStart: "+e.getMessage());
                }
            }
        };
        delayHandler.postDelayed(delayrunnable, 0);
    }
    protected void onStop() {
        super.onStop();
        delayHandler.removeCallbacks(delayrunnable);
    }


    //  this method is used to fetch order details
    @SuppressLint("SetTextI18n")
    public void FetchOrder(int status, String msg) {
        switch (status) {
            case Constants.ORDER_PAYED:
                img_order_placed.setImageDrawable(getResources().getDrawable(R.drawable.circle_order_history));
                tv_order_placed.setTextColor(getResources().getColor(R.color.dark_blue));
                tv_receive_order.setTextColor(getResources().getColor(R.color.dark_blue));
                tv_receive_order.setVisibility(View.VISIBLE);
                break;
            case Constants.ORDER_CONFIRMED:
                img_order_placed.setImageDrawable(getResources().getDrawable(R.drawable.circle_order_history_without_border));
                view_order_placed.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                img_order_confirm.setImageDrawable(getResources().getDrawable(R.drawable.circle_order_history));
                tvorder_confirm.setTextColor(getResources().getColor(R.color.dark_blue));
                tv_order_confirm_msg.setTextColor(getResources().getColor(R.color.dark_blue));
                tv_receive_order.setVisibility(View.VISIBLE);
                tv_order_confirm_msg.setVisibility(View.VISIBLE);
                break;
            case Constants.ORDER_PROCESSED:
                img_order_placed.setImageDrawable(getResources().getDrawable(R.drawable.circle_order_history_without_border));
                view_order_placed.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                img_order_confirm.setImageDrawable(getResources().getDrawable(R.drawable.circle_order_history_without_border));
                view_line_order_confirm.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                im_order_proccess.setImageDrawable(getResources().getDrawable(R.drawable.circle_order_history));
                tv_order_proccess.setTextColor(getResources().getColor(R.color.dark_blue));
                tv_order_proccess_msg.setTextColor(getResources().getColor(R.color.dark_blue));
                tv_order_proccess_msg.setVisibility(View.VISIBLE);
                tv_order_confirm_msg.setVisibility(View.VISIBLE);
                rel_bottom_lay.setVisibility(View.VISIBLE);

                break;
            case Constants.ORDER_READY:
                img_order_placed.setImageDrawable(getResources().getDrawable(R.drawable.circle_order_history_without_border));
                view_order_placed.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                img_order_confirm.setImageDrawable(getResources().getDrawable(R.drawable.circle_order_history_without_border));
                view_line_order_confirm.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                im_order_proccess.setImageDrawable(getResources().getDrawable(R.drawable.circle_order_history_without_border));
                view_order_proccess.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                img_order_ready.setImageDrawable(getResources().getDrawable(R.drawable.circle_order_history));
                tv_order_ready.setTextColor(getResources().getColor(R.color.dark_blue));
                tv_order_ready_msg.setTextColor(getResources().getColor(R.color.dark_blue));
                tv_order_ready_msg.setVisibility(View.VISIBLE);
                tv_order_confirm_msg.setVisibility(View.VISIBLE);
                tv_order_proccess_msg.setVisibility(View.VISIBLE);
                tv_receive_order.setVisibility(View.VISIBLE);
                break;
            case Constants.ORDER_CANCELED:
                img_order_placed.setImageDrawable(getResources().getDrawable(R.drawable.circle_order_history_without_border));
                view_order_placed.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                img_order_confirm.setImageDrawable(getResources().getDrawable(R.drawable.circle_order_history));
                tvorder_confirm.setTextColor(getResources().getColor(R.color.dark_blue));
                tv_order_confirm_msg.setTextColor(getResources().getColor(R.color.dark_blue));
                tvorder_confirm.setText(getString(R.string.order_canceled));
                tv_order_confirm_msg.setText(getString(R.string.msg_order_canceled));
                tv_order_confirm_msg.setVisibility(View.VISIBLE);
                break;
            case Constants.ORDER_DELIVERED:
                img_order_placed.setImageDrawable(getResources().getDrawable(R.drawable.circle_order_history_without_border));
                view_order_placed.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                img_order_confirm.setImageDrawable(getResources().getDrawable(R.drawable.circle_order_history_without_border));
                view_line_order_confirm.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                im_order_proccess.setImageDrawable(getResources().getDrawable(R.drawable.circle_order_history_without_border));
                view_order_proccess.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                img_order_ready.setImageDrawable(getResources().getDrawable(R.drawable.circle_order_history));
                tv_order_ready.setTextColor(getResources().getColor(R.color.dark_blue));
                tv_order_ready_msg.setTextColor(getResources().getColor(R.color.dark_blue));
                tv_order_ready.setText(getString(R.string.order_delivered));
                tv_order_ready_msg.setVisibility(View.GONE);
                break;


        }
    }

    @Override
    protected int getlayout() {
        return R.layout.activity_order_tracking;
    }

    @Override
    public void onClick(View v) {
        if (v==tv_view){
            Intent intent=new Intent(ActivityOrderTracking.this,
                    ActivityMyProfile.class);
            intent.putExtra("type","1");
            startActivity(intent);
        }
        if (v==btn_track_other_orders){
            Intent intent=new Intent(ActivityOrderTracking.this,
                    ActivityMyProfile.class);
            intent.putExtra("type","1");
            startActivity(intent);
        }

    }
}
