package com.dci.avicustomer.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dci.avicustomer.R;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.models.CommonMsgModel;
import com.dci.avicustomer.retrofit.AVIAPI;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityAppFeedBack extends BaseActivity implements View.OnClickListener {
    @Inject
    public AVIAPI aviapi;
    @BindView(R.id.ed_username)
    EditText ed_username;
    @BindView(R.id.ed_email_address)
    EditText ed_email_address;
    @BindView(R.id.ed_phone_num)
    EditText ed_phone_num;
    @BindView(R.id.ed_message)
    EditText ed_message;
    @BindView(R.id.btn_submit)
    Button btn_submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        ShoppingApplication.getContext().getComponent().inject(this);
        tv_title.setText(R.string.title_feedback);
        btn_submit.setOnClickListener(this);
    }

    @Override
    protected int getlayout() {
        return R.layout.activity_app_feedback;
    }

    public void submitReview(String uname, String email, String phone, String message) {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityAppFeedBack.this,
                    R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;
        }
        shwProgress();
        aviapi.submitReview(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID),
                uname, email, phone, message).enqueue(new Callback<CommonMsgModel>() {
            @Override
            public void onResponse(Call<CommonMsgModel> call, Response<CommonMsgModel> response) {
                hideprogress();
                if (response.body() != null) {
                    CommonMsgModel data = response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus() == Constants.STATUS_200) {
                        ed_email_address.setText("");
                        ed_message.setText("");
                        ed_phone_num.setText("");
                        ed_username.setText("");
                        alertcommon(getString(R.string.title_feedback),
                                getString(R.string.content_feedback));
                        //   UtilsDefault.updateSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                    } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                        submitReview(uname, email, phone, message);
                    } else {
                        Toast.makeText(ActivityAppFeedBack.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ActivityAppFeedBack.this,
                            R.string.server_error, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CommonMsgModel> call, Throwable t) {
                hideprogress();
                Toast.makeText(ActivityAppFeedBack.this,
                        R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == btn_submit) {
            String name = ed_username.getText().toString().trim();
            String email = ed_email_address.getText().toString().trim();
            String phonenum = ed_phone_num.getText().toString().trim();
            String mesg = ed_message.getText().toString().trim();
            if (name.equals("")) {
                ed_username.setError(getString(R.string.this_feild_empty));
                ed_username.requestFocus();
            } else if (email.equals("")) {
                ed_email_address.setError(getString(R.string.this_feild_empty));
                ed_email_address.requestFocus();
            } else if (!UtilsDefault.isEmailValid(email)) {
                ed_email_address.setError(getString(R.string.error_valid_email));
                ed_email_address.requestFocus();
            } else if (phonenum.equals("")) {
                ed_phone_num.setError(getString(R.string.this_feild_empty));
                ed_phone_num.requestFocus();
            } else if (mesg.equals("")) {
                ed_message.setError(getString(R.string.this_feild_empty));
                ed_message.requestFocus();
            } else {
                submitReview(name, email, phonenum, mesg);
            }

        }

    }
}
