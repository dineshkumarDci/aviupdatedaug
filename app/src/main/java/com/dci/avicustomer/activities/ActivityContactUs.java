package com.dci.avicustomer.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.dci.avicustomer.R;
import com.dci.avicustomer.helpers.ShoppingApplication;
import butterknife.BindView;
import butterknife.ButterKnife;
import static com.facebook.internal.CallbackManagerImpl.RequestCodeOffset.Share;

public class ActivityContactUs extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.tv_phonenum)
    TextView tv_phonenum;
    @BindView(R.id.tv_email)
    TextView tv_email;
    @BindView(R.id.tv_website)
    TextView tv_website;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        tv_phonenum.setOnClickListener(this);
        tv_email.setOnClickListener(this);
        tv_website.setOnClickListener(this);
        tv_title.setText(R.string.title_contactus);
    }

    @Override
    protected int getlayout() {
        return R.layout.activity_contactus;
    }

    @Override
    public void onClick(View v) {
        if (v==tv_email){
            try {
                Intent intent = new Intent (Intent.ACTION_VIEW , Uri.parse("mailto:" +
                        tv_email.getText().toString().trim()));
                startActivity(intent);
            } catch(Exception e) {
                Toast.makeText(this, "Sorry...You don't have any mail app", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
//            final Intent intent = new Intent(Intent.ACTION_VIEW)
//                    .setType("plain/text")
//                    .setData(Uri.parse("Avi@gmail.com"))
//                    .setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail")
//                    .putExtra(Intent.EXTRA_SUBJECT, "test")
//                    .putExtra(Intent.EXTRA_TEXT, "hello. this is a message sent from my demo app :-)");
//            startActivity(intent);
        }

    }
}
