package com.dci.avicustomer.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dci.avicustomer.R;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.models.CommonMsgModel;
import com.dci.avicustomer.retrofit.AVIAPI;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityChangePassword extends BaseActivity implements View.OnClickListener {
    @Inject
    public AVIAPI aviapi;
    @BindView(R.id.ed_current_pass)
    EditText ed_current_pass;
    @BindView(R.id.ed_new_pass)
    EditText ed_new_pass;
    @BindView(R.id.ed_confirm_password)
    EditText ed_confirm_password;
    @BindView(R.id.btn_save_changes)
    Button btn_save_changes;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        ShoppingApplication.getContext().getComponent().inject(this);
        tv_title.setText(getString(R.string.title_change_password));
        btn_save_changes.setOnClickListener(this);
    }
    @Override
    protected int getlayout() {
        return R.layout.activity_change_password ;
    }
    public void changePassword(String currentpass,String newpass,String confirmpass) {
        if (!UtilsDefault.isOnline()){
            Toast.makeText(ActivityChangePassword.this,
                    R.string.error_no_net,
                    Toast.LENGTH_SHORT).show();
            return;
        }
        shwProgress();
        aviapi.changePassword(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID),
                currentpass,newpass,confirmpass).enqueue(new Callback<CommonMsgModel>() {
            @Override
            public void onResponse(Call<CommonMsgModel> call, Response<CommonMsgModel> response) {
                hideprogress();
                if (response.body() != null) {
                    CommonMsgModel data = response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus() == Constants.STATUS_200) {
                        alertcommon(getString(R.string.title_change_password),
                                data.getMessage());
                        ed_confirm_password.setText("");
                        ed_current_pass.setText("");
                        ed_new_pass.setText("");

                        // Toast.makeText(ActivityForgotPassword.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                        //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));


                    }
                    else if (data.getStatus()==Constants.API_TOKENEXPIRY_STATUS){
                        changePassword(currentpass,newpass,confirmpass);
                    }
                    else {
                        alert( data.getMessage());
                        //   Toast.makeText(ActivityForgotPassword.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ActivityChangePassword.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<CommonMsgModel> call, Throwable t) {
                hideprogress();
                Toast.makeText(ActivityChangePassword.this,  R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v==btn_save_changes){
            String curentpass=ed_current_pass.getText().toString().trim();
            String newpass=ed_new_pass.getText().toString().trim();
            String confirmpass=ed_confirm_password.getText().toString().trim();
            if(curentpass.equals("")){
                ed_current_pass.setError(getString(R.string.this_feild_empty),null);
                ed_current_pass.requestFocus();

            }
            else if (newpass.equals("")){
                ed_new_pass.setError(getString(R.string.this_feild_empty),null);
                ed_new_pass.requestFocus();
            }
            else if (!UtilsDefault.isValidPassword(newpass)){
                ed_new_pass.setError(getString(R.string.error_password_short),null);
                ed_new_pass.requestFocus();
            }
            else if (confirmpass.equals("")){
                ed_confirm_password.setError(getString(R.string.this_feild_empty),null);
                ed_confirm_password.requestFocus();
            }
            else {
                changePassword(curentpass,newpass,confirmpass);
            }
        }

    }
}
