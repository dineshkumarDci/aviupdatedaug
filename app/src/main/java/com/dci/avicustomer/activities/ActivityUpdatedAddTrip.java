package com.dci.avicustomer.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.avicustomer.BuildConfig;
import com.dci.avicustomer.R;
import com.dci.avicustomer.adapter.AdapterUpdatedAddTrip;
import com.dci.avicustomer.adapter.AdapterUpdatedFlightStatus;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.ModelAddDocuments;
import com.dci.avicustomer.models.ModelAddTrip;
import com.dci.avicustomer.models.ModelInputAddtrip;
import com.dci.avicustomer.models.ModelUpcomingtrips;
import com.dci.avicustomer.models.ModelUpdatedFlightStatus;
import com.jaiselrahman.filepicker.activity.FilePickerActivity;
import com.jaiselrahman.filepicker.model.MediaFile;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityUpdatedAddTrip extends
        ActivityUpdatedFlightStatus implements OnItemClickListener {
    @BindView(R.id.tv_total_flight_number)
    TextView tv_total_flight_number;
    @BindView(R.id.lin_result_lay)
    LinearLayout lin_result_lay;
    List<ModelUpdatedFlightStatus.Data> flightStatusList = new ArrayList<>();
    AdapterUpdatedAddTrip adapterUpdatedAddTrip;
    Dialog mBottomSheetDialog;
    int selectedPos = -1;
    int selectionType = -1;
    StringBuilder selectedBordingPass = new StringBuilder();
    StringBuilder selectedEticket = new StringBuilder();
    ModelInputAddtrip modelInputAddtrip;
    AlertDialog addSucessfullyAlert;
    boolean isAddingTrip = false;
    String data = "data";


    //this method wil be called when activity inizialized
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        ShoppingApplication.getContext().getComponent().inject(this);
        btn_search.setOnClickListener(this);
        date_lay.setOnClickListener(this);
        tv_title.setText(getString(R.string.add_trip));
        ed_airline.setFocusable(false);
        ed_airline.setOnClickListener(this);
        initizilizeView();
    }


    // this method is used to initizialize views from base activity
    @Override
    public void initizilizeView() {
        adapterUpdatedAddTrip =
                new AdapterUpdatedAddTrip(ActivityUpdatedAddTrip.this, flightStatusList, 0);
        recyclerView.setLayoutManager(new LinearLayoutManager(ActivityUpdatedAddTrip.this));
        adapterUpdatedAddTrip.setonItemClick(this);
        recyclerView.setAdapter(adapterUpdatedAddTrip);
    }



    @Override
    protected int getlayout() {
        return R.layout.layout_updated_addtrip;
    }

    @Override
    public void onBackPressed() {
        if (isAddingTrip) {
            Toast.makeText(this, getString(R.string.processing_image), Toast.LENGTH_SHORT).show();
            return;
        }
//To view SearchLayout
        if (search_lay.getVisibility() == View.GONE) {
            search_lay.setVisibility(View.VISIBLE);
            lin_result_lay.setVisibility(View.GONE);
            // tv_total_flight_number.setVisibility(View.GONE);
        }
        else {
//           UtilsDefault.updateSharedPreference(Constants.SELECTED_AIRLINECODE, null);
//           UtilsDefault.updateSharedPreference(Constants.SELECTED_AIRLINENAME, null);
            finish();
        }
    }


    //Override method to get flight status.
    @Override
    public void getflightStatus(String airlines, String flightid, String date) {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityUpdatedAddTrip.this,
                    R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;
        }
        Log.d("finput", "getflightStatus: " + airlines + "-" + flightid + "-" + date);
        shwProgress();
        aviapi.getUpdatedFlightStatus(flightid, date).enqueue(new Callback<ModelUpdatedFlightStatus>() {
            @Override
            public void onResponse(Call<ModelUpdatedFlightStatus> call, Response<ModelUpdatedFlightStatus> response) {
                hideprogress();
                hideKeyboard();
                if (response.body() != null) {
                    ModelUpdatedFlightStatus data = response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus() == Constants.STATUS_200) {
                        if (data.getData() != null && data.getData().size() != 0) {
                            nest_scroll.scrollTo(0, 0);
                            search_lay.setVisibility(View.GONE);
                            lin_result_lay.setVisibility(View.VISIBLE);
                            flightStatusList.clear();
                            flightStatusList.addAll(data.getData());
                            tv_total_flight_number.setText(flightStatusList.size() + " Trips found");
                            adapterUpdatedAddTrip.notifyDataSetChanged();
//                            AdapterUpdatedFlightStatus adapterUpdatedFlightStatus =
//                                    new AdapterUpdatedFlightStatus(ActivityUpdatedAddTrip.this, flightStatusList, 0);
//                            recyclerView.setAdapter(adapterUpdatedFlightStatus);
//                            recyclerView.setLayoutManager(new LinearLayoutManager(ActivityUpdatedAddTrip.this));
                        }
                    } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                        getflightStatus(airlines, flightid, date);
                    } else {
                        lin_result_lay.setVisibility(View.GONE);
                        search_lay.setVisibility(View.VISIBLE);
                        Toast.makeText(ActivityUpdatedAddTrip.this, data.getMessage(),
                                Toast.LENGTH_SHORT).show();
                    }
                } else {
                    // updateUi(null);
                    search_lay.setVisibility(View.VISIBLE);
                    lin_result_lay.setVisibility(View.GONE);
                    Toast.makeText(ActivityUpdatedAddTrip.this,
                            R.string.server_error, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ModelUpdatedFlightStatus> call, Throwable t) {
                hideprogress();
                search_lay.setVisibility(View.VISIBLE);
                lin_result_lay.setVisibility(View.GONE);
                Log.d("faill", "onFailure: " + t.getMessage());
                Toast.makeText(ActivityUpdatedAddTrip.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    //upload images to server
    public void uploadMultiFile(List<MediaFile> filelist,
                                String section) {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityUpdatedAddTrip.this, R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;
        }
        shwProgress();

        RequestBody sections = RequestBody.create(MediaType.parse("text/plain"), section);
        RequestBody uid = RequestBody.create(MediaType.parse("text/plain"),
                UtilsDefault.getSharedPreferenceValue(Constants.USER_ID));
        //Log.d("section", "uploadMultiFile: "+section);

        aviapi.uploadMultiFile(getMultiart(filelist), sections, uid).enqueue(new Callback<ModelAddDocuments>() {
            @Override
            public void onResponse(Call<ModelAddDocuments> call, Response<ModelAddDocuments> response) {
                hideprogress();
                ModelAddDocuments data = response.body();
                if (data.getStatus() == Constants.STATUS_200) {
                    if (section.equals(Constants.SECTION_BOARDING)) {
                        selectedBordingPass = new StringBuilder();
                        for (ModelAddDocuments.Datum documents : data.getData()) {
                            if (data.getData().indexOf(documents) == data.getData().size() - 1) {
                                selectedBordingPass.append(documents.getId());
                            } else {
                                selectedBordingPass.append(documents.getId() + ",");
                            }
                        }
                        modelInputAddtrip.setBoardingPass(selectedBordingPass);
                        if (flightStatusList.get(selectedPos).getListEticket() != null &&
                                flightStatusList.get(selectedPos).getListEticket().size() != 0) {
                            uploadMultiFile(flightStatusList.get(selectedPos).getListEticket(), Constants.SECTION_ETICKET);
                        } else {
                            addTrip(modelInputAddtrip);
                        }


                    } else {
                        selectedEticket = new StringBuilder();
                        selectedEticket.setLength(0);
                        for (ModelAddDocuments.Datum documents : data.getData()) {
                            if (data.getData().indexOf(documents) == data.getData().size() - 1) {
                                selectedEticket.append(documents.getId());
                            } else {
                                selectedEticket.append(documents.getId() + ",");
                            }

                        }
                        modelInputAddtrip.seteTickets(selectedEticket);
                        addTrip(modelInputAddtrip);
                    }

                } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                    uploadMultiFile(filelist, section);
                } else {
                    isAddingTrip = false;
                    // btn_add_trip.setEnabled(true);
                    alert(data.getMessage());
                    //  Toast.makeText(ActivityMyDocsDetails.this, "Success " + data.getMessage(), Toast.LENGTH_LONG).show();
                    Log.d("failure", "onResponse: " + data.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ModelAddDocuments> call,
                                  Throwable t) {
                hideprogress();
                isAddingTrip = false;
                // btn_add_trip.setEnabled(true);
                alert(getString(R.string.server_error));
            }
        });

    }


    //creating multipart array
    public List<MultipartBody.Part> getMultiart(List<MediaFile> upFileList) {
        List<MultipartBody.Part> parts = new ArrayList<>();
        for (int i = 0; i < upFileList.size(); i++) {
            File file = new File(upFileList.get(i).getPath());
            Log.d("filepath", "uploadMultiFile: " + upFileList.get(i).getPath());
//            Uri uri;
//            if (Build.VERSION.SDK_INT >= 23) {
//                uri = FileProvider.getUriForFile(getApplicationContext(),
//                        BuildConfig.APPLICATION_ID + ".provider", file);
//            } else {
//                uri = Uri.fromFile(file);
//            }
//            byte[] bytes = new byte[0];
//            try {
//                bytes =compress(uri);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }

            MultipartBody.Part fileParts;
            String fileNames = new Date().getTime() +
                    file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("."));
            if (upFileList.get(i).getMimeType().contains("image")) {
                Uri uri;
                if (Build.VERSION.SDK_INT >= 23) {
                    uri = FileProvider.getUriForFile(getApplicationContext(),
                            BuildConfig.APPLICATION_ID + ".provider", file);
                } else {
                    uri = Uri.fromFile(file);
                }
                byte[] bytes = new byte[0];
                try {
                    bytes = compress(uri, file);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                RequestBody fileBody = null;
                //fileBody = RequestBody.create(MediaType.parse(getContentResolver().getType(uri)),bytes);
                fileBody = RequestBody.create(MediaType.parse(upFileList.get(i).getMimeType()),
                        bytes);
                fileParts = MultipartBody.Part.createFormData("file[]", fileNames, fileBody);
            } else {
                RequestBody fileBody = null;
                //fileBody = RequestBody.create(MediaType.parse(getContentResolver().getType(uri)),file);
                fileBody = RequestBody.create(MediaType.parse(upFileList.get(i).getMimeType()),
                        file);
                fileParts = MultipartBody.Part.createFormData("file[]",
                        fileNames, fileBody);

            }

            parts.add(fileParts);
        }
        return parts;
    }


    //To rotate images if its in potrait mode
    public static Bitmap rotateBitmap(Bitmap bitmap, int degrees) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                bitmap.getHeight(), matrix, true);
    }
    // this method is used to compress images
    public byte[] compress(Uri selectedImage, File file) throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage),
                null, o);
        final int REQUIRED_SIZE = 400;
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        Bitmap bitmap = BitmapFactory.decodeStream(
                getContentResolver().openInputStream(selectedImage), null, o2);

        ExifInterface exif = null;
        try {

            exif = new ExifInterface(file.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        int orientation = ExifInterface.ORIENTATION_NORMAL;

        if (exif != null)
            orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                bitmap = rotateBitmap(bitmap, 90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                bitmap = rotateBitmap(bitmap, 180);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                bitmap = rotateBitmap(bitmap, 270);
                break;
        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    @Override
    public void OnItemClickListener(int pos, View view) {

        if (view.getId() == R.id.img_add_Bording) {
            selectedPos = pos;
            selectionType = 1;
            showBottomFilePIcker(Constants.MAX_FILE_SELECT);

        } else if (view.getId() == R.id.img_add_etick) {
            selectedPos = pos;
            selectionType = 2;
            showBottomFilePIcker(Constants.MAX_FILE_SELECT);

        } else if (view.getId() == R.id.btn_add_trip && !isAddingTrip) {
            selectedPos = pos;
            // selectionType=2;
            modelInputAddtrip = new ModelInputAddtrip();
            modelInputAddtrip.setFlight_status(UtilsDefault.checkNull(flightStatusList.get(pos).getFlightStatus()));
            modelInputAddtrip.setAirlineName(UtilsDefault.checkNull(flightStatusList.get(pos).getAirline()));
            modelInputAddtrip.setAirport_id(Integer.parseInt(UtilsDefault.checkNull(UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID))));
            modelInputAddtrip.setUser_id(Integer.parseInt(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)));
            modelInputAddtrip.setFlight_code(UtilsDefault.checkNull(flightStatusList.get(pos).getAirlineCode()) +
                    UtilsDefault.checkNull(flightStatusList.get(pos).getFlightNumber()));
            modelInputAddtrip.setDep_airport_code(UtilsDefault.checkNull(flightStatusList.get(pos).getDepCode()));
            modelInputAddtrip.setArr_airport_code(UtilsDefault.checkNull(flightStatusList.get(pos).getArrCode()));
            modelInputAddtrip.setArr_airport_city(UtilsDefault.checkNull(flightStatusList.get(pos).getArrCity()));
            modelInputAddtrip.setDep_airport_city(UtilsDefault.checkNull(flightStatusList.get(pos).getDepCity()));
            modelInputAddtrip.setArr_airport_country(UtilsDefault.checkNull(flightStatusList.get(pos).getArrCountry()));
            modelInputAddtrip.setDep_airport_country(UtilsDefault.checkNull(flightStatusList.get(pos).getDeptCountry()));
            if (flightStatusList.get(pos).getDepTerminal() != null && !flightStatusList.get(pos).getDepTerminal().equals("")) {
                modelInputAddtrip.setDep_terminal(flightStatusList.get(pos).getDepTerminal());
            }
            if (flightStatusList.get(pos).getArrTerminal() != null && !flightStatusList.get(pos).getArrTerminal().equals("")) {
                modelInputAddtrip.setArr_terminal(flightStatusList.get(pos).getArrTerminal());
            }
            modelInputAddtrip.setDep_date(UtilsDefault.checkNull(flightStatusList.get(pos).getDepdate()));
            modelInputAddtrip.setArr_date(UtilsDefault.checkNull(flightStatusList.get(pos).getArrdate()));
            modelInputAddtrip.setDep_time(UtilsDefault.checkNull(flightStatusList.get(pos).getDepTime()));
            modelInputAddtrip.setArr_time(UtilsDefault.checkNull(flightStatusList.get(pos).getArrTime()));
            modelInputAddtrip.setDep_airport_name(UtilsDefault.checkNull(flightStatusList.get(pos).
                    getDepAirportName()));
            modelInputAddtrip.setArr_airport_name(UtilsDefault.checkNull(flightStatusList.get(pos).
                    getArrAirportName()));
            if (flightStatusList.get(pos).getArrGate() != null) {
                modelInputAddtrip.setArrGate(flightStatusList.get(pos).getArrGate());
            }
            if (flightStatusList.get(pos).getDepGate() != null) {
                modelInputAddtrip.setDepGate(flightStatusList.get(pos).getDepGate());
            }
            isAddingTrip = true;
            if (flightStatusList.get(selectedPos).getListBoarding() != null &&
                    flightStatusList.get(selectedPos).getListBoarding().size() != 0) {

                Toast.makeText(this, getString(R.string.uploading), Toast.LENGTH_SHORT).show();
                selectedBordingPass.setLength(0);
                uploadMultiFile(flightStatusList.get(selectedPos).getListBoarding(), Constants.SECTION_BOARDING);

            } else if (flightStatusList.get(selectedPos).getListEticket() != null &&
                    flightStatusList.get(selectedPos).getListEticket().size() != 0) {

                Toast.makeText(this, getString(R.string.uploading), Toast.LENGTH_SHORT).show();
                selectedEticket.setLength(0);
                uploadMultiFile(flightStatusList.get(selectedPos).getListEticket(), Constants.SECTION_ETICKET);
            } else {
                addTrip(modelInputAddtrip);

            }
            //  if()

        }

    }


    // get most last user trips
    public void getLatestTrip() {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityUpdatedAddTrip.this,
                    R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;

        }
        shwProgress();
        //  Log.d("aviparms", "search: "+UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)+search);
        String userid = "0";
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
            userid = UtilsDefault.getSharedPreferenceValue(Constants.USER_ID);
        }

        aviapi.getlatestTrip(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID),
                UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID),
                timeZone.getID()).enqueue(new Callback<ModelUpcomingtrips>() {
            @Override
            public void onResponse(Call<ModelUpcomingtrips> call, Response<ModelUpcomingtrips> response) {
                hideprogress();

                if (response.body() != null) {
                    ModelUpcomingtrips data = response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus() == Constants.STATUS_200) {
                        UtilsDefault.updateSharedPreference(Constants.AIRPORT_DOMESTIC_TYPE,
                                data.getUpTripTimeType());
                        UtilsDefault.updateSharedPreference(Constants.LATEST_DATE, data.getUpTripTime());
                        countDownStart();
                        alertTripAdded("", "");
                        adapterUpdatedAddTrip.notifyDataSetChanged();
                        // alertcommon(getString(R.string.title_my_trip), data.getMessage());
                        //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                    } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                        getLatestTrip();
                    } else {
                        UtilsDefault.updateSharedPreference(Constants.LATEST_DATE, data.getUpTripTime());
                        countDownStart();
                        alertTripAdded("", "");
                        //Toast.makeText(ActivityMyTrips.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    alertTripAdded("", "");
                    //   Toast.makeText(ActivityMyTrips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ModelUpcomingtrips> call, Throwable t) {
                hideprogress();
                alertTripAdded("", "");
                Log.d("failure", "onFailure: " + t.getMessage());
                //Toast.makeText(ActivityMyTrips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }


    //Api call to add trips
    public void addTrip(ModelInputAddtrip modelInputAddtrip) {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityUpdatedAddTrip.this,
                    R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;
        }
        shwProgress();
        modelInputAddtrip.setTime_zone(timeZone.getID());
        aviapi.addtrip(modelInputAddtrip).enqueue(new Callback<ModelAddTrip>() {
            @Override
            public void onResponse(Call<ModelAddTrip> call, Response<ModelAddTrip> response) {
                hideprogress();
                hideKeyboard();
                if (response.body() != null) {
                    ModelAddTrip data = response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus() == Constants.STATUS_200) {
                        // btn_add_trip.setEnabled(true);
                        flightStatusList.remove(selectedPos);
                        adapterUpdatedAddTrip.notifyItemRemoved(selectedPos);
                        adapterUpdatedAddTrip.notifyDataSetChanged();
                        getLatestTrip();
                        tv_total_flight_number.setText(flightStatusList.size() + " Trips found");
                        isAddingTrip = false;
                        // alertTripAdded("","");

                        //alertcommon(getString(R.string.add_my_tips),data.getMessage());

                        //UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                    } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                        addTrip(modelInputAddtrip);
                    } else {
                        isAddingTrip = false;
                        // btn_add_trip.setEnabled(true);
                        alert(data.getMessage());
                        //alertcommon(getString(R.string.add_my_tips),data.getMessage());
                        //  Toast.makeText(ActivityFlightSchedule.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    isAddingTrip = false;
                    //  btn_add_trip.setEnabled(true);
                    Toast.makeText(ActivityUpdatedAddTrip.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ModelAddTrip> call, Throwable t) {
                //btn_add_trip.setEnabled(true);
                isAddingTrip = false;
                hideprogress();
                hideKeyboard();
                Log.d("failure", "onFailure: " + t.getMessage());
                Toast.makeText(ActivityUpdatedAddTrip.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        handleImageSlection(requestCode, resultCode, data);
    }

    //handling selected images from image picker
    public void handleImageSlection(int requestCode, int resultCode,
                                    @Nullable Intent data) {
        try {
            if (resultCode == RESULT_OK
                    && data != null) {
                if (mBottomSheetDialog != null) {
                    mBottomSheetDialog.cancel();
                }

                if (selectionType == 1 && selectedPos != -1 && data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES) != null &&
                        data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES).size() != 0) {
                    if (flightStatusList.get(selectedPos).getListBoarding() != null) {
                        int totalcount = flightStatusList.get(selectedPos).getListBoarding().size() +
                                data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES).size();
                        if (totalcount > Constants.MAX_FILE_SELECT) {
                            //   Toast.makeText(this, getString(R.string.maximum_five_files), Toast.LENGTH_SHORT).show();
                            alertcommon(getString(R.string.add_trip), getString(R.string.maximum_five_files));
                            return;
                        }
                    }
                    ModelUpdatedFlightStatus.Data data1 = flightStatusList.get(selectedPos);
                    data1.setOpen(true);
                    List<MediaFile> list = new ArrayList<>();
                    if (data1.getListBoarding() != null) {
                        list.addAll(data1.getListBoarding());
                    }
                    list.addAll(data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES));
                    data1.setListBoarding(list);
                    flightStatusList.set(selectedPos, data1);
                    adapterUpdatedAddTrip.notifyItemChanged(selectedPos);
                } else if (selectionType == 2 && selectedPos != -1 &&
                        data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES) != null &&
                        data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES).size() != 0) {
                    if (flightStatusList.get(selectedPos).getListEticket() != null) {
                        int totalcount = flightStatusList.get(selectedPos).getListEticket().size() + data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES).size();
                        if (totalcount > Constants.MAX_FILE_SELECT) {
                            alertcommon(getString(R.string.add_trip), getString(R.string.maximum_five_files));
                            return;
                        }
                    }

                    ModelUpdatedFlightStatus.Data data1 = flightStatusList.get(selectedPos);
                    //data1.setListEticket(data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES));
                    data1.setOpen(true);

                    List<MediaFile> list = new ArrayList<>();
                    if (data1.getListEticket() != null) {
                        list.addAll(data1.getListEticket());
                    }
                    list.addAll(data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES));
                    data1.setListEticket(list);
                    flightStatusList.set(selectedPos, data1);
                    adapterUpdatedAddTrip.notifyItemChanged(selectedPos);
                }

//                if (sectionType == Integer.parseInt(Constants.SECTION_BOARDING)) {
//                    int totalcount = listBoarding.size() + data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES).size();
//                    if (totalcount > Constants.MAX_FILE_SELECT) {
//                        alertcommon(getString(R.string.add_trip),  getString(R.string.maximum_five_files));
//                        return;
//                    }
//                    recycle_boarding_images.setVisibility(View.VISIBLE);
//                    listBoarding.addAll(data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES));
//                    adapterSelectedFiles.notifyDataSetChanged();
//                } else {
//                    int totalcount = listEticket.size() + data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES).size();
//                    if (totalcount > Constants.MAX_FILE_SELECT) {
//                        alertcommon(getString(R.string.add_trip), getString(R.string.maximum_five_files));
//                        return;
//                    }
//                    recycle_eticket_images.setVisibility(View.VISIBLE);
//                    listEticket.addAll(data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES));
//                    adapterETicket.notifyDataSetChanged();
//                }

            }
//            else if (requestCode == IMAGE_REQUEST_CODE && resultCode == RESULT_OK
//                    && data != null) {
//                if (mBottomSheetDialog != null) {
//                    mBottomSheetDialog.cancel();
//                }
//                if (sectionType == Integer.parseInt(Constants.SECTION_BOARDING)) {
//                    int totalcount = listBoarding.size() + data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES).size();
//                    if (totalcount > Constants.MAX_FILE_SELECT) {
//                        alertcommon(getString(R.string.add_trip), getString(R.string.maximum_five_files));
//                        return;
//                    }
//                    recycle_boarding_images.setVisibility(View.VISIBLE);
//                    listBoarding.addAll(data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES));
//                    adapterSelectedFiles.notifyDataSetChanged();
//                } else {
//                    int totalcount = listEticket.size() + data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES).size();
//                    if (totalcount > Constants.MAX_FILE_SELECT) {
//                        alertcommon(getString(R.string.add_trip), getString(R.string.maximum_five_files));
//                        return;
//                    }
//                    recycle_eticket_images.setVisibility(View.VISIBLE);
//                    listEticket.addAll(data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES));
//                    adapterETicket.notifyDataSetChanged();
//                }
//
//            } else if (requestCode == CAMERA_REQUEST_CODE && resultCode == RESULT_OK
//                    && data != null) {
//                if (mBottomSheetDialog != null) {
//                    mBottomSheetDialog.cancel();
//                }
//                if (sectionType == Integer.parseInt(Constants.SECTION_BOARDING)) {
//                    int totalcount = listBoarding.size() + data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES).size();
//                    if (totalcount > Constants.MAX_FILE_SELECT) {
//                        alertcommon(getString(R.string.add_trip), getString(R.string.maximum_five_files));
//                        return;
//                    }
//                    recycle_boarding_images.setVisibility(View.VISIBLE);
//                    listBoarding.addAll(data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES));
//                    adapterSelectedFiles.notifyDataSetChanged();
//                } else {
//                    int totalcount = listEticket.size() + data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES).size();
//                    if (totalcount > Constants.MAX_FILE_SELECT) {
//                        alertcommon(getString(R.string.add_trip), getString(R.string.maximum_five_files));
//                        return;
//                    }
//                    recycle_eticket_images.setVisibility(View.VISIBLE);
//                    listEticket.addAll(data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES));
//                    adapterETicket.notifyDataSetChanged();
//                }
//            }
        } catch (Exception e) {
            Log.d("Exception", "handleImageSlection: "+"error");
        }
    }


    //Alert dialog to show trips has been added.
    public void alertTripAdded(String title, String content) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setCancelable(false);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_add_more_trip, null);
        alertDialogBuilder.setView(dialogView);
        TextView titletv = dialogView.findViewById(R.id.tv_alert_title);
        TextView tv_go_to_mytrips = dialogView.findViewById(R.id.tv_go_to_mytrips);
        //titletv.setText(title);
        TextView tv_content = dialogView.findViewById(R.id.tv_content);
        // tv_content.setText(content);
        btn_ok = dialogView.findViewById(R.id.btn_ok);
        if (flightStatusList.size() == 0) {
            btn_ok.setText(getString(R.string.close));
        }
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addSucessfullyAlert != null) {
                    if (flightStatusList.size() == 0) {
                        finish();
                    } else {
                        addSucessfullyAlert.dismiss();
                    }
                }
            }
        });
        tv_go_to_mytrips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Toast.makeText(ActivityUpdatedAddTrip.this, data.getMessage(), Toast.LENGTH_SHORT).show();
//                Intent intent = new Intent(ActivityUpdatedAddTrip.this, ActivityMyTrips.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(intent);
                finish();
            }
        });


        addSucessfullyAlert = alertDialogBuilder.create();
        addSucessfullyAlert.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        addSucessfullyAlert.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //Log.d(TAG, "onActivityResulttPermissionsResult: "+requestCode);
        if (requestCode == 101) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                //  Log.d(TAG, "onActivityResulttPermissionsResult: "+PackageManager.PERMISSION_GRANTED);
                showBottomFilePIcker(Constants.MAX_FILE_SELECT);

            } else if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_DENIED ||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                            == PackageManager.PERMISSION_DENIED) {


            } else {
                showBottomFilePIcker(Constants.MAX_FILE_SELECT);
            }
        }
    }

    public void showBottomFilePIcker(int maxSelect) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {
                //openSettings();

                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                android.Manifest.permission.CAMERA},
                        101);
                Toast.makeText(this, R.string.enable_camera, Toast.LENGTH_SHORT).show();

                if (!shouldShowRequestPermissionRationale(Manifest.permission.CAMERA) ||
                        !shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    openSettings();
                    Toast.makeText(this, R.string.enable_camera, Toast.LENGTH_SHORT).show();

                    return;
                }
                return;
            }
        }

        mBottomSheetDialog = new Dialog(this, R.style.PickerMaterialDialogSheet);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_bottom_picker, null);
        mBottomSheetDialog.setContentView(dialogView); // your custom view.
        mBottomSheetDialog.setCancelable(true);
        LinearLayout lin_docs_lay = dialogView.findViewById(R.id.lin_docs_lay);
        LinearLayout lin_gallery_lay = dialogView.findViewById(R.id.lin_gallery_lay);
        LinearLayout lin_camera_lay = dialogView.findViewById(R.id.lin_camera_lay);
        LinearLayout lin_scan_lay = dialogView.findViewById(R.id.lin_scan_lay);
        CardView card_cancel = dialogView.findViewById(R.id.card_cancel);
        card_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.cancel();
            }
        });
        lin_gallery_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setImagePicker(maxSelect);
            }
        });
        lin_docs_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFilePicker(maxSelect);
            }
        });
        lin_camera_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setcameraPicker(maxSelect);
            }
        });

        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();
    }
}
