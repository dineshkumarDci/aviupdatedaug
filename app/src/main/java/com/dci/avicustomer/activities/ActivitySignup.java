package com.dci.avicustomer.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.chaos.view.PinView;
import com.dci.avicustomer.R;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.models.ModelOTPSend;
import com.dci.avicustomer.models.RegisterModel;
import com.dci.avicustomer.retrofit.AVIAPI;
import com.msg91.sendotp.library.SendOtpVerification;
import com.msg91.sendotp.library.Verification;
import com.msg91.sendotp.library.VerificationListener;
import com.ybs.countrypicker.CountryPicker;
import com.ybs.countrypicker.CountryPickerListener;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindBitmap;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivitySignup extends BaseActivity implements View.OnClickListener, VerificationListener {

    @Inject
    public AVIAPI aviapi;
    @BindView(R.id.ed_username)
    EditText ed_username;
    @BindView(R.id.ed_emailid)
    EditText ed_emailid;
    @BindView(R.id.ed_mobilenumber)
    EditText ed_mobilenumber;
    @BindView(R.id.ed_password)
    EditText ed_password;
    @BindView(R.id.ed_confirm_password)
    EditText ed_confirm_password;
    @BindView(R.id.register_lay)
    ScrollView register_lay;
    @BindView(R.id.otp_lay)
    RelativeLayout otp_lay;
    @BindView(R.id.btn_register)
    Button btn_register;
    @BindView(R.id.countrycode_lay)
    LinearLayout countrycode_lay;
    @BindView(R.id.tv_signin)
    TextView tv_signin;
    @BindView(R.id.tv_countrycode)
    TextView tv_countrycode;
    @BindView(R.id.tv_editnumber)
    TextView editnumber;
    @BindView(R.id.btn_verify)
    Button btn_verify;
    @BindView(R.id.tv_otp_resend)
    TextView tv_otp_resend;
    @BindView(R.id.tv_timer)
    TextView tv_timer;
    @BindView(R.id.firstPinView)
    PinView firstPinView;
    @BindView(R.id.tv_mobile_number)
    TextView tv_mobile_number;

    @BindView(R.id.resend_lay)
    LinearLayout resend_lay;
    @BindView(R.id.tv_privacy)
    TextView tv_privacy;
    @BindView(R.id.tv_terms)
    TextView tv_terms;
    @BindView(R.id.check_box_terms)
    CheckBox check_box_terms;
    @BindView(R.id.ed_lastname)
    EditText ed_lastname;
    String countrycode = "+91";
    String otp;
    Verification mVerification;
    CountDownTimer timer;
    private String TAG = "ActivitySignup";
    String phoneNum = "";
    String emailid = "";
    int orginalOTP;
    String CountryUniqueCode="IN";
    //CognitoUser cognitoUsers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        ShoppingApplication.getContext().getComponent().inject(this);
        tv_signin.setOnClickListener(this);
        btn_register.setOnClickListener(this);
        countrycode_lay.setOnClickListener(this);
        btn_verify.setOnClickListener(this);
        editnumber.setOnClickListener(this);
        tv_otp_resend.setOnClickListener(this);

        firstPinView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                otp = s.toString();
                Log.d(TAG, "onTextChanged() called with: s = [" + s + "], start = [" + start + "], before = [" + before + "], count = [" + count + "]");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

//    public void intizializeAWS(){
//        Log.d(TAG, "intizializeAWS: ");
//        // setup AWS service configuration. Choosing default configuration
//        ClientConfiguration clientConfiguration = new ClientConfiguration();
//
//// Create a CognitoUserPool object to refer to your user pool
//        CognitoUserPool userPool = new CognitoUserPool(this, getString(R.string.poolid), getString(R.string.Clientid),
//                getString(R.string.ClientSecret), clientConfiguration);
//        CognitoUserAttributes userAttributes = new CognitoUserAttributes();
//
//// Add the user attributes. Attributes are added as key-value pairs
//// Adding user's given name.
//// Note that the key is "given_name" which is the OIDC claim for given name
//        userAttributes.addAttribute("given_name", "Dinesh");
//
//// Adding user's phone number
//        userAttributes.addAttribute("phone_number", "+919500781862");
//
//// Adding user's email address
//        userAttributes.addAttribute("email", "Dinesh.whalts@gmail.com");
//        SignUpHandler signupCallback = new SignUpHandler() {
//
//            @Override
//            public void onSuccess(CognitoUser cognitoUser, boolean userConfirmed,
//                                  CognitoUserCodeDeliveryDetails cognitoUserCodeDeliveryDetails) {
//                // Sign-up was successful
//
//                // Check if this user (cognitoUser) has to be confirmed
//                if(!userConfirmed) {
//                  cognitoUsers=cognitoUser;
//                    Log.d(TAG, "onSuccess: "+"notVerified");
//
//                    // This user has to be confirmed and a confirmation code was sent to the user
//                    // cognitoUserCodeDeliveryDetails will indicate where the confirmation code was sent
//                    // Get the confirmation code from user
//                }
//                else {
//                    Log.d(TAG, "onSuccess: "+"Verified");
//                    // The user has already been confirmed
//                }
//            }
//
//            @Override
//            public void onFailure(Exception exception) {
//                // Sign-up failed, check exception for the cause
//            }
//        };
//        userPool.signUpInBackground("12", "123123", userAttributes, null, signupCallback);
//    }
//    public void VerifyCode(String confirmationCode){
//        boolean forcedAliasCreation = false;
//        GenericHandler confirmationCallback = new GenericHandler() {
//
//            @Override
//            public void onSuccess() {
//
//                // User was successfully confirmed
//            }
//
//            @Override
//            public void onFailure(Exception exception) {
//                // User confirmation failed. Check exception for the cause.
//            }
//        };
//
//// Call API to confirm this user
//        cognitoUsers.confirmSignUpInBackground(confirmationCode, forcedAliasCreation, confirmationCallback);
//    }


    public void showCountryCode() {
        final CountryPicker picker = CountryPicker.newInstance("Select Country");  // dialog title
        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID) {
                picker.dismiss();
                countrycode = dialCode;
                CountryUniqueCode=code;
                tv_countrycode.setText(dialCode);
                Log.d(TAG, "onSelectCountry: " + "name=" + name + "code" + code + "dcode=" + dialCode + "fla" + flagDrawableResID);
                // Implement your code here
            }
        });
        picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
    }

    @Override
    protected int getlayout() {
        return R.layout.activity_signup;
    }


    @Override
    public void onClick(View v) {
        if (v == tv_signin) {
            finish();
            startActivity(new Intent(ActivitySignup.this, ActivityLogin.class));
        }

        if (v == countrycode_lay) {
            showCountryCode();
        }
        if (v == btn_register) {
            String name = ed_username.getText().toString().trim();
            String email = ed_emailid.getText().toString().trim();
            String phone = ed_mobilenumber.getText().toString().trim();
            String pass = ed_password.getText().toString().trim();
            String confirmpass = ed_confirm_password.getText().toString().trim();
            String cntry = tv_countrycode.getText().toString().trim();
            String lastname = ed_lastname.getText().toString().trim();
            if (name.equals("")) {
                ed_username.requestFocus();
                ed_username.setError(getString(R.string.error_uname));

            } else if (lastname.equals("")) {
                ed_lastname.requestFocus();
                ed_lastname.setError(getString(R.string.error_lastname));
            } else if (email.equals("")) {
                ed_emailid.requestFocus();
                ed_emailid.setError(getString(R.string.error_emailid));
            } else if (!UtilsDefault.isEmailValid(email)) {
                ed_emailid.requestFocus();
                ed_emailid.setError(getString(R.string.error_valid_email));
            } else if (cntry.equals("")) {
                Toast.makeText(this, R.string.error_country_code, Toast.LENGTH_SHORT).show();
            } else if (phone.equals("")) {
                ed_mobilenumber.requestFocus();
                ed_mobilenumber.setError(getString(R.string.error_mobile_number));
            } else if (phone.length() < 9) {
                ed_mobilenumber.requestFocus();
                ed_mobilenumber.setError(getString(R.string.error_invalid_mobile));
            } else if (pass.equals("")) {
                ed_password.requestFocus();
                ed_password.setError(getString(R.string.error_password),null);
            }
            else if(!UtilsDefault.isValidPassword(pass)){
                ed_password.requestFocus();
                alertcommon(getString(R.string.title_signup),getString(R.string.error_password_short));
               // ed_password.setError(getString(R.string.error_password_short),null);
            }



            else if (confirmpass.equals("")) {
                ed_confirm_password.requestFocus();
                ed_confirm_password.setError(getString(R.string.error_confirmpass),null);
            } else if (!pass.equals(confirmpass)) {
                ed_confirm_password.requestFocus();
                ed_confirm_password.setError(getString(R.string.invalid_confirmpass),null);
            }
//            else if (!check_box_terms.isChecked()){
//                Toast.makeText(this, R.string.title_terms, Toast.LENGTH_SHORT).show();
//            }

            else {
                //intizializeAWS();
               // sendMsg91(phone);
                phoneNum = phone;
                emailid = email;

                sendOTP();
                //showOtplay();


            }

        } else if (v == editnumber) {
            showRegisterlay();
        } else if (v == tv_otp_resend) {
            sendOTP();
            tv_timer.setVisibility(View.VISIBLE);

//            if (mVerification != null) {
//                startTimer();
//                Toast.makeText(this, "Resending OTP..", Toast.LENGTH_SHORT).show();
//                mVerification.resend("text");
//            }
        }

        if (v == btn_verify) {
            if (otp == null || otp.length() < 5 || orginalOTP != Integer.parseInt(otp)) {
                Toast.makeText(this, R.string.error_invalid_otp, Toast.LENGTH_SHORT).show();
            } else {
                //shwProgress();
                //VerifyCode(otp);
                register();
                //mVerification.verify(otp);
            }
        }
    }

    public void register() {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivitySignup.this, R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;

        }
        shwProgress();
        String name = ed_username.getText().toString().trim();
        String email = ed_emailid.getText().toString().trim();
        String phone = ed_mobilenumber.getText().toString().trim();
        String pass = ed_password.getText().toString().trim();
        String confirmpass = ed_confirm_password.getText().toString().trim();
        String cntry = tv_countrycode.getText().toString().trim();
        String lastname = ed_lastname.getText().toString().trim();
        aviapi.register(name, email, phone, pass, confirmpass, lastname, cntry,
                UtilsDefault.checkNull(UtilsDefault.getFcmSharedPreferenceValue(Constants.FCM_KEY)),
                Constants.DEV_ANDROID,CountryUniqueCode).enqueue(new Callback<RegisterModel>() {
            @Override
            public void onResponse(Call<RegisterModel> call, Response<RegisterModel> response) {
                hideprogress();
                if (response.body() != null) {
                    RegisterModel data = response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, data.getJwt_token());
                    if (data.getStatus() == Constants.STATUS_200) {
                        UtilsDefault.updateSharedPreference(Constants.USER_ID, String.valueOf(data.getData().getId()));
                        UtilsDefault.updateSharedPreference(Constants.FIRST_NAME, data.getData().getFirst_name());
                        UtilsDefault.updateSharedPreference(Constants.LAST_NAME, data.getData().getLast_name());
                        UtilsDefault.updateSharedPreference(Constants.MOBILE_NUMBER, data.getData().getMobile());
                        UtilsDefault.updateSharedPreference(Constants.GENDER, data.getData().getGender());
                        UtilsDefault.updateSharedPreference(Constants.EMAILID, data.getData().getEmail());
                        UtilsDefault.updateSharedPreference(Constants.PROFILE_IMG, data.getData().getProfile_img());
                        UtilsDefault.updateSharedPreference(Constants.USER_IMAGE_PATH,data.getPath());

                        Toast.makeText(ActivitySignup.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                        //   UtilsDefault.updateSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));

                        Intent intent = new Intent(ActivitySignup.this, ActivitySearch.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();

                        //  startActivity(new Intent(ActivitySignup.this, ActivitySearch.class));

                    } else {
                        Toast.makeText(ActivitySignup.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ActivitySignup.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<RegisterModel> call, Throwable t) {
                hideprogress();

                Toast.makeText(ActivitySignup.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void sendOTP() {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivitySignup.this, R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;

        }
        shwProgress();

        Log.d(TAG, "sendOTP: "+phoneNum+"-"+countrycode+"-"+emailid);

        aviapi.sendOTP(phoneNum, countrycode, emailid).enqueue(new Callback<ModelOTPSend>() {
            @Override
            public void onResponse(Call<ModelOTPSend> call, Response<ModelOTPSend> response) {
                hideprogress();
                if (response.body() != null) {
                    ModelOTPSend data = response.body();

                    if (data.getStatus() == Constants.STATUS_200) {
                        if (data.getOtp() != null) {
                            showOtplay();
                            orginalOTP = data.getOtp();
                        } else {

                                Toast.makeText(ActivitySignup.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                        }


                        //  startActivity(new Intent(ActivitySignup.this, ActivitySearch.class));

                    } else {
                        if (data.getMessage() != null) {
                            Toast.makeText(ActivitySignup.this, data.getMessage(), Toast.LENGTH_SHORT).show();

                        } else {
                            Toast.makeText(ActivitySignup.this, R.string.server_error, Toast.LENGTH_SHORT).show();

                        }
                    }
                } else {
                    Toast.makeText(ActivitySignup.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ModelOTPSend> call, Throwable t) {
                hideprogress();

                Toast.makeText(ActivitySignup.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void startTimer() {
        hideKeyboard();
        resend_lay.setVisibility(View.GONE);
        if (timer != null) {
            timer.cancel();
        }
        timer = new CountDownTimer(60000, 1000) { // adjust the milli seconds here
            public void onTick(long millisUntilFinished) {
                tv_timer.setVisibility(View.VISIBLE);
                tv_timer.setText("" + String.format("0%d:%d ",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            public void onFinish() {
                resend_lay.setVisibility(View.VISIBLE);
                tv_timer.setVisibility(View.GONE);

                // tv_timer.setText("done!");
            }
        }.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timer != null) {
            timer.cancel();
        }
    }

    public void sendMsg91(String number) {
//        progressDialog = new ProgressDialog(this);
//        progressDialog.setMessage("Verifying number");
//        progressDialog.show();
        mVerification = SendOtpVerification.createSmsVerification(SendOtpVerification
                .config(countrycode + number)
                .context(this)
                .senderId(getString(R.string.otp_sendername))
                .message(getString(R.string.avi_message_form))
                .otplength(getString(R.string.otp_length))
                .autoVerification(true)
                .build(), this);
        mVerification.initiate();
    }

    public void showOtplay() {
        hideKeyboard();
        register_lay.setVisibility(View.GONE);
        otp_lay.setVisibility(View.VISIBLE);
        tv_mobile_number.setText(tv_countrycode.getText().toString().trim() +
                ed_mobilenumber.getText().toString().trim());

        startTimer();
    }

    public void showRegisterlay() {
        if (timer != null) {
            timer.cancel();
        }
        hideKeyboard();
        register_lay.setVisibility(View.VISIBLE);
        otp_lay.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        if (otp_lay.getVisibility()==View.VISIBLE){
            showRegisterlay();
            return;
        }
        finish();
        startActivity(new Intent(ActivitySignup.this, ActivityLogin.class));
    }

    @Override
    public void onInitiated(String response) {

    }

    @Override
    public void onInitiationFailed(Exception paramException) {
        hideprogress();
        hideKeyboard();
        Toast.makeText(this, R.string.please_try_again, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onVerified(String response) {
        hideprogress();
        hideKeyboard();
        register();

    }

    @Override
    public void onVerificationFailed(Exception paramException) {
        hideprogress();
        hideKeyboard();
        Toast.makeText(this, R.string.msg_verification_failed, Toast.LENGTH_SHORT).show();
    }
}
