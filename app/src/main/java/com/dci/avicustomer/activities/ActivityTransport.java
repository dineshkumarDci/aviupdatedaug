package com.dci.avicustomer.activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.dci.avicustomer.R;
import com.dci.avicustomer.adapter.AdapterTransport;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.models.ModelAviTips;
import com.dci.avicustomer.models.ModelTransport;
import com.dci.avicustomer.retrofit.AVIAPI;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityTransport extends BaseActivity {

    @Inject
    public AVIAPI aviapi;
    @BindView(R.id.recycle_transport)
    RecyclerView recycle_transport;
    List<ModelTransport.Datum>list=new ArrayList<>();
    AdapterTransport adapterTransport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        ShoppingApplication.getContext().getComponent().inject(this);
        tv_title.setText(R.string.title_transport);
        getTransport();


    }

    @Override
    public void closeCommonAlert() {
        commonalert.dismiss();
        finish();
    }

    public void getTransport(){
        if (!UtilsDefault.isOnline()){
            Toast.makeText(ActivityTransport.this,
                    R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;

        }
        shwProgress();
        aviapi.getTransport(UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID)).enqueue(new Callback<ModelTransport>() {
            @Override
            public void onResponse(Call<ModelTransport> call, Response<ModelTransport> response) {
                hideprogress();


                if (response.body()!=null){
                    ModelTransport data=response.body();
//                    String header = response.headers().get(getString(R.string.header_find_key));
//                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus()== Constants.STATUS_200){
                        if (data.getData()!=null&&data.getData().size()!=0){
                            recycle_transport.setAdapter(new AdapterTransport(ActivityTransport.this,data.getData(),1,data.getUrl()));
                            recycle_transport.setLayoutManager(new LinearLayoutManager(ActivityTransport.this));

                        }
                        else {
                            alertcommon(getString(R.string.title_transport),getString(R.string.nodata));
                        }


                        //   UtilsDefault.updateSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                    }
                    else if (data.getStatus()==Constants.API_TOKENEXPIRY_STATUS){
                        getTransport();
                    }
                    else {
                        alertcommon(getString(R.string.title_transport),data.getMessage());
                        //Toast.makeText(ActivityTransport.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    alertcommon(getString(R.string.title_transport),
                            getString( R.string.server_error));
                   // Toast.makeText(ActivityTransport.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ModelTransport> call, Throwable t) {
                hideprogress();
                alertcommon(getString(R.string.title_transport),
                        getString( R.string.server_error));
               // Toast.makeText(ActivityTransport.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected int getlayout() {
        return R.layout.activity_transport;
    }
}
