package com.dci.avicustomer.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.dci.avicustomer.R;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.UtilsDefault;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivitySplash extends AppCompatActivity {
    private Animation animShow, animHide;
    ImageView img_green;
    @BindView(R.id.gif_logo)
    ImageView gif_logo;
    AlertDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        animShow = AnimationUtils.loadAnimation(this, R.anim.view_show);
        animHide = AnimationUtils.loadAnimation(this, R.anim.view_hide);
        img_green=findViewById(R.id.img_green);
        //Glide.with(this).asGif().load(R.raw.avi_gif).into(gif_logo);
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        }
        catch (PackageManager.NameNotFoundException e) {

        }
        catch (NoSuchAlgorithmException e) {

        }
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                visibleProceedCheckout("");
//            }
//        },300);
        activityRise();
    }
    public void visibleProceedCheckout(String type) {

        if (img_green.getVisibility() == View.INVISIBLE) {
            img_green.setVisibility(View.VISIBLE);
            img_green.startAnimation(animShow);
            animShow.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }
                @Override
                public void onAnimationEnd(Animation animation) {
                    activityRise();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            //paymentType=type;
        }
    }
    public void activityRise(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (UtilsDefault.getFcmSharedPreferenceValue(Constants.TERMS_TICKED)==null){
                    alertShowPrivacy();
                }
                else if (UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID)==null){
                    startActivity(new Intent(ActivitySplash.this,ActivitySearch.class));
                    finish();
                }
                else {
                    startActivity(new Intent(ActivitySplash.this,ActivityHome.class));
                    finish();
                }
            }
        },3000);
    }

    public void alertShowPrivacy() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_alert_terms, null);
        alertDialogBuilder.setView(dialogView);
        TextView terms=dialogView.findViewById(R.id.tv_terms);
        TextView tv_privacy=dialogView.findViewById(R.id.tv_privacy);
        CheckBox checkBox=dialogView.findViewById(R.id.check_box_terms);
        Button btn_register=dialogView.findViewById(R.id.btn_register);

        SpannableString termsstring = new SpannableString("Terms & Conditions");
        termsstring.setSpan(new UnderlineSpan(), 0, termsstring.length(), 0);
        terms.setText(termsstring);

        SpannableString privacy = new SpannableString("Privacy Policy ");
        privacy.setSpan(new UnderlineSpan(), 0, privacy.length(), 0);
        tv_privacy.setText(privacy);
        tv_privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ActivitySplash.this,ActivityCommonWebLoader.class);
                intent.putExtra("data",getString(R.string.privacy_url));
                intent.putExtra("title",getString(R.string.title_privacy));
                startActivity(intent);
            }
        });
        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ActivitySplash.this,ActivityCommonWebLoader.class);
                intent.putExtra("data",getString(R.string.terms_url));
                intent.putExtra("title",getString(R.string.title_terms_page));
                startActivity(intent);
            }
        });

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBox.isChecked()){
                    dialog.dismiss();
                    UtilsDefault.updateFcmSharedPreference(Constants.TERMS_TICKED,"yes");
                    startActivity(new Intent(ActivitySplash.this,ActivitySearch.class));
                    finish();

                }
                else {
                    Toast.makeText(ActivitySplash.this, R.string.title_terms, Toast.LENGTH_SHORT).show();
                }
            }
        });



        dialog = alertDialogBuilder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.show();
    }



}
