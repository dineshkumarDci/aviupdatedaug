package com.dci.avicustomer.activities;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.avicustomer.R;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.models.ModelFlightStatus;
import com.dci.avicustomer.models.ModelNewFlightStatus;
import com.dci.avicustomer.retrofit.AVIAPI;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityFlightStatus extends BaseActivity
        implements View.OnClickListener,
        DatePickerDialog.OnDateSetListener {
    @BindView(R.id.btn_search)
    Button btn_search;
    @BindView(R.id.tv_selected_date)
    TextView tv_selected_date;
    @BindView(R.id.date_lay)
    RelativeLayout date_lay;

    @BindView(R.id.ed_flightnumber)
    EditText ed_flightnumber;

    @BindView(R.id.ed_airline)
    EditText ed_airline;

    @BindView(R.id.tv_dept_time)
    TextView tv_dept_time;

    @BindView(R.id.tv_dept_date)
    TextView tv_dept_date;
    @BindView(R.id.tv_airlinename)
    TextView tv_airlinename;

    @BindView(R.id.tv_arrive_time)
    TextView tv_arrive_time;

    @BindView(R.id.tv_arrive_date)
    TextView tv_arrive_date;
    @BindView(R.id.tv_arr_actualtime_head)
    TextView tv_arr_actualtime_head;
    @BindView(R.id.tv_dept_code)
    TextView tv_dept_code;

    @BindView(R.id.tv_dept_city)
    TextView tv_dept_city;

    @BindView(R.id.tv_arrive_code)
    TextView tv_arrive_code;

    @BindView(R.id.tv_arrival_city)
    TextView tv_arrival_city;

    @BindView(R.id.tv_travel_time)
    TextView tv_travel_time;

    @BindView(R.id.tv_dept_terminal)
    TextView tv_dept_terminal;
    @BindView(R.id.tv_arrival_terminal)
    TextView tv_arrival_terminal;

    @BindView(R.id.tv_dep_actualtime_head)
    TextView tv_dep_actualtime_head;

    @BindView(R.id.tv_dept_status)
    TextView tv_dept_status;
    @BindView(R.id.frame_result_lay)
    FrameLayout frame_result_lay;
    @BindView(R.id.tv_flight_code)
    TextView tv_flight_code;
    @BindView(R.id.search_lay)
    LinearLayout search_lay;
    @BindView(R.id.user_login)
    ImageView user_login;
    @BindView(R.id.tv_dep_airport_name)
    TextView tv_dep_airport_name;
    @BindView(R.id.tv_dep_schedule_time)
    TextView tv_dep_schedule_time;
    @BindView(R.id.tv_dep_actualtime)
    TextView tv_dep_actualtime;
    @BindView(R.id.tv_dep_terminal_name)
    TextView tv_dep_terminal_name;
    @BindView(R.id.tv_arr_airport_name)
    TextView tv_arr_airport_name;
    @BindView(R.id.tv_arr_schedule_time)
    TextView tv_arr_schedule_time;
    @BindView(R.id.tv_arr_actualtime)
    TextView tv_arr_actualtime;
    @BindView(R.id.tv_arr_terminal_name)
    TextView tv_arr_terminal_name;
    @BindView(R.id.lin_details_lay)
    LinearLayout lin_details_lay;
    @BindView(R.id.tv_arr_status)
    TextView tv_arr_status;
    @BindView(R.id.dep_status)
    TextView dep_status;
    @BindView(R.id.tv_arr_delay)
    TextView tv_arr_delay;
    @BindView(R.id.tv_dep_delay)
    TextView tv_dep_delay;
    @Inject
    AVIAPI aviapi;
    final Calendar myCalendar = Calendar.getInstance();
    String selectedDate = "";


    @Override
    protected int getlayout() {
        return R.layout.activity_flight_status;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        ShoppingApplication.getContext().getComponent().inject(this);
        tv_title.setText(getString(R.string.sidemenu_flightstatus));
        btn_search.setOnClickListener(this);
        date_lay.setOnClickListener(this);
        user_login.setClipToOutline(true);
    }

    public void getflightStatus(String airlines, String flightid, String date) {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityFlightStatus.this,
                    R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;
        }
        Log.d("finput", "getflightStatus: " + airlines + "-" + flightid + "-" + date);
        shwProgress();
        aviapi.getFlightStatus(airlines, flightid, date).enqueue(new Callback<ModelNewFlightStatus>() {
            @Override
            public void onResponse(Call<ModelNewFlightStatus> call, Response<ModelNewFlightStatus> response) {
                hideprogress();
                hideKeyboard();
                if (response.body() != null) {
                    ModelNewFlightStatus data = response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus() == Constants.STATUS_200) {
                        if (data == null) {
                            Toast.makeText(ActivityFlightStatus.this,
                                    getString(R.string.server_error),
                                    Toast.LENGTH_SHORT).show();
                            return;
                        }

                        updateUi(data);
                    } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                        getflightStatus(airlines, flightid, date);
                    } else {
                        updateUi(data);
                        frame_result_lay.setVisibility(View.GONE);
                        search_lay.setVisibility(View.VISIBLE);
                        Toast.makeText(ActivityFlightStatus.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    // updateUi(null);
                    search_lay.setVisibility(View.VISIBLE);
                    frame_result_lay.setVisibility(View.GONE);
                    Toast.makeText(ActivityFlightStatus.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ModelNewFlightStatus> call, Throwable t) {
                hideprogress();
                updateUi(null);
                search_lay.setVisibility(View.VISIBLE);
                frame_result_lay.setVisibility(View.GONE);
                Log.d("faill", "onFailure: " + t.getMessage());
                Toast.makeText(ActivityFlightStatus.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void showDetailsScreen(ModelNewFlightStatus data) {
        lin_details_lay.setVisibility(View.VISIBLE);
        tv_dept_status.setBackground(getDrawable(R.drawable.bg_flight_dept_status));
        if (data != null && data.getData() != null && data.getData().getAirline() != null) {
            tv_flight_code.setText(data.getData().getAirline());
            tv_flight_code.setSelected(true);
        }
        if (data != null && data.getData() != null && data.getData().getAirlineCode() != null &&
                data != null && data.getData() != null && data.getData().getFlightNumber() != null) {
            tv_airlinename.setText(data.getData().getAirlineCode() + data.getData().getFlightNumber());

        }
        if (data.getData() != null && data.getData().getFlightStatus() != null &&
                data.getData().getFlightStatus().toLowerCase().equals(Constants.DELAYED)) {
            tv_dept_status.setBackground(getDrawable(R.drawable.bg_flight_red_status));
        } else {
            tv_dept_status.setBackground(getDrawable(R.drawable.bg_flight_dept_status));
        }
        boolean isdeparted = true;

        if (data.getData() != null && data.getData().getDep_delay() != null &&
                !data.getData().getDep_delay().equals("0")) {
            dep_status.setVisibility(View.VISIBLE);
            tv_dep_delay.setVisibility(View.VISIBLE);

            if (data.getData().getDepdate_actual() != null &&
                    data.getData().getDepTime_actual() != null &&
                    !data.getData().getDepdate_actual().equals("") &&
                    !data.getData().getDepTime_actual().equals("")) {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                Date newDate = null;
                try {
                    newDate = format.parse(data.getData().getDepdate_actual() + " " +
                            data.getData().getDepTime_actual());
                    if (Calendar.getInstance().getTime().after(newDate)) {
                        isdeparted=true;
                        tv_dept_status.setText(getString(R.string.dep_delayed) + " " +
                                data.getData().getDep_delay());
                        tv_dept_status.setBackground(getDrawable(R.drawable.bg_flight_red_status));
                    } else {
                        isdeparted=false;
                        tv_dept_status.setText(getString(R.string.dep_delay) + " " +
                                data.getData().getDep_delay());
                        tv_dept_status.setBackground(getDrawable(R.drawable.bg_flight_red_status));
                        tv_dep_actualtime_head.setText(getString(R.string.estimated_time));
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }


            tv_dep_delay.setText(getString(R.string.delayed_by) + " " + data.getData().getDep_delay());

        } else {
            dep_status.setVisibility(View.GONE);
            tv_dep_delay.setVisibility(View.GONE);
        }
        if (data.getData() != null && data.getData().getArr_delay() != null && !
                data.getData().getArr_delay().equals("0")) {
            tv_arr_status.setVisibility(View.VISIBLE);
            tv_arr_delay.setVisibility(View.VISIBLE);
            if (data.getData().getArrdate_actual() != null &&
                    data.getData().getArrTime_actual() != null &&
                    !data.getData().getArrdate_actual().equals("") &&
                    !data.getData().getArrTime_actual().equals("")) {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                Date newDate = null;
                try {
                    newDate = format.parse(data.getData().getArrdate_actual() + " " +
                            data.getData().getArrTime_actual());
                    if (Calendar.getInstance().getTime().after(newDate)&&isdeparted) {
                        tv_dept_status.setText(getString(R.string.arr_delayed) + " " +
                                data.getData().getArr_delay());
                        tv_dept_status.setBackground(getDrawable(R.drawable.bg_flight_red_status));
                    } else if (isdeparted){
                        tv_dept_status.setText(getString(R.string.arr_delay) + " " +
                                data.getData().getArr_delay());
                        tv_dept_status.setBackground(getDrawable(R.drawable.bg_flight_red_status));
                        tv_arr_actualtime_head.setText(getString(R.string.estimated_time));
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }


            tv_arr_delay.setText(getString(R.string.delayed_by) + " " + data.getData().getArr_delay());


        } else {
            tv_arr_status.setVisibility(View.GONE);
            tv_arr_delay.setVisibility(View.GONE);
        }

        if (data.getData().getFlightStatus().equals("Scheduled")) {
            tv_dep_actualtime_head.setText(getString(R.string.estimate_time));
        }
        try {
            if (data.getData().getFlightStatus().toLowerCase().equals(getString(R.string.cancelled))){
                tv_dept_status.setBackground(getDrawable(R.drawable.bg_flight_dept_status));
                tv_dept_status.setText(data.getData().getFlightStatus());
                tv_dep_delay.setText(getString(R.string.cancelled));
                tv_arr_delay.setText(getString(R.string.cancelled));
            }
        }
        catch (Exception e){

        }
    }

    public void updateUi(ModelNewFlightStatus data) {
        if (data != null && data.getData() != null) {
            search_lay.setVisibility(View.GONE);
            frame_result_lay.setVisibility(View.VISIBLE);

            tv_airlinename.setText(UtilsDefault.checkNull(data.getData().getAirline()));

            if (data.getData().getFlightStatus() != null && !
                    data.getData().getFlightStatus().equals("")) {
                tv_dept_status.setText(data.getData().getFlightStatus());
            }
            if (data.getData() != null) {
                if (data.getData().getArrCity() != null &&
                        data.getData().getArrCountry() != null) {
                    tv_arrival_city.setText(data.getData().getArrCity() + "," +
                            data.getData().getArrCountry());
                    tv_arr_airport_name.setText(data.getData().getArrCity() + "," +
                            data.getData().getArrCountry());

                } else {
                    tv_arrival_city.setText(getString(R.string.nill));
                    tv_arr_airport_name.setText(getString(R.string.nill));
                }

                if (data.getData().getArrCode() != null) {
                    tv_arrive_code.setText(data.getData().getArrCode());

                } else {
                    tv_arrive_code.setText(getString(R.string.nill));
                }
                if (data.getData().getAirlineCode() != null) {
                    tv_flight_code.setText(data.getData().getAirlineCode() +
                            data.getData().getFlightNumber());
                } else {
                    tv_flight_code.setText(getString(R.string.nill));
                }


                if (data.getData().getArrTime_actual() != null && data.getData().getArrdate_actual() != null) {
                    tv_arr_actualtime.setText(UtilsDefault.timeFormat(data.getData().getArrTime_actual()) + " , " +
                            UtilsDefault.dateFormatWithoutYear(
                                    data.getData().getArrdate_actual() + " 01:34:01"));
                } else {
                    tv_arr_actualtime.setText(getString(R.string.nill));
                }

                if (data.getData().getDepTime_actual() != null && data.getData().getDepdate_actual() != null) {
                    tv_dep_actualtime.setText(UtilsDefault.timeFormat(data.getData().getDepTime_actual()) + " , " +
                            UtilsDefault.dateFormatWithoutYear(
                                    data.getData().getDepdate_actual() + " 01:34:01"));
                } else {
                    tv_dep_actualtime.setText(getString(R.string.nill));
                }

                if (data.getData().getArrTime() != null) {
                    tv_arrive_time.setText(UtilsDefault.timeFormat(data.getData().getArrTime()));
                    tv_arr_schedule_time.setText(UtilsDefault.timeFormat(data.getData().getArrTime()) + " , " +
                            UtilsDefault.dateFormatWithoutYear(
                                    data.getData().getArrdate() + " 01:34:01"));

                } else {
                    tv_arrive_time.setText(getString(R.string.nill));
                    tv_arr_schedule_time.setText(getString(R.string.nill));
                }
                if (data.getData().getArrdate() != null) {
                    tv_arrive_date.setText(UtilsDefault.dateFormat(
                            data.getData().getArrdate() + " 01:34:01"));

                } else {
                    tv_arrive_date.setText(getString(R.string.nill));
                }


                if (data.getData().getArrTerminal() != null && !data.getData().getArrTerminal().equals("")) {
                    tv_arrival_terminal.setText(getString(R.string.text_terminal) + " " +
                            UtilsDefault.checkNull(data.getData().getArrTerminal())
                    );
                    tv_arr_terminal_name.setText(getString(R.string.text_terminal) + " " +
                            UtilsDefault.checkNull(data.getData().getArrTerminal()) + " "
                            + " , " + UtilsDefault.checkNull(data.getData().getArrGate()));
                }


            }

            // tv_dept_terminal.setText(R.string.text_terminal+" "+data.getData().getDeparture().getAirport().getAirportId().getAirportCode());
            //   tv_dept_terminal.setText(R.string.text_terminal+" "+data.getData().getDeparture().getAirport().getTerminal());
            //   UtilsDefault.updateSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));


            if (data.getData().getDepTerminal() != null && !data.getData().getDepTerminal().equals("")) {
                tv_dept_terminal.setText(getString(R.string.text_terminal) + " " +
                        UtilsDefault.checkNull(data.getData().getDepTerminal()));
                tv_dep_terminal_name.setText(getString(R.string.text_terminal) + " " +
                        UtilsDefault.checkNull(data.getData().getDepTerminal()) + " , "
                        + UtilsDefault.checkNull(data.getData().getDepGate()));
            }


            if (data.getData().getDepdate() != null) {
                tv_dept_date.setText(UtilsDefault.dateFormat(data.getData().getDepdate()
                        + " 01:34:01"));

            } else {
                tv_dept_date.setText(getString(R.string.nill));
            }
            if (data.getData().getDepTime() != null) {
                tv_dept_time.setText(UtilsDefault.timeFormat(data.getData().getDepTime()));
                tv_dep_schedule_time.setText(UtilsDefault.timeFormat(data.getData().getDepTime()) + " , " +
                        UtilsDefault.dateFormatWithoutYear(
                                data.getData().getDepdate() + " 01:34:01"));
            } else {
                tv_dept_time.setText(getString(R.string.nill));
                tv_dep_schedule_time.setText(getString(R.string.nill));
            }
            if (data.getData().getDepCity() != null &&
                    data.getData().getDeptCountry() != null) {
                tv_dept_city.setText(data.getData().getDepCity() + "," +
                        data.getData().getDeptCountry());
                tv_dep_airport_name.setText(data.getData().getDepCity() + "," +
                        data.getData().getDeptCountry());

            } else {
                tv_dept_city.setText(getString(R.string.nill));
                tv_dep_airport_name.setText(getString(R.string.nill));
            }
            if (data.getData().getDepCode() != null) {
                tv_dept_code.setText(data.getData().getDepCode());
            } else {
                tv_dept_code.setText(getString(R.string.nill));
            }

            if (data.getData().getDepTime() != null && data.getData().getArrTime() != null) {
                try {
                    if (data.getData().getDepTime() != null &&
                            data.getData().getArrTime() != null) {
                        tv_travel_time.setText(UtilsDefault.differentBetweenTime(data.getData().getDepTime(),
                                data.getData().getArrTime()));
                    } else {
                        tv_travel_time.setText(getString(R.string.nill));
                    }
                } catch (Exception e) {
                    Toast.makeText(ActivityFlightStatus.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            } else {
                tv_travel_time.setText(getString(R.string.nill));
            }
        } else {
            alert(getString(R.string.nodata));
        }
        showDetailsScreen(data);


    }

    @Override
    public void onClick(View v) {

        if (v == btn_search) {
            String airport = ed_airline.getText().toString().trim();
            String fligntnum = ed_flightnumber.getText().toString().trim();
//            if (airport.equals("")) {
//                ed_airline.setError(getString(R.string.this_feild_empty));
//                ed_airline.requestFocus();
//            }

            if (fligntnum.equals("")) {
                ed_flightnumber.setError(getString(R.string.this_feild_empty));
                ed_flightnumber.requestFocus();
            } else if (selectedDate.equals("")) {
                Toast.makeText(this, R.string.select_date, Toast.LENGTH_SHORT).show();
            } else {
                getflightStatus(airport, fligntnum, selectedDate.trim());
            }
        }
        if (v == date_lay) {
            DatePickerDialog datePickerDialog = new DatePickerDialog(ActivityFlightStatus.this, this, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePickerDialog.show();
        }
    }


    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        // TODO Auto-generated method stub
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, month);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        String mnth = "";
        if (month >= 9) {
            mnth = String.valueOf(month + 1);
        } else {
            int mntt = month + 1;
            mnth = "0" + mntt;
        }
        String day = "";
        if (dayOfMonth >9) {
            day = String.valueOf(dayOfMonth);
        } else {
            int dd = dayOfMonth;
            day = "0" + dd;
        }
        tv_selected_date.setText(UtilsDefault.dateFormat(year + "-" + mnth + "-" + day + " 01:34:01"));
        selectedDate = String.valueOf(year) + mnth + String.valueOf(day);

        Log.d("selectdate", "onDateSet: " + "" + year + "-" + mnth + "-" + dayOfMonth + "---" + selectedDate);

    }
}
