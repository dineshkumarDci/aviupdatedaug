package com.dci.avicustomer.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.StaticLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dci.avicustomer.R;
import com.dci.avicustomer.adapter.AdapterRestaurantItems;
import com.dci.avicustomer.adapter.AdapterRestroTitle;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.CommonMsgModel;
import com.dci.avicustomer.models.ModelRestaurant;
import com.dci.avicustomer.models.ModelShopList;
import com.dci.avicustomer.retrofit.AVIAPI;
import com.nex3z.notificationbadge.NotificationBadge;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityRestaurantDetails extends BaseActivity implements View.OnClickListener {
    @Inject
    public AVIAPI aviapi;
    @BindView(R.id.img_restro_file)
    ImageView img_restro_file;
    @BindView(R.id.card_restroimage)
    CardView card_restroimage;
    @BindView(R.id.img_expand_icon)
    ImageView img_expand_icon;
    @BindView(R.id.lin_secondlay)
    LinearLayout lin_secondlay;
    @BindView(R.id.img_restro_expand)
    ImageView img_restro_expand;
    @BindView(R.id.tv_restro_name)
    TextView tv_restro_name;
    @BindView(R.id.tv_open_time)
    TextView tv_open_time;
    @BindView(R.id.tv_days)
    TextView tv_days;
    @BindView(R.id.tv_terminal)
    TextView tv_terminal;
    @BindView(R.id.img_locate)
    ImageView img_locate;
    @BindView(R.id.img_phn_num)
    ImageView img_phn_num;
    @BindView(R.id.tv_open_status)
    TextView tv_open_status;
    @BindView(R.id.img_email)
    ImageView img_email;
    @BindView(R.id.tv_offer_code)
    TextView tv_offer_code;
    @BindView(R.id.recycle_restro_menu)
    RecyclerView recycle_restro_menu;
    @BindView(R.id.recycle_restro_items)
    RecyclerView recycle_restro_items;
    @BindView(R.id.rating)
    RatingBar rating;
    @BindView(R.id.tv_rating_value)
    TextView tv_rating_value;
    @BindView(R.id.img_cart)
    ImageView img_cart;
    @BindView(R.id.no_item)
    TextView no_item;
    @BindView(R.id.no_items)
    TextView no_items;
    @BindView(R.id.badge)
    NotificationBadge badgeview;
    @BindView(R.id.rel_icon)
    RelativeLayout rel_icon;
    @BindView(R.id.tv_estimate_time)
    TextView tv_estimate_time;
    @BindView(R.id.lin_star_lay)
    LinearLayout lin_star_lay;
    @BindView(R.id.tv_rest_type)
    TextView tv_rest_type;
    String emailid, mobilenumber;
    long lat, lang;
    String restroId;
    int selectedPost = -1;
    List<ModelRestaurant.MenuCatg> menuCatgList = new ArrayList<>();
    boolean isRestaurantAvailable = false;
    public String TAG = "ActivityRestaurantDetails";
    String latitude = "";
    String longitude = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        ShoppingApplication.getContext().getComponent().inject(this);
        img_expand_icon.setOnClickListener(this);
        tv_title.setText(getString(R.string.title_rest));
        if (getIntent().getStringExtra("id") != null) {
            restroId = getIntent().getStringExtra("id");
            Log.d(TAG, "onCreate: " + restroId);
            selectedPost = 0;
        }
        img_restro_expand.setClipToOutline(true);
        img_restro_file.setClipToOutline(true);
        img_locate.setOnClickListener(this);
        img_email.setOnClickListener(this);
        img_phn_num.setOnClickListener(this);
        rel_icon.setOnClickListener(this);
        lin_star_lay.setOnClickListener(this);
        tv_restro_name.setSelected(true);
    }
    @Override
    protected void onResume() {
        super.onResume();
        if (getIntent().getStringExtra("id") != null) {
            selectedPost = 0;
            if (restroId==null){
                restroId = getIntent().getStringExtra("id");
            }
            getRestroDetails();
        }
    }

    public void renewCart(String itemid, String type) {
        /**
         * if
         *  1 - add / 2 - reduce / 3 - delete row
         */
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityRestaurantDetails.this,
                    R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;
        }
        shwProgress();
        //  Log.d("aviparms", "search: "+UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)+search);
        String userid = "0";
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
            userid = UtilsDefault.getSharedPreferenceValue(Constants.USER_ID);
        }
        Log.d("tag", "addCart: " + itemid);
        aviapi.renewCart(itemid, UtilsDefault.getSharedPreferenceValue(Constants.USER_ID),
                UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID),
                UtilsDefault.getSharedPreferenceValue(Constants.TERMINAL_ID), type).enqueue(new Callback<CommonMsgModel>() {
            @Override
            public void onResponse(Call<CommonMsgModel> call, Response<CommonMsgModel> response) {
                hideprogress();
                hideKeyboard();

                if (response.body() != null) {
                    CommonMsgModel data = response.body();
//                    String header = response.headers().get(getString(R.string.header_find_key));
//                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus() == Constants.STATUS_200) {
                        if (commonalert != null) {
                            commonalert.cancel();
                        }
                        getRestroDetails();


                        //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                    } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {

                        renewCart(itemid, type);
                    } else {

                        Toast.makeText(ActivityRestaurantDetails.this,
                                data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ActivityRestaurantDetails.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<CommonMsgModel> call, Throwable t) {
                hideprogress();
                hideKeyboard();
                Log.d("failure", "onFailure: " + t.getMessage());
                Toast.makeText(ActivityRestaurantDetails.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void alertcommon(String title, String content, String itemid, String type) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setCancelable(true);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_common_alert_dialog, null);
        alertDialogBuilder.setView(dialogView);
        TextView titletv = dialogView.findViewById(R.id.tv_alert_title);
        titletv.setText(title);
        TextView tv_content = dialogView.findViewById(R.id.tv_content);
        tv_content.setText(content);
        btn_ok = dialogView.findViewById(R.id.btn_ok);
        btn_ok.setText(getResources().getString(R.string.text_add));
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                renewCart(itemid, type);
            }
        });


        commonalert = alertDialogBuilder.create();
        commonalert.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        commonalert.show();
    }

    public void addCart(String itemid, String type) {

        /**
         * if
         *  1 - add / 2 - reduce / 3 - delete row
         */
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityRestaurantDetails.this, R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;
        }
        shwProgress();
        //  Log.d("aviparms", "search: "+UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)+search);
        String userid = "0";
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
            userid = UtilsDefault.getSharedPreferenceValue(Constants.USER_ID);
        }
        Log.d("tag", "addCart: " + itemid);
        aviapi.addCart(itemid, UtilsDefault.getSharedPreferenceValue(Constants.USER_ID),
                UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID),
                UtilsDefault.getSharedPreferenceValue(Constants.TERMINAL_ID), type).
                enqueue(new Callback<CommonMsgModel>() {
            @Override
            public void onResponse(Call<CommonMsgModel> call, Response<CommonMsgModel> response) {
                hideprogress();
                hideKeyboard();

                if (response.body() != null) {
                    CommonMsgModel data = response.body();
//                    String header = response.headers().get(getString(R.string.header_find_key));
//                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus() == Constants.STATUS_200) {
                        getRestroDetails();


                        //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                    } else if (data.getStatus() == Constants.STATUS_501) {
                        alertcommon(getString(R.string.items_already_in_cart), data.getMessage(), itemid, type);

                    } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {

                        addCart(itemid, type);
                    } else {

                        Toast.makeText(ActivityRestaurantDetails.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ActivityRestaurantDetails.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<CommonMsgModel> call, Throwable t) {
                hideprogress();
                hideKeyboard();
                Log.d("failure", "onFailure: " + t.getMessage());
                Toast.makeText(ActivityRestaurantDetails.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getRestroDetails() {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityRestaurantDetails.this, R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;
        }
        shwProgress();
        //  Log.d("aviparms", "search: "+UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)+search);
        String userid = "0";
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
            userid = UtilsDefault.getSharedPreferenceValue(Constants.USER_ID);

        }
        aviapi.getRestroDetails(restroId,
                UtilsDefault.getSharedPreferenceValue(Constants.USER_ID),
                UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID),
                UtilsDefault.getSharedPreferenceValue(Constants.TERMINAL_ID)).
                enqueue(new Callback<ModelRestaurant>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(Call<ModelRestaurant> call, Response<ModelRestaurant> response) {
                        hideprogress();
                        hideKeyboard();

                        if (response.body() != null) {
                            ModelRestaurant data = response.body();
                            String header = response.headers().get(getString(R.string.header_find_key));
                            UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                            if (data.getStatus() == Constants.STATUS_200) {
                                if (data.getData().getRestaurent().getETA() != null && !
                                        data.getData().getRestaurent().getETA().equals("")) {
                                    if (data.getData().getRestaurent().getETA().contains(":")) {
                                        String time[] = data.getData().getRestaurent().getETA().split(":");

                                        StringBuffer stringBuffer = new StringBuffer();
                                        if (!time[0].equals("00")) {
                                            stringBuffer.append(time[0] + " Hrs ");
                                        }
                                        if (!time[1].equals("00")) {
                                            stringBuffer.append(time[1] + " Mins");
                                        }
                                        tv_estimate_time.setText("Average Time response : " + stringBuffer);

                                    }
                                }

                                if (data.getData().getRestaurent().getRestaurant_type() != null) {
                                    tv_rest_type.setVisibility(View.VISIBLE);
                                    tv_rest_type.setText(UtilsDefault.checkNull(data.getData().getRestaurent().getRestaurant_type().
                                            getName()));
                                } else {
                                    tv_rest_type.setVisibility(View.GONE);
                                }


                                tv_title.setText(data.getData().getRestaurent().getName());
                                if (data.getData().getRestaurent().getShop_image().contains(",")) {
                                    String[] image = data.getData().getRestaurent().getShop_image().split(",");
                                    Glide.with(ActivityRestaurantDetails.this).
                                            load(data.getImagePath().getRestaurant() + image[0]).into(img_restro_expand);
                                    Glide.with(ActivityRestaurantDetails.this).
                                            load(data.getImagePath().getRestaurant() + image[0]).into(img_restro_file);
                                } else {
                                    Glide.with(ActivityRestaurantDetails.this).load(data.getImagePath().getRestaurant() + data.getData().getRestaurent().
                                            getShop_image()).into(img_restro_expand);
                                    Glide.with(ActivityRestaurantDetails.this).load(data.getImagePath().getRestaurant() + data.getData().getRestaurent().
                                            getShop_image()).into(img_restro_file);
                                }

                                tv_restro_name.setText(UtilsDefault.checkNull(data.getData().getRestaurent().getName()));
                                if (data.getData().getRestaurent().getOffer_code() != null &&
                                        !data.getData().getRestaurent().getOffer_code().equals("")) {
                                    tv_offer_code.setText(UtilsDefault.checkNull(data.getData().getRestaurent().
                                            getOffer_code()));

                                } else {
                                    tv_offer_code.setVisibility(View.INVISIBLE);
                                }

                                StringBuilder stringBuilder = new StringBuilder();
                                stringBuilder.append(UtilsDefault.checkNull(data.getData().getRestaurent()
                                        .getTerminal().getName()));
                                if (data.getData().getRestaurent().getArea_type() != null && data.
                                        getData().getRestaurent().getArea_type() == Constants.AREA_DEP) {
                                    stringBuilder.append("-" + "Departure");
                                } else {
                                    stringBuilder.append("-" + "Arrival");
                                }
                                tv_terminal.setText(stringBuilder);

                                tv_rating_value.setText("( " + data.getData().getRestaurent().getRating_count() + " )");
                                if (data.getData().getRestaurent().getAverage_rating() != null &&
                                        !data.getData().getRestaurent().getAverage_rating().equals("")) {
                                    rating.setRating(Float.valueOf(data.getData().getRestaurent().
                                            getAverage_rating()));
                                }
                                emailid = data.getData().getRestaurent().getEmail();
                                mobilenumber = data.getData().getRestaurent().getPhone();
                                if (data.getData().getMenuCatg() != null && data.getData().getMenuCatg().size() != 0) {
                                    menuCatgList.clear();
                                    menuCatgList.addAll(data.getData().getMenuCatg());
                                    recycle_restro_menu.setVisibility(View.VISIBLE);
                                    no_item.setVisibility(View.GONE);
                                    recycle_restro_menu.setLayoutManager(new LinearLayoutManager(ActivityRestaurantDetails.this, LinearLayoutManager.HORIZONTAL, false));
                                    AdapterRestroTitle adapterRestaurantItems = new AdapterRestroTitle(ActivityRestaurantDetails.this,
                                            menuCatgList, selectedPost);
                                    adapterRestaurantItems.setonItemClick(new OnItemClickListener() {
                                        @Override
                                        public void OnItemClickListener(int pos, View view) {
                                            selectedPost = pos;
                                            setRestroItemsData(data.getImagePath().getMenu());
                                            adapterRestaurantItems.notifyDataSetChanged();
                                        }
                                    });
                                    recycle_restro_menu.setAdapter(adapterRestaurantItems);
                                    setRestroItemsData(data.getImagePath().getMenu());

                                } else {
                                    recycle_restro_menu.setVisibility(View.GONE);
                                    // no_item.setVisibility(View.VISIBLE);


                                }
                                //latitude=data.getData().getRestaurent().getl
                                badgeview.setNumber(data.getTotalCart(), true);
                                if (data.getData().getRestaurent().getOpenClose() != null &&
                                        data.getData().getRestaurent().getOpenClose() == 1) {
                                    tv_open_status.setVisibility(View.VISIBLE);
                                    tv_open_status.setText(getString(R.string.open));
                                    tv_open_status.setTextColor(getResources().getColor(R.color.dark_green));
                                    isRestaurantAvailable = true;

                                    if (data.getData().getRestaurent().getRestaurant_times() != null &&
                                            data.getData().getRestaurent().getRestaurant_times().size() != 0) {
                                        try {
                                            if (data.getData().getRestaurent().getRestaurant_times() != null &&
                                                    data.getData().getRestaurent().getRestaurant_times().size() != 0 &&
                                                    data.getData().getRestaurent().getRestaurant_times().get(0).getOpen_days() != null) {
                                                tv_days.setText(data.getData().getRestaurent().
                                                        getRestaurant_times().get(0).getOpen_days());
                                                tv_days.setSelected(true);
                                            }
                                        }
                                        catch (Exception e){

                                        }


                                        SimpleDateFormat sdf = new SimpleDateFormat("EEE");
                                        Date d = new Date();
                                        String dayOfTheWeek = sdf.format(d);
                                        Log.d("day", "onBindViewHolder: " + dayOfTheWeek);
                                        boolean isshopOpen = false;
                                        boolean isCurrentDayAvailable = false;

                                        if (data.getData().getRestaurent().getRestaurant_times().get(0).
                                                getOpen_24_hrs().
                                                equalsIgnoreCase("yes")) {
                                            tv_open_time.setText(getString(R.string.hrs_open));
                                            tv_open_status.setTextColor(getResources().getColor(R.color.dark_green));
                                            isRestaurantAvailable = true;
                                            String[] Days = data.getData().getRestaurent().
                                                    getRestaurant_times().get(0).
                                                    getOpen_days().split(",");

                                            for (String day : Days) {
                                                Log.d("apiday", "onBindViewHolder: " + day);
                                                if (dayOfTheWeek.toLowerCase().equals(day.toLowerCase())) {
                                                    isCurrentDayAvailable = true;
                                                }

                                            }
                                            if (isCurrentDayAvailable) {
                                                tv_open_status.setText(getString(R.string.open));
                                                tv_open_status.setTextColor(getResources().
                                                        getColor(R.color.dark_green));
                                                isRestaurantAvailable = true;

                                            }
                                            else {
                                                isRestaurantAvailable = false;
                                                tv_open_status.setText(getString(R.string.closed));
                                                tv_open_status.setTextColor(getResources().
                                                        getColor(R.color.red));
                                            }


                                        } else if (data.getData().getRestaurent().
                                                getRestaurant_times().get(0).getOpen_days().
                                                contains(",")) {

                                            tv_open_time.setText(data.getData().getRestaurent().getRestaurant_times().get(0).
                                                    getFrom_time()
                                                    + data.getData().getRestaurent().getRestaurant_times().get(0).getFrom_am_pm()
                                                    + " To " +
                                                    data.getData().getRestaurent().getRestaurant_times().get(0).getTo_time() +
                                                    data.getData().getRestaurent().getRestaurant_times().get(0).getTo_am_pm());
                                            String[] Days = data.getData().getRestaurent().
                                                    getRestaurant_times().get(0).
                                                    getOpen_days().split(",");

                                            for (String day : Days) {
                                                Log.d("apiday", "onBindViewHolder: " + day);
                                                if (dayOfTheWeek.toLowerCase().equals(day.toLowerCase())) {
                                                    isCurrentDayAvailable = true;
                                                }

                                            }
                                            if (isCurrentDayAvailable) {
                                                try {
                                                    isshopOpen = checkTimeIntervel(data.getData().getRestaurent().getRestaurant_times().get(0).
                                                                    getFrom_time()
                                                                    + " " + data.getData().getRestaurent().getRestaurant_times().get(0).getFrom_am_pm().toUpperCase(),
                                                            data.getData().getRestaurent().getRestaurant_times().get(0).getTo_time() + " " +
                                                                    data.getData().getRestaurent().getRestaurant_times().get(0).getTo_am_pm().toUpperCase());
                                                } catch (Exception e) {

                                                }

                                            }
                                            else {
                                                isRestaurantAvailable = false;
                                                tv_open_status.setText(getString(R.string.closed));
                                                tv_open_status.setTextColor(getResources().
                                                        getColor(R.color.red));
                                            }

                                            if (isshopOpen) {
                                                tv_open_status.setText(getString(R.string.open));
                                                tv_open_status.setTextColor(getResources().
                                                        getColor(R.color.dark_green));
                                                isRestaurantAvailable = true;
                                            } else {
                                                isRestaurantAvailable = false;
                                                tv_open_status.setText(getString(R.string.closed));
                                                tv_open_status.setTextColor(getResources().
                                                        getColor(R.color.red));
                                            }


                                        } else {
                                            if (data.getData().getRestaurent().getRestaurant_times().get(0).
                                                    getOpen_days().toLowerCase().contains(dayOfTheWeek)) {
                                                tv_open_time.setText(data.getData().getRestaurent().getRestaurant_times().get(0).
                                                        getFrom_time()
                                                        + data.getData().getRestaurent().getRestaurant_times().get(0).getFrom_am_pm()
                                                        + " To " +
                                                        data.getData().getRestaurent().getRestaurant_times().get(0).getTo_time() +
                                                        data.getData().getRestaurent().getRestaurant_times().get(0).getTo_am_pm());
                                                isshopOpen = checkTimeIntervel(data.getData().getRestaurent().getRestaurant_times().get(0).
                                                                getFrom_time()
                                                                + " " + data.getData().getRestaurent().getRestaurant_times().get(0).getFrom_am_pm().toUpperCase(),
                                                        data.getData().getRestaurent().getRestaurant_times().get(0).getTo_time() + " " +
                                                                data.getData().getRestaurent().getRestaurant_times().get(0).getTo_am_pm().toUpperCase());
                                                if (isshopOpen) {
                                                    isRestaurantAvailable = true;
                                                    tv_open_status.setText(getString(R.string.open));
                                                    tv_open_status.setTextColor(getResources().
                                                            getColor(R.color.dark_green));
                                                } else {
                                                    isRestaurantAvailable = false;
                                                    tv_open_status.setText(getString(R.string.closed));
                                                    tv_open_status.setTextColor(getResources().
                                                            getColor(R.color.red));
                                                }

                                            }
                                        }


                                    }
                                } else {
                                    tv_open_status.setVisibility(View.VISIBLE);
                                    isRestaurantAvailable = false;
                                    tv_open_status.setText(getString(R.string.closed));
                                    tv_open_status.setTextColor(getResources().
                                            getColor(R.color.red));
                                    try {
                                        if (data.getData().getRestaurent().getRestaurant_times() != null &&
                                                data.getData().getRestaurent().getRestaurant_times().size() != 0 &&
                                                data.getData().getRestaurent().getRestaurant_times().get(0).getOpen_days() != null) {
                                            tv_days.setText(data.getData().getRestaurent().
                                                    getRestaurant_times().get(0).getOpen_days());
                                            tv_days.setSelected(true);
                                        }
                                        if (data.getData().getRestaurent().getRestaurant_times().get(0).
                                                getOpen_24_hrs().
                                                equalsIgnoreCase("yes")) {
                                            tv_open_time.setText(getString(R.string.hrs_open));
                                            // tv_open_status.setTextColor(getResources().getColor(R.color.dark_green));

                                        } else {

                                            try {
                                                tv_open_time.setText(data.getData().getRestaurent().getRestaurant_times().get(0).
                                                        getFrom_time()
                                                        + data.getData().getRestaurent().getRestaurant_times().get(0).getFrom_am_pm()
                                                        + " To " +
                                                        data.getData().getRestaurent().getRestaurant_times().get(0).getTo_time() +
                                                        data.getData().getRestaurent().getRestaurant_times().get(0).getTo_am_pm());
                                            } catch (Exception e) {

                                            }

                                        }
                                    }
                                    catch (Exception e){

                                    }

                                }



                                // SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
//                                    Date d = new Date();
//                                    String dayOfTheWeek = sdf.format(d);
//                                    for (ModelRestaurant.Restaurent.Retail_time datas :
//                                            data.getData().getRestaurent().getRestaurant_times()) {
//                                        Log.d("days", "onResponse: " + datas.getDay() + "-" +
//                                                dayOfTheWeek.toLowerCase());
//                                        if (datas.getDay() != null && datas.getDay().toLowerCase().
//                                                equals(dayOfTheWeek.toLowerCase())) {
//                                            if (datas.getFrom_time() != null && datas.getTo_time() != null) {
//                                                tv_open_time.setText(datas.getFrom_time() + " to " + datas.getTo_time());
//
//                                            }
//                                        }
//                                    }
                                if (data.getData().getRestaurent().getLatitude() != null) {
                                    latitude = data.getData().getRestaurent().getLatitude();
                                }
                                if (data.getData().getRestaurent().getLongitude() != null) {
                                    longitude = data.getData().getRestaurent().getLongitude();
                                }


                                //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                            } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {

                                getRestroDetails();
                            } else {
                                alertcommon(getString(R.string.title_rest), data.getMessage());
                                //Toast.makeText(ActivityRestaurantDetails.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            alertcommon(getString(R.string.title_rest), getString(R.string.server_error));
                            // Toast.makeText(ActivityRestaurantDetails.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<ModelRestaurant> call, Throwable t) {
                        hideprogress();
                        hideKeyboard();
                        alertcommon(getString(R.string.title_rest), getString(R.string.server_error));
                        Log.d("failure", "onFailure: " + t.getMessage());
                        // Toast.makeText(ActivityRestaurantDetails.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void closeCommonAlert() {
        commonalert.dismiss();
        finish();
    }

    public Boolean checkTimeIntervel(String startTime, String endTime) {
        try {
            // String string1 = "20:11:13";
            Date c = Calendar.getInstance().getTime();
            System.out.println("Current time => " + c);

            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
            String formattedDate = df.format(c);
            String fromDate = formattedDate + " " + startTime;
            Log.d("times", "checkTimeIntervel: " + startTime + "end" + endTime);
            Date time1 = new SimpleDateFormat("dd-MMM-yyyy hh:mm aa").parse(fromDate);
            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTime(time1);
            //  calendar1.add(Calendar.DATE, 1);
            Log.d(TAG, "checkTimeIntervel1: " + calendar1.getTime().toString());

            //String string2 = "14:49:00";

            String todate = formattedDate + " " + endTime;
            Date time2 = new SimpleDateFormat("dd-MMM-yyyy hh:mm aa").parse(todate);
            Calendar calendar2 = Calendar.getInstance();
            calendar2.setTime(time2);
            calendar2.add(Calendar.DATE, 1);
            Log.d(TAG, "checkTimeIntervel2: " + calendar2.getTime().toString());

            // String someRandomTime = "01:00:00";
            //  Date d = new SimpleDateFormat("hh:mm aa").parse(someRandomTime);
            Calendar calendar3 = Calendar.getInstance();
            Date x = calendar3.getTime();
            Log.d(TAG, "checkTimeIntervel3: " + calendar3.getTime().toString());
            if (x.after(calendar1.getTime()) && x.before(calendar2.getTime())) {
                Log.d(TAG, "checkTimeIntervelss: " + "validated");

                System.out.println(true);
                return true;
            } else {
                Log.d(TAG, "checkTimeIntervelss: " + "false");
                return false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
            Log.d("timer", "checkTimeIntervel: " + "parseerror");
            return false;
        }

    }

    public void setRestroItemsData(String path) {
        try {
            if (selectedPost != -1) {
                if (menuCatgList.get(selectedPost).getRestaurantmenu() != null &&
                        menuCatgList.get(selectedPost).getRestaurantmenu().size() != 0) {
                    List<ModelRestaurant.Restaurantmenu> list = new ArrayList<>();
                    list.clear();
                    list.addAll(menuCatgList.get(selectedPost).getRestaurantmenu());
                    recycle_restro_items.setLayoutManager(new LinearLayoutManager(ActivityRestaurantDetails.this));
                    AdapterRestaurantItems adapterRestaurantItems = new AdapterRestaurantItems(
                            ActivityRestaurantDetails.this, list, path);
                    adapterRestaurantItems.setonItemClick(new OnItemClickListener() {
                        @Override
                        public void OnItemClickListener(int pos, View view) {
                            if (view.getId() == R.id.img_plus) {
//                            if (!isRestaurantAvailable){
//                                Toast.makeText(ActivityRestaurantDetails.this, getResources().getString(R.string.restro_not_avialable), Toast.LENGTH_SHORT).show();
//                                return;
//                            }
                                if (menuCatgList.size() != 0) {
                                    if (!isRestaurantAvailable) {
                                        Toast.makeText(ActivityRestaurantDetails.this, getResources().getString(R.string.restro_not_avialable), Toast.LENGTH_SHORT).show();
                                        return;
                                    }
                                    addCart(String.valueOf(list.get(pos).getId()), Constants.CART_PLUS);
                                }
                            } else {
                                if (!isRestaurantAvailable) {
                                    Toast.makeText(ActivityRestaurantDetails.this, getResources().getString(R.string.restro_not_avialable), Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                if (menuCatgList.size() != 0) {

                                    addCart(String.valueOf(list.get(pos).getId()), Constants.CART_MINUS);
                                }
                            }

                        }
                    });
                    recycle_restro_items.setAdapter(adapterRestaurantItems);
                    no_items.setVisibility(View.GONE);
                } else {
                    recycle_restro_items.setVisibility(View.GONE);
                }

            }
        } catch (Exception e) {

        }

    }

    @Override
    protected int getlayout() {
        return R.layout.activity_restro_details;
    }

    @Override
    public void onClick(View v) {
        if (v == img_expand_icon) {
            if (lin_secondlay.getVisibility() == View.GONE) {
                img_expand_icon.setImageResource(R.drawable.up_arrow);
                lin_secondlay.setVisibility(View.VISIBLE);
                img_restro_file.setVisibility(View.GONE);
                img_restro_expand.setVisibility(View.VISIBLE);
            } else if (lin_secondlay.getVisibility() == View.VISIBLE) {
                img_expand_icon.setImageResource(R.drawable.down_icon);
                lin_secondlay.setVisibility(View.GONE);
                img_restro_expand.setVisibility(View.GONE);
                img_restro_file.setVisibility(View.VISIBLE);
            }
        }
        if (v == lin_star_lay) {
            Intent intent = new Intent(ActivityRestaurantDetails.this, ActivityUserReviews.class);
            intent.putExtra("id", restroId);
            startActivity(intent);
        }
        if (v == rel_icon) {
//            if (!isRestaurantAvailable) {
//                Toast.makeText(ActivityRestaurantDetails.this, getResources().getString(R.string.restro_not_avialable), Toast.LENGTH_SHORT).show();
//                return;
//            }
            startActivity(new Intent(ActivityRestaurantDetails.this,
                    ActivityCartView.class));
        }

        if (v == img_email) {
            if (emailid != null && !
                    emailid.equals("")) {
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + emailid.trim()));
                    startActivity(intent);
                } catch (Exception e) {
                    Toast.makeText(this, "Sorry...You don't have any mail app", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(this, R.string.no_email, Toast.LENGTH_SHORT).show();
            }
        }
        if (v == img_locate) {
            if (!latitude.equals("") &&
                    !longitude.equals("")) {
                Uri gmmIntentUri = Uri.parse("geo:<" + latitude + ">,<" + longitude + ">?q=<lat>,<long>(Label+Name)");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            } else {
                Toast.makeText(this, R.string.locate_detailsnot, Toast.LENGTH_SHORT).show();
            }

        }
        if (v == img_phn_num) {
            if (mobilenumber != null && !
                    mobilenumber.equals("")) {
                Uri u = Uri.parse("tel:" + mobilenumber.trim());
                Intent i = new Intent(Intent.ACTION_DIAL, u);
                startActivity(i);
            } else {
                Toast.makeText(this, R.string.no_phone_num, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
