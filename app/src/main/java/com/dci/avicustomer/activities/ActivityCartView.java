package com.dci.avicustomer.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.avicustomer.R;
import com.dci.avicustomer.adapter.AdapterCartView;
import com.dci.avicustomer.ccavenue.AvenuesParams;
import com.dci.avicustomer.ccavenue.ServiceUtility;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.LocationUpdatesComponent;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.CommonMsgModel;
import com.dci.avicustomer.models.ModelApplyCoupon;
import com.dci.avicustomer.models.ModelCartView;
import com.dci.avicustomer.models.ModelDocumentDetailsList;
import com.dci.avicustomer.models.ModelOrderCheck;
import com.dci.avicustomer.retrofit.AVIAPI;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.dynamic.IFragmentWrapper;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import im.delight.android.location.SimpleLocation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityCartView extends BaseActivity
        implements
        CheckBox.OnCheckedChangeListener,
        View.OnClickListener, OnItemClickListener {
    @Inject
    public AVIAPI aviapi;
    @BindView(R.id.check_takeway)
    CheckBox check_takeway;
    @BindView(R.id.spinn_num_pax)
    Spinner spinn_num_pax;
    @BindView(R.id.check_dine_in)
    CheckBox check_dine_in;
    @BindView(R.id.btn_add_promo)
    Button btn_add_promo;
    @BindView(R.id.tv_subtotal)
    TextView tv_subtotal;
    @BindView(R.id.tv_discount_amnt)
    TextView tv_discount_amnt;
    @BindView(R.id.tv_totalamnt)
    TextView tv_totalamnt;
    @BindView(R.id.btn_checkout)
    Button btn_checkout;
    @BindView(R.id.recycle_cart_items)
    RecyclerView recycle_cart_items;
    @BindView(R.id.tv_total_items)
    TextView tv_total_items;
    @BindView(R.id.spinner_img)
    ImageView spinner_img;
    @BindView(R.id.edit_add_promo)
    EditText edit_add_promo;
    @BindView(R.id.tv_service_charge)
    TextView tv_service_charge;
    @BindView(R.id.end_lay)
    LinearLayout end_lay;
    @BindView(R.id.lin_lay_dinein)
    LinearLayout lin_lay_dinein;
    @BindView(R.id.rel_lay_takeaway)
    RelativeLayout rel_lay_takeaway;

    @BindView(R.id.lin_gst_lay)
    LinearLayout lin_gst_lay;
    @BindView(R.id.lin_convn_lay)
    LinearLayout lin_convn_lay;
    @BindView(R.id.lin_handling_lay)
    LinearLayout lin_handling_lay;
    @BindView(R.id.lin_deliver_lay)
    LinearLayout lin_deliver_lay;
    @BindView(R.id.lin_discount_lay)
    LinearLayout lin_discount_lay;
    @BindView(R.id.tv_gst)
    TextView tv_gst;
    @BindView(R.id.tv_convn_fee)
    TextView tv_convn_fee;
    @BindView(R.id.tv_handling_fee)
    TextView tv_handling_fee;
    private LocationUpdatesComponent locationUpdatesComponent;
    private SimpleLocation location;
    String Eta_time = "";
    String couponAmnt = "";
    Double price = 0.0;
    Double discount = 0.0;
    Double subTotal = 0.0;
    String TAG = "ActivityCartView";
    int locationPermission = 100;
    String currency = "";
    String restroid = "";
    String no_ofPax = "1";
    String coupon = "";
    String total = "";
    String totalamnt = "";
    List<String> list = new ArrayList<>();
    List<ModelCartView.Cart> cart_list = new ArrayList<>();
    String place = "";
    boolean isAppClose = false;
    static String Tag = "cartview";
    String cityName = "";

    public String getCountrycode() {
        String locale;
        TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        String countryCodeValue = tm.getNetworkCountryIso();
        Log.d(Tag, "getCountrycode: " + countryCodeValue.toLowerCase());
        return countryCodeValue.toLowerCase();

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        ShoppingApplication.getContext().getComponent().inject(this);

        check_dine_in.setOnCheckedChangeListener(this);
        check_takeway.setOnCheckedChangeListener(this);
       // check_takeway.setChecked(true);
        spinner_img.setOnClickListener(this);
        btn_add_promo.setOnClickListener(this);
        btn_checkout.setOnClickListener(this);
        ArrayAdapter<String> spinnerArrayAdapter = new
                ArrayAdapter<String>(ActivityCartView.this, R.layout.simple_spinner_item,
                R.id.text,
                getNoPax());
        spinn_num_pax.setAdapter(spinnerArrayAdapter);
        spinn_num_pax.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                no_ofPax = list.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        location = new SimpleLocation(this);

        getCartList();
        // displayLocationSettingsRequest();

        //locationRequest();

    }

    public void locationRequest() {

        location = new SimpleLocation(this);

        location.setListener(new SimpleLocation.Listener() {

            public void onPositionChanged() {
                final double latitude = location.getLatitude();
                final double longitude = location.getLongitude();
                Log.d(TAG, "dataLocation: " + latitude);
                if (latitude != 0.0 && longitude != 0.0) {
                    getUserGeoInfo(latitude, longitude);
                }

                // new location data has been received and can be accessed
            }

        });

        final double latitude = location.getLatitude();
        final double longitude = location.getLongitude();
        Log.d(TAG, "dataLocation: " + latitude);
        if (latitude != 0.0 && longitude != 0.0) {
            getUserGeoInfo(latitude, longitude);
        }
        //Log.d(TAG, "dataLocation: " + location.getLatitude());
    }

    @Override
    protected void onResume() {
        super.onResume();
//        if (android.os.Build.VERSION.SDK_INT >=
//                android.os.Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(this,
//                android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
//                ContextCompat.checkSelfPermission(
//                        this,
//                        android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(this, new String[]{
//                            android.Manifest.permission.ACCESS_COARSE_LOCATION,
//                            android.Manifest.permission.ACCESS_FINE_LOCATION},
//                    locationPermission);
//            Toast.makeText(this, R.string.enable_location, Toast.LENGTH_SHORT).show();
//            return;
//        }
//
//        if (location != null) {
//            if (location.hasLocationEnabled()) {
//                locationRequest();
//                location.beginUpdates();
//            } else {
//                locationAlert();
//
//            }
//        }


        // make the device update its location

        // ...
    }


    public void locationAlert() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(getString(R.string.turnonlocation));

        alertDialogBuilder.setPositiveButton("Turnon",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        dialog.dismiss();
                        SimpleLocation.openSettings(ActivityCartView.this);

                    }
                });

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void confirmAlert() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(getString(R.string.payment_confirmmsg));

        alertDialogBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        dialog.dismiss();
                        authenticationFromBackend();
                        //  SimpleLocation.openSettings(ActivityCartView.this);

                    }
                });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        dialog.dismiss();
                        //SimpleLocation.openSettings(ActivityCartView.this);
                    }
                });

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    protected void onPause() {
        // stop location updates (saves battery)

//        try {
//            if (location != null)
//                location.endUpdates();
//        } catch (Exception e) {
//
//        }


        // ...

        super.onPause();
    }

    public List<String> getNoPax() {
        list.clear();
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        list.add("6");
        list.add("7");
        list.add("8");
        list.add("9");
        list.add("10");
        return list;
    }

//    private void displayLocationSettingsRequest() {
//        if (ContextCompat.checkSelfPermission(this,
//                android.Manifest.permission.ACCESS_COARSE_LOCATION)
//                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
//                this,
//                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(this, new String[]{
//                            android.Manifest.permission.ACCESS_COARSE_LOCATION,
//                            android.Manifest.permission.ACCESS_FINE_LOCATION},
//                    locationPermission);
//            Toast.makeText(this, R.string.enable_location, Toast.LENGTH_SHORT).show();
//            return;
//        }
//        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(ActivityCartView.this)
//                .addApi(LocationServices.API).build();
//        googleApiClient.connect();
//        LocationRequest locationRequest = LocationRequest.create();
//        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
//        locationRequest.setInterval(10000);
//        locationRequest.setFastestInterval(10000 / 2);
//        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
//        builder.setAlwaysShow(true);
//        SettingsClient client = LocationServices.getSettingsClient(this);
//        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
//
//        task.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
//            @Override
//            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
//                locationRequest();
//                //  StartLocationUpdates();
//
//                //Toast.makeText(ActivitySetting.this, "addOnSuccessListener", Toast.LENGTH_SHORT).show();
//                // All location settings are satisfied. The client can initialize
//                // location requests here.
//                // ...
//
//            }
//        });
//        task.addOnCanceledListener(this, new OnCanceledListener() {
//            @Override
//            public void onCanceled() {
//
//            }
//        });
//        task.addOnCompleteListener(this, new OnCompleteListener<LocationSettingsResponse>() {
//            @Override
//            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
//                locationRequest();
//                //StartLocationUpdates();
//                Log.d(TAG, "onComplete: " + "onComplete");
//            }
//        });
//
//        task.addOnFailureListener(this, new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                // Toast.makeText(ActivitySetting.this, "addOnFailureListener", Toast.LENGTH_SHORT).show();
//                if (e instanceof ResolvableApiException) {
//                    // Location settings are not satisfied, but this can be fixed
//                    // by showing the user a dialog.
//                    try {
//                        // Show the dialog by calling startResolutionForResult(),
//                        // and check the result in onActivityResult().
//                        ResolvableApiException resolvable = (ResolvableApiException) e;
//                        resolvable.startResolutionForResult(ActivityCartView.this,
//                                REQUEST_CHECK_SETTINGS);
//                    } catch (IntentSender.SendIntentException sendEx) {
//                        // Ignore the error.
//                    }
//                }
//            }
//        });
//    }

    public void StartLocationUpdates() {
        //   Toast.makeText(this, "Calling", Toast.LENGTH_SHORT).show();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this, new String[]{
                                android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION},
                        locationPermission);
                return;


            }
        }
        locationUpdatesComponent.onCreate(this);

        locationUpdatesComponent.onStart();


    }

    void getUserGeoInfo(double lat, double lon) {
        Geocoder geoCoder = new Geocoder(ActivityCartView.this, Locale.getDefault());
        if (Geocoder.isPresent()) {
            List<Address> addresses = null;
            try {
                addresses = geoCoder.getFromLocation(lat, lon, 1);
                if (addresses.size() > 0) {
                    String city = addresses.get(0).getLocality();
                    cityName = city;
                   // Log.d(TAG, "getUserGeoInfo: " + city);


                    // obtain all information from addresses.get(0)
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(TAG, "onActivityResulttPermissionsResult: " + requestCode);
        if (requestCode == locationPermission) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "onActivityResulttPermissionsResult: " + PackageManager.PERMISSION_GRANTED);

                locationRequest();
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!shouldShowRequestPermissionRationale(android.Manifest.permission.ACCESS_COARSE_LOCATION) ||
                            !shouldShowRequestPermissionRationale(android.Manifest.permission.ACCESS_COARSE_LOCATION)) {
                        openSettings();
                        Toast.makeText(this, getString(R.string.enable_locationPermision), Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                //locationRequest();
                locationRequest();
            }
        }
    }

    public void setPrice() {
        price = 0.0;
        tv_subtotal.setText(currency + new DecimalFormat("##.##").format(subTotal));
        if (discount != 0.0) {
            price = subTotal - discount;
        } else {
            price = subTotal;
        }
        tv_discount_amnt.setText(currency + new DecimalFormat("##.##").format(discount));
        tv_totalamnt.setText("Total  " + currency
                + " " + new DecimalFormat("##.##").format(price));
    }

    public void applyCoupon(String code, String type, String restroId) {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityCartView.this, R.string.error_no_net,
                    Toast.LENGTH_SHORT).show();
            return;
        }
        shwProgress();
        //  Log.d("aviparms", "search: "+UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)+search);
        String userid = "0";
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
            userid = UtilsDefault.getSharedPreferenceValue(Constants.USER_ID);
        }
        aviapi.applyCoupon(UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID),
                total, code, type, restroId).enqueue(new Callback<ModelApplyCoupon>() {
            @Override
            public void onResponse(Call<ModelApplyCoupon> call, Response<ModelApplyCoupon> response) {
                hideprogress();
                hideKeyboard();
                if (response.body() != null) {
                    ModelApplyCoupon data = response.body();
                    String header = response.headers().get(
                            getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus() == Constants.STATUS_200) {
                        edit_add_promo.setEnabled(false);
                        btn_add_promo.setText("Remove");
//                        subTotal = Double.valueOf(data.getAmount());
//                        discount=Double.valueOf(data.getDiscount());
//                        setPrice();
                        getCartList();

                        //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                    } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                        applyCoupon(code, type, restroId);
                    } else {
                        Toast.makeText(ActivityCartView.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    alertcommon(getString(R.string.mycart), getString(R.string.server_error));


                    //   Toast.makeText(ActivityMyTrips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ModelApplyCoupon> call, Throwable t) {

                hideprogress();
                hideKeyboard();
                alertcommon(getString(R.string.mycart), getString(R.string.server_error));
                //Log.d("failure", "onFailure: " + t.getMessage());
                //Toast.makeText(ActivityMyTrips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getCartList() {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityCartView.this,
                    R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;
        }
        shwProgress();
        //  Log.d("aviparms", "search: "+UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)+search);
        String userid = "0";
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)!= null) {
            userid = UtilsDefault.getSharedPreferenceValue(Constants.USER_ID);
        }
        aviapi.getCartList(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID),
                UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID),
                UtilsDefault.getSharedPreferenceValue(Constants.TERMINAL_ID), coupon,place).enqueue(new Callback<ModelCartView>() {
            @Override
            public void onResponse(Call<ModelCartView> call, Response<ModelCartView> response) {
                hideprogress();
                hideKeyboard();
                if (response.body() != null) {
                    ModelCartView data = response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus() == Constants.STATUS_200) {

                        if (data.getEta() != null && !
                                data.getEta() .equals("")) {
                            if (data.getEta() .contains(":")) {
                                String time[] = data.getEta() .split(":");

                                StringBuffer stringBuffer = new StringBuffer();
                                if (!time[0].equals("00")) {
                                    stringBuffer.append(time[0] + " Hrs ");
                                }
                                if (!time[1].equals("00")) {
                                    stringBuffer.append(time[1] + " Mins");
                                }
                                Eta_time=stringBuffer.toString();
                                //tv_estimate_time.setText("Average Time response : " + stringBuffer);

                            }
                        }

                        if (data.getServiceTypeID() != null && data.getServiceTypeID() == 1) {
                            rel_lay_takeaway.setVisibility(View.VISIBLE);
                            lin_lay_dinein.setVisibility(View.GONE);
                            check_takeway.setChecked(true);

                        } else if (data.getServiceTypeID() != null && data.getServiceTypeID() == 2) {
                            rel_lay_takeaway.setVisibility(View.GONE);
                            lin_lay_dinein.setVisibility(View.VISIBLE);
                            check_dine_in.setChecked(true);
                        }

                        if (data.getCart() != null && data.getCart().size() != 0) {
                            cart_list.clear();
                            cart_list.addAll(data.getCart());
                            AdapterCartView adapterCartView = new AdapterCartView(ActivityCartView.this,
                                    cart_list, data.getImagePath().getMenu());
                            adapterCartView.setonItemClick(ActivityCartView.this);
                            recycle_cart_items.setLayoutManager(new LinearLayoutManager(ActivityCartView.this));
                            recycle_cart_items.setAdapter(adapterCartView);
                            tv_title.setText("Cart (" + data.getCart().size() + ")");
                            tv_total_items.setText("Total Items: " + data.getCart().size());

                            if (data.getCalculation().getCoupon() != null &&
                                    !data.getCalculation().getCoupon().equals("0")) {
                                lin_discount_lay.setVisibility(View.VISIBLE);
                                tv_discount_amnt.setText(data.getCalculation().getCurrencyCode() + " " + data.getCalculation().getCoupon());

                            }
                            else {
                                lin_discount_lay.setVisibility(View.GONE);
                            }
                            if (data.getCalculation().getConvenience_fee() != null
                                    && !data.getCalculation().getConvenience_fee().equals("0")) {
                                lin_convn_lay.setVisibility(View.VISIBLE);
                                tv_convn_fee.setText(data.getCalculation().getCurrencyCode() + " " + data.getCalculation().getConvenience_fee());

                            }
                            else {
                                lin_convn_lay.setVisibility(View.GONE);
                            }
                            if (data.getCalculation().getHandling_fee() != null &&
                                    !data.getCalculation().getHandling_fee().equals("0")) {
                                lin_handling_lay.setVisibility(View.VISIBLE);
                                tv_handling_fee.setText(data.getCalculation().getCurrencyCode() + " " + data.getCalculation().getHandling_fee());

                            }
                            else {
                                lin_handling_lay.setVisibility(View.GONE);
                            }

                            if (data.getCalculation().getGst_amt() != null &&
                                    !data.getCalculation().getGst_amt().equals("0")) {
                                lin_gst_lay.setVisibility(View.VISIBLE);
                                tv_gst.setText(data.getCalculation().getCurrencyCode() + " " + data.getCalculation().getGst_amt());

                            }
                            else {
                                lin_gst_lay.setVisibility(View.GONE);
                            }

                            if (data.getCalculation().getDelivery_fee() != null &&
                                    !data.getCalculation().getDelivery_fee().equals("0")) {
                                lin_deliver_lay.setVisibility(View.VISIBLE);
                                tv_service_charge.setText(data.getCalculation().getCurrencyCode() + " " + data.getCalculation().getDelivery_fee());

                            }
                            else {
                                lin_deliver_lay.setVisibility(View.GONE);
                            }


                            restroid = String.valueOf(data.getCart().get(0).getRestaurantmenucategory().getRestaurant_id());
                            currency = data.getCart().get(0).getRestaurantmenucategory().getCurrency().getCode();

                            tv_subtotal.setText(data.getCalculation().getCurrencyCode() + " " + data.getCalculation().getMenuTotal());
                            couponAmnt = data.getCalculation().getCoupon();
                            tv_totalamnt.setText("Grand Total  " + data.getCalculation().getCurrencyCode()
                                    + " " + data.getCalculation().getSubtotal());
                            total = data.getCalculation().getMenuTotal();
                            totalamnt = data.getCalculation().getSubtotal();
                            // setPrice();
                            // tv_discount_amnt.setText(currency + " 0.00");
                        } else {
                            Toast.makeText(ActivityCartView.this, R.string.nodata, Toast.LENGTH_SHORT).show();
                            finish();
                            // alertcommon("MyCart", getString(R.string.nodata));
                        }

                        //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                    } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                        getCartList();
                    } else {


                        //Toast.makeText(ActivityMyTrips.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    isAppClose = false;
                    alertcommon("MyCart", getString(R.string.server_error));


                    //   Toast.makeText(ActivityMyTrips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ModelCartView> call, Throwable t) {

                hideprogress();
                hideKeyboard();
                alertcommon("MyCart", getString(R.string.server_error));
                Log.d("failure", "onFailure: " + t.getMessage());
                //Toast.makeText(ActivityMyTrips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void authenticationFromBackend() {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityCartView.this, R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;
        }
        shwProgress();
        //  Log.d("aviparms", "search: "+UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)+search);
        String userid = "0";
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
            userid = UtilsDefault.getSharedPreferenceValue(Constants.USER_ID);

        }
        Log.d("inputs", "authenticationFromBackend: " + UtilsDefault.
                getSharedPreferenceValue(Constants.USER_ID)
                + "-" + UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID) + "-" +
                UtilsDefault.getSharedPreferenceValue(Constants.TERMINAL_ID) + totalamnt + "-" + place + "-" + no_ofPax + "-" +
                coupon + "-" + discount);
        aviapi.orderWebAuth(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID),
                UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID),
                UtilsDefault.getSharedPreferenceValue(Constants.TERMINAL_ID),
                totalamnt, place, no_ofPax, coupon, couponAmnt).enqueue(new Callback<ModelOrderCheck>() {
            @Override
            public void onResponse(Call<ModelOrderCheck> call,
                                   Response<ModelOrderCheck> response) {
                hideprogress();
                hideKeyboard();
                if (response.body() != null) {
                    ModelOrderCheck data = response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus() == Constants.STATUS_200) {
                        if (data.getOrderid() != null && !data.getOrderid().equals("")) {


                            Intent intent = new Intent(ActivityCartView.this, WebViewActivity.class);
                            intent.putExtra(AvenuesParams.ACCESS_CODE, getResources().getString(R.string.accescode));
                            intent.putExtra(AvenuesParams.MERCHANT_ID, getResources().getString(R.string.mer_id));
                            intent.putExtra(AvenuesParams.ORDER_ID, data.getOrderid());
                            intent.putExtra(AvenuesParams.ETA_TIME,Eta_time);
                            intent.putExtra(AvenuesParams.CURRENCY, currency);
                            intent.putExtra(AvenuesParams.AMOUNT, totalamnt);
                            intent.putExtra(AvenuesParams.REDIRECT_URL, getResources().getString(R.string.ccAvenuehandlerurl));
                            intent.putExtra(AvenuesParams.CANCEL_URL, getResources().getString(R.string.ccAvenuehandlerurl));
                            intent.putExtra(AvenuesParams.RSA_KEY_URL, getResources().getString(R.string.getrsa_url));
                            startActivity(intent);
                        } else {
                            Toast.makeText(ActivityCartView.this, getString(R.string.order_not_avail), Toast.LENGTH_SHORT).show();
                        }


                        //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                    } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                        authenticationFromBackend();
                    } else {
                        alertcommon("MyCart", data.getMessage());
                        //Toast.makeText(ActivityCartView.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    alertcommon("MyCart", getString(R.string.server_error));


                    //   Toast.makeText(ActivityMyTrips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ModelOrderCheck> call, Throwable t) {
                hideprogress();
                hideKeyboard();
                alertcommon("MyCart", getString(R.string.server_error));
                Log.d("failure", "onFailure: " + t.getMessage());
                //Toast.makeText(ActivityMyTrips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void closeCommonAlert() {
        if (isAppClose) {
            commonalert.dismiss();
            finish();
        } else {
            commonalert.dismiss();
        }

    }

    @Override
    protected int getlayout() {
        return R.layout.activity_cartview;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (buttonView == check_dine_in) {
            if (isChecked == true) {
                check_takeway.setChecked(false);
                place = "2";
                end_lay.setVisibility(View.VISIBLE);
                getCartList();


            } else {
                place = "";
                end_lay.setVisibility(View.GONE);
            }

        } else {
            if (buttonView == check_takeway) {
                if (isChecked == true) {
                    check_dine_in.setChecked(false);
                    place = "1";
                    end_lay.setVisibility(View.GONE);
                    getCartList();

                } else {
                    place = "";
                    end_lay.setVisibility(View.GONE);
                }
            }
        }

    }

    @Override
    public void onClick(View v) {

//        Log.d("times", "airporttime: " + UtilsDefault.getSharedPreferenceValue(Constants.TIME_ZONE) +
//                "devicezone" + timeZone.getID());
        if (v == spinner_img) {
            spinn_num_pax.performClick();
        }
        if (v == btn_checkout) {

            if (place.equals("")) {
                Toast.makeText(this, getString(R.string.select_dinein), Toast.LENGTH_SHORT).show();
            }
//            else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
//                    checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
//                    && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
//                    != PackageManager.PERMISSION_GRANTED) {
//                ActivityCompat.requestPermissions(this, new String[]{
//                                android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION},
//                        locationPermission);
//                return;
//
//            } else if (!location.hasLocationEnabled()) {
//                locationAlert();
//
//            } else if (cityName == null || cityName.equals("")) {
//                Toast.makeText(this, getString(R.string.unable_detect_location),
//                        Toast.LENGTH_SHORT).show();
//            }
//            else if (UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_CITY) != null && !
//                    UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_CITY).equals("") &&
//                    !UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_CITY).toLowerCase().
//                            equals(cityName.toLowerCase())) {
//
//                Toast.makeText(this, getString(R.string.rest_not_available), Toast.LENGTH_SHORT).show();
//                Log.d("time", "onClick: " + timeZone.getID() + "airportzone" +
//                        UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_CITY) + cityName);
//            }
            else {
                confirmAlert();

            }

            // startActivity(new Intent(ActivityCartView.this,ActivityOrderTracking.class));
        }
        if (v == btn_add_promo) {
            if (btn_add_promo.getText().equals(getString(R.string.add_promo))) {
                String promocode = edit_add_promo.getText().toString().trim();
                if (promocode.equals("")) {
                    edit_add_promo.requestFocus();
                    edit_add_promo.setError("Enter your coupon code");
                } else if (restroid.equals("")) {
                    Toast.makeText(this,
                            getString(R.string.restro_not_avail), Toast.LENGTH_SHORT).show();
                } else {
                    coupon = promocode;
                    applyCoupon(promocode, "2", restroid);
                }
            } else {
                coupon = "";
                edit_add_promo.setEnabled(true);
                edit_add_promo.setText("");
                btn_add_promo.setText(getString(R.string.add_promo));
                getCartList();
            }

        }
    }

    public void addCart(String itemid, String type) {

        /**
         * if
         *  1 - add / 2 - reduce / 3 - delete row
         */
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityCartView.this, R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;
        }
        shwProgress();
        //  Log.d("aviparms", "search: "+UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)+search);
        String userid = "0";
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
            userid = UtilsDefault.getSharedPreferenceValue(Constants.USER_ID);
        }
        Log.d("tag", "addCart: " + itemid);
        aviapi.addCart(itemid, UtilsDefault.getSharedPreferenceValue(Constants.USER_ID),
                UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID),
                UtilsDefault.getSharedPreferenceValue(Constants.TERMINAL_ID), type).
                enqueue(new Callback<CommonMsgModel>() {
                    @Override
                    public void onResponse(Call<CommonMsgModel> call, Response<CommonMsgModel> response) {
                        hideprogress();
                        hideKeyboard();

                        if (response.body() != null) {
                            CommonMsgModel data = response.body();
//                    String header = response.headers().get(getString(R.string.header_find_key));
//                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                            if (data.getStatus() == Constants.STATUS_200) {
                                getCartList();


                                //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                            } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {

                                addCart(itemid, type);
                            } else {

                                Toast.makeText(ActivityCartView.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(ActivityCartView.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<CommonMsgModel> call, Throwable t) {
                        hideprogress();
                        hideKeyboard();
                        Log.d("failure", "onFailure: " + t.getMessage());
                        Toast.makeText(ActivityCartView.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                    }
                });
    }


    @Override
    public void OnItemClickListener(int pos, View view) {
        if (view.getId() == R.id.img_plus) {
            if (cart_list.size() != 0) {


                addCart(String.valueOf(cart_list.get(pos).getRestaurantmenucategory().getId()), Constants.CART_PLUS);
            }
        } else if (view.getId() == R.id.img_minus) {
            if (cart_list.size() != 0) {
                addCart(String.valueOf(cart_list.get(pos).getRestaurantmenucategory().getId()), Constants.CART_MINUS);
            }
        } else if (view.getId() == R.id.img_delete_icon) {
            if (cart_list.size() != 0) {
                addCart(String.valueOf(cart_list.get(pos).getRestaurantmenucategory().getId()), Constants.CART_DELETE);
            }
        }

    }


}
