package com.dci.avicustomer.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.dci.avicustomer.BuildConfig;
import com.dci.avicustomer.R;
import com.dci.avicustomer.adapter.AdapterMyDocsDetails;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.ExifUtil;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.CommonMsgModel;
import com.dci.avicustomer.models.ModelAddDocuments;
import com.dci.avicustomer.models.ModelDocumentDetailsList;
import com.dci.avicustomer.retrofit.AVIAPI;
import com.jaiselrahman.filepicker.activity.FilePickerActivity;
import com.jaiselrahman.filepicker.model.MediaFile;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityMyDocsDetails extends BaseActivity implements View.OnClickListener, OnItemClickListener {

    @Inject
    public AVIAPI aviapi;
    @BindView(R.id.recycle_my_trips)
    RecyclerView recycle_my_trips;
    @BindView(R.id.img_add_icon)
    ImageView img_add_icon;
    @BindView(R.id.tv_nodata)
    TextView tv_nodata;
    String section = "";
    String title = "";
    List<MediaFile> fileList = new ArrayList<>();
    List<ModelDocumentDetailsList.Document>listMyDocs=new ArrayList<>();
    Dialog mBottomSheetDialog;
    AdapterMyDocsDetails adapterMyDocsDetails;
    AlertDialog exitDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        ShoppingApplication.getContext().getComponent().inject(this);
        img_add_icon.setOnClickListener(this);
        if (getIntent().getStringExtra("section") != null) {
            section = getIntent().getStringExtra("section");
            title = getIntent().getStringExtra("title");
            tv_title.setText(title);
            getMyDocs(section);
        }
    }

    public void getMyDocs(String section) {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityMyDocsDetails.this,
                    R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;

        }
        shwProgress();
        //  Log.d("aviparms", "search: "+UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)+search);
        String userid = "0";
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
            userid = UtilsDefault.getSharedPreferenceValue(Constants.USER_ID);
        }
        aviapi.getMyDocs(section, UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)).enqueue(new Callback<ModelDocumentDetailsList>() {
            @Override
            public void onResponse(Call<ModelDocumentDetailsList> call,
                                   Response<ModelDocumentDetailsList> response) {
                hideprogress();
                hideKeyboard();
                if (response.body() != null) {
                    ModelDocumentDetailsList data = response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus() == Constants.STATUS_200) {
                        if (data.getData().getDocument() != null && data.getData().getDocument().size() != 0) {
                            tv_nodata.setVisibility(View.GONE);
                            listMyDocs.clear();
                            listMyDocs.addAll(data.getData().getDocument());
                             adapterMyDocsDetails=new AdapterMyDocsDetails(ActivityMyDocsDetails.this,
                                     listMyDocs,
                                    data.getData().getImagePath());
                            adapterMyDocsDetails.setonItemClick(ActivityMyDocsDetails.this);
                            recycle_my_trips.setAdapter(adapterMyDocsDetails);
                            recycle_my_trips.setLayoutManager(new LinearLayoutManager(ActivityMyDocsDetails.this));
                        } else {
                            tv_nodata.setVisibility(View.VISIBLE);
                            //alertcommon(title, getString(R.string.nodata));
                        }


                        //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                    } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                        getMyDocs(section);
                    } else {
                        recycle_my_trips.setVisibility(View.GONE);
                        tv_nodata.setVisibility(View.VISIBLE);
                        tv_nodata.setText(data.getMessage());
                      //  alertcommon(title, data.getMessage());
                        //Toast.makeText(ActivityMyTrips.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    recycle_my_trips.setVisibility(View.GONE);
                    tv_nodata.setVisibility(View.VISIBLE);
                    tv_nodata.setText(getString(R.string.server_error));
                  //  alertcommon(title, getString(R.string.server_error));

                    //   Toast.makeText(ActivityMyTrips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ModelDocumentDetailsList> call, Throwable t) {
                tv_nodata.setVisibility(View.VISIBLE);
                tv_nodata.setText(getString(R.string.server_error));
                hideprogress();
                hideKeyboard();
                recycle_my_trips.setVisibility(View.GONE);


                Log.d("failure", "onFailure: " + t.getMessage());
                //Toast.makeText(ActivityMyTrips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void uploadMultiFile(List<MediaFile> filepath) {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityMyDocsDetails.this,
                    R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;
        }
        shwProgress();

        RequestBody sections = RequestBody.create(
                MediaType.parse("text/plain"),
                section);

        RequestBody uid = RequestBody.create(
                MediaType.parse("text/plain"),
                UtilsDefault.getSharedPreferenceValue(Constants.USER_ID));
        Log.d("section", "uploadMultiFile: "+section);

        aviapi.uploadMultiFile(getMultiart(fileList),
                sections, uid).enqueue(new Callback<ModelAddDocuments>() {
            @Override
            public void onResponse(Call<ModelAddDocuments> call, Response<ModelAddDocuments> response) {
                hideprogress();
                ModelAddDocuments data = response.body();
                if (data.getStatus() == Constants.STATUS_200) {
                    getMyDocs(section);
                    Toast.makeText(ActivityMyDocsDetails.this,
                            response.message(), Toast.LENGTH_LONG).show();
                } else {
                    alertcommon(title,  data.getMessage());
                  //  Toast.makeText(ActivityMyDocsDetails.this, "Success " + data.getMessage(), Toast.LENGTH_LONG).show();
                    Log.d("failure", "onResponse: " + data.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ModelAddDocuments> call, Throwable t) {
                hideprogress();
                alertcommon(title, getString(R.string.server_error));
            }
        });


    }

    public List<MultipartBody.Part> getMultiart(List<MediaFile> upFileList) {
        List<MultipartBody.Part> parts = new ArrayList<>();
        for (int i = 0; i < upFileList.size(); i++) {
            File file = new File(upFileList.get(i).getPath());
            Log.d("filepath", "uploadMultiFile: " + upFileList.get(i).getPath());
            Uri uri;
            if (Build.VERSION.SDK_INT >= 23) {
                uri = FileProvider.getUriForFile(getApplicationContext(),
                        BuildConfig.APPLICATION_ID + ".provider", file);
            } else {
                uri = Uri.fromFile(file);
            }


            String fileNames = new Date().getTime() +
                    file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("."));
            MultipartBody.Part fileParts;
            if (upFileList.get(i).getMimeType().contains("image")){
                byte[] bytes = new byte[0];
                try {
                    bytes =compress(uri,file);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                RequestBody fileBody = null;
                fileBody = RequestBody.create(MediaType.parse(upFileList.get(i).getMimeType()),
                        bytes);
               fileParts = MultipartBody.Part.createFormData("file[]",fileNames, fileBody);
            }
            else {
                RequestBody fileBody = null;
               // fileBody = RequestBody.create(MediaType.parse(getContentResolver().getType(uri)),file);
                fileBody = RequestBody.create(MediaType.parse(upFileList.get(i).getMimeType()),file);
                fileParts = MultipartBody.Part.createFormData("file[]", fileNames, fileBody);
            }

            parts.add(i, fileParts);
        }
        return parts;
    }

    private byte[] compress(Uri selectedImage,File file) throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o);
        final int REQUIRED_SIZE = 400;
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        Bitmap bitmap=BitmapFactory.decodeStream(
                getContentResolver().openInputStream(selectedImage), null, o2);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(file.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        int orientation = ExifInterface.ORIENTATION_NORMAL;

        if (exif != null)
            orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                bitmap = rotateBitmap(bitmap, 90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                bitmap = rotateBitmap(bitmap, 180);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                bitmap = rotateBitmap(bitmap, 270);
                break;
        }

        // bitmap = ExifUtil.rotateBitmap(selectedImage.getPath(), bitmap);
//        Bitmap bitmap1=null;
//        try {
//            bitmap1=modifyOrientation(bitmap,selectedImage.toString());
//        } catch (IOException e) {
//            e.printStackTrace();
//            Log.d("exception1", "compress: "+"not data");
//        }

        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }
    public static Bitmap rotateBitmap(Bitmap bitmap, int degrees) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }
    public static Bitmap modifyOrientation(Bitmap bitmap, String image_absolute_path)
            throws IOException {
        Log.d("fullpath", "modifyOrientation: "+image_absolute_path);
        ExifInterface ei = new ExifInterface(image_absolute_path);
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotate(bitmap, 90);

            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotate(bitmap, 180);

            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotate(bitmap, 270);

            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                return flip(bitmap, true, false);

            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                return flip(bitmap, false, true);

            default:
                return bitmap;
        }
    }

    public static Bitmap rotate(Bitmap bitmap, float degrees) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        Log.d("rotate", "rotate: ");
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public static Bitmap flip(Bitmap bitmap, boolean horizontal, boolean vertical) {
        Matrix matrix = new Matrix();
        Log.d("flip", "flip: ");
        matrix.preScale(horizontal ? -1 : 1, vertical ? -1 : 1);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public void delete(String id,int pos) {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(this, R.string.error_no_net,
                    Toast.LENGTH_SHORT).show();
            return;
        }
        shwProgress();
        aviapi.deleteDocuments(id, UtilsDefault.getSharedPreferenceValue(Constants.USER_ID), 0).enqueue(new Callback<CommonMsgModel>() {
            @Override
            public void onResponse(Call<CommonMsgModel> call,
                                   Response<CommonMsgModel> response) {
                hideprogress();
                if (response.body() != null) {
                    CommonMsgModel data = response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    UtilsDefault.updateFcmSharedPreference(Constants.API_KEY, data.getJwt_token());
                    if (data.getStatus() == Constants.STATUS_200) {
                        listMyDocs.remove(pos);
                        adapterMyDocsDetails.notifyItemRemoved(pos);
                        if(listMyDocs.size()==0){
                            tv_nodata.setVisibility(View.VISIBLE);
                        }

                    } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                        delete(id,pos);
                    } else {
                        Toast.makeText(ActivityMyDocsDetails.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ActivityMyDocsDetails.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CommonMsgModel> call, Throwable t) {
                hideprogress();
                Log.d("errorapi", "onFailure: "+t.getMessage());
                Toast.makeText(ActivityMyDocsDetails.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected int getlayout() {
        return R.layout.activity_my_trips;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == FILE_REQUEST_CODE && resultCode == RESULT_OK
                    && data != null) {

                fileList.clear();
                fileList.addAll(data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES));
                if (fileList.size() != 0) {
                    uploadMultiFile(fileList);
                }


            } else if (requestCode == IMAGE_REQUEST_CODE && resultCode == RESULT_OK
                    && data != null) {
                fileList.clear();
                fileList.addAll(data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES));
                if (fileList.size() != 0) {
                    uploadMultiFile(fileList);
                }

            } else if (requestCode == CAMERA_REQUEST_CODE && resultCode == RESULT_OK
                    && data != null) {
                fileList.clear();
                fileList.addAll(data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES));
                if (fileList.size() != 0) {
                    uploadMultiFile(fileList);
                }
            }
        } catch (Exception e) {
        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //Log.d(TAG, "onActivityResulttPermissionsResult: "+requestCode);
        if (requestCode == 101) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED&&
                    grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                //  Log.d(TAG, "onActivityResulttPermissionsResult: "+PackageManager.PERMISSION_GRANTED);
                showBottomFilePIcker();
            }
           else if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE )
                    == PackageManager.PERMISSION_DENIED ||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA )
                            == PackageManager.PERMISSION_DENIED){
                    Toast.makeText(this, R.string.enable_camera, Toast.LENGTH_SHORT).show();

               // Toast.makeText(this, R.string.enable_camera, Toast.LENGTH_SHORT).show();
                return;
            }

            else {
                showBottomFilePIcker();
            }
        }
    }

    public void showBottomFilePIcker() {

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                android.Manifest.permission.CAMERA},
                        101);
                Toast.makeText(this, R.string.enable_camera, Toast.LENGTH_SHORT).show();
                if (!shouldShowRequestPermissionRationale( Manifest.permission.CAMERA)||
                        !shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                    openSettings();
                    Toast.makeText(this, R.string.enable_camera, Toast.LENGTH_SHORT).show();
                    return;
                }

                return;
            }
        }


        mBottomSheetDialog = new Dialog(this, R.style.PickerMaterialDialogSheet);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_bottom_picker, null);
        mBottomSheetDialog.setContentView(dialogView); // your custom view.
        mBottomSheetDialog.setCancelable(true);
        LinearLayout lin_docs_lay = dialogView.findViewById(R.id.lin_docs_lay);
        LinearLayout lin_gallery_lay = dialogView.findViewById(R.id.lin_gallery_lay);
        LinearLayout lin_camera_lay = dialogView.findViewById(R.id.lin_camera_lay);
        LinearLayout lin_scan_lay = dialogView.findViewById(R.id.lin_scan_lay);
        CardView card_cancel = dialogView.findViewById(R.id.card_cancel);
        card_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.cancel();
            }
        });
        lin_gallery_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setImagePicker(Constants.MAX_FILE_SELECT);
            }
        });
        lin_docs_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFilePicker(Constants.MAX_FILE_SELECT);
            }
        });
        lin_camera_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setcameraPicker(Constants.MAX_FILE_SELECT);
            }
        });
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();
    }

    @Override
    public void onClick(View v) {
        if (v == img_add_icon) {
            showBottomFilePIcker();
        }
    }

    @Override
    public void OnItemClickListener(int pos, View view) {
        if (view.getId()==R.id.image_delete){
            listMyDocs.get(pos).getId();
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setMessage(getString(R.string.msg_delete_docs));
            alertDialogBuilder.setPositiveButton("Yes",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            exitDialog.dismiss();
                            delete(String.valueOf(listMyDocs.get(pos).getId()),pos);

                        }
                    });
            alertDialogBuilder.setNegativeButton("No",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            exitDialog.dismiss();

                        }
                    });

            exitDialog= alertDialogBuilder.create();
            exitDialog.show();

        }

    }
}
