package com.dci.avicustomer.activities;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.avicustomer.R;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.MyNotificationPublisher;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.models.ModelUpcomingtrips;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.jaiselrahman.filepicker.activity.FilePickerActivity;
import com.jaiselrahman.filepicker.config.Configurations;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.provider.Settings.System.DATE_FORMAT;

/**
 * Created by dineshkumars on 4/20/2018.
 */

public abstract class BaseActivity extends AppCompatActivity {
    ImageView my_cart, notification;
    //  NotificationBadge notify_badge, cart_badge;
    boolean isloading = false;
    LinearLayout image_lay;
    int totalcount = -1;
    ProgressDialog dialog;
    TextView tv_clickhere;
    LinearLayout timer_lay;
    AlertDialog alertDialog, commonalert;
    ImageView imgback;
    int REQUEST_CHECK_SETTINGS = 100;
    TextView tv_title;
    boolean isLastPage = false;
    Button btn_ok;
    int FILE_REQUEST_CODE = 100;
    int IMAGE_REQUEST_CODE = 101;
    int CAMERA_REQUEST_CODE = 102;
    TextView tv_hrs, tv_mins, tv_secs;
    private Handler handler = new Handler();
    private Runnable runnable;
    TimeZone timeZone;

    public void openSettings() {
        startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.fromParts("package", getPackageName(), null)));
//        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//        Uri uri = Uri.fromParts("package", getPackageName(), null);
//        intent.setData(uri);
//        startActivityForResult(intent, 101);
    }

    public void shwProgress() {
        dialog.setMessage(getString(R.string.please_wait));

        dialog.setCancelable(false);
        //  ProgressBar progressbar=(ProgressBar)dialog.findViewById(android.R.id.progress);
        // progressbar.getIndeterminateDrawable().setColorFilter(Color.parseColor("#16172b"), android.graphics.PorterDuff.Mode.SRC_IN);
        dialog.show();
    }

    public void showSoftKeyboard() {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        // inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
    }


    @Override
    protected void onDestroy() {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        super.onDestroy();
    }

    public void hideprogress() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    public void setFilePicker(int maxSelect) {
        Intent intent = new Intent(this, FilePickerActivity.class);
        intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
                .setCheckPermission(true)
                .setShowImages(false)
                .setShowFiles(true)
                .setShowVideos(false)
                .setSuffixes("pdf")
                .enableImageCapture(false)
                .setMaxSelection(maxSelect)
                .setIgnoreNoMedia(true)
                .build());
        startActivityForResult(intent, FILE_REQUEST_CODE);

    }

    public void setImagePicker(int maxSelect) {
        Intent intent = new Intent(this, FilePickerActivity.class);
        intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
                .setCheckPermission(true)
                .setShowImages(true)
                .setShowVideos(false)
                .setShowFiles(false)
                .enableImageCapture(false)
                .setMaxSelection(maxSelect)
                .setIgnoreNoMedia(true)
                .build());
        startActivityForResult(intent, IMAGE_REQUEST_CODE);
    }

    //    public File saveBitmapToFile(File file){
//
//
//        long length = file.length();
//        length = length/1024;
//        if (length>400){
//            try {
//                // BitmapFactory options to downsize the image
//                BitmapFactory.Options o = new BitmapFactory.Options();
//                o.inJustDecodeBounds = true;
//                o.inSampleSize = 6;
//                // factor of downsizing the image
//
//                FileInputStream inputStream = new FileInputStream(file);
//                //Bitmap selectedBitmap = null;
//                BitmapFactory.decodeStream(inputStream, null, o);
//                inputStream.close();
//
//                // The new size we want to scale to
//                final int REQUIRED_SIZE=400;
//
//                // Find the correct scale value. It should be the power of 2.
//                int scale = 1;
//                while(o.outWidth / scale / 2 >= REQUIRED_SIZE &&
//                        o.outHeight / scale / 2 >= REQUIRED_SIZE) {
//                    scale *= 2;
//                }
//
//                BitmapFactory.Options o2 = new BitmapFactory.Options();
//                o2.inSampleSize = scale;
//                inputStream = new FileInputStream(file);
//
//                Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
//                inputStream.close();
//
//                // here i override the original image file
//                file.createNewFile();
//                FileOutputStream outputStream = new FileOutputStream(file);
//
//                selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 200 , outputStream);
//
//                return file;
//            } catch (Exception e) {
//                return file;
//            }
//        }
//        else {
//            return file;
//        }
//
//    }
    public void setcameraPicker(int maxSelect) {
        Intent intent = new Intent(this, FilePickerActivity.class);
        intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
                .setCheckPermission(true)
                .setShowImages(true)
                .setShowVideos(false)
                .setShowFiles(false)
                .enableImageCapture(true)
                .enableVideoCapture(false)
                .setIgnoreNoMedia(true)
                .setMaxSelection(maxSelect)
                .build());
        intent.putExtra("camera", "camera");
        startActivityForResult(intent, CAMERA_REQUEST_CODE);

    }

    public void closeCommonAlert() {
        commonalert.dismiss();
    }

    public void alert(String msg) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(msg);
        alertDialogBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        dialog.dismiss();

                    }
                });

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    public void alertcommon(String title, String content) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setCancelable(false);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_common_alert_dialog, null);
        alertDialogBuilder.setView(dialogView);
        TextView titletv = dialogView.findViewById(R.id.tv_alert_title);
        titletv.setText(title);
        TextView tv_content = dialogView.findViewById(R.id.tv_content);
        tv_content.setText(content);
        btn_ok = dialogView.findViewById(R.id.btn_ok);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeCommonAlert();
            }
        });


        commonalert = alertDialogBuilder.create();
        commonalert.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        commonalert.show();
    }


    public void alertShowLogin(String title) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setCancelable(true);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_login_required, null);
        alertDialogBuilder.setView(dialogView);
        TextView titletv = dialogView.findViewById(R.id.tv_title);
        titletv.setText(title);
        Button btn_login = dialogView.findViewById(R.id.btn_login);


        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.cancel();
                startActivity(new Intent(BaseActivity.this, ActivityLogin.class));

            }
        });


        alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialog.show();
    }

    public void scheduleNotification(Context context, long delay, int notificationId) {//delay is after how much time(in millis) from current time you want to schedule the notification


        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "")
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.appicons))
                .setSmallIcon(R.drawable.alarm_notify)
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText(getString(R.string.msg_boardingclose))
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            NotificationChannel channel = new NotificationChannel(channelId,
//                    "Channel human readable title",
//                    NotificationManager.IMPORTANCE_DEFAULT);
//            notificationManager.createNotificationChannel(channel);
//        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelId = getString(R.string.default_notification_channel_id);
            String name = getString(R.string.default_notification_channel_id);
            NotificationChannel channel = new NotificationChannel(channelId, name,
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(getString(R.string.msg_boardingclose));
            notificationManager.createNotificationChannel(channel);
            notificationBuilder.setChannelId(channelId);
        }


        Intent intent = new Intent(context, ActivityHome.class);
        PendingIntent activity = PendingIntent.getActivity(context, notificationId, intent,
                PendingIntent.FLAG_CANCEL_CURRENT);
        notificationBuilder.setContentIntent(activity);
        Notification notification = notificationBuilder.build();
        Intent notificationIntent = new Intent(context, MyNotificationPublisher.class);
        notificationIntent.putExtra(MyNotificationPublisher.NOTIFICATION_ID, notificationId);
        notificationIntent.putExtra(MyNotificationPublisher.NOTIFICATION, notification);


        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, notificationId,
                notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        long futureInMillis = SystemClock.elapsedRealtime() + delay;
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
        if (Build.VERSION.SDK_INT >= 23) {
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                    futureInMillis, pendingIntent);
        } else if (Build.VERSION.SDK_INT >= 19) {
            alarmManager.setExact(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent);
        } else {
            alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent);
        }
        // alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getlayout());
        timeZone = TimeZone.getDefault();

        try {
            imgback = findViewById(R.id.backicon);
            tv_title = findViewById(R.id.tv_tool_tittle);
            tv_title.setSelected(true);
            tv_secs = findViewById(R.id.tv_secs);
            tv_hrs = findViewById(R.id.tv_hrs);
            tv_mins = findViewById(R.id.tv_mins);
            tv_clickhere = findViewById(R.id.tv_clickhere);
            timer_lay = findViewById(R.id.timer_lay);

            imgback.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            tv_clickhere.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(BaseActivity.this);
                        alertDialogBuilder.setCancelable(true);
                        LayoutInflater inflater = BaseActivity.this.getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.alert_common_alert_dialog, null);
                        alertDialogBuilder.setView(dialogView);
                        TextView titletv = dialogView.findViewById(R.id.tv_alert_title);
                        titletv.setText("AVI Trips");
                        TextView tv_content = dialogView.findViewById(R.id.tv_content);
                        tv_content.setText(getString(R.string.no_trip_alert_message));
                        btn_ok = dialogView.findViewById(R.id.btn_ok);
                        btn_ok.setText("Add Trip");

                        btn_ok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                closeCommonAlert();
                                // startActivity(new Intent(BaseActivity.this, ActivityAddTrip.class));

                                startActivity(new Intent(BaseActivity.this, ActivityUpdatedAddTrip.class));
                            }
                        });


                        commonalert = alertDialogBuilder.create();
                        commonalert.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

                        commonalert.show();

                    } else {
                        alertShowLogin(getString(R.string.login_messge));
                    }
                }
            });

        } catch (Exception e) {

        }


        dialog = new ProgressDialog(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            countDownStart();
        } catch (Exception e) {

        }
    }

    public void countDownStart() {
        // Log.d("coundown", "countDownStart: "+"started");
        if (UtilsDefault.getSharedPreferenceValue(Constants.LATEST_DATE) != null) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            try {
                Calendar allrmcal = Calendar.getInstance();
                Date event_date = format.parse(UtilsDefault.getSharedPreferenceValue(Constants.LATEST_DATE));
                allrmcal.setTime(event_date);
                Calendar currentCal = Calendar.getInstance();
                Date current_date = currentCal.getTime();
                if (!current_date.after(event_date)) {
                    long delay = allrmcal.getTimeInMillis() - currentCal.getTimeInMillis();
                    scheduleNotification(this, delay, 1);

                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
            Log.d("UptripTime", "countDownStart: " + UtilsDefault.getSharedPreferenceValue(Constants.LATEST_DATE));
            Log.d("coundown", "countDownStart: " + "started" + UtilsDefault.getSharedPreferenceValue(Constants.LATEST_DATE));
            // Log.d("triptime", "countDownStart: "+UtilsDefault.getSharedPreferenceValue(Constants.LATEST_DATE));
            //  Log.d("coundown", "countDownStart: "+"started"+UtilsDefault.getSharedPreferenceValue(Constants.LATEST_DATE));
            runnable = new Runnable() {
                @Override
                public void run() {
                    try {
                        handler.postDelayed(this, 1000);
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
                        Calendar curentcals = Calendar.getInstance();

//                        if (UtilsDefault.getSharedPreferenceValue(Constants.TIME_ZONE) != null) {
//                            TimeZone tz = TimeZone.getTimeZone(UtilsDefault.
//                                    getSharedPreferenceValue(Constants.TIME_ZONE));
//                            curentcal.setTimeZone(tz);
//                            format.setTimeZone(tz);
//                        }
                        Date event_date =
                                format.parse(UtilsDefault.getSharedPreferenceValue(Constants.LATEST_DATE));
                        Date current_date = curentcals.getTime();
                        Log.d("currenttime", "run: " + current_date.toString());
                        Log.d("eventdate", "run: " + event_date.toString());
                        if (!current_date.after(event_date)) {
                            timer_lay.setVisibility(View.VISIBLE);
                            tv_clickhere.setVisibility(View.GONE);
                            long diff = event_date.getTime() - current_date.getTime();
                            long Days = diff / (24 * 60 * 60 * 1000);
                            long Hours = diff / (60 * 60 * 1000) % 24;
                            long Minutes = diff / (60 * 1000) % 60;
                            long Seconds = diff / 1000 % 60;
                            //
                            //  tv_days.setText(String.format("%02d", Days));
                            //   Log.d("coundown", "countDownStart: "+Hours);

                            tv_hrs.setText(String.format("%02d", Hours));
                            tv_mins.setText(String.format("%02d", Minutes));
                            tv_secs.setText(String.format("%02d", Seconds));
                            if (UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_DOMESTIC_TYPE).
                                    equals("0") && Integer.parseInt(String.format("%02d", Minutes)) < 25 &&
                                    String.format("%02d", Hours).equals("00")) {
                                tv_hrs.setTextColor(getResources().getColor(R.color.darkred));
                                tv_mins.setTextColor(getResources().getColor(R.color.darkred));
                                tv_secs.setTextColor(getResources().getColor(R.color.darkred));

                            } else if (UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_DOMESTIC_TYPE).
                                    equals("1") && Integer.parseInt(String.format("%02d", Minutes)) < 45 &&
                                    String.format("%02d", Hours).equals("00")) {
                                tv_hrs.setTextColor(getResources().getColor(R.color.darkred));
                                tv_mins.setTextColor(getResources().getColor(R.color.darkred));
                                tv_secs.setTextColor(getResources().getColor(R.color.darkred));

                            } else {
                                tv_hrs.setTextColor(getResources().getColor(R.color.dark_blue));
                                tv_mins.setTextColor(getResources().getColor(R.color.dark_blue));
                                tv_secs.setTextColor(getResources().getColor(R.color.dark_blue));
                            }
                            if (String.format("%02d", Seconds).equals("00") &&
                                    String.format("%02d", Minutes).equals("00") &&
                                    String.format("%02d", Hours).equals("00")) {
                                timer_lay.setVisibility(View.GONE);
                                tv_clickhere.setVisibility(View.VISIBLE);
                                handler.removeCallbacks(runnable);
                            }
                        } else {
                            timer_lay.setVisibility(View.GONE);

                            tv_clickhere.setVisibility(View.VISIBLE);
                            handler.removeCallbacks(runnable);
                            Log.d("coundown", "countDownStart: " + "not upcoming");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        // Log.d("coundown", "countDownStart: "+e.getMessage());
                    }
                }
            };
            handler.postDelayed(runnable, 0);
        } else {
            try {
                timer_lay.setVisibility(View.GONE);
                tv_clickhere.setVisibility(View.VISIBLE);
                handler.removeCallbacks(runnable);

            } catch (Exception e) {

            }
        }

    }

    protected void onStop() {
        super.onStop();
        handler.removeCallbacks(runnable);
    }

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        // finish();
    }


    protected abstract int getlayout();
}
