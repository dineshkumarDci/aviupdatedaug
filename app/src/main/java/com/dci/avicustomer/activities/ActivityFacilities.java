package com.dci.avicustomer.activities;

import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.dci.avicustomer.R;
import com.dci.avicustomer.adapter.AdapterFacilitiesUpdated;
import com.dci.avicustomer.adapter.AdapterFacility;
import com.dci.avicustomer.adapter.AdapterSectionRecycler;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.models.ModelFacilities;
import com.dci.avicustomer.models.ModelSearchResult;
import com.dci.avicustomer.retrofit.AVIAPI;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityFacilities extends BaseActivity {
    @BindView(R.id.recycle_facilities)
    RecyclerView recycle_facilities;
    @Inject
    public AVIAPI aviapi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        ShoppingApplication.getContext().getComponent().
                inject(this);
        tv_title.setText(R.string.title_fc);
        getfacilities();
    }
    @Override
    public void closeCommonAlert() {
        commonalert.dismiss();
        finish();
    }
    @Override
    protected int getlayout() {
        return R.layout.activity_facilities;
    }
    public void getfacilities(){
        if (!UtilsDefault.isOnline()){
            Toast.makeText(ActivityFacilities.this, R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;
        }
        shwProgress();
        Log.d("aviparms",
                "search: "+UtilsDefault.getSharedPreferenceValue(Constants.USER_ID));
        String userid="0";
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)!=null){
            userid=UtilsDefault.getSharedPreferenceValue(Constants.USER_ID);
        }
        aviapi.getAirportFacility(userid,UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID),
                UtilsDefault.getSharedPreferenceValue(Constants.TERMINAL_ID),
                UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_TYPE)).enqueue(new Callback<ModelFacilities>() {

            @Override
            public void onResponse(Call<ModelFacilities> call,
                                   Response<ModelFacilities> response) {
                hideprogress();
                hideKeyboard();
                if(response.body()!=null){
                    ModelFacilities data=response.body();
//                    String header = response.headers().get(getString(R.string.header_find_key));
//                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus()== Constants.STATUS_200){
                        if (data.getData().size()==0&&data.getData().size()==0){
                            alertcommon(getString(R.string.title_fc),
                                    getString(R.string.mo_result));
                           // Toast.makeText(ActivityFacilities.this,
                            // R.string.mo_result, Toast.LENGTH_SHORT).show();
                        }
                        else {
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ActivityFacilities.this);
                            recycle_facilities.setLayoutManager(linearLayoutManager);
                            AdapterFacilitiesUpdated adapterRecycler = new AdapterFacilitiesUpdated(data.getData(), ActivityFacilities.this);
                            //AdapterFacility adapterRecycler = new AdapterFacility(data.getData(),ActivityFacilities.this);
                            recycle_facilities.setAdapter(adapterRecycler);
                        }



                        //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));



                    }
                    else if (data.getStatus()==Constants.API_TOKENEXPIRY_STATUS){
                        getfacilities();
                    }
                    else {
                        alertcommon(getString(R.string.title_fc),data.getMessage());
                       // Toast.makeText(ActivityFacilities.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Toast.makeText(ActivityFacilities.this,R.string.server_error, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ModelFacilities> call, Throwable t) {
                hideprogress();
                hideKeyboard();
                Toast.makeText(ActivityFacilities.this,R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
