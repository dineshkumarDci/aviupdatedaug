package com.dci.avicustomer.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dci.avicustomer.R;
import com.dci.avicustomer.adapter.AdapterBoardingPass;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.CommonMsgModel;
import com.dci.avicustomer.models.ModelInputAddtrip;
import com.dci.avicustomer.models.ModelNewFlightStatus;
import com.dci.avicustomer.models.ModelTripDetails;
import com.dci.avicustomer.models.ModelUpcomingtrips;
import com.dci.avicustomer.retrofit.AVIAPI;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityMyTripDetails extends BaseActivity implements View.OnClickListener {

    @Inject
    public AVIAPI aviapi;


    @BindView(R.id.tv_dept_time)
    TextView tv_dept_time;

    @BindView(R.id.tv_dept_date)
    TextView tv_dept_date;

    @BindView(R.id.tv_arrive_time)
    TextView tv_arrive_time;

    @BindView(R.id.tv_arrive_date)
    TextView tv_arrive_date;

    @BindView(R.id.tv_dept_code)
    TextView tv_dept_code;

    @BindView(R.id.tv_dept_city)
    TextView tv_dept_city;

    @BindView(R.id.tv_arrive_code)
    TextView tv_arrive_code;

    @BindView(R.id.tv_arrival_city)
    TextView tv_arrival_city;

    @BindView(R.id.tv_travel_time)
    TextView tv_travel_time;

    @BindView(R.id.tv_dept_terminal)
    TextView tv_dept_terminal;
    @BindView(R.id.tv_arrival_terminal)
    TextView tv_arrival_terminal;
    @BindView(R.id.tv_airlinename)
    TextView tv_airlinename;


    @BindView(R.id.frame_result_lay)
    FrameLayout frame_result_lay;
    @BindView(R.id.tv_flight_code)
    TextView tv_flight_code;
    @BindView(R.id.recycle_boardingpass)
    RecyclerView recycle_boardingpass;
    @BindView(R.id.recycle_eticket)
    RecyclerView recycle_eticket;
    @BindView(R.id.img_edit_icon)
    ImageView img_edit_icon;
    @BindView(R.id.img_delete_icon)
    ImageView img_delete_icon;
    @BindView(R.id.tv_boarding_nodata)
    TextView tv_boarding_nodata;
    @BindView(R.id.tv_ticket_nodata)
    TextView tv_ticket_nodata;
    @BindView(R.id.user_login)
    ImageView user_login;
    @BindView(R.id.search_lay)
    LinearLayout search_lay;
    @BindView(R.id.tv_dept_status)
    Button tv_dept_status;
    String tripid = "";
    ModelInputAddtrip modelInputAddtrip;
    boolean issearchResultSucess = false;
    AdapterBoardingPass adapterBoardingPass;
    AdapterBoardingPass adapterEticket;
    ModelTripDetails data;
    List<ModelTripDetails.ETicketImg> eTicketImgs = new ArrayList<>();
    List<ModelTripDetails.BoardImg> boardingimages = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        ShoppingApplication.getContext().getComponent().inject(this);
        if (getIntent().getStringExtra("trip_id") != null) {
            tripid = getIntent().getStringExtra("trip_id");
            getTripDetails();
        }
        img_delete_icon.setOnClickListener(this);
        img_edit_icon.setOnClickListener(this);
        tv_title.setText(getString(R.string.title_my_trip));
        user_login.setClipToOutline(true);

    }

    public void deleteAlert(String msg) {
        AlertDialog.Builder alertDialogBuilder =
                new AlertDialog.Builder(this);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setMessage(msg);
        alertDialogBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        deleteTrips();

                    }
                });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        dialog.dismiss();
                    }
                });

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void getLatestTrip(String msg) {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityMyTripDetails.this,
                    R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;

        }
        shwProgress();
        //  Log.d("aviparms", "search: "+UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)+search);
        String userid = "0";
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
            userid = UtilsDefault.getSharedPreferenceValue(Constants.USER_ID);
        }

        aviapi.getlatestTrip(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID),
                UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID), timeZone.getID()).
                enqueue(new Callback<ModelUpcomingtrips>() {
            @Override
            public void onResponse(Call<ModelUpcomingtrips> call, Response<ModelUpcomingtrips> response) {
                hideprogress();

                if (response.body() != null) {
                    ModelUpcomingtrips data = response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus() == Constants.STATUS_200) {
                        UtilsDefault.updateSharedPreference(Constants.AIRPORT_DOMESTIC_TYPE,
                                data.getUpTripTimeType());
                        UtilsDefault.updateSharedPreference(Constants.LATEST_DATE, data.getUpTripTime());
                        countDownStart();
                        alertcommon(getString(R.string.title_my_trip), msg);
                        //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                    } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                        getLatestTrip(msg);
                    } else {
                        UtilsDefault.updateSharedPreference(Constants.AIRPORT_DOMESTIC_TYPE,
                                data.getUpTripTimeType());
                        UtilsDefault.updateSharedPreference(Constants.LATEST_DATE, data.getUpTripTime());
                        countDownStart();
                        alertcommon(getString(R.string.title_my_trip), msg);
                        //Toast.makeText(ActivityMyTrips.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {


                    //   Toast.makeText(ActivityMyTrips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ModelUpcomingtrips> call, Throwable t) {
                hideprogress();
                Log.d("failure", "onFailure: " + t.getMessage());
                //Toast.makeText(ActivityMyTrips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void deleteTrips() {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityMyTripDetails.this, R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;

        }
        Log.d("finput", "getflightStatus: " + tripid);
        shwProgress();
        aviapi.deleteTrip(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID), tripid).
                enqueue(new Callback<CommonMsgModel>() {
                    @Override
                    public void onResponse(Call<CommonMsgModel> call, Response<CommonMsgModel> response) {
                        hideprogress();
                        hideKeyboard();
                        if (response.body() != null) {
                            CommonMsgModel data = response.body();
                            String header = response.headers().get(getString(R.string.header_find_key));
                            UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                            if (data.getStatus() == Constants.STATUS_200) {

                                getLatestTrip(data.getMessage());


                            } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                                deleteTrips();
                            } else {
                                alertcommon(getString(R.string.title_my_trip), data.getMessage());
                                //  Toast.makeText(ActivityMyTripDetails.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            alertcommon(getString(R.string.title_my_trip), getString(R.string.server_error));
                            //Toast.makeText(ActivityMyTripDetails.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<CommonMsgModel> call, Throwable t) {
                        hideprogress();
                        alertcommon(getString(R.string.title_my_trip), getString(R.string.server_error));
                        Log.d("faill", "onFailure: " + t.getMessage());
                        //  Toast.makeText(ActivityMyTripDetails.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void updateUiTripDetails() {
        frame_result_lay.setVisibility(View.VISIBLE);

        if (data.getData().getFlight_status() != null && !
                data.getData().getFlight_status().equals("")) {
            if (data.getData().getFlight_status().toLowerCase().equals("delayed")) {
                tv_dept_status.setText(data.getData().getFlight_status());
                tv_dept_status.setBackground(getDrawable(R.drawable.bg_flight_red_status));
            } else {
                tv_dept_status.setText(data.getData().getFlight_status());
                tv_dept_status.setBackground(getDrawable(R.drawable.bg_flight_dept_status));
            }
        }
        if (data.getData() != null) {
            if (data.getData().getArr_airport_city() != null &&
                    data.getData().getArr_airport_country() != null) {
                tv_arrival_city.setText(data.getData().getArr_airport_city() + "," +
                        data.getData().getArr_airport_country());

            }
            if (data.getData().getArr_airport_code() != null) {
                tv_arrive_code.setText(data.getData().getArr_airport_code());

            }
            if (data.getData().getFlight_code() != null) {
                tv_flight_code.setText(data.getData().getFlight_code());
            }
            if (data.getData().getArr_time() != null) {
                tv_arrive_time.setText(UtilsDefault.timeFormat(data.getData().getArr_time()));

            }
            if (data.getData().getArr_date() != null) {
                tv_arrive_date.setText(UtilsDefault.dateFormat(data.getData().getArr_date() + " 01:34:01"));

            }
            if (data.getData().getArr_terminal() != null) {
                tv_arrival_terminal.setText(getString(R.string.text_terminal) + " " + data.getData().getArr_terminal() + UtilsDefault.checkNull(data.getData().getArrGate()));

            }

        }

        // tv_dept_terminal.setText(R.string.text_terminal+" "+data.getData().getDeparture().getAirport().getAirportId().getAirportCode());
        //   tv_dept_terminal.setText(R.string.text_terminal+" "+data.getData().getDeparture().getAirport().getTerminal());
        //   UtilsDefault.updateSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));

        if (data.getData().getDep_terminal() != null) {
            tv_dept_terminal.setText(getString(R.string.text_terminal) + " " + data.getData().getDep_terminal() +
                    " , " + UtilsDefault.checkNull(data.getData().getArrGate()));

        }
        if (data.getData().getDep_date() != null) {
            tv_dept_date.setText(UtilsDefault.dateFormat(data.getData().getDep_date() + " 01:34:01"));

        }
        if (data.getData().getDep_time() != null) {
            tv_dept_time.setText(UtilsDefault.timeFormat(data.getData().getDep_time()));
        }
        if (data.getData().getDep_airport_city() != null && data.getData().getDep_airport_code() != null) {
            tv_dept_city.setText(data.getData().getDep_airport_city() + "," + data.getData().getDep_airport_country());

        }
        if (data.getData().getDep_airport_code() != null) {
            tv_dept_code.setText(data.getData().getDep_airport_code());
        }

        if (data.getData().getDep_time() != null && data.getData().getArr_time() != null) {
            try {
                if (data.getData().getDep_time() != null &&
                        data.getData().getArr_time() != null) {
                    tv_travel_time.setText(UtilsDefault.differentBetweenTime(data.getData().getDep_date()+" "+data.getData().getDep_time(),
                            data.getData().getArr_date()+" "+data.getData().getArr_time()));
                }
            } catch (Exception e) {
                Toast.makeText(ActivityMyTripDetails.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        }

        if (data.getData().getDepGate() == null || data.getData().getDepGate().equals("")) {
// holder.tv_arrval_terminal
        } else {
            tv_dept_terminal.append(
                    String.valueOf(" , " + data.getData().getDepGate()));
        }

        if (data.getData().getArrGate() == null || data.getData().getArrGate().equals("")) {
// holder.tv_arrval_terminal
        } else {
            tv_arrival_terminal.append(
                    String.valueOf(" " + data.getData().getArrGate()));
        }
//        if (data.getData()!=null&&data.getData().getDep_delay()!=null&&
//                !data.getData().getDep_delay().equals("0")){
////            dep_status.setVisibility(View.VISIBLE);
////            tv_dep_delay.setVisibility(View.VISIBLE);
//
//            tv_dept_status.setText(getString(R.string.dep_delay)+" "+data.getData().getDep_delay());
//            tv_dept_status.setBackground(getDrawable(R.drawable.bg_flight_red_status));
//        }
//        if (data.getData()!=null&&data.getData().getArr_delay()!=null&&!
//                data.getData().getArr_delay().equals("0")){
////            tv_arr_status.setVisibility(View.VISIBLE);
////            tv_arr_delay.setVisibility(View.VISIBLE);
//          //  tv_arr_delay.setText(getString(R.string.delayed_by)+" "+data.getData().getArr_delay());
//            tv_dept_status.setText(getString(R.string.arr_delay)+" "+data.getData().getArr_delay());
//            tv_dept_status.setBackground(getDrawable(R.drawable.bg_flight_red_status));
//
//        }


//        if (data != null && data.getDep_delay() != null &&
//                !data.getDep_delay().equals("0")) {
//            holder.dep_status.setVisibility(View.VISIBLE);
//            holder. tv_dep_delay.setVisibility(View.VISIBLE);

        boolean isdeparted = false;
        if (data.getData().getAct_dep_date() != null &&
                data.getData().getAct_dep_time() != null &&
                !data.getData().getAct_dep_date().equals("") &&
                !data.getData().getAct_dep_time().equals("")) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date newDate = null;
            try {
                newDate = format.parse(data.getData().getAct_dep_date() + " " +
                        data.getData().getAct_dep_time());
                if (Calendar.getInstance().getTime().after(newDate)) {
                    isdeparted = true;
                    if (data == null || data.getData().getDep_delay() == null ) {
                        //   holder.dep_status.setVisibility(View.VISIBLE);
                        //   holder.tv_dep_delay.setVisibility(View.VISIBLE);
                    } else {
                        if(!data.getData().getDep_delay().equals("0")){
                            tv_dept_status.setText(getString(R.string.dep_delayed) + " " +
                                    data.getData().getDep_delay());
                            tv_dept_status.setBackground(getDrawable(R.drawable.bg_flight_red_status));
                        }
//                        tv_dept_status.setText(getString(R.string.dep_delayed) + " " +
//                                data.getData().getDep_delay());
//                        tv_dept_status.setBackground(getDrawable(R.drawable.bg_flight_red_status));
                    }

                } else {
                    isdeparted = false;
                    if (data == null || data.getData().getDep_delay() == null) {
                        // holder.dep_status.setVisibility(View.VISIBLE);
                        //holder.tv_dep_delay.setVisibility(View.VISIBLE);
                    } else {
                        if(!data.getData().getDep_delay().equals("0")){
                            tv_dept_status.setText(getString(R.string.dep_delay) + " " +
                                    data.getData().getDep_delay());
                            tv_dept_status.setBackground(getDrawable(R.drawable.bg_flight_red_status));
                        }

                        // tv_dep_actualtime_head.setText(getString(R.string.estimated_time));
                    }

                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }


        // holder.tv_dep_delay.setText(mContext.getString(R.string.delayed_by) + " " + data.getDep_delay());

//
//        if (data!= null && data.getArr_delay() != null && !
//                data.getArr_delay().equals("0")) {
//            holder.tv_arr_status.setVisibility(View.VISIBLE);
//            holder.tv_arr_delay.setVisibility(View.VISIBLE);
        if (data.getData().getAct_arr_date() != null &&
                data.getData().getAct_arr_time() != null &&
                !data.getData().getAct_arr_date().equals("") &&
                !data.getData().getAct_arr_time().equals("")) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date newDate = null;


            try {
                newDate = format.parse(data.getData().getAct_arr_date() + " " +
                        data.getData().getAct_arr_time());
                if (!Calendar.getInstance().getTime().after(newDate)) {
                    //tv_arr_actualtime_head.setText(mContext.getString(R.string.estimated_time));
                }
                if (Calendar.getInstance().getTime().after(newDate)) {
                    if (data == null || data.getData().getArr_delay() == null) {
//                        tv_arr_status.setVisibility(View.VISIBLE);
//                        holder.tv_arr_delay.setVisibility(View.VISIBLE);
                    } else {
                        if(!data.getData().getArr_delay().equals("0")){
                            tv_dept_status.setText(getString(R.string.arr_delayed) + " " +
                                    data.getData().getArr_delay());
                            tv_dept_status.setBackground(getDrawable(R.drawable.bg_flight_red_status));
                        }
                    }

                } else {
                    if (data == null || data.getData().getArr_delay() == null) {
                        //holder.tv_arr_status.setVisibility(View.VISIBLE);
                        // holder.tv_arr_delay.setVisibility(View.VISIBLE);
                    } else {
                        if (isdeparted) {
                            if(!data.getData().getArr_delay().equals("0")){
                                tv_dept_status.setText(getString(R.string.arr_delay) + " " +
                                        data.getData().getArr_delay());
                                tv_dept_status.setBackground(getDrawable(R.drawable.bg_flight_red_status));
                            }
//                            tv_dept_status.setText(getString(R.string.arr_delay) + " " +
//                                    data.getData().getArr_delay());
//                            tv_dept_status.setBackground(getDrawable(R.drawable.bg_flight_red_status));
                        }

                        //  holder.tv_arr_actualtime_head.setText(getString(R.string.estimated_time));
                    }

                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }


        if (data.getData().getArr_delay() != null && !data.getData().getArr_delay().equals("0")) {
//            holder. tv_arr_delay.setText(mContext.getString(R.string.delayed_by) + " " + data.getArr_delay());
//            holder.tv_arr_delay.setTextColor(mContext.getResources().getColor(R.color.red_status));

        }


//        }
//        else {
//            holder. tv_arr_status.setVisibility(View.GONE);
//            holder. tv_arr_delay.setVisibility(View.GONE);
//        }
        try {
            if(data.getData().getFlight_status().toLowerCase().equals(getResources().getString(R.string.cancelled))){
                tv_dept_status.setBackground(getResources().getDrawable(R.drawable.bg_flight_red_status));
                tv_dept_status.setText(data.getData().getFlight_status());
//                holder.tv_dep_delay.setText(mContext.getResources().getString(R.string.cancelled));
//                holder.tv_arr_delay.setText(mContext.getResources().getString(R.string.cancelled));
            }
        }
        catch (Exception e){

        }

        tv_flight_code.setText(data.getData().getAirline());
        tv_flight_code.setSelected(true);
        tv_airlinename.setText(data.getData().getFlight_code());
    }

    public void getTripDetails() {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityMyTripDetails.this,
                    R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;
        }
        Log.d("finput", "getflightStatus: " + tripid);
        shwProgress();
        aviapi.getMyTripsDetails(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID), tripid, timeZone.getID()).enqueue(new Callback<ModelTripDetails>() {
            @Override
            public void onResponse(Call<ModelTripDetails> call, Response<ModelTripDetails> response) {
                hideprogress();
                hideKeyboard();
                if (response.body() != null) {
                    data = response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus() == Constants.STATUS_200) {
                        updateUiTripDetails();
                        if (data.getBoardImg() == null || data.getBoardImg().size() == 0) {
                            tv_boarding_nodata.setVisibility(View.VISIBLE);
                        } else {
                            tv_boarding_nodata.setVisibility(View.GONE);
                            recycle_boardingpass.setVisibility(View.VISIBLE);
                            boardingimages.clear();
                            boardingimages.addAll(data.getBoardImg());
                            adapterBoardingPass = new AdapterBoardingPass(ActivityMyTripDetails.this, boardingimages, null, 0);
                            recycle_boardingpass.setLayoutManager(new LinearLayoutManager(ActivityMyTripDetails.this, LinearLayoutManager.HORIZONTAL, false));
                            recycle_boardingpass.setAdapter(adapterBoardingPass);
                            adapterBoardingPass.setonItemClick(new OnItemClickListener() {
                                @Override
                                public void OnItemClickListener(int pos, View view) {

                                    String MimeType = getMimeType(boardingimages.get(pos).getImage());
                                    if (MimeType.contains("image")) {
                                        showImage(null, boardingimages.get(pos).getImage());
                                    } else {
                                        Intent intent = new Intent(ActivityMyTripDetails.this, ActivityCommonWebLoader.class);
                                        intent.putExtra("data",
                                                "https://drive.google.com/viewerng/viewer?embedded=true&url=" +
                                                        boardingimages.get(pos).getImage());
                                        if (boardingimages.get(pos).getImage() != null &&
                                                boardingimages.get(pos).getImage().equals("")) {
                                            intent.putExtra("title", boardingimages.get(pos).getImage());
                                        }

                                        startActivity(intent);
                                        // https://drive.google.com/viewerng/viewer?embedded=true&url=
                                    }

                                }
                            });


                        }

                        if (data.geteTicketImg() == null || data.geteTicketImg().size() == 0) {
                            tv_ticket_nodata.setVisibility(View.VISIBLE);
                        } else {
                            tv_ticket_nodata.setVisibility(View.GONE);
                            recycle_eticket.setVisibility(View.VISIBLE);

                            eTicketImgs.clear();
                            eTicketImgs.addAll(data.geteTicketImg());
                            adapterEticket = new AdapterBoardingPass(ActivityMyTripDetails.this, null, eTicketImgs, 0);
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ActivityMyTripDetails.this, LinearLayoutManager.HORIZONTAL, false);
                            recycle_eticket.setLayoutManager(linearLayoutManager);
                            adapterEticket.setonItemClick(new OnItemClickListener() {
                                @Override
                                public void OnItemClickListener(int pos, View view) {
                                    String MimeType = getMimeType(eTicketImgs.get(pos).getImage());
                                    if (MimeType.contains("image")) {
                                        showImage(null, eTicketImgs.get(pos).getImage());
                                    } else {
                                        Intent intent = new Intent(ActivityMyTripDetails.this, ActivityCommonWebLoader.class);
                                        intent.putExtra("data",
                                                "https://drive.google.com/viewerng/viewer?embedded=true&url=" +
                                                        eTicketImgs.get(pos).getImage());
                                        intent.putExtra("title", eTicketImgs.get(pos).getImage());
                                        startActivity(intent);
                                        // https://drive.google.com/viewerng/viewer?embedded=true&url=
                                    }
                                }
                            });

                            recycle_eticket.setAdapter(adapterEticket);


                        }
                    } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                        getTripDetails();
                    } else {
                        alertcommon(getString(R.string.title_my_trip), data.getMessage());
                        //  Toast.makeText(ActivityMyTripDetails.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    alertcommon(getString(R.string.title_my_trip), getString(R.string.server_error));
                    //Toast.makeText(ActivityMyTripDetails.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ModelTripDetails> call, Throwable t) {
                hideprogress();
                alertcommon(getString(R.string.title_my_trip), getString(R.string.server_error));
                Log.d("faill", "onFailure: " + t.getMessage());
                //  Toast.makeText(ActivityMyTripDetails.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void showImage(Drawable drawable, String imgUrl) {
        Dialog builder = new Dialog(this);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {

                //nothing;
            }
        });

        ImageView imageView = new ImageView(this);
        // imageView.setImageDrawable(drawable);
        // imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        builder.addContentView(imageView, new RelativeLayout.LayoutParams(
                1000,
                1000));

        Glide.with(this).load(imgUrl).
                placeholder(R.drawable.folder).into(imageView);

        builder.show();
    }


    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }

    @Override
    public void closeCommonAlert() {
        commonalert.dismiss();
        finish();
    }

    @Override
    protected int getlayout() {
        return R.layout.activity_mytrip_details;
    }

    @Override
    public void onClick(View v) {
        if (v == img_delete_icon) {
            deleteAlert(getString(R.string.sure_to_delete_trip));
            // deleteTrips();
        }
        if (v == img_edit_icon) {
            if (data != null && data.getStatus() == Constants.STATUS_200) {

                Intent intent = new Intent(ActivityMyTripDetails.this, ActivityEditTrip.class);
                intent.putExtra("data", data);
                startActivity(intent);
                finish();
            } else {
                alert(getString(R.string.trip_not_avail));
            }
//            adapterBoardingPass.setEdtitType(1);
//            adapterBoardingPass.notifyDataSetChanged();
//
//            adapterEticket.setEdtitType(1);
//            adapterEticket.notifyDataSetChanged();

        }

    }
}
