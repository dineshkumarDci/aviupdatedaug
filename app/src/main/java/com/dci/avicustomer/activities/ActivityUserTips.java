package com.dci.avicustomer.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.avicustomer.R;
import com.dci.avicustomer.adapter.AdapterUserTips;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.models.CommonMsgModel;
import com.dci.avicustomer.models.ModelUserTipList;
import com.dci.avicustomer.retrofit.AVIAPI;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityUserTips extends BaseActivity implements View.OnClickListener {
    @Inject
    public AVIAPI aviapi;
    @BindView(R.id.recycle_usertips)
    RecyclerView recycle_usertips;
    @BindView(R.id.img_add_icon)
    ImageView img_add_icon;
    AlertDialog alertAddTips;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        ShoppingApplication.getContext().getComponent().inject(this);
        getUserTips();
        tv_title.setText(R.string.title_user_tips);
        img_add_icon.setOnClickListener(this);

    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void getUserTips(){
        if (!UtilsDefault.isOnline()){
            Toast.makeText(ActivityUserTips.this, R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;

        }
        shwProgress();
        aviapi.userTips(UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID),"1").enqueue(new Callback<ModelUserTipList>() {
            @Override
            public void onResponse(Call<ModelUserTipList> call, Response<ModelUserTipList> response) {
                hideprogress();
                if (response.body()!=null){
                    ModelUserTipList data=response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus()== Constants.STATUS_200){
                        if (data.getData()!=null&&data.getData().size()!=0){
                            recycle_usertips.setLayoutManager(new LinearLayoutManager(ActivityUserTips.this));
                            AdapterUserTips adapterUserTips=new AdapterUserTips(ActivityUserTips.this,data.getData(),1);
                            recycle_usertips.setAdapter(adapterUserTips);
                        }
                        else {
                            Toast.makeText(ActivityUserTips.this, R.string.nodata, Toast.LENGTH_SHORT).show();
                        }
                        //   UtilsDefault.updateSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                    }
                    else if (data.getStatus()==Constants.API_TOKENEXPIRY_STATUS){
                        getUserTips();
                    }
                    else {
                        Toast.makeText(ActivityUserTips.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Toast.makeText(ActivityUserTips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ModelUserTipList> call, Throwable t) {
                hideprogress();
                Toast.makeText(ActivityUserTips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void addUserTips(String des){
        if (!UtilsDefault.isOnline()){
            Toast.makeText(ActivityUserTips.this, R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;
        }
        shwProgress();
        aviapi.addUserTips(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID),
                UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID),des).
                enqueue(new Callback<CommonMsgModel>() {
            @Override
            public void onResponse(Call<CommonMsgModel> call, Response<CommonMsgModel> response) {
                hideprogress();
                if (response.body()!=null){
                    CommonMsgModel data=response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                  //  UtilsDefault.updateSharedPreference(Constants.AUTH_TOKEN,data.getJwt_token());
                    if (data.getStatus()== Constants.STATUS_200){
                        alertAddTips.dismiss();
                        getUserTips();

                    }
                    else if (data.getStatus()==Constants.API_TOKENEXPIRY_STATUS){
                        addUserTips(des);
                    }
                    else {
                        Toast.makeText(ActivityUserTips.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Toast.makeText(ActivityUserTips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<CommonMsgModel> call, Throwable t) {
                hideprogress();
                Toast.makeText(ActivityUserTips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void alertAddTips(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setCancelable(true);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_alert_addusertips, null);
        alertDialogBuilder.setView(dialogView);
        EditText editips = dialogView.findViewById(R.id.ed_add_tips);

        Button btn_add = dialogView.findViewById(R.id.btn_add);


        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tips=editips.getText().toString().trim();
                if (tips.equals("")){
                    editips.setError(getString(R.string.error_enter_tips));
                    editips.requestFocus();

                }
                else {
                    addUserTips(tips);
                }

            }
        });


        alertAddTips = alertDialogBuilder.create();
        alertAddTips.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertAddTips.show();
    }

    @Override
    protected int getlayout() {
        return R.layout.activity_user_tips;
    }

    @Override
    public void onClick(View v) {
        if (v==img_add_icon){
            alertAddTips();
        }

    }
}
