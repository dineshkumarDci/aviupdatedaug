package com.dci.avicustomer.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dci.avicustomer.R;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.models.ModelShopList;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityShopDetails extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.tv_shopname)
    TextView tv_shopname;
    @BindView(R.id.tv_shop_address)
    TextView tv_shop_address;
    @BindView(R.id.tv_open_time)
    TextView tv_open_time;
    @BindView(R.id.tv_terminal_name)
    TextView tv_terminal_name;
    @BindView(R.id.tv_describtion)
    TextView tv_describtion;
    @BindView(R.id.tv_offer_code)
    TextView tv_offer_code;
    @BindView(R.id.img_phn_num)
    ImageView img_phn_num;
    @BindView(R.id.img_email)
    ImageView img_email;
    @BindView(R.id.web_coupondes)
    WebView web_coupondes;
    @BindView(R.id.web_des)
    WebView web_des;
    @BindView(R.id.img_locate)
    ImageView img_locate;
    @BindView(R.id.img_shop)
    ImageView img_shop;
    @BindView(R.id.rating)
    RatingBar rating;
    @BindView(R.id.tv_open_status)
     TextView tv_open_status;
    ModelShopList.Datum datum;
    String imgbase="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        tv_title.setText(R.string.title_shop);
        if (getIntent().getSerializableExtra("data") != null) {
            datum = (ModelShopList.Datum) getIntent().getSerializableExtra("data");
            tv_shopname.setText(datum.getName());
            StringBuilder stringBuilder=new StringBuilder();
            stringBuilder.append(UtilsDefault.checkNull(datum.getTerminal().getName()));
            if (datum.getArea_type()!=null&&datum.getArea_type()== Constants.AREA_DEP){
                stringBuilder.append("-"+"Departure");
            }
            else {
                stringBuilder.append("-"+"Arrival");
            }
            tv_terminal_name.setText(stringBuilder);

            tv_shop_address.setText(datum.getAddress());
            SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
            Date d = new Date();
            String dayOfTheWeek = sdf.format(d);
            if (datum.getRetail_times() != null && datum.getRetail_times().size() != 0) {
                for (ModelShopList.Datum.Retail_time data : datum.getRetail_times()) {
                    if (data.getDay() != null && data.getDay().toLowerCase().equals(dayOfTheWeek.toLowerCase())) {
                        tv_open_time.setText(data.getFrom_time() + " to " + data.getTo_time());
                    }
                }
            }
            if (datum.getWorking_hours()!=null){
                tv_open_time.setText(datum.getWorking_hours());
            }
//            tv_terminal_name.setText("Terminal " + UtilsDefault.checkNull(datum.getTerminal().getName())
//                    + "-" + UtilsDefault.checkNull(datum.getAirport_type()) + "-" +
//                    UtilsDefault.checkNull(datum.getArea_type()));
            tv_describtion.setText(UtilsDefault.checkNull(datum.getDescription()));
            if (datum.getCoupons() != null && datum.getCoupons().size() != 0) {
                tv_offer_code.setVisibility(View.VISIBLE);
                tv_offer_code.setText(datum.getCoupons().get(0).getOffer_code());
                web_coupondes.loadData(datum.getCoupons().get(0).getDescription(),
                        "text/html; charset=utf-8", "base64");
            }
            imgbase=getIntent().getStringExtra("imgbase");
            web_des.loadData(datum.getDescription(), "text/html; charset=utf-8",
                    "base64");
            Glide.with(this).load(imgbase+"/"+
                    datum.getProfile_img()).into(img_shop);

            if (datum.getOpenClose()!=null&&
                    datum.getOpenClose()==1){
               tv_open_status.setText("Open");
                tv_open_status.setTextColor(getResources().getColor(R.color.dark_green));
            }
            else {
               tv_open_status.setText("Closed");
               tv_open_status.setTextColor(getResources().getColor(R.color.red));
            }
        }
        img_email.setOnClickListener(this);
        img_phn_num.setOnClickListener(this);
        img_shop.setClipToOutline(true);
        img_locate.setOnClickListener(this);
    }

    @Override
    protected int getlayout() {
        return R.layout.activity_shop_details;
    }

    @Override
    public void onClick(View v) {
        if (v==img_email){
            if (datum.getEmail()!=null&&!
                    datum.getEmail().equals("")){
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" +
                            datum.getEmail().trim()));

                    startActivity(intent);
                } catch (Exception e) {
                    Toast.makeText(this, "Sorry...You don't have any mail app", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
            else {
                Toast.makeText(this, R.string.no_email, Toast.LENGTH_SHORT).show();
            }
        }
        if (v==img_locate){
            if (datum.getLatitude()!=null&&
                    datum.getLongitude()!=null){
                Uri gmmIntentUri = Uri.parse("geo:<" + datum.getLatitude() + ">,<" +
                        datum.getLongitude() + ">?q=<" +
                        datum.getLatitude() + ">,<" +
                        datum.getLongitude() + ">(" +
                        datum.getName() + ")");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
            else {
                Toast.makeText(this, R.string.locate_detailsnot, Toast.LENGTH_SHORT).show();
            }

        }
        if (v==img_phn_num){
            if (datum.getPhone()!=null&&!
                    datum.getPhone().equals("")){
                Uri u = Uri.parse("tel:" + datum.getPhone().trim());
                Intent i = new Intent(Intent.ACTION_DIAL, u);
                startActivity(i);
            }
            else {
                Toast.makeText(this, R.string.no_phone_num, Toast.LENGTH_SHORT).show();
            }
        }

    }
}
