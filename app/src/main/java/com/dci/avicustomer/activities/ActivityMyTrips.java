package com.dci.avicustomer.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.avicustomer.R;
import com.dci.avicustomer.adapter.AdapterMytrips;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.ModelMyTrips;
import com.dci.avicustomer.models.ModelRefresh;
import com.dci.avicustomer.models.ModelShopList;
import com.dci.avicustomer.models.ModelUpcomingtrips;
import com.dci.avicustomer.retrofit.AVIAPI;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityMyTrips extends BaseActivity implements View.OnClickListener, OnItemClickListener {

    @Inject
    public AVIAPI aviapi;
    @BindView(R.id.recycle_my_trips)
    RecyclerView recycle_my_trips;
    @BindView(R.id.tv_nodata)
    TextView tv_nodata;
    @BindView(R.id.img_add_icon)
    ImageView img_add_icon;
    AdapterMytrips adapterMytrips;
    List<ModelMyTrips.MyTrip>list=new ArrayList<>();
    int clickedpos=-1;
    boolean isDatasPresent=false;
    boolean isRefreshCalling=false;
    String tripid;
    AlertDialog clickhere_alert;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        ShoppingApplication.getContext().getComponent().inject(this);
        tv_title.setText(R.string.title_my_trip);
        img_add_icon.setOnClickListener(this);
        tv_clickhere.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (clickedpos!=-1&&tripid!=null){
            //shwProgress();
           // refershTrip(tripid,clickedpos);
            tripid=null;
            getMyTrip();

            //getLatestTrip();
        }
        else {
            getMyTrip();
        }

    }




    public void refershTrip(String tripId,int pos) {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityMyTrips.this,
                    R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;

        }
       // shwProgress();
        //  Log.d("aviparms", "search: "+UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)+search);
        String userid = "0";
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
            userid = UtilsDefault.getSharedPreferenceValue(Constants.USER_ID);
        }
        Log.d("currentTime", "getMyTrip: "+tripId);
        aviapi.refreshTrip(tripId,UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)).enqueue(new Callback<ModelMyTrips>() {
            @Override
            public void onResponse(Call<ModelMyTrips> call, Response<ModelMyTrips> response) {
                hideprogress();
                isRefreshCalling=false;
                hideKeyboard();
                if (response.body() != null) {
                    ModelMyTrips data = response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus() == Constants.STATUS_200) {
                        list.set(pos,data.getData().getMyTrips().get(0));
                        adapterMytrips.notifyItemChanged(pos);
                        getLatestTrip();

                        //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                    } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                        refershTrip(tripId,pos);
                    }
                    else if (data.getStatus()==Constants.STATUS_500){
                        list.remove(clickedpos);
                        if (list.size()==0){
                            tv_nodata.setVisibility(View.VISIBLE);
                            recycle_my_trips.setVisibility(View.GONE);
                            return;

                        }

                        adapterMytrips.notifyItemRemoved(clickedpos);
                        getLatestTrip();
                    }

                    else {
                        getLatestTrip();
                        recycle_my_trips.setVisibility(View.GONE);
                        tv_nodata.setVisibility(View.VISIBLE);
                        //alertcommon(getString(R.string.title_my_trip),data.getMessage());
                        //Toast.makeText(ActivityMyTrips.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
//                    recycle_my_trips.setVisibility(View.GONE);
//                    tv_nodata.setVisibility(View.VISIBLE);
                    alertcommon(getString(R.string.title_my_trip),getString(R.string.server_error));

                    //   Toast.makeText(ActivityMyTrips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ModelMyTrips> call, Throwable t) {
                isRefreshCalling=false;
                hideprogress();
                hideKeyboard();
//                recycle_my_trips.setVisibility(View.GONE);
//                tv_nodata.setVisibility(View.VISIBLE);
                alertcommon(getString(R.string.title_my_trip),
                        getString(R.string.server_error));
                Log.d("failure", "onFailure: " + t.getMessage());
                //Toast.makeText(ActivityMyTrips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void getLatestTrip() {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityMyTrips.this, R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;
        }
        shwProgress();
        //  Log.d("aviparms", "search: "+UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)+search);
        String userid = "0";
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
            userid = UtilsDefault.getSharedPreferenceValue(Constants.USER_ID);
        }
        Log.d("currentTime", "getMyTrip: "+UtilsDefault.getCurrentdateandTime());
        aviapi.getlatestTrip(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID),
                UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID),timeZone.getID()).enqueue(new Callback<ModelUpcomingtrips>() {
            @Override
            public void onResponse(Call<ModelUpcomingtrips> call, Response<ModelUpcomingtrips> response) {
                hideprogress();
                hideKeyboard();
                if (response.body() != null) {
                    ModelUpcomingtrips data = response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus() == Constants.STATUS_200) {
                        UtilsDefault.updateSharedPreference(Constants.AIRPORT_DOMESTIC_TYPE,
                                data.getUpTripTimeType());
                        UtilsDefault.updateSharedPreference(Constants.LATEST_DATE,data.getUpTripTime());
                        countDownStart();

                        //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                    } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                        getLatestTrip();
                    } else {
                       // recycle_my_trips.setVisibility(View.GONE);
                       // tv_nodata.setVisibility(View.VISIBLE);
                       // alertcommon(getString(R.string.title_my_trip),data.getMessage());
                        UtilsDefault.updateSharedPreference(Constants.LATEST_DATE,null);
                        countDownStart();

                        //Toast.makeText(ActivityMyTrips.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                  //  recycle_my_trips.setVisibility(View.GONE);
                  //  tv_nodata.setVisibility(View.VISIBLE);
                 //   alertcommon(getString(R.string.title_my_trip),getString(R.string.server_error));

                    //   Toast.makeText(ActivityMyTrips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ModelUpcomingtrips> call, Throwable t) {
                hideprogress();
                hideKeyboard();
                //recycle_my_trips.setVisibility(View.GONE);
               // tv_nodata.setVisibility(View.VISIBLE);
               // alertcommon(getString(R.string.title_my_trip),getString(R.string.server_error));
                Log.d("failure", "onFailure: " + t.getMessage());
                //Toast.makeText(ActivityMyTrips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getMyTrip() {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityMyTrips.this, R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;

        }
        shwProgress();
        //  Log.d("aviparms", "search: "+UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)+search);
        String userid = "0";
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
            userid = UtilsDefault.getSharedPreferenceValue(Constants.USER_ID);
        }
        Log.d("currentTime", "getMyTrip: "+UtilsDefault.getCurrentdateandTime());
        aviapi.getMyTrips(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID),
                UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID),
                UtilsDefault.getCurrentdateandTime(),timeZone.getID()).enqueue(new Callback<ModelMyTrips>() {


            @Override
            public void onResponse(Call<ModelMyTrips> call, Response<ModelMyTrips> response) {
                hideprogress();
                hideKeyboard();
                if (response.body() != null) {
                    ModelMyTrips data = response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus() == Constants.STATUS_200) {
                        if (data.getData().getMyTrips()!=null&&data.getData().getMyTrips().size()!=0){
                            list.clear();
                            list.addAll(data.getData().getMyTrips());
                            adapterMytrips=new AdapterMytrips(ActivityMyTrips.this,list);
                            adapterMytrips.setOnItemClick(ActivityMyTrips.this);
                            recycle_my_trips.setLayoutManager(new LinearLayoutManager(
                                    ActivityMyTrips.this));
                            recycle_my_trips.setAdapter(adapterMytrips);
                            recycle_my_trips.setVisibility(View.VISIBLE);
                            tv_nodata.setVisibility(View.GONE);
//                            if (clickedpos!=-1){
//
//                               getLatestTrip();
//                            }


                        }

                        else {
                            recycle_my_trips.setVisibility(View.GONE);
                            tv_nodata.setVisibility(View.VISIBLE);
                            if (list.size()==0){
                                emptyUpcomingTrips();
                            }

                          //  alertcommon(getString(R.string.title_my_trip),getString(R.string.nodata));
                        }
                        if (list.size()==0){
                            isDatasPresent=true;
                        }

                     
                        //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                    } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                       getMyTrip();
                    } else {
                        recycle_my_trips.setVisibility(View.GONE);
                        tv_nodata.setVisibility(View.VISIBLE);
                        tv_nodata.setText(data.getMessage());
                        if (list.size()==0){
                            emptyUpcomingTrips();
                        }
                       // alertcommon(getString(R.string.title_my_trip),data.getMessage());
                        //Toast.makeText(ActivityMyTrips.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    recycle_my_trips.setVisibility(View.GONE);
                    tv_nodata.setVisibility(View.VISIBLE);
                    tv_nodata.setText(getString(R.string.server_error));
                   // tv_nodata.setText(data.getMessage());
                   // alertcommon(getString(R.string.title_my_trip),getString(R.string.server_error));

                 //   Toast.makeText(ActivityMyTrips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ModelMyTrips> call, Throwable t) {
                hideprogress();
                hideKeyboard();
                recycle_my_trips.setVisibility(View.GONE);
                tv_nodata.setVisibility(View.VISIBLE);
                tv_nodata.setText(getString(R.string.server_error));
              //  alertcommon(getString(R.string.title_my_trip),getString(R.string.server_error));
                Log.d("failure", "onFailure: " + t.getMessage());
                //Toast.makeText(ActivityMyTrips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void emptyUpcomingTrips(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setCancelable(true);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_common_alert_dialog, null);
        alertDialogBuilder.setView(dialogView);
        TextView titletv = dialogView.findViewById(R.id.tv_alert_title);
        titletv.setText(getString(R.string.sidemenu_mytrips));
        TextView tv_content = dialogView.findViewById(R.id.tv_content);
        tv_content.setText(getString(R.string.trip_empty));
        btn_ok = dialogView.findViewById(R.id.btn_ok);
        btn_ok.setText(getString(R.string.add_trip));

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickhere_alert!=null){
                    clickhere_alert.cancel();
                }
                //startActivity(new Intent(ActivityMyTrips.this,ActivityAddTrip.class));
                startActivity(new Intent(ActivityMyTrips.this,ActivityUpdatedAddTrip.class));
            }
        });


        clickhere_alert = alertDialogBuilder.create();
        clickhere_alert.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        clickhere_alert.show();
    }
    @Override
    protected int getlayout() {
        return R.layout.activity_my_trips;
    }

    @Override
    public void onClick(View v) {
        if (v==img_add_icon){
           // startActivity(new Intent(ActivityMyTrips.this,ActivityAddTrip.class));
            startActivity(new Intent(ActivityMyTrips.this,ActivityUpdatedAddTrip.class));

        }
        if (v==tv_clickhere){
            startActivity(new Intent(ActivityMyTrips.this,ActivityUpdatedAddTrip.class));
          //  startActivity(new Intent(ActivityMyTrips.this,ActivityAddTrip.class));
        }
    }

    @Override
    public void onBackPressed() {
        if (isRefreshCalling){
            Toast.makeText(this, R.string.please_wait, Toast.LENGTH_SHORT).show();
        }
        else {
            finish();
        }
    }

    @Override
    public void OnItemClickListener(int pos, View view) {
        tripid=String.valueOf(list.get(pos).getId());
        clickedpos=pos;
        if (view.getId()==R.id.img_refresh){
            if (!isRefreshCalling){
                isRefreshCalling=true;
                tripid=String.valueOf(list.get(pos).getId());
                clickedpos=pos;
                refershTrip(tripid,pos);
            }
            else {
                Toast.makeText(this, R.string.please_wait, Toast.LENGTH_SHORT).show();
            }
        }
        else {
            Intent intent=new Intent(this, ActivityMyTripDetails.class);
            intent.putExtra("trip_id",String.valueOf(list.get(pos).getId()));
            startActivity(intent);
        }


    }

}
