package com.dci.avicustomer.activities;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.avicustomer.R;
import com.dci.avicustomer.adapter.AdapterFilterTerminalList;
import com.dci.avicustomer.adapter.AdapterShopList;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.PaginationScrollListener;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.ModelShopList;
import com.dci.avicustomer.retrofit.AVIAPI;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityShopList extends BaseActivity implements View.OnClickListener, OnItemClickListener {

    private static final long SEARCH_DELAY = 600;
    @Inject
    public AVIAPI aviapi;
    int pagenum = 1;
    @BindView(R.id.recycle_shoplist)
    RecyclerView recycle_shoplist;
    @BindView(R.id.ed_search)
    EditText ed_search;
    @BindView(R.id.btn_filter)
    RelativeLayout btn_filter;
    String airport_type = "";
    String airport_area = "";
    List<ModelShopList.Terminal> terminalList = new ArrayList<>();
    List<ModelShopList.Datum> datalist = new ArrayList<>();
    int terminalSelectedPos = -1;
    String terminalid = "";
    String searchKey = "";
    boolean isFilterApply = false;
    Dialog mBottomSheetDialog;
    AdapterShopList adapterRecycler;
    private Timer timer;
    String imgbaseurl="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        ShoppingApplication.getContext().getComponent().inject(this);
        tv_title.setText(R.string.title_shop);
        intizialize();
    }

    @Override
    protected int getlayout() {
        return R.layout.activity_shoplist;
    }

    public void intizialize() {
        btn_filter.setOnClickListener(this);
        ed_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (timer != null) {
                    timer.cancel();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                if (s.toString().equals("")) {
                                    cleardata();
                                    searchKey = "";
                                    getShopList();
                                    // search();
//                                        getData(String.valueOf(pagenum), false);
                                } else {
                                    cleardata();
                                    searchKey = s.toString();
                                    getShopList();

                                }


                            }
                        });
                        // getData(String.valueOf(pagenum), false);


                    }
                }, SEARCH_DELAY);


            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ActivityShopList.this);
        recycle_shoplist.setLayoutManager(linearLayoutManager);
        recycle_shoplist.setHasFixedSize(true);
        adapterRecycler = new AdapterShopList(ActivityShopList.this,
                datalist, 1);
        recycle_shoplist.setAdapter(adapterRecycler);
        recycle_shoplist.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyboard();
                return false;
            }
        });

        recycle_shoplist.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                if (datalist.size() >= Constants.NEW_PAGE_SIZE) {
                    pagenum++;
                    getShopList();

                }

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isloading;
            }
        });


        getShopList();
    }
    public String getBaseUrl(){
        return imgbaseurl;
    }

    public void getShopList() {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityShopList.this, R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;

        }
        shwProgress();
        //  Log.d("aviparms", "search: "+UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)+search);
        String userid = "0";
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
            userid = UtilsDefault.getSharedPreferenceValue(Constants.USER_ID);
        }
        Log.d("shopsInput", "getShopList: " + UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID) + "-page-" + pagenum + "-searchkey-" + searchKey + "-Tid-" + terminalid + "-ai-" + airport_type + "-aa-" + airport_area);
        aviapi.shoplist(UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID), pagenum,
                searchKey, UtilsDefault.getSharedPreferenceValue(Constants.TERMINAL_ID), airport_type,
                UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_TYPE),Constants.NEW_PAGE_SIZE).
                enqueue(new Callback<ModelShopList>() {

            @Override
            public void onResponse(Call<ModelShopList> call,
                                   Response<ModelShopList> response) {
                hideprogress();
              //  hideKeyboard();
                if (response.body() != null) {
                    ModelShopList data = response.body();
                    if (datalist.size() == totalcount) {
                        isLastPage = true;
                    }
//                    String header = response.headers().get(getString(R.string.header_find_key));
//                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus() == Constants.STATUS_200) {
                        totalcount= data.getTotal();
                        imgbaseurl=data.getUrl();
                        if (mBottomSheetDialog != null) {
                            mBottomSheetDialog.dismiss();
                        }
                        if (data.getData() == null && data.getData().size() == 0) {
                            if (datalist.size()==0){
                                alertcommon(getString(R.string.title_eat_drink),
                                        getString(R.string.nodata));
                            }

                            //alertcommon(getString(R.string.title_shop), getString(R.string.nodata));
                        } else {
                            datalist.addAll(data.getData());
                            adapterRecycler.notifyDataSetChanged();
                            if (datalist.size() == data.getTotal()) {
                                isLastPage = true;
                            }


//                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ActivityShopList.this);
//                            recycle_shoplist.setLayoutManager(linearLayoutManager);
//                            recycle_shoplist.setHasFixedSize(true);
//                            AdapterShopList adapterRecycler = new AdapterShopList(ActivityShopList.this,
//                                    data.getData(),1);
//                            recycle_shoplist.setAdapter(adapterRecycler);
                        }
                        if (data.getTerminals() != null && data.getTerminals().size() != 0) {
                            terminalList.clear();
                            terminalList.addAll(data.getTerminals());
                            if (pagenum != 1)
                                pagenum--;
                        }
                        //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                    } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                        if (pagenum != 1)
                            pagenum--;
                        getShopList();
                    } else {
                        if (pagenum != 1)
                            pagenum--;
                        alertcommon(getString(R.string.title_shop), data.getMessage());

                        //Toast.makeText(ActivityShopList.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (pagenum != 1)
                        pagenum--;
                    alertcommon(getString(R.string.title_shop), getString(R.string.server_error));
                  //  Toast.makeText(ActivityShopList.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ModelShopList> call, Throwable t) {
                if (pagenum != 1)
                    pagenum--;
                hideprogress();
              //  hideKeyboard();
                alertcommon(getString(R.string.title_shop), getString(R.string.server_error));
                Log.d("failure", "onFailure: " + t.getMessage());
                Toast.makeText(ActivityShopList.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void cleardata() {
        datalist.clear();
        adapterRecycler.notifyDataSetChanged();
        pagenum = 1;
        isLastPage = false;
        searchKey = "";
    }

    public void showBottomFilterList() {
        mBottomSheetDialog = new Dialog(this, R.style.MaterialDialogSheet);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_bottom_filter_shops, null);
        TextView tv_rest = dialogView.findViewById(R.id.tv_rest);
        TextView tv_apply = dialogView.findViewById(R.id.tv_apply);
        TextView tv_domestic = dialogView.findViewById(R.id.tv_domestic);
        TextView tv_international = dialogView.findViewById(R.id.tv_international);
        TextView tv_depature = dialogView.findViewById(R.id.tv_depature);
        TextView tv_arrival = dialogView.findViewById(R.id.tv_arrival);
        RecyclerView recycle_terminal = dialogView.findViewById(R.id.recycle_terminal);
        recycle_terminal.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        AdapterFilterTerminalList adapterFilterTerminalList = new AdapterFilterTerminalList(this, terminalList, terminalSelectedPos);
        adapterFilterTerminalList.setonItemClick(this);
        recycle_terminal.setAdapter(adapterFilterTerminalList);
        tv_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isFilterApply = true;
                cleardata();
                getShopList();
            }
        });
        tv_rest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                airport_area = "";
                airport_type = "";
                terminalid = "";
                terminalSelectedPos = -1;

                adapterFilterTerminalList.updateRowIndex(-1);
                tv_depature.setBackground(getDrawable(R.drawable.shop_filter_unselect));
                tv_depature.setTextColor(getResources().getColor(R.color.filter_grey));
                tv_arrival.setBackground(getDrawable(R.drawable.shop_filter_unselect));
                tv_arrival.setTextColor(getResources().getColor(R.color.filter_grey));
                tv_domestic.setBackground(getDrawable(R.drawable.shop_filter_unselect));
                tv_domestic.setTextColor(getResources().getColor(R.color.filter_grey));
                tv_international.setBackground(getDrawable(R.drawable.shop_filter_unselect));
                tv_international.setTextColor(getResources().getColor(R.color.filter_grey));
                if (isFilterApply) {
                    cleardata();
                    getShopList();
                    mBottomSheetDialog.dismiss();
                } else {
                    mBottomSheetDialog.dismiss();
                }
                isFilterApply = false;
            }
        });
        if (isFilterApply) {
            if (airport_area.equals("1")) {
                tv_depature.setBackground(getDrawable(R.drawable.shop_filter_select));
                tv_depature.setTextColor(getResources().getColor(R.color.white));
            } else if (airport_area.equals("2")) {
                tv_arrival.setBackground(getDrawable(R.drawable.shop_filter_select));
                tv_arrival.setTextColor(getResources().getColor(R.color.white));
            }
            if (airport_type.equals("1")) {
                tv_domestic.setBackground(getDrawable(R.drawable.shop_filter_select));
                tv_domestic.setTextColor(getResources().getColor(R.color.white));
            } else if (airport_type.equals("2")) {
                tv_international.setBackground(getDrawable(R.drawable.shop_filter_select));
                tv_international.setTextColor(getResources().getColor(R.color.white));
            }
        }


        tv_international.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_domestic.setBackground(getDrawable(R.drawable.shop_filter_unselect));
                tv_domestic.setTextColor(getResources().getColor(R.color.filter_grey));
                airport_type = "";
                changeSelectedBg(v);

            }
        });
        tv_depature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_arrival.setBackground(getDrawable(R.drawable.shop_filter_unselect));
                tv_arrival.setTextColor(getResources().getColor(R.color.filter_grey));
                airport_area = "";
                changeSelectedBg(v);

            }
        });

        tv_domestic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_international.setBackground(getDrawable(R.drawable.shop_filter_unselect));
                tv_international.setTextColor(getResources().getColor(R.color.filter_grey));
                airport_type = "";
                changeSelectedBg(v);
            }
        });
        tv_arrival.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_depature.setBackground(getDrawable(R.drawable.shop_filter_unselect));
                tv_depature.setTextColor(getResources().getColor(R.color.filter_grey));
                airport_area = "";
                changeSelectedBg(v);
            }
        });

        mBottomSheetDialog.setContentView(dialogView); // your custom view.
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();
    }

    public void changeSelectedBg(View view) {

        if (((TextView) view).getCurrentTextColor() == getResources().getColor(R.color.white)) {
            view.setBackground(getDrawable(R.drawable.shop_filter_unselect));
            ((TextView) view).setTextColor(getResources().getColor(R.color.filter_grey));
            if (view.getId() == R.id.tv_domestic) {
                airport_type = "";

            }
            if (view.getId() == R.id.tv_international) {
                airport_type = "";
            }
            if (view.getId() == R.id.tv_depature) {
                airport_area = "";

            }
            if (view.getId() == R.id.tv_arrival) {
                airport_area = "";

            }
            Log.d("log", "onClick: " + "unselect");
        } else {
            view.setBackground(getDrawable(R.drawable.shop_filter_select));
            ((TextView) view).setTextColor(getResources().getColor(R.color.white));
            if (view.getId() == R.id.tv_domestic) {
                airport_type = "1";

            }
            if (view.getId() == R.id.tv_international) {
                airport_type = "2";
            }
            if (view.getId() == R.id.tv_depature) {
                airport_area = "1";

            }
            if (view.getId() == R.id.tv_arrival) {
                airport_area = "2";

            }

        }
        Log.d("message", "onClick: " + "at" + airport_type + "aa" + airport_area);
    }

    @Override
    public void onClick(View v) {
        if (v == btn_filter) {
            showBottomFilterList();
        }

    }

    @Override
    public void OnItemClickListener(int pos, View view) {
        terminalSelectedPos = pos;
        terminalid = String.valueOf(terminalList.get(pos).getTerminal_id());


    }
}
