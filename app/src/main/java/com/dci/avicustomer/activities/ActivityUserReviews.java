package com.dci.avicustomer.activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.dci.avicustomer.R;
import com.dci.avicustomer.adapter.AdapterEatDrinks;
import com.dci.avicustomer.adapter.AdapterUserReviews;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.PaginationScrollListener;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.models.ModelEatAndDrink;
import com.dci.avicustomer.models.ModelUserReviews;
import com.dci.avicustomer.retrofit.AVIAPI;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityUserReviews extends BaseActivity {
    @BindView(R.id.recycle_reviews)
    RecyclerView recycle_reviews;
    AdapterUserReviews adapterRecycler;
    List<ModelUserReviews.Review> list = new ArrayList<>();
    int pagenum = 1;
    @Inject
    public AVIAPI aviapi;
    String rest_id = "";
    String imagebase = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        ShoppingApplication.getContext().getComponent().inject(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ActivityUserReviews.this);
        recycle_reviews.setLayoutManager(linearLayoutManager);
        recycle_reviews.setHasFixedSize(true);
        adapterRecycler = new AdapterUserReviews(ActivityUserReviews.this,
                list);
        recycle_reviews.setAdapter(adapterRecycler);
        recycle_reviews.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                if (list.size() >= Constants.NEW_PAGE_SIZE) {
                    pagenum++;
                    getUserReviews(rest_id);

                }

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isloading;
            }
        });
        tv_title.setText(getString(R.string.title_review));
        if (getIntent().getStringExtra("id") != null) {
            rest_id = getIntent().getStringExtra("id");
            getUserReviews(rest_id);
        }

    }
    public String getImageBase(){
        return imagebase;
    }

    public void getUserReviews(String rest_id) {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityUserReviews.this, R.
                    string.error_no_net, Toast.LENGTH_SHORT).show();
            return;
        }
        shwProgress();
        //  Log.d("aviparms", "search: "+UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)+search);
        String userid = "0";
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
            userid = UtilsDefault.getSharedPreferenceValue(Constants.USER_ID);
        }

        aviapi.getUserReviews(rest_id, pagenum).enqueue(new Callback<ModelUserReviews>() {
            @Override
            public void onResponse(Call<ModelUserReviews> call, Response<ModelUserReviews> response) {
                hideprogress();
                hideKeyboard();
                if (response.body() != null) {
                    ModelUserReviews data = response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (list.size()== totalcount) {
                        isLastPage = true;
                    }
                    if (data.getStatus() == Constants.STATUS_200) {
                        imagebase = data.getImagePath();
                        totalcount= data.getTotal();

                        if (data.getReview() == null || data.getReview().size() == 0) {
                            alertcommon(getString(R.string.title_review),
                                    getString(R.string.nodata));
//                            Toast.makeText(ActivityUserReviews.this,
//                                    data.getMessage(), Toast.LENGTH_SHORT).show();
                        } else {
                            list.addAll(data.getReview());
                            adapterRecycler.notifyDataSetChanged();
                            if (list.size() == data.getTotal()) {
                                isLastPage = true;
                            }
                            tv_title.setText(getString(R.string.title_review)+"("+data.getTotal()+")");


//                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ActivityShopList.this);
//                            recycle_shoplist.setLayoutManager(linearLayoutManager);
//                            recycle_shoplist.setHasFixedSize(true);
//                            AdapterShopList adapterRecycler = new AdapterShopList(ActivityShopList.this,
//                                    data.getData(),1);
//                            recycle_shoplist.setAdapter(adapterRecycler);
                        }
//                        if (data.getTerminals() != null && data.getTerminals().size() != 0) {
//                            terminalList.clear();
//                            terminalList.addAll(data.getTerminals());
//                            if (pagenum != 1)
//                                pagenum--;
//                        }
                        //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                    } else if (data.getStatus() ==
                            Constants.API_TOKENEXPIRY_STATUS) {
                        if (pagenum != 1)
                            pagenum--;
                        getUserReviews(rest_id);
                    } else {
                        if (pagenum != 1)
                            pagenum--;
                        alertcommon(getString(R.string.title_review), data.getMessage());
                        // Toast.makeText(ActivityEatAndDrink.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (pagenum != 1)
                        pagenum--;
                    alertcommon(getString(R.string.title_review), getString(R.string.server_error));
//                    Toast.makeText(ActivityEatAndDrink.this,
//                            R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ModelUserReviews> call, Throwable t) {
                if (pagenum != 1)
                    pagenum--;
                hideprogress();
                hideKeyboard();
                alertcommon(getString(R.string.title_review), getString(R.string.server_error));

                Log.d("failure", "onFailure: " + t.getMessage());
                // Toast.makeText(ActivityEatAndDrink.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected int getlayout() {
        return R.layout.activity_user_reviews;
    }
}
