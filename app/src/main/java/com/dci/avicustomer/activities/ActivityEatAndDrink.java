package com.dci.avicustomer.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.dci.avicustomer.R;
import com.dci.avicustomer.adapter.AdapterEatDrinks;
import com.dci.avicustomer.adapter.AdapterShopList;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.PaginationScrollListener;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.models.ModelEatAndDrink;
import com.dci.avicustomer.models.ModelShopList;
import com.dci.avicustomer.retrofit.AVIAPI;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityEatAndDrink extends BaseActivity
        implements View.OnClickListener {
    @BindView(R.id.recycle_restro_list)
    RecyclerView recycle_restro_list;
    @BindView(R.id.btn_filter)
    RelativeLayout btn_filter;
    @BindView(R.id.ed_search)
    EditText ed_search;
    String airport_type = "";
    String airport_area = "";
    List<ModelEatAndDrink.Terminal> terminalList = new ArrayList<>();
    List<ModelEatAndDrink.Datum> datalist = new ArrayList<>();
    int terminalSelectedPos = -1;
    String terminalid = "";
    String searchKey = "";
    boolean isFilterApply = false;
    Dialog mBottomSheetDialog;
    AdapterEatDrinks adapterRecycler;
    private Timer timer;
    String imgbaseurl="";
    private static final long SEARCH_DELAY = 600;
    @Inject
    public AVIAPI aviapi;
    int pagenum = 1;
    String type="";

    @Override
    public void onBackPressed() {
        if (type.equals("")){
            super.onBackPressed();
        }
        else {
            Intent intent=new Intent(ActivityEatAndDrink.this,ActivityHome.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        ShoppingApplication.getContext().
                getComponent().inject(this);
        tv_title.setText(getString(R.string.title_eat_drink));
        intizialize();
        if (getIntent().getStringExtra("type")!=null){
            type=getIntent().getStringExtra("type");
        }
    }
    public String getBaseUrl(){
        return imgbaseurl;
    }

    public void intizialize() {
        btn_filter.setOnClickListener(this);
        ed_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (timer != null) {
                    timer.cancel();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                if (s.toString().equals("")) {
                                    searchKey = "";
                                    cleardata();
                                    getEatDrink();
                                    // search();
//                                        getData(String.valueOf(pagenum), false);
                                } else {
                                    cleardata();
                                    searchKey = s.toString();
                                    getEatDrink();
                                }
                            }
                        });
                        // getData(String.valueOf(pagenum), false);
                    }
                }, SEARCH_DELAY);

            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ActivityEatAndDrink.this);
        recycle_restro_list.setLayoutManager(linearLayoutManager);
        recycle_restro_list.setHasFixedSize(true);
        adapterRecycler = new AdapterEatDrinks(ActivityEatAndDrink.this,
                datalist, 1);
        recycle_restro_list.setAdapter(adapterRecycler);
        recycle_restro_list.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                if (datalist.size() >= Constants.PAGE_SIZE) {
                    pagenum++;
                    getEatDrink();

                }

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isloading;
            }
        });
        recycle_restro_list.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyboard();
                return false;
            }
        });


        getEatDrink();
    }
    public void cleardata() {
        datalist.clear();
        adapterRecycler.notifyDataSetChanged();
        pagenum = 1;
        isLastPage = false;
        searchKey = "";
    }
    public void getEatDrink() {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityEatAndDrink.this, R.
                    string.error_no_net, Toast.LENGTH_SHORT).show();
            return;
        }
        shwProgress();
        //  Log.d("aviparms", "search: "+UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)+search);
        String userid = "0";
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
            userid = UtilsDefault.getSharedPreferenceValue(Constants.USER_ID);
        }
        Log.d("shopsInput", "getShopList: " + UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID)
                + "-page-" + pagenum + "-searchkey-" + searchKey + "-Tid-" +
                terminalid + "-ai-" + UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_TYPE) + "-aa-"
                + airport_area);
        aviapi.getEatAndDrink(UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID), pagenum, searchKey,
                UtilsDefault.getSharedPreferenceValue(Constants.TERMINAL_ID),
                airport_type, UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_TYPE),
                Constants.NEW_PAGE_SIZE).enqueue(new Callback<ModelEatAndDrink>() {
            @Override
            public void onResponse(Call<ModelEatAndDrink> call, Response<ModelEatAndDrink> response) {
                hideprogress();
               // hideKeyboard();
                if (response.body() != null) {
                    ModelEatAndDrink data = response.body();
                    if (datalist.size() == totalcount) {
                        isLastPage = true;
                    }
//                    String header = response.headers().get(getString(R.string.header_find_key));
//                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus() == Constants.STATUS_200) {
                        totalcount= data.getTotal();
                        imgbaseurl=data.getImagePath().getRestaurant();
                        if (mBottomSheetDialog != null) {
                            mBottomSheetDialog.dismiss();
                        }
                        if (data.getData() == null || data.getData().size() == 0) {
                            if (datalist.size()==0){
                                alertcommon(getString(R.string.title_eat_drink),
                                        getString(R.string.nodata));
                            }

//                            Toast.makeText(ActivityEatAndDrink.this,
//                                    data.getMessage(), Toast.LENGTH_SHORT).show();
                        } else {
                            datalist.addAll(data.getData());
                            adapterRecycler.notifyDataSetChanged();
                            if (datalist.size() == data.getTotal()) {
                                isLastPage = true;
                            }
//                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ActivityShopList.this);
//                            recycle_shoplist.setLayoutManager(linearLayoutManager);
//                            recycle_shoplist.setHasFixedSize(true);
//                            AdapterShopList adapterRecycler = new AdapterShopList(ActivityShopList.this,
//                                    data.getData(),1);
//                            recycle_shoplist.setAdapter(adapterRecycler);
                        }
//                        if (data.getTerminals() != null && data.getTerminals().size() != 0) {
//                            terminalList.clear();
//                            terminalList.addAll(data.getTerminals());
//                            if (pagenum != 1)
//                                pagenum--;
//                        }
                        //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                    } else if (data.getStatus() ==
                            Constants.API_TOKENEXPIRY_STATUS) {
                        if (pagenum != 1)
                            pagenum--;
                        getEatDrink();
                    } else {
                        if (pagenum != 1)
                            pagenum--;
                        alertcommon(getString(R.string.title_eat_drink),data.getMessage());


                       // Toast.makeText(ActivityEatAndDrink.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (pagenum != 1)
                        pagenum--;
                    alertcommon(getString(R.string.title_eat_drink),getString(R.string.server_error));
//                    Toast.makeText(ActivityEatAndDrink.this,
//                            R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ModelEatAndDrink> call, Throwable t) {
                if (pagenum != 1)
                    pagenum--;
                hideprogress();
               // hideKeyboard();
                alertcommon(getString(R.string.title_eat_drink),getString(R.string.server_error));

                Log.d("failure", "onFailure: " + t.getMessage());
               // Toast.makeText(ActivityEatAndDrink.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }



    @Override
    protected int getlayout() {
        return R.layout.activity_eat_drink;
    }

    @Override
    public void onClick(View v) {

    }
}
