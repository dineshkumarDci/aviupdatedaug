package com.dci.avicustomer.activities;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.dci.avicustomer.R;
import com.dci.avicustomer.adapter.ViewPagerAdapter;
import com.dci.avicustomer.fragments.FragmentMyCompletedTrips;
import com.dci.avicustomer.fragments.FragmentMyOrders;
import com.dci.avicustomer.fragments.FragmentMyProfile;
import com.dci.avicustomer.fragments.FragmentOfferList;
import com.dci.avicustomer.helpers.FCViewPager;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityMyProfile extends ImagePickerActivity {

    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.test)
    ImageView test;
    @BindView(R.id.viewpager)
    FCViewPager viewpager;
    FragmentMyProfile fragmentMyProfile=new FragmentMyProfile();
    int tabPos=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        setupViewPager(viewpager);
        tabs.setupWithViewPager(viewpager);
        viewpager.setEnableSwipe(false);

        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                //viewpager.setCurrentItem(tab.getPosition());
                tabPos=tab.getPosition();
                if (tab.getPosition()==0){
                    tv_title.setText(getString(R.string.title_my_profile));
                }
                else if (tab.getPosition()==1){
                    tv_title.setText(getString(R.string.title_my_order));
                }
                else if (tab.getPosition()==2){
                    tv_title.setText(getString(R.string.title_my_completed_trips));
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        tv_title.setText(getString(R.string.title_my_profile));
        tabs.getTabAt(0).select();
        viewpager.setCurrentItem(0);

        if (getIntent().getStringExtra("type")!=null){
            tabs.getTabAt(1).select();
            viewpager.setCurrentItem(1);
        }


    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        Fragment fragmentMyProfile=new FragmentMyProfile();

        adapter.addFragment(fragmentMyProfile, getString(R.string.title_my_profile));

        Fragment fragmentMyOrders=new FragmentMyOrders();

        adapter.addFragment(fragmentMyOrders, getString(R.string.title_my_order));

        Fragment fragmentMyCompletedTrips=new FragmentMyCompletedTrips();

        adapter.addFragment(fragmentMyCompletedTrips, getString(R.string.title_my_completed_trips));
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        if (tabPos==0){
            Fragment page = getSupportFragmentManager().findFragmentByTag("android:switcher:" +
                    R.id.viewpager + ":" +
                    viewpager.getCurrentItem());
            if ( ((FragmentMyProfile)page).getServiceCalling()){

            }
            else {
                super.onBackPressed();
            }

        }
        else {
            super.onBackPressed();
        }
    }

    @Override
    protected int getlayout() {
        return R.layout.activity_my_profile;
    }

    @Override
    protected void onImageSelect(String Base64, Bitmap bmp, Uri uri, File file) {
        Log.d("images", "onImageSelect: "+uri+"bmp"+bmp);

        Fragment page = getSupportFragmentManager().findFragmentByTag("android:switcher:" +
                R.id.viewpager + ":" +
                viewpager.getCurrentItem());
        test.setImageBitmap(bmp);
        // based on the current position you can then cast the page to the correct
        // class and call the method:
        if (viewpager.getCurrentItem() == 0
                && page != null) {
        }
      //  Toast.makeText(this, "Called", Toast.LENGTH_SHORT).show();

        try {

            ((FragmentMyProfile)page).SetProfileImage(uri,bmp,file);

        }
        catch (Exception e){

        }



    }


}
