package com.dci.avicustomer.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.avicustomer.R;
import com.dci.avicustomer.adapter.AdapterMyDocsDetails;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.models.ModelDocumentDetailsList;
import com.dci.avicustomer.retrofit.AVIAPI;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityMyDocument extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.card_boarding)
    RelativeLayout card_boarding;
    @BindView(R.id.card_personaldocs)
    RelativeLayout card_personaldocs;
    @BindView(R.id.card_visa)
    RelativeLayout card_visa;
    @Inject
    public AVIAPI aviapi;
    @BindView(R.id.tv_Personalcount)
    TextView tv_Personalcount;
    @BindView(R.id.tv_boardingcount)
    TextView tv_boardingcount;
    @BindView(R.id.tv_visacount)
    TextView tv_visacount;
    @BindView(R.id.tv_eticketcount)
    TextView tv_eticketcount;
    @BindView(R.id.card_e_docs)
    RelativeLayout card_e_docs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        ShoppingApplication.getContext().getComponent().inject(this);
        card_boarding.setOnClickListener(this);
        card_personaldocs.setOnClickListener(this);
        card_visa.setOnClickListener(this);
        card_e_docs.setOnClickListener(this);
        tv_title.setText(R.string.my_docs);

    }

    @Override
    protected void onResume() {
        super.onResume();
        getMyDocs();
    }

    public void getMyDocs() {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityMyDocument.this, R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;
        }
        shwProgress();
        //  Log.d("aviparms", "search: "+UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)+search);
        String userid = "0";
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
            userid = UtilsDefault.getSharedPreferenceValue(Constants.USER_ID);
        }
        aviapi.getMyDocs("1", UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)).
                enqueue(new Callback<ModelDocumentDetailsList>() {
            @Override
            public void onResponse(Call<ModelDocumentDetailsList> call, Response<ModelDocumentDetailsList> response) {
                hideprogress();
                hideKeyboard();
                if (response.body() != null) {
                    ModelDocumentDetailsList data = response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus() == Constants.STATUS_200) {
                        if (data.getCount().getBoardPass()!=null){
                            tv_boardingcount.setText(String.valueOf(data.getCount().getBoardPass())+" "+getString(R.string.document));
                        }
                        if (data.getCount().getPresDoce()!=null){
                            tv_Personalcount.setText(String.valueOf(data.getCount().getPresDoce())+" "+getString(R.string.document));
                        }
                        if (data.getCount().getVisaDoce()!=null){
                            tv_visacount.setText(String.valueOf(data.getCount().getVisaDoce())+" "+getString(R.string.document));
                        }
                        if (data.getCount().getEticket()!=null){
                            tv_eticketcount.setText(String.valueOf(data.getCount().getEticket())+" "+getString(R.string.document));
                        }


                        //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                    } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                        getMyDocs();
                    } else {
                        
                        
                        //Toast.makeText(ActivityMyTrips.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                 

                    //   Toast.makeText(ActivityMyTrips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ModelDocumentDetailsList> call, Throwable t) {
             
                hideprogress();
                hideKeyboard();
               
                Log.d("failure", "onFailure: " + t.getMessage());
                //Toast.makeText(ActivityMyTrips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    protected int getlayout() {
        return R.layout.activity_mydocuments;
    }

    @Override
    public void onClick(View v) {
        if (v==card_boarding){
            Intent intent=new Intent(ActivityMyDocument.this,ActivityMyDocsDetails.class);
            intent.putExtra("section",Constants.SECTION_BOARDING);
            intent.putExtra("title",getString(R.string.boarding_pass));
            startActivity(intent);
        }
        if (v==card_personaldocs){
            Intent intent=new Intent(ActivityMyDocument.this,ActivityMyDocsDetails.class);
            intent.putExtra("section",Constants.SECTION_PERSONAL);
            intent.putExtra("title",getString(R.string.personal_documents));
            startActivity(intent);
        }
        if (v==card_visa){
            Intent intent=new Intent(ActivityMyDocument.this,ActivityMyDocsDetails.class);
            intent.putExtra("section",Constants.SECTION_VISA);
            intent.putExtra("title",getString(R.string.visa_documents));
            startActivity(intent);

        }
        if (v==card_e_docs){
            Intent intent=new Intent(ActivityMyDocument.this,ActivityMyDocsDetails.class);
            intent.putExtra("section",Constants.SECTION_ETICKET);
            intent.putExtra("title",getString(R.string.etickets_documents));
            startActivity(intent);

        }


    }
}
