package com.dci.avicustomer.activities;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dci.avicustomer.R;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.models.ModelAviTips;
import com.dci.avicustomer.models.ModelLatestNews;
import com.dci.avicustomer.retrofit.AVIAPI;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityLatestNews extends BaseActivity implements View.OnClickListener {
    @Inject
    public AVIAPI aviapi;
    @BindView(R.id.left_arrow)
    ImageView left_arrow;
    @BindView(R.id.img_news)
    ImageView img_news;
    @BindView(R.id.rightarrow)
    ImageView rightarrow;
    @BindView(R.id.tv_news_title)
    TextView tv_news_title;
    @BindView(R.id.tv_news_date)
    TextView tv_news_date;
    @BindView(R.id.tv_news_content)
    WebView tv_news_content;
    List<ModelLatestNews.Datum>list=new ArrayList<>();
    int postion=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        ShoppingApplication.getContext().getComponent().inject(this);
        left_arrow.setOnClickListener(this);
        rightarrow.setOnClickListener(this);
        imgback.setOnClickListener(this);
        WebSettings webSettings = tv_news_content.getSettings();
        webSettings.setJavaScriptEnabled(true);
        if (getIntent().getStringExtra("news_id")!=null){
            getLatestNews(getIntent().getStringExtra("news_id"));

        }
        tv_title.setText(R.string.title_latest_news);


    }
    @Override
    protected int getlayout() {
        return R.layout.activity_latest_news;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void getLatestNews(String newsid){
        if (!UtilsDefault.isOnline()){
            Toast.makeText(ActivityLatestNews.this, R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;

        }
        shwProgress();
        aviapi.latestNews(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID),UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID)).enqueue(new Callback<ModelLatestNews>() {
            @Override
            public void onResponse(Call<ModelLatestNews> call, Response<ModelLatestNews> response) {
                hideprogress();


                if (response.body()!=null){
                    ModelLatestNews data=response.body();
//                    String header = response.headers().get(getString(R.string.header_find_key));
//                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                   // UtilsDefault.updateSharedPreference(Constants.AUTH_TOKEN,data.getJwt_token());
                    if (data.getStatus()== Constants.STATUS_200){
                        if (data.getData()!=null&&data.getData().size()!=0){
                            list.clear();
                            list.addAll(data.getData());
                            showNews(newsid);
                        }


                        //   UtilsDefault.updateSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));



                    }
                    else {
                        Toast.makeText(ActivityLatestNews.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Toast.makeText(ActivityLatestNews.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ModelLatestNews> call, Throwable t) {
                hideprogress();
                Toast.makeText(ActivityLatestNews.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void showNews(String id){
        if (!id.equals("")){
            for (ModelLatestNews.Datum datum:list){
                if (String.valueOf(datum.getId()).equals(id)){
                    postion=list.indexOf(datum);
                }
            }
        }
        Glide.with(this).load(list.get(postion).getImage()).into(img_news);
        if (list.get(postion).getDescription()!=null&&!list.get(postion).getDescription().equals("")){
            tv_news_content.loadData(list.get(postion).getDescription(), "text/html; charset=utf-8", "base64");
        }
        //tv_news_content.setText(list.get(postion).getDescription());
        tv_news_title.setText(list.get(postion).getTitle());
        tv_news_date.setText(UtilsDefault.dateFormat(list.get(postion).getUpdated_at()));
    }

    @Override
    public void onClick(View v) {
        if (v==rightarrow){
            if (list.size() != 0 && postion != list.size()-1) {
                postion++;
                showNews("");
            }
        }
        if (v==left_arrow){
            if (list.size() != 0 && postion != 0) {
                postion--;
                showNews("");
            }
        }
        if (v==imgback){
            onBackPressed();
        }

    }
}
