package com.dci.avicustomer.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.dci.avicustomer.R;
import com.dci.avicustomer.adapter.AdapterTopOffer;
import com.dci.avicustomer.adapter.ViewPagerAdapter;
import com.dci.avicustomer.fragments.FragmentOfferList;
import com.dci.avicustomer.helpers.FCViewPager;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.models.ModelOfferList;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityOfferPage extends BaseActivity {
    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.viewpager)
    FCViewPager viewpager;
    @BindView(R.id.recycle_offer_top_lay)
    RecyclerView recycle_offer_top_lay;
    AdapterTopOffer adapterTopOffer;
    List<ModelOfferList.TopOffer>topList=new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        setupViewPager(viewpager);
        tabs.setupWithViewPager(viewpager);
        viewpager.setEnableSwipe(false);
        tv_title.setText(R.string.title_offer);
        adapterTopOffer=new AdapterTopOffer(ActivityOfferPage.this,topList);
        recycle_offer_top_lay.setAdapter(adapterTopOffer);
        recycle_offer_top_lay.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
    }
    public void UpdateTopOffer(List<ModelOfferList.TopOffer>topLists, String Restropath,
                               String couponPath, String shops){
        topList.clear();
        topList.addAll(topLists);
        adapterTopOffer.setImagePath(Restropath,couponPath,shops);
        adapterTopOffer.notifyDataSetChanged();
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        Fragment fragmentAll=new FragmentOfferList();
        Bundle bundle=new Bundle();
        bundle.putString("type","1");
        fragmentAll.setArguments(bundle);
        adapter.addFragment(fragmentAll, getString(R.string.title_alloffers));

        Fragment fragmenteats=new FragmentOfferList();
        Bundle bundleEats=new Bundle();
        bundleEats.putString("type","3");
        fragmenteats.setArguments(bundleEats);
        adapter.addFragment(fragmenteats, getString(R.string.title_eat_drink));

        Fragment fragmentshop=new FragmentOfferList();
        Bundle bundleshop=new Bundle();
        fragmentshop.setArguments(bundleshop);
        bundleshop.putString("type","2");
        adapter.addFragment(fragmentshop, getString(R.string.title_shop));
        viewPager.setAdapter(adapter);
    }


    @Override
    protected int getlayout() {
        return R.layout.activity_offers;
    }

}
