package com.dci.avicustomer.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.dci.avicustomer.R;
import com.dci.avicustomer.ccavenue.AvenuesParams;
import com.dci.avicustomer.ccavenue.ConstantsCCavenue;
import com.dci.avicustomer.ccavenue.RSAUtility;
import com.dci.avicustomer.ccavenue.ServiceHandler;
import com.dci.avicustomer.ccavenue.ServiceUtility;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.models.ModelAddFavourite;
import com.dci.avicustomer.retrofit.AVIAPI;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EncodingUtils;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
public class WebViewActivity extends BaseActivity {
    private ProgressDialog dialog;
    Intent mainIntent;
    String html, encVal;
    String amount;
    String vResponse;
    Boolean isreacharged=false;
    String eta_time="";
    @Inject
    public AVIAPI aviapi;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ShoppingApplication.getContext().getComponent().inject(this);
//        setTitles("Make your payment");
//        wallete.setVisibility(View.GONE);

        mainIntent = getIntent();
        amount = mainIntent.getStringExtra(AvenuesParams.AMOUNT);
        eta_time=mainIntent.getStringExtra(AvenuesParams.ETA_TIME);
        tv_title.setText(getString(R.string.title_payment));


        //get_RSA_key( mainIntent.getStringExtra(AvenuesParams.ACCESS_CODE),mainIntent.getStringExtra(AvenuesParams.ORDER_ID));


        // Calling async task to get display content
        new RenderView().execute();
      // alertThanks();
    }

    @Override
    public void alert(String msg) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(msg);
        alertDialogBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        dialog.dismiss();
                        finish();
                    }
                });
        alertDialogBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        dialog.dismiss();

                    }
                });

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        alert(getString(R.string.exit_payment));

    }

    @Override
    protected int getlayout() {
        return R.layout.activity_webview;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
           // showExitAlert();
        }
        return super.onOptionsItemSelected(item);
    }




//    public void saveamount(final String status) {
//        if (status.equals("Transaction Successful!")) {
//            MyApiService service = new MyApiService(WebViewActivity.this);
//            Map<String, String> map = new HashMap<>();
//            map.put("user_id", UtilsDefault.getSharedPreferenceValue(Constants.USER_ID));
//            map.put("w_type", "1");
//            map.put("w_amount", amount);
//            map.put("w_gateway", "1");
//
//            Call call = service.getApiCall(ApiMethod.POST, getString(R.string.SavewaleteParms), map);
//            service.execute(1, "Loading", call, SavewalletModel.class, new APIListener<SavewalletModel>() {
//                @Override
//                public void onSuccess(int from, Call call, Response response, SavewalletModel res) {
//                    Log.d("dinres", "onSuccess: " + res.getStatus());
//                    if (from == 1) {
//                        if (response.isSuccessful()) {
//                            SavewalletModel data = res;
//                            Utils.updateSharedPreference(Constants.wallet_amount, data.getTotalwallet());
//                            // hiderefresh();
//
//                            Log.d("apistst", "onSuccess: " + data.getStatus());
//                            if (Integer.parseInt(data.getStatus()) == 1) {
//
//                                Intent intent = new Intent(getApplicationContext(), StatusActivity.class);
//                                intent.putExtra("transStatus", status);
//                                startActivity(intent);
//
//
//                            } else {
//                                Toast.makeText(WebViewActivity.this, data.getMessage(), Toast.LENGTH_SHORT).show();
//                            }
//
//                        }
//
//                    }
//                    // Toast.makeText(this, "Sucess", Toast.LENGTH_SHORT).show();
//
//                }
//
//                @Override
//                public void onFailure(int from, Call call, Throwable t) {
//
//                    Toasty.error(WebViewActivity.this, getResources().getString(R.string.server_error)).show();
//
//                }
//
//                @Override
//                public void onNetworkFailure(int from) {
//
//                    Toasty.error(WebViewActivity.this, getResources().getString(R.string.no_net)).show();
//                }
//            });
//
//        } else {
//            Intent intent = new Intent(getApplicationContext(), StatusActivity.class);
//            intent.putExtra("transStatus", status);
//            startActivity(intent);
//
//
//        }
//
//    }

    public void get_RSA_key(final String ac, final String od) {
        if (!UtilsDefault.isOnline()){
            Toast.makeText(WebViewActivity.this, R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;

        }
        shwProgress();
//
//        CcavenueApiService service = new CcavenueApiService(WebViewActivity.this);
//        Map<String, String> map = new HashMap<>();
//        map.put(AvenuesParams.ACCESS_CODE,  ac);
//        map.put(AvenuesParams.ORDER_ID, od);
//
//        Call call = service.getApiCall(ApiMethod.POST, "sGetRSA", map);

        aviapi.getCCavenueRSA("1231").
                enqueue(new Callback<JSONObject>() {
                    @Override
                    public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                        hideprogress();
                        hideKeyboard();
                        vResponse = response.toString();
                        Log.d("dinres", "onSuccess: " + vResponse);
                        new RenderView().execute();

                    }

                    @Override
                    public void onFailure(Call<JSONObject> call, Throwable t) {
                        hideprogress();
                        hideKeyboard();
                        Toast.makeText(WebViewActivity.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                    }
                });



//        LoadingDialog.showLoadingDialog(WebViewActivity.this, "Loading...");
//
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, mainIntent.getStringExtra(AvenuesParams.RSA_KEY_URL),
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        //Toast.makeText(WebViewActivity.this,response,Toast.LENGTH_LONG).show();
//                        LoadingDialog.cancelLoading();
//                        vResponse = response;
//                        if (vResponse.contains("!ERROR!")) {
//                            show_alert(vResponse);
//                        } else {
//                            new RenderView().execute();
//                        }
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        LoadingDialog.cancelLoading();
//                        //Toast.makeText(WebViewActivity.this,error.toString(),Toast.LENGTH_LONG).show();
//                    }
//                }) {
//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put(AvenuesParams.ACCESS_CODE, ac);
//                params.put(AvenuesParams.ORDER_ID, od);
//                return params;
//            }
//
//        };
//        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
//                30000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        RequestQueue requestQueue = Volley.newRequestQueue(this);
//        requestQueue.add(stringRequest);
    }

    /**
     * Async task class to get json by making HTTP call
     */
    private class RenderView extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            dialog = new ProgressDialog(WebViewActivity.this, R.style.AppCompatAlertDialogStyle);
            dialog.setMessage("Please wait...");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            // Making a request to url and getting response
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(AvenuesParams.ACCESS_CODE, mainIntent.getStringExtra(AvenuesParams.ACCESS_CODE)));
            params.add(new BasicNameValuePair(AvenuesParams.ORDER_ID, mainIntent.getStringExtra(AvenuesParams.ORDER_ID)));

            String vResponse = sh.makeServiceCall(mainIntent.getStringExtra(AvenuesParams.RSA_KEY_URL),
                    ServiceHandler.POST, params);

         //   Log.d("sresd", "doInBackground: " + vResponse);
            System.out.println(vResponse);
            if (!ServiceUtility.chkNull(vResponse).equals("")
                    && ServiceUtility.chkNull(vResponse).toString().indexOf("ERROR") == -1) {
                try {
                    JSONObject jObject = new JSONObject(vResponse);
                    String aJsonString = jObject.getString("token");
                    StringBuffer vEncVal = new StringBuffer("");
                    vEncVal.append(ServiceUtility.addToPostParams(AvenuesParams.AMOUNT, mainIntent.getStringExtra(AvenuesParams.AMOUNT)));
                    vEncVal.append(ServiceUtility.addToPostParams(AvenuesParams.CURRENCY, mainIntent.getStringExtra(AvenuesParams.CURRENCY)));
                      Log.d("encrypt", "doInBackground: " + vEncVal);
                    encVal = RSAUtility.encrypt(vEncVal.substring(0, vEncVal.length() - 1), aJsonString);
                    Log.d("decrypt", "doInBackground: " + encVal);
                  //  Log.d("sresd", "doInBackground: " + encVal);
                } catch (Exception e) {


                    e.printStackTrace();
                }

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (dialog.isShowing())
                dialog.dismiss();

            @SuppressWarnings("unused")
            class MyJavaScriptInterface {
                @JavascriptInterface
                public void processHTML(String html) {
                    // process the html as needed by the app
                    Log.d("htmlls", "processHTML: "+html);
                    String status = null;
                    if (html.indexOf("Failure") != -1) {
                        status = "Transaction Declined!";
                        alertcommon("Payment","Your"+" Transaction Declined!");
                    } else if (html.indexOf("Success") != -1) {
                        status = "Transaction Successful!";
                        alertThanks();
                        // saveamount(status);
                        // return;
                    } else if (html.indexOf("Aborted") != -1) {
                        status = "Transaction Cancelled!";
                        alertcommon("Payment","Your"+" Transaction Cancelled!");
                    }  else {
                        status = "Status Not Known!";
                       // alertcommon("Payment",getString(R.string.server_error));

                    }

                   // saveamount(status);
                    //Toast.makeText(getApplicationContext(), status, Toast.LENGTH_SHORT).show();
//                    Intent intent = new Intent(getApplicationContext(), StatusActivity.class);
//                    intent.putExtra("transStatus", status);
//                    startActivity(intent);
                }
            }

            final WebView webview =  findViewById(R.id.webview);
            webview.getSettings().setJavaScriptEnabled(true);
            webview.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");
            webview.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(webview, url);
                    Log.d("htmlls", "processHTML: "+url);
                    if (url.indexOf("/ccavResponseHandler") != -1) {
                        webview.loadUrl("javascript:window." +
                                "HTMLOUT.processHTML('<head>'+document.getElementsByTagName('html')[0]." +
                                "innerHTML+'</head>');");
                    }
//                    if(url.indexOf("/ccavResponseHandler.jsp")!=-1){
//                        webview.loadUrl("javascript:window.HTMLOUT.processHTML
//                        ('<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>');");
//                    }
                }

                @Override
                public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                    Toast.makeText(getApplicationContext(), "Oh no! " + description, Toast.LENGTH_SHORT).show();
                }
            });
            try {
            /* An instance of this class will be registered as a JavaScript interface */
                StringBuffer params = new StringBuffer();
                params.append(ServiceUtility.addToPostParams(AvenuesParams.ACCESS_CODE, mainIntent.getStringExtra(AvenuesParams.ACCESS_CODE)));
                params.append(ServiceUtility.addToPostParams(AvenuesParams.MERCHANT_ID, mainIntent.getStringExtra(AvenuesParams.MERCHANT_ID)));
                params.append(ServiceUtility.addToPostParams(AvenuesParams.ORDER_ID, mainIntent.getStringExtra(AvenuesParams.ORDER_ID)));
                params.append(ServiceUtility.addToPostParams(AvenuesParams.REDIRECT_URL, mainIntent.getStringExtra(AvenuesParams.REDIRECT_URL)));
                params.append(ServiceUtility.addToPostParams(AvenuesParams.CANCEL_URL, mainIntent.getStringExtra(AvenuesParams.CANCEL_URL)));
                params.append(ServiceUtility.addToPostParams(AvenuesParams.ENC_VAL, URLEncoder.encode(encVal, "UTF-8")));
//                UtilsDefault.updateSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
//                UtilsDefault.updateSharedPreference(Constants.FIRST_NAME,data.getData().getFirst_name());
//                UtilsDefault.updateSharedPreference(Constants.LAST_NAME,data.getData().getLast_name());
//                UtilsDefault.updateSharedPreference(Constants.MOBILE_NUMBER,data.getData().getMobile());
//                UtilsDefault.updateSharedPreference(Constants.GENDER,data.getData().getGender());
//                UtilsDefault.updateSharedPreference(Constants.EMAILID,data.getData().getEmail());
//                UtilsDefault.updateSharedPreference(Constants.PROFILE_IMG,data.getData().getProfile_img());
                params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_TEL,
                        UtilsDefault.getSharedPreferenceValue(Constants.MOBILE_NUMBER)));
                params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_EMAIL,
                        UtilsDefault.getSharedPreferenceValue(Constants.EMAILID)));
                params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_NAME,
                        UtilsDefault.getSharedPreferenceValue(Constants.FIRST_NAME)));
//                params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_ADDRESS,"test"));
//                params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_CITY,"test"));
//                params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_STATE,"test"));
//                params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_ZIP,"625402"));
//               // params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_COUNTRY,"test"));
//                params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_TEL,"950078881"));
//                params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_EMAIL,"test@gmail.com"));
//                params.append(ServiceUtility.addToPostParams(AvenuesParams.DELIVERY_NAME,"test"));
//                params.append(ServiceUtility.addToPostParams(AvenuesParams.DELIVERY_ADDRESS,"test"));
//            //    params.append(ServiceUtility.addToPostParams(AvenuesParams.DELIVERY_CITY,"test"));
//              //  params.append(ServiceUtility.addToPostParams(AvenuesParams.DELIVERY_STATE,"test"));
//              //  params.append(ServiceUtility.addToPostParams(AvenuesParams.DELIVERY_ZIP,"625402"));
//               // params.append(ServiceUtility.addToPostParams(AvenuesParams.DELIVERY_COUNTRY,"test"));
//                params.append(ServiceUtility.addToPostParams(AvenuesParams.DELIVERY_TEL,"9500788899"));

                String vPostParams = params.substring(0, params.length() - 1);
                try {
                    webview.postUrl(ConstantsCCavenue.DEMO_URL,
                            EncodingUtils.getBytes(vPostParams,
                                    "UTF-8"));
                } catch (Exception e) {
                    showToast("Exception occured while opening webview.");
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(WebViewActivity.this, getString(R.string.server_error),
                        Toast.LENGTH_SHORT).show();
                finish();
            }


        }

    }

    public void alertThanks() {
        android.app.AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setCancelable(false);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_alert_thankyou_order, null);
        alertDialogBuilder.setView(dialogView);
        TextView titletv = dialogView.findViewById(R.id.tv_order_id);
        titletv.setText("Order Id #"+mainIntent.getStringExtra(AvenuesParams.ORDER_ID));
        TextView tv_content = dialogView.findViewById(R.id.tv_content);
        TextView ed_emailid=dialogView.findViewById(R.id.ed_emailid);
        TextView tv_estimate_time=dialogView.findViewById(R.id.tv_estimate_time);
        if (eta_time!=null&&!eta_time.equals("")){
            tv_estimate_time.setText("Estimated time:"+eta_time);

        }
        ed_emailid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commonalert.dismiss();
                Intent intent=new Intent(WebViewActivity.this,ActivityOrderTracking.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("type","1");
                intent.putExtra("id",mainIntent.getStringExtra(AvenuesParams.ORDER_ID));
                startActivity(intent);
                finish();
            }
        });
        RelativeLayout rel_track_order=dialogView.findViewById(R.id.rel_track_order);

        btn_ok = dialogView.findViewById(R.id.btn_submit);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeCommonAlert();
                Intent intent=new Intent(WebViewActivity.this,ActivityEatAndDrink.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("type","1");
                startActivity(intent);
                finish();
            }
        });
        rel_track_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commonalert.dismiss();
                Intent intent=new Intent(WebViewActivity.this,ActivityOrderTracking.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("type","1");
                intent.putExtra("id",mainIntent.getStringExtra(AvenuesParams.ORDER_ID));
                startActivity(intent);
                finish();
            }
        });


        commonalert = alertDialogBuilder.create();
        commonalert.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        commonalert.show();
    }

    public void showToast(String msg) {
        Toast.makeText(this, "Toast: " + msg, Toast.LENGTH_LONG).show();
    }
} 