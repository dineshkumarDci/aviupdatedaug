package com.dci.avicustomer.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.dci.avicustomer.R;
import com.dci.avicustomer.adapter.AdapterSearchAirlines;
import com.dci.avicustomer.adapter.AdapterSectionRecycler;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.ModelSearchAirline;
import com.dci.avicustomer.models.ModelSearchResult;
import com.dci.avicustomer.models.SectionedDataModel;
import com.dci.avicustomer.retrofit.AVIAPI;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityAirlineSearch extends BaseActivity implements View.OnClickListener {
    @Inject
    public AVIAPI aviapi;
    @BindView(R.id.ed_search)
    EditText ed_search;
    @BindView(R.id.search_recyler_view)
    RecyclerView search_recyler_view;
    @BindView(R.id.img_search)
    ImageView img_search;
    private Timer timer;
    String search = "";
    private static final long SEARCH_DELAY = 600;
    @BindView(R.id.img_blur)
    ImageView img_blur;
    @BindView(R.id.img_back)
    ImageView img_back;
    public String TAG = "ActivitySearch";

    @Override
    protected int getlayout() {
        return R.layout.activity_search;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        ShoppingApplication.getContext().getComponent().inject(this);
        img_blur.setVisibility(View.GONE);
        img_back.setOnClickListener(this);
        ed_search.setHint(getString(R.string.head_search_airline));
        ed_search.setOnClickListener(this);
        search_recyler_view.setVisibility(View.VISIBLE);
        img_search.setOnClickListener(this);
        ed_search.requestFocus();

        search_recyler_view.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == 0) {
                    hideKeyboard();
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                //  hideKeyboard();
            }
        });
        ed_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }


            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (timer != null) {
                    timer.cancel();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                if (s.toString().equals("")) {
                                    // shwProgress();
                                    search = "+";
                                    List<ModelSearchAirline.Datum> list = new ArrayList<>();
                                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ActivityAirlineSearch.this);
                                    search_recyler_view.setLayoutManager(linearLayoutManager);
                                    search_recyler_view.setHasFixedSize(true);
                                    AdapterSearchAirlines adapterRecycler = new AdapterSearchAirlines(ActivityAirlineSearch.this,
                                            list);
                                    search_recyler_view.setAdapter(adapterRecycler);
                                    img_search.setImageDrawable(getResources().getDrawable(R.drawable.icon_search));
                                    // search();
//                                        getData(String.valueOf(pagenum), false);
                                } else {
                                    img_search.setImageDrawable(getResources().getDrawable(R.drawable.close_del_item));
                                    if (!UtilsDefault.isOnline()) {
                                        Toast.makeText(ActivityAirlineSearch.this, "", Toast.LENGTH_SHORT).show();
                                        return;
                                    }

                                   // shwProgress();
                                    search = s.toString();
                                    search();

                                }


                            }
                        });
                        // getData(String.valueOf(pagenum), false);


                    }
                }, SEARCH_DELAY);


            }
        });
    }

    public void search() {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityAirlineSearch.this, R.string.error_no_net,
                    Toast.LENGTH_SHORT).show();
            return;
        }
      //  shwProgress();
        Log.d("aviparms", "search: " + UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) + search);
        String userid = "0";
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
            userid = UtilsDefault.getSharedPreferenceValue(Constants.USER_ID);
        }
        aviapi.searchArlines(search).enqueue(new Callback<ModelSearchAirline>() {
            @Override
            public void onResponse(Call<ModelSearchAirline> call, Response<ModelSearchAirline> response) {
             //   hideprogress();
                if (response.body() != null) {
                    ModelSearchAirline data = response.body();
                    if (data.getStatus() == Constants.STATUS_200) {
                        if (data.getData() != null &
                                data.getData().size() != 0) {
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ActivityAirlineSearch.this);
                            search_recyler_view.setLayoutManager(linearLayoutManager);
                            search_recyler_view.setHasFixedSize(true);
                            AdapterSearchAirlines adapterRecycler = new AdapterSearchAirlines(ActivityAirlineSearch.this,
                                    data.getData());
                            search_recyler_view.setAdapter(adapterRecycler);
                        } else {
                            List<ModelSearchAirline.Datum> list = new ArrayList<>();

                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ActivityAirlineSearch.this);
                            search_recyler_view.setLayoutManager(linearLayoutManager);
                            search_recyler_view.setHasFixedSize(true);
                            AdapterSearchAirlines adapterRecycler = new AdapterSearchAirlines(ActivityAirlineSearch.this, list);
                            search_recyler_view.setAdapter(adapterRecycler);
                        }
                        //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));

                    } else {
                        List<ModelSearchAirline.Datum> list = new ArrayList<>();

                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ActivityAirlineSearch.this);
                        search_recyler_view.setLayoutManager(linearLayoutManager);
                        search_recyler_view.setHasFixedSize(true);
                        AdapterSearchAirlines adapterRecycler = new AdapterSearchAirlines(ActivityAirlineSearch.this,
                                list);
                        search_recyler_view.setAdapter(adapterRecycler);
                        Toast.makeText(ActivityAirlineSearch.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ActivityAirlineSearch.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ModelSearchAirline> call, Throwable t) {
               // hideprogress();
                //   hideKeyboard();
                Toast.makeText(ActivityAirlineSearch.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == img_back) {
            finish();
        }
        if (v == img_search) {
            Log.d(TAG, "onClick: ");
            if (img_search.getDrawable().getConstantState() == getResources().getDrawable(R.drawable.close_del_item).getConstantState()) {
                ed_search.setText("");
                Log.d(TAG, "onClick: " + "data");
            }
        }

    }


}
