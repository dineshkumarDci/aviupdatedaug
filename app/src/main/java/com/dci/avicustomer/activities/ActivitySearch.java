package com.dci.avicustomer.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.avicustomer.R;
import com.dci.avicustomer.adapter.AdapterSectionRecycler;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.LocationUpdatesComponent;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.ModelAddFavourite;
import com.dci.avicustomer.models.ModelSearchResult;
import com.dci.avicustomer.models.RegisterModel;
import com.dci.avicustomer.models.SectionedDataModel;
import com.dci.avicustomer.retrofit.AVIAPI;
import com.facebook.appevents.codeless.CodelessLoggingEventListener;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import it.sephiroth.android.library.tooltip.Tooltip;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivitySearch extends BaseActivity implements OnItemClickListener,View.OnClickListener,
        LocationUpdatesComponent.ILocationProvider {
    @Inject
    public AVIAPI aviapi;
    @BindView(R.id.ed_search)
    EditText ed_search;
    @BindView(R.id.search_recyler_view)
    RecyclerView search_recyler_view;
    @BindView(R.id.img_blur)
    ImageView img_blur;
    @BindView(R.id.img_back)
    ImageView img_back;
    @BindView(R.id.location_title)
    TextView location_title;
    @BindView(R.id.img_search)
    ImageView img_search;
    private Timer timer;
    String search="";
    private static final long SEARCH_DELAY = 600;
    int locationPermission=100;
    public String TAG="ActivitySearch";
    private LocationUpdatesComponent locationUpdatesComponent;
    String lat="";
    String lng="";
    boolean isLocationSearchDone=false;
    Tooltip.TooltipView tooltip;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        ShoppingApplication.getContext().getComponent().inject(this);
        tooltip=Tooltip.make(ActivitySearch.this,
                new Tooltip.Builder(101)
                        .anchor(ed_search, Tooltip.Gravity.BOTTOM)
                        .activateDelay(800)
                        .showDelay(300)
                        .text(getString(R.string.select_airport))
                        .maxWidth(ViewGroup.LayoutParams.WRAP_CONTENT)
                        .withArrow(true)
                        .withOverlay(true)
                        .withStyleId(R.style.ToolTipAltStyle)
                        .build());
        if (UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID)==null){
            tooltip.show();
        }
        locationUpdatesComponent = new LocationUpdatesComponent(this);
        locationUpdatesComponent.onCreate(this);
        initizializeView();
        img_back.setOnClickListener(this);
        checkLocationSetting();
        //displayLocationSettingsRequest();
        img_search.setOnClickListener(this);
        ed_search.requestFocus();
        search_recyler_view.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == 0) {
                   //hideKeyboard();
                }
               //
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
              //hideKeyboard();
            }
        });

        Log.d(TAG, "onCreate: "+UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID));
       // showSoftKeyboard();
    }

    private void displayLocationSettingsRequest() {
        if ( ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION )
                != PackageManager.PERMISSION_GRANTED ) {

            ActivityCompat.requestPermissions( this, new String[] {
                    android.Manifest.permission.ACCESS_COARSE_LOCATION ,
                            android.Manifest.permission.ACCESS_FINE_LOCATION},
                    locationPermission );
            Toast.makeText(this, R.string.enable_location, Toast.LENGTH_SHORT).show();
            return;
        }
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(ActivitySearch.this)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);
        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

        task.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                StartLocationUpdates();
                Log.d(TAG, "success: "+"onsuccess");
                //Toast.makeText(ActivitySetting.this, "addOnSuccessListener", Toast.LENGTH_SHORT).show();
                // All location settings are satisfied. The client can initialize
                // location requests here.
                // ...
            }
        });
        task.addOnCompleteListener(this, new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                StartLocationUpdates();
                Log.d(TAG, "onComplete: "+"onComplete");
            }
        });

        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG, "onFailure: "+"onFailure");
                // Toast.makeText(ActivitySetting.this, "addOnFailureListener", Toast.LENGTH_SHORT).show();
                if (e instanceof ResolvableApiException) {
                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(ActivitySearch.this,
                                REQUEST_CHECK_SETTINGS);
                    } catch (IntentSender.SendIntentException sendEx) {
                        // Ignore the error.
                    }
                }
            }
        });
    }
    public void checkLocationSetting(){
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(ActivitySearch.this)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);
        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG, "onFailure: ");
              //  displayLocationSettingsRequest();

            }
        });

        task.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                Log.d(TAG, "success: "+"onsuccess");
                displayLocationSettingsRequest();


                //Toast.makeText(ActivitySetting.this, "addOnSuccessListener", Toast.LENGTH_SHORT).show();
                // All location settings are satisfied. The client can initialize
                // location requests here.
                // ...
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(TAG, "onActivityResulttPermissionsResult: "+requestCode);
        if (requestCode == locationPermission) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED&&grantResults[1] ==
                    PackageManager.PERMISSION_GRANTED) {

                Log.d(TAG, "onActivityResulttPermissionsResult: "+PackageManager.PERMISSION_GRANTED);
                displayLocationSettingsRequest();
            } else {
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED&&grantResults[1] ==
                        PackageManager.PERMISSION_DENIED ){
                    Toast.makeText(this, R.string.enable_location, Toast.LENGTH_SHORT).show();
                    return;
                }
                displayLocationSettingsRequest();
            }
        }
    }
    public void initizializeView(){
        ed_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (timer != null) {
                    timer.cancel();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                location_title.setVisibility(View.GONE);
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                if (s.toString().equals("")) {

                                    Tooltip.remove(ActivitySearch.this,101);
                                   // shwProgress();
                                    search="+";
                                    List<SectionedDataModel>list=new ArrayList<>();
                                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ActivitySearch.this);
                                    search_recyler_view.setLayoutManager(linearLayoutManager);
                                    search_recyler_view.setHasFixedSize(true);
                                    AdapterSectionRecycler adapterRecycler = new AdapterSectionRecycler(ActivitySearch.this,
                                            list,ActivitySearch.this);
                                    search_recyler_view.setAdapter(adapterRecycler);
                                    img_search.setImageDrawable(getResources().getDrawable(R.drawable.icon_search));
                                   // search();
//                                        getData(String.valueOf(pagenum), false);
                                } else {
                                    img_search.setImageDrawable(getResources().getDrawable(R.drawable.close_del_item));
                                    if (!UtilsDefault.isOnline()){
                                        Toast.makeText(ActivitySearch.this, R.string.error_no_net, Toast.LENGTH_SHORT).show();
                                        return;
                                    }
                                    Tooltip.remove(ActivitySearch.this,101);
                                 //   shwProgress();
                                    search=s.toString();
                                    if(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)==null){
                                        search();
                                    }
                                    else {
                                        searchLogin();
                                    }

                                }


                            }
                        });
                        // getData(String.valueOf(pagenum), false);


                    }
                }, SEARCH_DELAY);



            }
        });


       

    }

    public void addFavourite(String id){
        if (!UtilsDefault.isOnline()){
            Toast.makeText(ActivitySearch.this, R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;

        }
        shwProgress();

        Log.d("aviparms", "search: "+UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)+id);
        aviapi.addfavourite(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID),id).
                enqueue(new Callback<ModelAddFavourite>() {
            @Override
            public void onResponse(Call<ModelAddFavourite> call, Response<ModelAddFavourite> response) {
                hideprogress();
               // hideKeyboard();


                if (response.body()!=null){

                    ModelAddFavourite data=response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus()== Constants.STATUS_200){
                        Toast.makeText(ActivitySearch.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                        search();
                        //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                    }
                    else if (data.getStatus()==Constants.API_TOKENEXPIRY_STATUS){
                        addFavourite(id);
                    }
                    else {
                        Toast.makeText(ActivitySearch.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Toast.makeText(ActivitySearch.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ModelAddFavourite> call, Throwable t) {
                hideprogress();
               // hideKeyboard();
                Toast.makeText(ActivitySearch.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void searchLogin(){
        if (!UtilsDefault.isOnline()){
            Toast.makeText(ActivitySearch.this, getResources().getString(R.string.error_no_net)
                    , Toast.LENGTH_SHORT).show();
            return;
        }
      //  shwProgress();
        Log.d("aviparms", "search: "+UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)+search+lat+"-"+lng);
        String userid="0";
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)!=null){
            userid=UtilsDefault.getSharedPreferenceValue(Constants.USER_ID);
        }
        aviapi.searchLogin(userid,search,lat,lng).enqueue(new Callback<ModelSearchResult>() {
            @Override
            public void onResponse(Call<ModelSearchResult> call, Response<ModelSearchResult> response) {
               // hideprogress();

                if (response.body()!=null){
                    ModelSearchResult data=response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus()== Constants.STATUS_200){
                        if (data.getData().getFavAirports().size()==0&&data.getData().getAirports().size()==0){
                            Toast.makeText(ActivitySearch.this, R.string.mo_result,
                                    Toast.LENGTH_SHORT).show();
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ActivitySearch.this);
                            search_recyler_view.setLayoutManager(linearLayoutManager);
                            search_recyler_view.setHasFixedSize(true);
                            AdapterSectionRecycler adapterRecycler = new AdapterSectionRecycler(ActivitySearch.this,
                                    PrepareData(data.getData()),ActivitySearch.this);
                            search_recyler_view.setAdapter(adapterRecycler);
                        }
                        else {
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager
                                    (ActivitySearch.this);
                            search_recyler_view.setLayoutManager(linearLayoutManager);
                            search_recyler_view.setHasFixedSize(true);
                            AdapterSectionRecycler adapterRecycler = new AdapterSectionRecycler(ActivitySearch.this,
                                    PrepareData(data.getData()),ActivitySearch.this);
                            search_recyler_view.setAdapter(adapterRecycler);
                        }

                        //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));



                    }
                    else if (data.getStatus()==Constants.API_TOKENEXPIRY_STATUS){
                        searchLogin();
                    }
                    else {
                        Toast.makeText(ActivitySearch.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Toast.makeText(ActivitySearch.this,R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ModelSearchResult> call, Throwable t) {
              //  hideprogress();


                Toast.makeText(ActivitySearch.this,R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void search(){
        if (!UtilsDefault.isOnline()){
            Toast.makeText(ActivitySearch.this, getResources().getString(R.string.error_no_net), Toast.LENGTH_SHORT).show();
            return;
        }
       // shwProgress();
        Log.d("aviparms", "search: "+UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)+search);
        String userid="0";
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)!=null){
            userid=UtilsDefault.getSharedPreferenceValue(Constants.USER_ID);
        }
        aviapi.search(userid,search,lat,lng).enqueue(new Callback<ModelSearchResult>() {
            @Override
            public void onResponse(Call<ModelSearchResult> call, Response<ModelSearchResult> response) {
               // hideprogress();

                if (response.body()!=null){
                    ModelSearchResult data=response.body();
                    if(data.getJwt_token()!=null){

                    }
                    if (data.getStatus()== Constants.STATUS_200){
                        if (data.getData().getFavAirports().size()==0&&
                                data.getData().getAirports().size()==0){
                            Toast.makeText(ActivitySearch.this, R.string.mo_result, Toast.LENGTH_SHORT).show();
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ActivitySearch.this);
                            search_recyler_view.setLayoutManager(linearLayoutManager);
                            search_recyler_view.setHasFixedSize(true);
                            AdapterSectionRecycler adapterRecycler = new AdapterSectionRecycler(ActivitySearch.this,
                                    PrepareData(data.getData()),ActivitySearch.this);
                            search_recyler_view.setAdapter(adapterRecycler);

                        }
                        else {
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ActivitySearch.this);
                            search_recyler_view.setLayoutManager(linearLayoutManager);
                            search_recyler_view.setHasFixedSize(true);
                            AdapterSectionRecycler adapterRecycler = new AdapterSectionRecycler(ActivitySearch.this,
                                    PrepareData(data.getData()),ActivitySearch.this);
                            search_recyler_view.setAdapter(adapterRecycler);
                        }
                        //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));

                    }
                    else {
                        Toast.makeText(ActivitySearch.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Toast.makeText(ActivitySearch.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ModelSearchResult> call, Throwable t) {
               // hideprogress();
             //   hideKeyboard();
                Toast.makeText(ActivitySearch.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }
    public List<SectionedDataModel> PrepareData(ModelSearchResult.Data data){
        List<SectionedDataModel>list=new ArrayList<>();
        list.clear();
        if (data.getFavAirports()!=null&&data.getFavAirports().size()!=0){
            SectionedDataModel model=new SectionedDataModel(data.getFavAirports(),"Favourites");
            list.add(model);
        }

        SectionedDataModel model2=new SectionedDataModel(data.getAirports(),"Search Results");
        list.add(model2);
        return list;
    }
    public void StartLocationUpdates(){
     //   Toast.makeText(this, "Calling", Toast.LENGTH_SHORT).show();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions( this, new String[] {
                                android.Manifest.permission.ACCESS_COARSE_LOCATION , android.Manifest.permission.ACCESS_FINE_LOCATION},
                        locationPermission );
                return ;
            }
        }
        locationUpdatesComponent.onCreate(this);

        locationUpdatesComponent.onStart();
    }

    @Override
    protected int getlayout() {
        return R.layout.activity_search;
    }

    @Override
    public void OnItemClickListener(int pos, View view) {
        if (view.getId()==R.id.img_fav){
            if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)==null){

                alertShowLogin(getString(R.string.login_messge_favourite));
            }
            else {
                addFavourite(String.valueOf(pos));
            }


        }

    }

    @Override
    public void onClick(View v) {
        if (v==img_back){
            finish();
        }
        if (v==img_search){
            Log.d(TAG, "onClick: ");
            if (img_search.getDrawable().getConstantState()==getResources().getDrawable(R.drawable.close_del_item).getConstantState()){
                ed_search.setText("");
                Log.d(TAG, "onClick: "+"data");
            }
        }

    }

    @Override
    public void onLocationUpdate(Location location) {
        if (isLocationSearchDone){
            return;
        }
        lat=String.valueOf(location.getLatitude());
        lng=String.valueOf(location.getLongitude());
        if (!lat.equals("")&&!lng.equals("")){
            location_title.setVisibility(View.VISIBLE);
            tooltip.hide();
            Tooltip.remove(ActivitySearch.this,101);
            if(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)==null){
                search();
            }
            else {
                searchLogin();
            }

        }
        isLocationSearchDone=true;
      // Toast.makeText(this, ""+"onLocationUpdate: "+location.getLatitude()+"long"+location.getLongitude(), Toast.LENGTH_SHORT).show();
        Log.d("context", "onLocationUpdate: "+location.getLatitude()+"long"+location.getLongitude());
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (locationUpdatesComponent != null) {
            locationUpdatesComponent.onStop();
        }

    }
}
