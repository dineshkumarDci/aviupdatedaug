package com.dci.avicustomer.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.dci.avicustomer.R;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.LocationUpdatesComponent;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.models.CommonMsgModel;
import com.dci.avicustomer.models.ModelAddFavourite;
import com.dci.avicustomer.models.ModelUserProfile;
import com.dci.avicustomer.retrofit.AVIAPI;
import com.github.angads25.toggle.interfaces.OnToggledListener;
import com.github.angads25.toggle.model.ToggleableView;
import com.github.angads25.toggle.widget.LabeledSwitch;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import it.sephiroth.android.library.tooltip.Tooltip;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivitySetting extends BaseActivity implements View.OnClickListener,
        LocationUpdatesComponent.ILocationProvider {
    @BindView(R.id.toogle_notification)
    LabeledSwitch toogle_notification;
    @BindView(R.id.toogle_location)
    LabeledSwitch toogle_location;
    @BindView(R.id.changepass_lay)
    RelativeLayout changepass_lay;
    @BindView(R.id.privacy_lay)
    RelativeLayout privacy_lay;
    @BindView(R.id.terms_lay)
    RelativeLayout terms_lay;
    @BindView(R.id.logout_lay)
    RelativeLayout logout_lay;
    String TAG = "ActivitySetting";
    int locationPermission = 100;
    private LocationUpdatesComponent locationUpdatesComponent;
    @Inject
    public AVIAPI aviapi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        ShoppingApplication.getContext().getComponent().inject(this);
        tv_title.setText(R.string.title_setting);
        privacy_lay.setOnClickListener(this);
        terms_lay.setOnClickListener(this);
        changepass_lay.setOnClickListener(this);
        logout_lay.setOnClickListener(this);
//        if (UtilsDefault.getFcmSharedPreferenceValue(Constants.NOTIFICATION_STATUS)!=null&&
//                UtilsDefault.getFcmSharedPreferenceValue(Constants.NOTIFICATION_STATUS).equals("1")){
//            toogle_notification.setOn(true);
//
//        }
//        else if (UtilsDefault.getFcmSharedPreferenceValue(Constants.NOTIFICATION_STATUS)!=null&&
//                UtilsDefault.getFcmSharedPreferenceValue(Constants.NOTIFICATION_STATUS).equals("2")){
//            toogle_notification.setOn(false);
//
//        }
//        else {
//            toogle_notification.setOn(true);
//        }
        toogle_location.setOnToggledListener(new OnToggledListener() {
            @Override
            public void onSwitched(ToggleableView toggleableView, boolean isOn) {
                if (isOn) {
                    displayLocationSettingsRequest();
                } else {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            }
        });
        toogle_notification.setOnToggledListener(new OnToggledListener() {
            @Override
            public void onSwitched(ToggleableView toggleableView, boolean isOn) {
                if (isOn){
                    switchNotification(Constants.NOTIFY_ON);
                    UtilsDefault.updateFcmSharedPreference(Constants.NOTIFICATION_STATUS,"1");
                }
                else {
                    switchNotification(Constants.NOTIFY_OFF);
                    UtilsDefault.updateFcmSharedPreference(Constants.NOTIFICATION_STATUS,"2");
                }
            }
        });
        locationUpdatesComponent = new LocationUpdatesComponent(this);
        locationUpdatesComponent.onCreate(this);
        checkLocationSetting();
        getUserProfile();
    }

    public void getUserProfile() {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivitySetting.this,
                    R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;
        }
        shwProgress();
        //  Log.d("aviparms", "search: "+UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)+search);
        String userid = "0";
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
            userid = UtilsDefault.getSharedPreferenceValue(Constants.USER_ID);
        }

        Log.d("uid",
                "updateProfile: " + UtilsDefault.getSharedPreferenceValue(Constants.USER_ID));
        aviapi.getUserProfile().
                enqueue(new Callback<ModelUserProfile>() {
                    @Override
                    public void onResponse(Call<ModelUserProfile> call, Response<ModelUserProfile> response) {
                        hideprogress();
                        if (response.body() != null) {
                            ModelUserProfile data = response.body();
                            String header = response.headers().get(getString(R.string.header_find_key));
                            UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                            if (data.getStatus() == Constants.STATUS_200) {
                               if (data.getData().getNotification_id()==1){
                                   toogle_notification.setOn(true);
                               }
                               else {
                                   toogle_notification.setOn(false);
                               }

                                //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                            } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                                getUserProfile();
                                //countDownStart();
                            } else {
                                Toast.makeText(ActivitySetting.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(ActivitySetting.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<ModelUserProfile> call, Throwable t) {
                        hideprogress();
                        Log.d("failure", "onFailure: " + t.getMessage());
                        Toast.makeText(ActivitySetting.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                    }
                });
    }


    @Override
    public void onLocationUpdate(Location location) {
        //  Toast.makeText(this, ""+"onLocationUpdate: "+location.getLatitude()+"long"+location.getLongitude(), Toast.LENGTH_SHORT).show();
        Log.d("context", "onLocationUpdate: "+location.getLatitude()+"long"+location.getLongitude());
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (locationUpdatesComponent != null) {
            locationUpdatesComponent.onStop();
        }
    }

    public void switchNotification(String type){
        if (!UtilsDefault.isOnline()){
            Toast.makeText(ActivitySetting.this, R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;

        }
        shwProgress();

        Log.d("aviparms", "search: "+UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)+type);
        aviapi.switchNotification(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID),type).
                enqueue(new Callback<CommonMsgModel>() {
                    @Override
                    public void onResponse(Call<CommonMsgModel> call, Response<CommonMsgModel> response) {
                        hideprogress();
                        // hideKeyboard();
                        if (response.body()!=null){

                            CommonMsgModel data=response.body();
                            String header = response.headers().get(getString(R.string.header_find_key));
                            UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                            if (data.getStatus()== Constants.STATUS_200){
                                Toast.makeText(ActivitySetting.this, data.getMessage(), Toast.LENGTH_SHORT).show();

                                //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                            }
                            else if (data.getStatus()==Constants.API_TOKENEXPIRY_STATUS){
                                switchNotification(type);
                            }
                            else {
                                Toast.makeText(ActivitySetting.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else {
                            Toast.makeText(ActivitySetting.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<CommonMsgModel> call, Throwable t) {
                        hideprogress();
                        // hideKeyboard();
                        Toast.makeText(ActivitySetting.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: " + resultCode);
        if (requestCode == locationPermission) {
            checkLocationSetting();

            // displayLocationSettingsRequest();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(TAG, "onActivityResulttPermissionsResult: " + requestCode);
        if (requestCode == locationPermission) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "onActivityResulttPermissionsResult: " + PackageManager.PERMISSION_GRANTED);
                displayLocationSettingsRequest();
            } else {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    Toast.makeText(this, R.string.enable_location, Toast.LENGTH_SHORT).show();
                    return;

                }
                displayLocationSettingsRequest();
            }
        }
    }

    public void checkLocationSetting() {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(ActivitySetting.this)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);
        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

        task.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                toogle_location.setOn(true);

                //Toast.makeText(ActivitySetting.this, "addOnSuccessListener", Toast.LENGTH_SHORT).show();
                // All location settings are satisfied. The client can initialize
                // location requests here.
                // ...
            }
        });
        task.addOnCanceledListener(this, new OnCanceledListener() {
            @Override
            public void onCanceled() {
                toogle_location.setOn(false);
            }
        });
        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                // Toast.makeText(ActivitySetting.this, "addOnFailureListener", Toast.LENGTH_SHORT).show();
                toogle_location.setOn(false);
            }
        });



    }


    private void displayLocationSettingsRequest() {
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    locationPermission);
            Toast.makeText(this, R.string.enable_location, Toast.LENGTH_SHORT).show();
            return;
        }
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(ActivitySetting.this)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);
        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

        task.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                StartLocationUpdates();

                //Toast.makeText(ActivitySetting.this, "addOnSuccessListener", Toast.LENGTH_SHORT).show();
                // All location settings are satisfied. The client can initialize
                // location requests here.
                // ...

            }
        });
        task.addOnCanceledListener(this, new OnCanceledListener() {
            @Override
            public void onCanceled() {

            }
        });
        task.addOnCompleteListener(this, new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                StartLocationUpdates();
                Log.d(TAG, "onComplete: "+"onComplete");
            }
        });

        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                // Toast.makeText(ActivitySetting.this, "addOnFailureListener", Toast.LENGTH_SHORT).show();
                if (e instanceof ResolvableApiException) {
                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(ActivitySetting.this,
                                REQUEST_CHECK_SETTINGS);
                    } catch (IntentSender.SendIntentException sendEx) {
                        // Ignore the error.
                    }
                }
            }
        });
    }
    public void StartLocationUpdates(){
        //   Toast.makeText(this, "Calling", Toast.LENGTH_SHORT).show();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions( this, new String[] {
                                android.Manifest.permission.ACCESS_COARSE_LOCATION , android.Manifest.permission.ACCESS_FINE_LOCATION},
                        locationPermission );
                return ;


            }
        }
        locationUpdatesComponent.onCreate(this);

        locationUpdatesComponent.onStart();
    }

    @Override
    protected int getlayout() {
        return R.layout.activity_setting;
    }

    @Override
    public void onClick(View v) {
        if (v == privacy_lay) {
            Intent intent = new Intent(ActivitySetting.this, ActivityCommonWebLoader.class);
            intent.putExtra("data", getString(R.string.privacy_url));
            intent.putExtra("title", getString(R.string.title_privacy));
            startActivity(intent);
        }
        if (v == terms_lay) {
            Intent intent = new Intent(ActivitySetting.this, ActivityCommonWebLoader.class);
            intent.putExtra("data", getString(R.string.terms_url));
            intent.putExtra("title", getString(R.string.title_terms_page));
            startActivity(intent);
        }
        if (v == changepass_lay) {
            startActivity(new Intent(ActivitySetting.this, ActivityChangePassword.class));
        }
        if (v == logout_lay) {
            UtilsDefault.clearSesson();
            Intent intent = new Intent(ActivitySetting.this, ActivityLogin.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }

    }
}
