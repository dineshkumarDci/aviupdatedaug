package com.dci.avicustomer.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.dci.avicustomer.BuildConfig;
import com.dci.avicustomer.R;
import com.dci.avicustomer.adapter.AdapterCartView;
import com.dci.avicustomer.adapter.DrawerAdapter;
import com.dci.avicustomer.fragments.FragmentHome;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.CommonMsgModel;
import com.dci.avicustomer.models.DrawerItem;
import com.dci.avicustomer.models.ModelCartView;
import com.dci.avicustomer.retrofit.AVIAPI;
import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityHome extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        View.OnClickListener,OnItemClickListener {
    @BindView(R.id.recycle_side_menu)
    RecyclerView recyclerView;
    @BindView(R.id.img_drawer)
    ImageView img_drawer;
    @BindView(R.id.ed_search)
    TextView ed_search;
    @BindView(R.id.img_search)
    ImageView img_search;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.img_userimage)
    ImageView img_userimage ;
    @BindView(R.id.tv_user_email)
    TextView tv_user_email ;
    @BindView(R.id.tv_user_name)
    TextView tv_user_name ;
    @BindView(R.id.logout_lay)
    RelativeLayout logout_lay;
    DrawerLayout drawerLayout;
    Fragment fragment;
    @BindView(R.id.img_edit)
    ImageView img_edit;
    @Inject
    public AVIAPI aviapi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        ShoppingApplication.getContext().getComponent().inject(this);
//        Toolbar toolbar =  findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        logout_lay.setOnClickListener(this);
        img_edit.setOnClickListener(this);
        img_search.setOnClickListener(this);
        img_userimage.setOnClickListener(this);
        ed_search.setSelected(true);
//        Calendar cal_Two = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
//        System.out.println(cal_Two.getTime());
//        System.out.println(cal_Two.getTimeZone());
//
//        Log.d("fcmKey",
//                "onCreate: "+UtilsDefault.getFcmSharedPreferenceValue(Constants.FCM_KEY));
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action",
                        Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
       // ed_search.setFocusable(false);
        ed_search.setOnClickListener(this);

        drawerLayout =  findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.addDrawerListener(toggle);
//        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        DrawerAdapter adapter=new DrawerAdapter(this,getDrawerData(),0);
        adapter.setonItemClick(this);
        recyclerView.setAdapter(adapter);

        img_drawer.setOnClickListener(this);
        setFragment(new FragmentHome());
        setupUserDetails();
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)==null){
            title.setText("Login");
        }
        setAirportName();
        AppCenter.start(getApplication(), "01284e51-2090-4a23-86d7-fb3501759399",
                Analytics.class, Crashes.class);

        //<set SMS attributes>
      //  sendSMSMessage(snsClient, message, phoneNumber, smsAttributes);

    }
    public void setAirportName(){
        if(UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_NAME)!=null){
            ed_search.setText(UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_NAME));
        }
    }

//    public static void sendSMSMessage(AmazonSNSClient snsClient, String message,
//                                      String phoneNumber, Map<String, MessageAttributeValue> smsAttributes) {
//        PublishResult result = snsClient.publish(new PublishRequest()
//                .withMessage(getMessage(login, email, name, subject, content)
//               // .withPhoneNumber(phoneNumber)
//                .withMessageAttributes(smsAttributes));
//        System.out.println(result); // Prints the message ID.
//    }
//
    @Override
    protected void onResume() {
        super.onResume();
        setupUserDetails();
        hideKeyboard();
    }


    public void logout() {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityHome.this,
                    R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;
        }
        shwProgress();
        //  Log.d("aviparms", "search: "+UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)+search);
        String userid = "0";
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
            userid = UtilsDefault.getSharedPreferenceValue(Constants.USER_ID);
        }
        aviapi.logout(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)).enqueue(new Callback<CommonMsgModel>() {
            @Override
            public void onResponse(Call<CommonMsgModel> call, Response<CommonMsgModel> response) {
                hideprogress();
                hideKeyboard();
                if (response.body() != null) {
                    CommonMsgModel data = response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus() == Constants.STATUS_200) {
                        UtilsDefault.clearSesson();
                        Intent intent=new Intent(ActivityHome.this,ActivityLogin.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();

                        //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                    } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                      logout();
                    } else {

                        UtilsDefault.clearSesson();
                        Intent intent=new Intent(ActivityHome.this,ActivityLogin.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                        //Toast.makeText(ActivityHome.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {

                    UtilsDefault.clearSesson();
                    Intent intent=new Intent(ActivityHome.this,ActivityLogin.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                   // Toast.makeText(ActivityHome.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<CommonMsgModel> call, Throwable t) {
                UtilsDefault.clearSesson();
                Intent intent=new Intent(ActivityHome.this,ActivityLogin.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                hideprogress();
                hideKeyboard();
               // alertcommon("MyCart", getString(R.string.server_error));
                Log.d("failure", "onFailure: " + t.getMessage());
                //Toast.makeText(ActivityMyTrips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void setupUserDetails(){
        if (UtilsDefault.getSharedPreferenceValue(Constants.FIRST_NAME)!=null&&
                UtilsDefault.getSharedPreferenceValue(Constants.LAST_NAME)!=null){
            tv_user_name.setText(UtilsDefault.getSharedPreferenceValue(Constants.FIRST_NAME)+" "+
                    UtilsDefault.getSharedPreferenceValue(Constants.LAST_NAME));
            tv_user_email.setText(UtilsDefault.getSharedPreferenceValue(Constants.EMAILID));
            Glide.with(this).load(UtilsDefault.getSharedPreferenceValue(Constants.USER_IMAGE_PATH)+UtilsDefault.getSharedPreferenceValue(Constants.PROFILE_IMG))
                    .placeholder(R.drawable.placeholder_icon).into(img_userimage);

        }
        else {
            tv_user_name.setText(R.string.title_guest);
            tv_user_email.setText("");
            Glide.with(this).load(UtilsDefault.getSharedPreferenceValue(Constants.USER_IMAGE_PATH)+
                    UtilsDefault.getSharedPreferenceValue(Constants.PROFILE_IMG)).
                    placeholder(R.drawable.placeholder_icon).into(img_userimage);
        }


    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            UtilsDefault.updateSharedPreference(Constants.WEATHER_REPORT,null);
            alert(getString(R.string.exit_app));
           // super.onBackPressed();
        }
        hideKeyboard();
    }
    public void setFragment(Fragment fragments) {
        fragment = fragments;
        if (fragment != null) {
            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.addToBackStack("test");
            fragmentTransaction.replace(R.id.container, fragment, "About Us");
            fragmentTransaction.commit();
        }
    }

    public List<DrawerItem> getDrawerData() {
        List<DrawerItem> list = new ArrayList<>();
        list.clear();
        DrawerItem drawerItem = new DrawerItem();
        drawerItem.setIcon(R.drawable.home_icon_silhouette);
        drawerItem.setTitle(getString(R.string.sidemenu_home));
        list.add(drawerItem);
        drawerItem = new DrawerItem();
        drawerItem.setIcon(R.drawable.home_mytrip);
        drawerItem.setTitle(getString(R.string.sidemenu_mytrips));
        list.add(drawerItem);
        drawerItem = new DrawerItem();
        drawerItem.setIcon(R.drawable.home_flightstatus);
        drawerItem.setTitle(getString(R.string.sidemenu_flightstatus));
        list.add(drawerItem);
        drawerItem = new DrawerItem();
        drawerItem.setIcon(R.drawable.flightschedule);
        drawerItem.setTitle(getString(R.string.sidemenu_flight_schedule));
        list.add(drawerItem);
        drawerItem = new DrawerItem();
        drawerItem.setIcon(R.drawable.mydocument);
        drawerItem.setTitle(getString(R.string.my_docs));
        list.add(drawerItem);
        drawerItem = new DrawerItem();
        drawerItem.setIcon(R.drawable.side_order_track);
        drawerItem.setTitle(getString(R.string.order_track));
        list.add(drawerItem);
        drawerItem = new DrawerItem();
        drawerItem.setIcon(R.drawable.settings);
        drawerItem.setTitle(getString(R.string.setting));
        list.add(drawerItem);
        drawerItem = new DrawerItem();
        drawerItem.setIcon(R.drawable.contact);
        drawerItem.setTitle(getString(R.string.sidemenu_contactus));
        list.add(drawerItem);
        drawerItem = new DrawerItem();
        drawerItem.setIcon(R.drawable.feedback);
        drawerItem.setTitle(getString(R.string.sidemenu_feedback));
        list.add(drawerItem);
        drawerItem = new DrawerItem();
        drawerItem.setIcon(R.drawable.share);
        drawerItem.setTitle(getString(R.string.sidemenu_shareapp));
        list.add(drawerItem);
        return list;
    }

    @Override
    protected int getlayout() {
        return R.layout.activity_home;
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; thi                    android:id="@+id/img_search"s adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.activity_home, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        if(v==img_drawer){
            if(!drawerLayout.isDrawerOpen(GravityCompat.START)) drawerLayout.openDrawer(Gravity.START);
            else drawerLayout.closeDrawer(Gravity.END);
        }
        if (v==img_userimage){
            if(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)!=null){
                startActivity(new Intent(ActivityHome.this,ActivityMyProfile.class));

            }
            else {
                alertShowLogin(getString(R.string.login_messge));
            }
        }
        if(v==ed_search||v==img_search){
            startActivity(new Intent(ActivityHome.this,ActivitySearch.class));
        }
        if (v==img_edit){
            if(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)!=null){
                startActivity(new Intent(ActivityHome.this,ActivityMyProfile.class));

            }
            else {
                alertShowLogin(getString(R.string.login_messge));
            }
        }
        if (v==logout_lay){
            if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)==null){
                Intent intent=new Intent(ActivityHome.this,ActivityLogin.class);
                startActivity(intent);
            }
            else {
                logout();

            }

        }
    }

    @Override
    public void OnItemClickListener(int pos, View view) {
        if (pos==8){
            if(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)==null){
                alertShowLogin(getString(R.string.login_messge));
                return;

            }
            startActivity(new Intent(ActivityHome.this,ActivityAppFeedBack.class));
        }
        if (pos==2){

            if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) == null) {
                alertShowLogin(getString(R.string.login_messge));
                return;

            }
            //startActivity(new Intent(getActivity(), ActivityFlightStatus.class));
            startActivity(new Intent(ActivityHome.this, ActivityUpdatedFlightStatus.class));


//            if(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)==null){
//                alertShowLogin(getString(R.string.login_messge));
//                return;
//
//            }
//            startActivity(new Intent(ActivityHome.this,ActivityFlightStatus.class));
        }
        if (pos==5){
            if(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)==null){
                alertShowLogin(getString(R.string.login_messge));
                return;

            }
            Intent intent=new Intent(ActivityHome.this,ActivityOrderTracking.class);
            intent.putExtra("id","0");
            startActivity(intent);
        }
        if (pos==6){
            if(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)==null){
                alertShowLogin(getString(R.string.login_messge));
                return;

            }
            startActivity(new Intent(ActivityHome.this,ActivitySetting.class));
        }
        if (pos==4){
            if(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)==null){
                alertShowLogin(getString(R.string.login_messge));
                return;
            }
            startActivity(new Intent(ActivityHome.this,ActivityMyDocument.class));
        }
        if (pos==0){
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        if (pos==7){
            startActivity(new Intent(ActivityHome.this,ActivityContactUs.class));
        }
        if(pos==3){
            if(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)==null){
                alertShowLogin(getString(R.string.login_messge));
                return;

            }
            startActivity(new Intent(ActivityHome.this,ActivityFlightSchedule.class));
        }
        if (pos==1){
            if(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)==null){
                alertShowLogin(getString(R.string.login_messge));
                return;

            }
            startActivity(new Intent(ActivityHome.this,ActivityMyTrips.class));
        }
        if(pos==9){
            try {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));

                String shareMessage= "\nI recommend you to try AVI app for all your Air travel, works for me\n\n";
                shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID +"\n\n";
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                startActivity(Intent.createChooser(shareIntent, "choose one"));
            } catch(Exception e) {
                //e.toString();
            }
        }

    }
    @Override
    public void alert(String msg) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(msg);
        alertDialogBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        FragmentManager fm = getSupportFragmentManager();
                        for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                            fm.popBackStack();
                        }
                        dialog.dismiss();
                        finish();


                    }
                });
        alertDialogBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        dialog.dismiss();

                    }
                });

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
