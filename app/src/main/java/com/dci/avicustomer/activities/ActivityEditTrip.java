package com.dci.avicustomer.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.dci.avicustomer.BuildConfig;
import com.dci.avicustomer.R;
import com.dci.avicustomer.adapter.AdapterBoardingPass;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.interfaces.OnImageRemoveListener;
import com.dci.avicustomer.models.CommonMsgModel;
import com.dci.avicustomer.models.ModelAddDocuments;
import com.dci.avicustomer.models.ModelAddTrip;
import com.dci.avicustomer.models.ModelInputAddtrip;
import com.dci.avicustomer.models.ModelNewFlightStatus;
import com.dci.avicustomer.models.ModelTripDetails;
import com.dci.avicustomer.retrofit.AVIAPI;
import com.jaiselrahman.filepicker.activity.FilePickerActivity;
import com.jaiselrahman.filepicker.model.MediaFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityEditTrip extends ActivityAddTrip {
    @Inject
    public AVIAPI aviapi;
    AdapterBoardingPass adapterBoardingPassEdit;
    AdapterBoardingPass adapterEticketEdit;
    List<ModelTripDetails.BoardImg> listOfBoardingFromServer = new ArrayList<>();
    List<ModelTripDetails.ETicketImg> listOfEticketFromServer = new ArrayList<>();
    int tripid;
    ModelTripDetails modelTripDetails;
    //  HashMap<Long,MediaFile>list=new HashMap<Long, MediaFile>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        ShoppingApplication.getContext().getComponent().inject(this);
        tv_title.setText(R.string.edit_trip);

    }

    @Override
    public void updateUi(ModelNewFlightStatus flightData) {
        super.updateUi(flightData);
        if (flightData != null && flightData.getData() != null &&
                flightData.getStatus() == Constants.STATUS_200) {
            issearchResultSucess = true;
            modelInputAddtrip = new ModelInputAddtrip();
            modelInputAddtrip.setFlight_status(UtilsDefault.checkNull(flightData.getData().getFlightStatus()));
            modelInputAddtrip.setAirlineName(UtilsDefault.checkNull(flightData.getData().getAirline()));
            modelInputAddtrip.setAirport_id(Integer.parseInt(UtilsDefault.checkNull(UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID))));
            modelInputAddtrip.setUser_id(Integer.parseInt(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)));
            modelInputAddtrip.setFlight_code(UtilsDefault.checkNull(flightData.getData().getAirlineCode() + UtilsDefault.checkNull(flightData.getData().getFlightNumber())));
            modelInputAddtrip.setDep_airport_code(UtilsDefault.checkNull(flightData.getData().getDepCode()));
            modelInputAddtrip.setArr_airport_code(UtilsDefault.checkNull(flightData.getData().getArrCode()));
            modelInputAddtrip.setArr_airport_city(UtilsDefault.checkNull(flightData.getData().getArrCity()));
            modelInputAddtrip.setDep_airport_city(UtilsDefault.checkNull(flightData.getData().getDepCity()));
            modelInputAddtrip.setArr_airport_country(UtilsDefault.checkNull(flightData.getData().getArrCountry()));
            modelInputAddtrip.setDep_airport_country(UtilsDefault.checkNull(flightData.getData().getDeptCountry()));
            if (flightData.getData().getDepTerminal() != null && !flightData.getData().getDepTerminal().equals("")) {
                modelInputAddtrip.setDep_terminal(flightData.getData().getDepTerminal());
            }
            if (flightData.getData().getArrTerminal() != null && !flightData.getData().getArrTerminal().equals("")) {
                modelInputAddtrip.setArr_terminal(flightData.getData().getArrTerminal());
            }
            modelInputAddtrip.setDep_date(UtilsDefault.checkNull(flightData.getData().getDepdate()));
            modelInputAddtrip.setArr_date(UtilsDefault.checkNull(flightData.getData().getArrdate()));
            modelInputAddtrip.setDep_time(UtilsDefault.checkNull(flightData.getData().getDepTime()));
            modelInputAddtrip.setArr_time(UtilsDefault.checkNull(flightData.getData().getArrTime()));
            modelInputAddtrip.setDep_airport_name(UtilsDefault.checkNull(flightData.getData().getDepAirportName()));
            modelInputAddtrip.setArr_airport_name(UtilsDefault.checkNull(flightData.getData().getArrAirportName()));
            modelInputAddtrip.setId(tripid);
            search_lay.setVisibility(View.VISIBLE);
            nest_result_lay.setVisibility(View.VISIBLE);
        } else {
            issearchResultSucess = false;
        }
    }

    @Override
    public void handleImageSlection(int requestCode,
                                    int resultCode, @Nullable Intent data) {
        try {
            if (requestCode == FILE_REQUEST_CODE && resultCode == RESULT_OK
                    && data != null) {
                if (mBottomSheetDialog != null) {
                    mBottomSheetDialog.cancel();
                }
                if (sectionType == Integer.parseInt(Constants.SECTION_BOARDING)) {
                    int totalcount = recycle_boarding_images.getChildCount() +
                            data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES).size();
                    if (totalcount > 5) {
                        alertcommon(getString(R.string.add_trip), getString(R.string.maximum_five_files));
                        return;
                    }
                    recycle_boarding_images.setVisibility(View.VISIBLE);
                    // removBordingDuplicate(data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES));
                    listBoarding.clear();
                    listBoarding.addAll(data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES));

                    for (MediaFile file : listBoarding) {
                        ModelTripDetails.BoardImg boardingimage = new ModelTripDetails().new BoardImg();
                        boardingimage.setImage(file.getPath());
                        boardingimage.setId(null);
                        boardingimage.setType("1");
                        boardingimage.setMimetype(file.getMimeType());
                        listOfBoardingFromServer.add(boardingimage);
                    }

                    //removeBordingDuplicates(listOfBoardingFromServer);
                    adapterBoardingPassEdit.notifyDataSetChanged();
                } else if (sectionType == Integer.parseInt(Constants.SECTION_ETICKET)) {
                    int totalcount = recycle_eticket_images.getChildCount() + data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES).size();
                    if (totalcount > 5) {
                        alertcommon(getString(R.string.add_trip),
                                getString(R.string.maximum_five_files));
                        return;
                    }
                    recycle_eticket_images.setVisibility(View.VISIBLE);
                    listEticket.clear();
                    listEticket.addAll(data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES));
                    //  listEticket.addAll(data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES));
                    for (MediaFile file : listEticket) {
                        ModelTripDetails.ETicketImg eticket = new ModelTripDetails().new ETicketImg();
                        eticket.setImage(file.getPath());
                        eticket.setId(null);
                        eticket.setType("1");
                        eticket.setMimetype(file.getMimeType());
                        listOfEticketFromServer.add(eticket);
                    }
                    // removeEticketDuplicates(listOfEticketFromServer);
                    adapterEticketEdit.notifyDataSetChanged();
                }

            } else if (requestCode == IMAGE_REQUEST_CODE && resultCode == RESULT_OK
                    && data != null) {
                if (mBottomSheetDialog != null) {
                    mBottomSheetDialog.cancel();
                }
                if (sectionType == Integer.parseInt(Constants.SECTION_BOARDING)) {
                    int totalcount = recycle_boarding_images.getChildCount() + data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES).size();
                    if (totalcount > 5) {
                        alertcommon(getString(R.string.add_trip),
                                getString(R.string.maximum_five_files));
                        return;
                    }
                    listBoarding.clear();
                    listBoarding.addAll(data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES));
                    // listBoarding.addAll(data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES));
                    for (MediaFile file : listBoarding) {
                        ModelTripDetails.BoardImg boardingimage = new ModelTripDetails().new BoardImg();
                        boardingimage.setImage(file.getPath());
                        boardingimage.setId(null);
                        boardingimage.setType("1");
                        boardingimage.setMimetype(file.getMimeType());
                        listOfBoardingFromServer.add(boardingimage);
                    }
                    // removeBordingDuplicates(listOfBoardingFromServer);
                    adapterBoardingPassEdit.notifyDataSetChanged();
                } else if (sectionType == Integer.parseInt(Constants.SECTION_ETICKET)) {
                    int totalcount = recycle_eticket_images.getChildCount() + data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES).size();
                    if (totalcount > 5) {
                        alertcommon(getString(R.string.add_trip), getString(R.string.maximum_five_files));
                        return;
                    }
                    recycle_eticket_images.setVisibility(View.VISIBLE);
                    //  removEticketDuplicate(data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES));
                    listEticket.clear();
                    listEticket.addAll(data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES));
                    for (MediaFile file : listEticket) {
                        int i = listEticket.indexOf(file);
                        ModelTripDetails.ETicketImg eticket = new ModelTripDetails().new ETicketImg();
                        eticket.setImage(file.getPath());
                        eticket.setId(null);
                        eticket.setMimetype(file.getMimeType());
                        eticket.setType("1");
                        listOfEticketFromServer.add(eticket);
                        //listOfEticketFromServer.removeIf(n -> (n % 3 == 0));
                    }

                    // removeEticketDuplicates(listOfEticketFromServer);
                    adapterEticketEdit.notifyDataSetChanged();
                }


            } else if (requestCode == CAMERA_REQUEST_CODE && resultCode == RESULT_OK
                    && data != null) {
                if (mBottomSheetDialog != null) {
                    mBottomSheetDialog.cancel();
                }
                if (sectionType == Integer.parseInt(Constants.SECTION_BOARDING)) {
                    int totalcount = listOfBoardingFromServer.size() +
                            data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES).size();
                    if (totalcount > 5) {
                        alertcommon(getString(R.string.add_trip), getString(R.string.maximum_five_files));
                        return;
                    }
                    recycle_boarding_images.setVisibility(View.VISIBLE);
                    listBoarding.clear();
                    listBoarding.addAll(data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES));

                    //listBoarding.addAll(data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES));
                    for (MediaFile file : listBoarding) {
                        ModelTripDetails.BoardImg boardingimage = new ModelTripDetails().new BoardImg();
                        boardingimage.setImage(file.getPath());
                        boardingimage.setId(null);
                        boardingimage.setType("1");
                        boardingimage.setMimetype(file.getMimeType());
                        listOfBoardingFromServer.add(boardingimage);
                    }
                    // removeBordingDuplicates(listOfBoardingFromServer);
                    adapterBoardingPassEdit.notifyDataSetChanged();
                } else if (sectionType == Integer.parseInt(Constants.SECTION_ETICKET)) {
                    int totalcount = recycle_eticket_images.getChildCount() + data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES).size();
                    if (totalcount > 5) {
                        alertcommon(getString(R.string.add_trip), getString(R.string.maximum_five_files));
                        return;
                    }
                    recycle_eticket_images.setVisibility(View.VISIBLE);
                    listEticket.clear();
                    listEticket.addAll(data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES));
                    // listEticket.addAll(data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES));
                    for (MediaFile file : listEticket) {
                        int i = listEticket.indexOf(file);
                        ModelTripDetails.ETicketImg eticket = new ModelTripDetails().new ETicketImg();
                        eticket.setImage(file.getPath());
                        eticket.setId(null);
                        eticket.setType("1");
                        eticket.setMimetype(file.getMimeType());
                        listOfEticketFromServer.add(eticket);
//                        if (!listOfEticketFromServer.contains(eticket)) {
//                            listOfEticketFromServer.add(eticket);
//                        }
//                        if (listEticket.get(i).getId()==file.getId()){
//                            listEticket.remove(file);
//                        }

                    }
                    //removeEticketDuplicates(listOfEticketFromServer);
                    adapterEticketEdit.notifyDataSetChanged();
                }

            }
        } catch (Exception e) {
        }
    }


    public void updateUiTripDetails(ModelTripDetails data) {
        frame_result_lay.setVisibility(View.VISIBLE);
        tripid = data.getData().getId();
        if (data.getData().getFlight_status() != null) {
            if(data.getData().getFlight_status().toLowerCase().equals("delayed")){
                tv_dept_status.setText(data.getData().getFlight_status());
                tv_dept_status.setBackground(getDrawable(R.drawable.bg_flight_red_status));
            }
            else {
                tv_dept_status.setText(data.getData().getFlight_status());
                tv_dept_status.setBackground(getDrawable(R.drawable.bg_flight_dept_status));
            }
          //  tv_dept_status.setText(data.getData().getFlight_status());
        }

        if (data!=null&&data.getData()!=null&&data.getData().getAirline()!=null){
            tv_flight_code.setText(data.getData().getAirline());
            tv_flight_code.setSelected(true);
        }
        if (data!=null&&data.getData()!=null&&data.getData().getFlight_code()!=null){
            tv_airlinename.setText(data.getData().getFlight_code() );
        }
        if (data.getData() != null) {
            if (data.getData().getArr_airport_city() != null &&
                    data.getData().getArr_airport_country() != null) {
                tv_arrival_city.setText(data.getData().getArr_airport_city() + "," +
                        data.getData().getArr_airport_country());
            }
            if (data.getData().getArr_airport_code() != null) {
                tv_arrive_code.setText(data.getData().getArr_airport_code());

            }
//            if (data.getData().getArr_airport_code() != null) {
//                tv_flight_code.setText(data.getData().getFlight_code());
//            }
            if (data.getData().getArr_time() != null) {
                tv_arrive_time.setText(UtilsDefault.timeFormat(data.getData().getArr_time()));

            }
            if (data.getData().getArr_date() != null) {
                tv_arrive_date.setText(UtilsDefault.dateFormat(data.getData().getArr_date() + " 01:34:01"));

            }
            if (data.getData().getArr_terminal() != null) {
                tv_arrival_terminal.setText(getString(R.string.text_terminal) + " " + data.getData().getArr_terminal());

            }

        }

        // tv_dept_terminal.setText(R.string.text_terminal+" "+data.getData().getDeparture().getAirport().getAirportId().getAirportCode());
        //   tv_dept_terminal.setText(R.string.text_terminal+" "+data.getData().getDeparture().getAirport().getTerminal());
        //   UtilsDefault.updateSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));

        if (data.getData().getDep_terminal() != null) {
            tv_dept_terminal.setText(getString(R.string.text_terminal) + " " + data.getData().getDep_terminal());

        }
        if (data.getData().getDep_date() != null) {
            tv_dept_date.setText(UtilsDefault.dateFormat(data.getData().getDep_date() + " 01:34:01"));

        }
        if (data.getData().getDep_time() != null) {
            tv_dept_time.setText(UtilsDefault.timeFormat(data.getData().getDep_time()));

        }
        if (data.getData().getDep_airport_city() != null && data.getData().getDep_airport_code() != null) {
            tv_dept_city.setText(data.getData().getDep_airport_city() + "," + data.getData().getDep_airport_country());

        }
        if (data.getData().getDep_airport_code() != null) {
            tv_dept_code.setText(data.getData().getDep_airport_code());

        }

        if (data.getData().getDepGate()==null||data.getData().getDepGate().equals("")){
// holder.tv_arrval_terminal
        }
        else {
            tv_dept_terminal.append(String.valueOf(" "+data.getData().getDepGate()));
        }

        if (data.getData().getArrGate()==null||data.getData().getArrGate().equals("")){
// holder.tv_arrval_terminal
        }
        else {
            tv_arrival_terminal.append(
                    String.valueOf(" "+data.getData().getArrGate()));
        }

        if (data.getData().getDep_time() != null && data.getData().getArr_time() != null) {
            try {
                if (data.getData().getDep_time() != null &&
                        data.getData().getArr_time() != null) {
                    tv_travel_time.setText(UtilsDefault.differentBetweenTime(data.getData().getDep_date()+" "+data.getData().getDep_time(),
                            data.getData().getArr_date()+" "+data.getData().getArr_time()));
                }
            } catch (Exception e) {
                Toast.makeText(ActivityEditTrip.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        }
//        if (data.getData()!=null&&data.getData().getDep_delay()!=null&&
//                !data.getData().getDep_delay().equals("0")){
////            dep_status.setVisibility(View.VISIBLE);
////            tv_dep_delay.setVisibility(View.VISIBLE);
//            tv_dep_delay.setText(getString(R.string.delayed_by)+" "+data.getData().getDep_delay());
//            tv_dept_status.setText(getString(R.string.dep_delay)+" "+data.getData().getDep_delay());
//            tv_dept_status.setBackground(getDrawable(R.drawable.bg_flight_red_status));
//
//        }
//        if (data.getData()!=null&&data.getData().getArr_delay()!=null&&!
//                data.getData().getArr_delay().equals("0")){
////            tv_arr_status.setVisibility(View.VISIBLE);
////            tv_arr_delay.setVisibility(View.VISIBLE);
//            tv_arr_delay.setText(getString(R.string.delayed_by)+" "+data.getData().getArr_delay());
//            tv_dept_status.setText(getString(R.string.arr_delay)+" "+data.getData().getArr_delay());
//            tv_dept_status.setBackground(getDrawable(R.drawable.bg_flight_red_status));
//
//        }
        boolean isdeparted = false;
        if (data.getData().getAct_dep_date() != null &&
                data.getData().getAct_dep_time() != null &&
                !data.getData().getAct_dep_date().equals("") &&
                !data.getData().getAct_dep_time().equals("")) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date newDate = null;
            try {
                newDate = format.parse(data.getData().getAct_dep_date() + " " +
                        data.getData().getAct_dep_time());
                if (Calendar.getInstance().getTime().after(newDate)) {
                    isdeparted = true;
                    if (data == null || data.getData().getDep_delay() == null
                           ) {
                        //   holder.dep_status.setVisibility(View.VISIBLE);
                        //   holder.tv_dep_delay.setVisibility(View.VISIBLE);
                    } else {
                        if ( !data.getData().getDep_delay().equals("0")){
                            tv_dept_status.setText(getString(R.string.dep_delayed) + " " +
                                    data.getData().getDep_delay());
                            tv_dept_status.setBackground(getDrawable(R.drawable.bg_flight_red_status));
                        }

                    }

                } else {
                    isdeparted = false;
                    if (data == null || data.getData().getDep_delay() == null) {
                        // holder.dep_status.setVisibility(View.VISIBLE);
                        //holder.tv_dep_delay.setVisibility(View.VISIBLE);
                    } else {
                        if (! data.getData().getDep_delay().equals("0")){
                            tv_dept_status.setText(getString(R.string.dep_delay) + " " +
                                    data.getData().getDep_delay());
                            tv_dept_status.setBackground(getDrawable(R.drawable.bg_flight_red_status));
                        }


                        // tv_dep_actualtime_head.setText(getString(R.string.estimated_time));
                    }

                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }


        // holder.tv_dep_delay.setText(mContext.getString(R.string.delayed_by) + " " + data.getDep_delay());

//
//        if (data!= null && data.getArr_delay() != null && !
//                data.getArr_delay().equals("0")) {
//            holder.tv_arr_status.setVisibility(View.VISIBLE);
//            holder.tv_arr_delay.setVisibility(View.VISIBLE);
        if (data.getData().getAct_arr_date() != null &&
                data.getData().getAct_arr_time() != null &&
                !data.getData().getAct_arr_date().equals("") &&
                !data.getData().getAct_arr_time().equals("")) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date newDate = null;


            try {
                newDate = format.parse(data.getData().getAct_arr_date() + " " +
                        data.getData().getAct_arr_time());
                if (!Calendar.getInstance().getTime().after(newDate)) {
                    //tv_arr_actualtime_head.setText(mContext.getString(R.string.estimated_time));
                }
                if (Calendar.getInstance().getTime().after(newDate)) {
                    if (data == null || data.getData().getArr_delay() == null
                    ) {
//                        tv_arr_status.setVisibility(View.VISIBLE);
//                        holder.tv_arr_delay.setVisibility(View.VISIBLE);
                    } else {
                        if (! data.getData().getArr_delay().equals("0")){
                            tv_dept_status.setText(getString(R.string.arr_delayed) + " " +
                                    data.getData().getArr_delay());
                            tv_dept_status.setBackground(getDrawable(R.drawable.bg_flight_red_status));
                        }

                    }

                } else {
                    if (data == null ||data.getData().getArr_delay() == null) {
                        //holder.tv_arr_status.setVisibility(View.VISIBLE);
                        // holder.tv_arr_delay.setVisibility(View.VISIBLE);
                    } else {
                        if (isdeparted) {
                            if (! data.getData().getArr_delay().equals("0")){
                                tv_dept_status.setText(getString(R.string.arr_delay) + " " +
                                        data.getData().getArr_delay());
                                tv_dept_status.setBackground(getDrawable(R.drawable.bg_flight_red_status));
                            }

                        }

                        //  holder.tv_arr_actualtime_head.setText(getString(R.string.estimated_time));
                    }

                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }


        if (data.getData().getArr_delay() != null && !data.getData().getArr_delay().equals("0")) {
//            holder. tv_arr_delay.setText(mContext.getString(R.string.delayed_by) + " " + data.getArr_delay());
//            holder.tv_arr_delay.setTextColor(mContext.getResources().getColor(R.color.red_status));

        }
        try {
            if(data.getData().getFlight_status().toLowerCase().equals(getResources().getString(R.string.cancelled))){
                tv_dept_status.setBackground(getResources().getDrawable(R.drawable.bg_flight_red_status));
                tv_dept_status.setText(data.getData().getFlight_status());
//                holder.tv_dep_delay.setText(mContext.getResources().getString(R.string.cancelled));
//                holder.tv_arr_delay.setText(mContext.getResources().getString(R.string.cancelled));
            }
        }
        catch (Exception e){

        }

        fetchTripDetailsUsInputMOdel(data);
    }

    public void fetchTripDetailsUsInputMOdel(ModelTripDetails data) {
        if (data != null && data.getData() != null && data.getStatus() == Constants.STATUS_200) {
            issearchResultSucess = true;
            modelInputAddtrip = new ModelInputAddtrip();
            modelInputAddtrip.setAirlineName(UtilsDefault.checkNull(data.getData().getAirline()));
            modelInputAddtrip.setAirport_id(Integer.parseInt(UtilsDefault.checkNull(UtilsDefault.
                    getSharedPreferenceValue(Constants.AIRPORT_ID))));
            modelInputAddtrip.setUser_id(Integer.parseInt(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)));
            modelInputAddtrip.setFlight_code(UtilsDefault.checkNull(UtilsDefault.checkNull(data.getData().getFlight_code())));
            modelInputAddtrip.setDep_airport_code(UtilsDefault.checkNull(data.getData().getDep_airport_code()));
            modelInputAddtrip.setArr_airport_code(UtilsDefault.checkNull(data.getData().getArr_airport_code()));
            modelInputAddtrip.setArr_airport_city(UtilsDefault.checkNull(data.getData().getArr_airport_city()));
            modelInputAddtrip.setDep_airport_city(UtilsDefault.checkNull(data.getData().getDep_airport_city()));
            modelInputAddtrip.setArr_airport_country(UtilsDefault.checkNull(data.getData().getArr_airport_country()));
            modelInputAddtrip.setDep_airport_country(UtilsDefault.checkNull(data.getData().getDep_airport_country()));
            if (data.getData().getDep_terminal() != null && !data.getData().getDep_terminal().equals("")) {
                modelInputAddtrip.setDep_terminal((String.valueOf(data.getData().getDep_terminal())));
            }
            if (data.getData().getArr_terminal() != null && !data.getData().getArr_terminal().equals("")) {
                modelInputAddtrip.setArr_terminal(String.valueOf(data.getData().getArr_terminal()));
            }
            modelInputAddtrip.setDep_date(UtilsDefault.checkNull(data.getData().getDep_date()));
            modelInputAddtrip.setArr_date(UtilsDefault.checkNull(data.getData().getArr_date()));
            modelInputAddtrip.setDep_time(UtilsDefault.checkNull(data.getData().getDep_time()));
            modelInputAddtrip.setArr_time(UtilsDefault.checkNull(data.getData().getArr_time()));
            modelInputAddtrip.setId(tripid);
            if (data.getData().getArrGate()!=null){
                modelInputAddtrip.setArrGate(data.getData().getArrGate());

            }
            if (data.getData().getDepGate()!=null){
                modelInputAddtrip.setDepGate(data.getData().getDepGate());
            }
            modelInputAddtrip.setFlight_status(UtilsDefault.checkNull(data.getData().getFlight_status()));

           // search_lay.setVisibility(View.VISIBLE);
            nest_result_lay.setVisibility(View.VISIBLE);
        } else {
            issearchResultSucess = false;
        }
    }

    @Override
    public void intizializeView() {
        search_lay.setVisibility(View.GONE);
        if (getIntent().getSerializableExtra("data") != null) {
            btn_add_trip.setText(R.string.text_save);
            modelTripDetails = (ModelTripDetails) getIntent().getSerializableExtra("data");
            listOfBoardingFromServer.clear();
            listOfBoardingFromServer.addAll(modelTripDetails.getBoardImg());
            listOfEticketFromServer.clear();
            listOfEticketFromServer.addAll(modelTripDetails.geteTicketImg());

            recycle_boarding_images.setVisibility(View.VISIBLE);
            recycle_boarding_images.setVisibility(View.VISIBLE);
            adapterBoardingPassEdit = new AdapterBoardingPass(ActivityEditTrip.this,
                    listOfBoardingFromServer, null, 1);

            recycle_boarding_images.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,
                    false));
            recycle_boarding_images.setAdapter(adapterBoardingPassEdit);
            adapterBoardingPassEdit.setOnIMageRemove(new OnImageRemoveListener() {
                @Override
                public void OnIMageremove(int pos, View view, int Type) {
                    if (listOfBoardingFromServer.get(pos).getType() == null) {
                        delete(1, String.valueOf(listOfBoardingFromServer.get(pos).getId()), pos);
                    } else {
                        listOfBoardingFromServer.remove(pos);
                        adapterBoardingPassEdit.notifyItemRemoved(pos);
                    }
//                recycle_boarding_images.setVisibility(View.VISIBLE);
//                listOfBoardingFromServer.remove(pos);
//                adapterBoardingPassEdit.notifyItemChanged(pos);
                    if (listOfBoardingFromServer.size() == 0) {
                        recycle_boarding_images.setVisibility(View.GONE);
                    }
                }
            });


            adapterEticketEdit = new AdapterBoardingPass(ActivityEditTrip.this, null,
                    listOfEticketFromServer, 1);
            adapterEticketEdit.setOnIMageRemove(new OnImageRemoveListener() {
                @Override
                public void OnIMageremove(int pos, View view, int Type) {

                    if (listOfEticketFromServer.get(pos).getType() == null) {
                        delete(2, String.valueOf(listOfEticketFromServer.get(pos).getId()), pos);

                    } else {
                        listOfEticketFromServer.remove(pos);
                        adapterEticketEdit.notifyItemRemoved(pos);

                    }
                    if (listOfEticketFromServer.size() == 0) {
                        recycle_eticket_images.setVisibility(View.GONE);
                    }

//                recycle_eticket_images.setVisibility(View.VISIBLE);
//                listOfEticketFromServer.remove(pos);
//                adapterEticketEdit.notifyItemChanged(pos);

                }
            });
            recycle_eticket_images.setVisibility(View.VISIBLE);
            recycle_eticket_images.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,
                    false));
            recycle_eticket_images.setAdapter(adapterEticketEdit);


        }
        updateUiTripDetails(modelTripDetails);

    }
    @Override
    public void showDetailsScreen(ModelNewFlightStatus data) {
        if (data!=null&&data.getData()!=null&&data.getData().getAirline()!=null){
            tv_flight_code.setText(data.getData().getAirline());
            tv_flight_code.setSelected(true);
        }
        if (data!=null&&data.getData()!=null&&data.getData().getAirlineCode()!=null&&
                data!=null&&data.getData()!=null&&data.getData().getFlightNumber()!=null){
            tv_airlinename.setText(data.getData().getAirlineCode() + data.getData().getFlightNumber());
        }
        if (data.getData()!=null&&data.getData().getFlightStatus()!=null&&
                data.getData().getFlightStatus().toLowerCase().equals(Constants.DELAYED)){
            if(data.getData().getFlightStatus().toLowerCase().equals("delayed")){
                tv_dept_status.setText(data.getData().getFlightStatus());
                tv_dept_status.setBackground(getDrawable(R.drawable.bg_flight_red_status));
            }
            else {
                tv_dept_status.setText(data.getData().getFlightStatus());
                tv_dept_status.setBackground(getDrawable(R.drawable.bg_flight_dept_status));
            }
        }
        else {
            tv_dept_status.setBackground(getDrawable(R.drawable.bg_flight_dept_status));
        }

        if (data.getData().getDepGate()==null||data.getData().getDepGate().equals("")){
// holder.tv_arrval_terminal
        }
        else {
            tv_dept_terminal.append(
                    String.valueOf(" "+data.getData().getDepGate()));
        }

        if (data.getData().getArrGate()==null||data.getData().getArrGate().equals("")){
// holder.tv_arrval_terminal
        }
        else {
            tv_arrival_terminal.append(
                    String.valueOf(" "+data.getData().getArrGate()));
        }

//        if (data.getData()!=null&&data.getData().getDep_delay()!=null&&
//                !data.getData().getDep_delay().equals("0")){
////            dep_status.setVisibility(View.VISIBLE);
////            tv_dep_delay.setVisibility(View.VISIBLE);
//            tv_dep_delay.setText(getString(R.string.delayed_by)+" "+data.getData().getDep_delay());
//            tv_dept_status.setText(getString(R.string.dep_delay)+" "+data.getData().getDep_delay());
//            tv_dept_status.setBackground(getDrawable(R.drawable.bg_flight_red_status));
//
//        }
//        if (data.getData()!=null&&data.getData().getArr_delay()!=null&&!
//                data.getData().getArr_delay().equals("0")){
////            tv_arr_status.setVisibility(View.VISIBLE);
////            tv_arr_delay.setVisibility(View.VISIBLE);
//            tv_arr_delay.setText(getString(R.string.delayed_by)+" "+data.getData().getArr_delay());
//            tv_dept_status.setText(getString(R.string.arr_delay)+" "+data.getData().getArr_delay());
//            tv_dept_status.setBackground(getDrawable(R.drawable.bg_flight_red_status));
//
//        }
    }

    @Override
    protected int getlayout() {
        return R.layout.activity_add_trip;
    }

    public void delete(int from, String id, int pos) {
        shwProgress();
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(this, R.string.error_no_net,
                    Toast.LENGTH_SHORT).show();
            return;
        }
        aviapi.deleteDocuments(id, UtilsDefault.getSharedPreferenceValue(Constants.USER_ID), tripid).
                enqueue(new Callback<CommonMsgModel>() {
            @Override
            public void onResponse(Call<CommonMsgModel> call,
                                   Response<CommonMsgModel> response) {
                hideprogress();


                if (response.body() != null) {
                    CommonMsgModel data = response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    UtilsDefault.updateFcmSharedPreference(Constants.API_KEY, data.getJwt_token());
                    if (data.getStatus() == Constants.STATUS_200) {
                        if (from == 1) {
                            listOfBoardingFromServer.remove(pos);
                            adapterBoardingPassEdit.notifyItemRemoved(pos);
                        } else {
                            listOfEticketFromServer.remove(pos);
                            adapterEticketEdit.notifyItemRemoved(pos);
                        }


                    } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                        delete(from, id, pos);
                    } else {
                        Toast.makeText(ActivityEditTrip.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ActivityEditTrip.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CommonMsgModel> call, Throwable t) {
                hideprogress();
                Toast.makeText(ActivityEditTrip.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }


    // its for Edit trip
    @Override
    public void addTrip(ModelInputAddtrip modelInputAddtrip) {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityEditTrip.this, R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;
        }
        shwProgress();
        if (modelTripDetails.getBoardImg() != null && listOfBoardingFromServer.size() != 0) {
            for (ModelTripDetails.BoardImg data : listOfBoardingFromServer) {
                if (data.getId() != null) {
                    selectedBordingPass.append(data.getId() + ",");
                }

            }
            modelInputAddtrip.setBoardingPass(selectedBordingPass);

        }
        if (modelTripDetails.geteTicketImg() != null && listOfEticketFromServer.size() != 0) {
            for (ModelTripDetails.ETicketImg data : listOfEticketFromServer) {
                // selectedEticket.append(data.getId());
                if (data.getId() != null) {
                    selectedEticket.append(data.getId() + ",");
                }
            }
            modelInputAddtrip.seteTickets(selectedEticket);
        }
        modelInputAddtrip.setTime_zone(timeZone.getID());
        aviapi.editMyTrip(modelInputAddtrip).enqueue(new Callback<ModelAddTrip>() {
            @Override
            public void onResponse(Call<ModelAddTrip> call, Response<ModelAddTrip> response) {
                hideprogress();
                hideKeyboard();

                if (response.body() != null) {
                    ModelAddTrip data = response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus() == Constants.STATUS_200) {
                        Toast.makeText(ActivityEditTrip.this, getString(R.string.trip_updated), Toast.LENGTH_SHORT).show();
                        finish();
                        //alertcommon(getString(R.string.add_my_tips), data.getMessage());

                        //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                    } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                        addTrip(modelInputAddtrip);
                    } else {
                        btn_add_trip.setEnabled(true);
                        // Toast.makeText(ActivityEditTrip.this, getString(R.string.uploading), Toast.LENGTH_SHORT).show();
                        Toast.makeText(ActivityEditTrip.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                       // finish();
                        //alertcommon(getString(R.string.add_my_tips),data.getMessage());
                        //  Toast.makeText(ActivityFlightSchedule.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    btn_add_trip.setEnabled(true);
                    Toast.makeText(ActivityEditTrip.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ModelAddTrip> call, Throwable t) {

                hideprogress();
                btn_add_trip.setEnabled(true);
                hideKeyboard();
                Log.d("failure", "onFailure: " + t.getMessage());
                Toast.makeText(ActivityEditTrip.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void closeCommonAlert() {
        commonalert.dismiss();
    }

    @Override
    public void onClick(View v) {
        Log.d("onClick", "onClick: ");
        if (v == btn_search) {
            String airport = ed_airline.getText().toString().trim();
            String fligntnum = ed_flightnumber.getText().toString().trim();
//            if (airport.equals("")) {
//                ed_airline.setError(getString(R.string.this_feild_empty));
//                ed_airline.requestFocus();
//            }

            if (fligntnum.equals("")) {
                ed_flightnumber.setError(getString(R.string.this_feild_empty));
                ed_flightnumber.requestFocus();
            } else if (selectedDate.equals("")) {
                Toast.makeText(this, R.string.select_date, Toast.LENGTH_SHORT).show();
            } else {
                getflightStatus(airport, fligntnum, selectedDate.trim());
            }
        }
        if (v == date_lay) {
            new DatePickerDialog(ActivityEditTrip.this, this, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show();
        }
        if (v == img_add_Bording) {
            if (listOfBoardingFromServer.size() >= Constants.MAX_FILE_SELECT) {
                alertcommon(getString(R.string.add_trip), getString(R.string.maximum_five_files));
                return;
            }
            sectionType = Integer.parseInt(Constants.SECTION_BOARDING);
            int maxSelect;
            if (listOfBoardingFromServer.size() == 0) {
                maxSelect = Constants.MAX_FILE_SELECT;

            } else {
                maxSelect = Constants.MAX_FILE_SELECT - listOfBoardingFromServer.size();
            }

            showBottomFilePIcker(maxSelect);
        }
        if (v == img_add_etick) {
            if (listOfEticketFromServer.size() >= Constants.MAX_FILE_SELECT) {
                alertcommon(getString(R.string.add_trip),
                        getString(R.string.maximum_five_files));
                return;
            }
            sectionType = Integer.parseInt(Constants.SECTION_ETICKET);
            int maxSelect;
            if (listOfEticketFromServer.size() == 0) {
                maxSelect = Constants.MAX_FILE_SELECT;
            } else {
                maxSelect = Constants.MAX_FILE_SELECT - listOfEticketFromServer.size();
            }
            showBottomFilePIcker(maxSelect);
        }
        if (v == btn_add_trip && btn_add_trip.getText() == getString(R.string.text_save)) {
            if (!issearchResultSucess) {
                alert(getString(R.string.please_select_trip));
                return;
            }
            if (listOfBoardingFromServer.size() != 0 && checkBoardingNewIMageAvailable()) {
                btn_add_trip.setEnabled(false);
                Toast.makeText(this, getString(R.string.uploading), Toast.LENGTH_SHORT).show();
                uploadBordingFile(listOfBoardingFromServer, Constants.SECTION_BOARDING, modelInputAddtrip);
            } else if (listOfEticketFromServer.size() != 0 && checkEticketNewIMageAvailable()) {
                btn_add_trip.setEnabled(false);
                Toast.makeText(this, getString(R.string.uploading), Toast.LENGTH_SHORT).show();
                selectedEticket.setLength(0);
                uploadEticketFile(listOfEticketFromServer, Constants.SECTION_ETICKET, modelInputAddtrip);
            } else {
                addTrip(modelInputAddtrip);

            }


        }


    }

    public boolean checkBoardingNewIMageAvailable() {
        boolean isdataavail = false;
        for (ModelTripDetails.BoardImg data : listOfBoardingFromServer) {
            if (data.getType() != null) {
                isdataavail = true;
                return isdataavail;
            }
        }
        return isdataavail;
    }

    public boolean checkEticketNewIMageAvailable() {
        boolean isdataavail = false;
        for (ModelTripDetails.ETicketImg data : listOfEticketFromServer) {
            if (data.getType() != null) {
                isdataavail = true;
                return isdataavail;
            }
        }
        return isdataavail;
    }


//    public void deletelistBoarding(String path) {
//        for (MediaFile file : listBoarding) {
//            if (file.getPath().equals(path)) {
//                listBoarding.remove(file);
//            }
//        }
//    }
//
//    public void deletelistEticket(String path) {
//        for (MediaFile file : listEticket) {
//            if (file.getPath().equals(path)) {
//                listEticket.remove(file);
//            }
//        }
//    }


    public void uploadBordingFile(List<ModelTripDetails.BoardImg> filelist, String section,
                                  ModelInputAddtrip modelInputAddtrip) {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityEditTrip.this,
                    R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;
        }
        shwProgress();

        RequestBody sections = RequestBody.create(
                MediaType.parse("text/plain"),
                section);

        RequestBody uid = RequestBody.create(
                MediaType.parse("text/plain"),
                UtilsDefault.getSharedPreferenceValue(Constants.USER_ID));
        Log.d("section", "uploadMultiFile: " + section);

        aviapi.uploadMultiFile(getBoardingMultiPart(filelist), sections, uid).
                enqueue(new Callback<ModelAddDocuments>() {
            @Override
            public void onResponse(Call<ModelAddDocuments> call, Response<ModelAddDocuments> response) {
                hideprogress();
                ModelAddDocuments data = response.body();
                if (data.getStatus() == Constants.STATUS_200) {
                    if (section.equals(Constants.SECTION_BOARDING)) {
                        selectedBordingPass = new StringBuilder();
                        for (ModelAddDocuments.Datum documents : data.getData()) {
                            selectedBordingPass.append(String.valueOf(documents.getId()) + ",");
                        }
                        modelInputAddtrip.setBoardingPass(selectedBordingPass);
                        if (listOfEticketFromServer.size() != 0 && checkEticketNewIMageAvailable()) {
                            btn_add_trip.setEnabled(false);
                            Toast.makeText(ActivityEditTrip.this, getString(R.string.uploading), Toast.LENGTH_SHORT).show();

                            uploadEticketFile(listOfEticketFromServer, Constants.SECTION_ETICKET, modelInputAddtrip);
                        } else {
                            addTrip(modelInputAddtrip);
                        }
                    }

                }
                else if (data.getStatus()==Constants.API_TOKENEXPIRY_STATUS){
                    uploadBordingFile(filelist,section,modelInputAddtrip);
                }

                else {
                    btn_add_trip.setEnabled(true);
                    alert(data.getMessage());
                    //  Toast.makeText(ActivityMyDocsDetails.this, "Success " + data.getMessage(), Toast.LENGTH_LONG).show();
                    Log.d("failure", "onResponse: " + data.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ModelAddDocuments> call, Throwable t) {
                hideprogress();
                btn_add_trip.setEnabled(true);
                alert(getString(R.string.server_error));
            }
        });


    }

    public void uploadEticketFile(List<ModelTripDetails.ETicketImg> filelist, String section,
                                  ModelInputAddtrip modelInputAddtrip) {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityEditTrip.this, R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;
        }
        shwProgress();

        RequestBody sections = RequestBody.create(
                MediaType.parse("text/plain"),
                section);

        RequestBody uid = RequestBody.create(
                MediaType.parse("text/plain"),
                UtilsDefault.getSharedPreferenceValue(Constants.USER_ID));
        Log.d("section", "uploadMultiFile: " + section);

        aviapi.uploadMultiFile(getEticketMultiPart(filelist), sections, uid).enqueue(new Callback<ModelAddDocuments>() {
            @Override
            public void onResponse(Call<ModelAddDocuments> call, Response<ModelAddDocuments> response) {
                hideprogress();
                ModelAddDocuments data = response.body();
                if (data.getStatus() == Constants.STATUS_200) {
                    if (listOfEticketFromServer.size() != 0) {
                        selectedEticket = new StringBuilder();
                        selectedEticket.setLength(0);
                        for (ModelAddDocuments.Datum documents : data.getData()) {
                            selectedEticket.append(String.valueOf(documents.getId()) + ",");
                        }
                        modelInputAddtrip.seteTickets(selectedEticket);
                    }
                    addTrip(modelInputAddtrip);
                }
                else if (data.getStatus()==Constants.API_TOKENEXPIRY_STATUS){
                    uploadEticketFile(filelist,section,modelInputAddtrip);
                }


                else {
                    btn_add_trip.setEnabled(true);
                    alert(data.getMessage());
                    //  Toast.makeText(ActivityMyDocsDetails.this, "Success " + data.getMessage(), Toast.LENGTH_LONG).show();
                    Log.d("failure", "onResponse: " + data.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ModelAddDocuments> call, Throwable t) {
                hideprogress();
                btn_add_trip.setEnabled(true);
                alert(getString(R.string.server_error));
            }
        });


    }


    public List<MultipartBody.Part> getBoardingMultiPart(List<ModelTripDetails.BoardImg> upFileList) {
        List<MultipartBody.Part> parts = new ArrayList<>();
        for (int i = 0; i < upFileList.size(); i++) {
            if (upFileList.get(i).getType() != null) {
                File file = new File(upFileList.get(i).getImage());

                Log.d("filepath", "uploadMultiFile: " + upFileList.get(i).getImage());

//                Uri uri;
//                if (Build.VERSION.SDK_INT >= 23) {
//                    uri = FileProvider.getUriForFile(getApplicationContext(),
//                            BuildConfig.APPLICATION_ID + ".provider", file);
//                } else {
//                    //uri = Uri.parse(file.getAbsolutePath());
//                    uri = Uri.fromFile(file);
//                }
//                byte[] bytes = new byte[0];
//                try {
//                    bytes =compress(uri);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
                // MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
                // String ext=mimeTypeMap.getExtensionFromMimeType(file.getAbsoluteFile());
                //String fileName = new Date().getTime() +MimeTypeMap.getFileExtensionFromUrl(file.toString()) ;
                // Log.d("filenames", "getBoardingMultiPart: "+fileName);
                String fileNames = new Date().getTime() +
                        file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("."));


                MultipartBody.Part fileParts;
                if (upFileList.get(i).getMimetype().contains("image")) {
                    Uri uri;
                    if (Build.VERSION.SDK_INT >= 23) {
                        uri = FileProvider.getUriForFile(getApplicationContext(),
                                BuildConfig.APPLICATION_ID + ".provider", file);
                    } else {
                        //uri = Uri.parse(file.getAbsolutePath());
                        uri = Uri.fromFile(file);
                    }
                    byte[] bytes = new byte[0];
                    try {
                        bytes = compress(uri,file);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    RequestBody fileBody = null;
                    fileBody = RequestBody.create(MediaType.parse(upFileList.get(i).getMimetype()),
                            bytes);
                    fileParts = MultipartBody.Part.createFormData("file[]",
                            fileNames, fileBody);

                } else {
                    RequestBody fileBody = null;
                    // fileBody = RequestBody.create(MediaType.parse(getContentResolver().getType(uri)),file);
                    fileBody = RequestBody.create(MediaType.parse(upFileList.get(i).getMimetype()), file);

                    fileParts = MultipartBody.Part.createFormData("file[]",
                            fileNames, fileBody);
                    String fileNam = new Date().getTime() +
                            file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("."));
                    Log.d("fileNam", "diness: " + fileNam);

                    // fileParts = MultipartBody.Part.createFormData("file[]", file.getName(), fileBody);
                }


                parts.add(fileParts);
            }

        }
        return parts;
    }
    @Override
    public void onBackPressed() {
        if (!btn_add_trip.isEnabled()){
            Toast.makeText(this, getString(R.string.processing_image), Toast.LENGTH_SHORT).show();
        }
        else {
            super.onBackPressed();
        }
    }

    public List<MultipartBody.Part> getEticketMultiPart(List<ModelTripDetails.ETicketImg> upFileList) {
        List<MultipartBody.Part> parts = new ArrayList<>();
        for (int i = 0; i < upFileList.size(); i++) {
            if (upFileList.get(i).getType() != null) {
                File file = new File(upFileList.get(i).getImage());
                Log.d("filepath", "uploadMultiFile: " + upFileList.get(i).getImage());

                String fileNames = new Date().getTime() +
                        file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("."));

                MultipartBody.Part fileParts;
                if (upFileList.get(i).getMimetype().contains("image")) {
                    Uri uri;
                    if (Build.VERSION.SDK_INT >= 23) {
                        uri = FileProvider.getUriForFile(getApplicationContext(),
                                BuildConfig.APPLICATION_ID + ".provider", file);
                    } else {
                        uri = Uri.fromFile(file);

                    }
                    byte[] bytes = new byte[0];
                    try {
                        bytes = compress(uri,file);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    RequestBody fileBody = null;
                    fileBody = RequestBody.create(MediaType.parse(upFileList.get(i).getMimetype()),
                            bytes);
                    fileParts = MultipartBody.Part.createFormData("file[]", fileNames, fileBody);
                } else {
                    RequestBody fileBody = null;
                    fileBody = RequestBody.create(MediaType.parse(upFileList.get(i).getMimetype()), file);
                    fileParts = MultipartBody.Part.createFormData("file[]", fileNames, fileBody);
                }

                parts.add(fileParts);
            }

        }
        return parts;
    }
    //    @Override
//    public void OnIMageremove(int pos, View view, int Type) {
//        if (Type==1){
//            delete(Type,listOfBoardingFromServer.get(pos).geti);
//        }
//
//
//    }
}
