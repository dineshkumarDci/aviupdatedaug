package com.dci.avicustomer.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.avicustomer.R;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.models.ModelFloorMap;
import com.dci.avicustomer.retrofit.AVIAPI;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityCommonWebLoader extends
        BaseActivity implements View.OnClickListener {
    @Inject
    public AVIAPI aviapi;
    @BindView(R.id.webview)
    WebView webView;
    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.tv_airport_name)
    TextView tv_airport_name;
    @BindView(R.id.boarding_lay)
    RelativeLayout boarding_lay;
    @BindView(R.id.btn_mapview)
    Button btn_mapview;
    String airport = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        ShoppingApplication.getContext().getComponent().
                inject(this);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        boarding_lay.setVisibility(View.GONE);
        if (getIntent().getStringExtra("data") != null) {
            webView.loadUrl(getIntent().getStringExtra("data"));
            tv_title.setText(getIntent().getStringExtra("title"));
        } else {
            if (UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_NAME) != null) {
                tv_airport_name.setVisibility(View.VISIBLE);
                tv_airport_name.setText(UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_NAME));
                tv_title.setText(R.string.floor_map);
            }
            if (UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_LATITUDE)!=null&&
                    UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_LATITUDE)!=null){
                btn_mapview.setVisibility(View.VISIBLE);
                btn_mapview.setOnClickListener(this);
            }
            boarding_lay.setVisibility(View.VISIBLE);
            getFloorMap();
        }


        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String urlNewString) {
                view.loadUrl(urlNewString);
                // }
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap facIcon) {
                //SHOW LOADING IF IT ISNT ALREADY VISIBLE
            }

            @Override
            public void onPageFinished(WebView view, String url) {
//                webView.loadUrl("javascript:(function() { " +
//                        "document.querySelector('[role=\"toolbar\"]').remove();})()");
                webView.loadUrl("javascript:(function() { " +
                        "document.querySelector('[role=\"toolbar\"]').remove();})()");
                progress.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void getFloorMap() {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityCommonWebLoader.this,
                    R.string.error_no_net,
                    Toast.LENGTH_SHORT).show();
            return;
        }
        shwProgress();
        aviapi.getFloorMap(UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID)).
                enqueue(new Callback<ModelFloorMap>() {
            @Override
            public void onResponse(Call<ModelFloorMap> call, Response<ModelFloorMap> response) {
                hideprogress();
                hideKeyboard();
                if (response.body() != null) {
                    ModelFloorMap data = response.body();
                    if (data.getStatus() == Constants.STATUS_200) {
                        webView.loadUrl(data.getUrl() + "/" + data.getData().get(0).getFloormap());
                        //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                    } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                        getFloorMap();
                    } else {
                        progress.setVisibility(View.GONE);
                        alertcommon(getString(R.string.floor_map), data.getMessage());
                        //  Toast.makeText(ActivityFlightSchedule.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    progress.setVisibility(View.GONE);
                    alertcommon(getString(R.string.floor_map),getString(R.string.server_error));
                   // Toast.makeText(ActivityCommonWebLoader.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ModelFloorMap> call, Throwable t) {
                progress.setVisibility(View.GONE);
                alertcommon(getString(R.string.floor_map),getString(R.string.server_error));
                hideprogress();
                hideKeyboard();
                Log.d("failure", "onFailure: " + t.getMessage());
              //  Toast.makeText(ActivityCommonWebLoader.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected int getlayout() {
        return R.layout.activity_common_webpage;
    }

    @Override
    public void onClick(View v) {
        if (v==btn_mapview){
                Uri gmmIntentUri = Uri.parse("geo:<" +
                        UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_LATITUDE) + ">,<" +
                        UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_LONGITUDE) + ">?q=<" +
                        UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_LATITUDE) + ">,<" +
                        UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_LONGITUDE) + ">(" +
                        UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_NAME) + ")");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);

        }
    }
}
