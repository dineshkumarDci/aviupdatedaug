package com.dci.avicustomer.activities;

import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Base64;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.avicustomer.R;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.models.ModelAviTips;
import com.dci.avicustomer.models.ModelHome;
import com.dci.avicustomer.models.RegisterModel;
import com.dci.avicustomer.retrofit.AVIAPI;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityAviTips extends BaseActivity implements View.OnClickListener {

    @Inject
    public AVIAPI aviapi;
    @BindView(R.id.prevoius_icon)
    ImageView prevoius_icon;
    @BindView(R.id.next_icon)
    ImageView next_icon;
    @BindView(R.id.tv_page_num)
    TextView tv_page_num;
    @BindView(R.id.tv_tips)
    WebView tv_tips;
    List<ModelAviTips.Datum>aviTips=new ArrayList<>();
    int postion=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        ShoppingApplication.getContext().getComponent().inject(this);
        prevoius_icon.setOnClickListener(this);
        next_icon.setOnClickListener(this);
        imgback.setOnClickListener(this);
        tv_title.setText(R.string.title_avi_tips);
        getAviTips();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void getAviTips(){
        if (!UtilsDefault.isOnline()){
            Toast.makeText(ActivityAviTips.this,
                    R.string.error_no_net,
                    Toast.LENGTH_SHORT).show();
            return;
        }
        shwProgress();
        aviapi.aviTIps(UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID)).
                enqueue(new Callback<ModelAviTips>() {
            @Override
            public void onResponse(Call<ModelAviTips> call, Response<ModelAviTips> response) {
                hideprogress();
                if (response.body()!=null){
                    ModelAviTips data=response.body();
//                    String header = response.headers().get(getString(R.string.header_find_key));
//                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus()== Constants.STATUS_200){
                        aviTips.clear();
                        aviTips.addAll(data.getData());
                        settipsData();
                        //UtilsDefault.updateSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                    }
                    else if (data.getStatus()==Constants.API_TOKENEXPIRY_STATUS){
                        getAviTips();
                    }
                    else {
                        Toast.makeText(ActivityAviTips.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Toast.makeText(ActivityAviTips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ModelAviTips> call, Throwable t) {
                hideprogress();
                Toast.makeText(ActivityAviTips.this,
                        R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void settipsData(){
        tv_page_num.setText(postion+1+" of "+String.valueOf(aviTips.size()));



          //  byte[]base64= Base64.decode(aviTips.get(postion).getDescription(), Base64.DEFAULT);
          //  String html = new String(base64, "UTF-8");
            tv_tips.getSettings().setJavaScriptEnabled(true);
            tv_tips.loadDataWithBaseURL("", aviTips.get(postion).getDescription(), "text/html", "UTF-8", "");

//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                tv_tips.load(Html.fromHtml(html, Html.FROM_HTML_MODE_COMPACT));
//            } else {
//                tv_tips.setText(Html.fromHtml(html));
//            }

       // tv_tips.setText(aviTips.get(postion).getDescription());

    }


    @Override
    protected int getlayout() {
        return R.layout.activity_avi_tips;
    }

    @Override
    public void onClick(View v) {
        if (v==next_icon) {
            if (aviTips.size() != 0 && postion
                    != aviTips.size()-1) {
                postion++;
                settipsData();
            }
        }
        if (v == prevoius_icon) {
            if (aviTips.size() != 0 && postion != 0) {
                postion--;
                settipsData();
            }
        }
        if (v==imgback){
            onBackPressed();
        }
    }
}
