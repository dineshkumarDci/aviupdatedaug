package com.dci.avicustomer.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.avicustomer.R;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.models.CommonMsgModel;
import com.dci.avicustomer.retrofit.AVIAPI;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityForgotPassword extends BaseActivity implements View.OnClickListener {
    @Inject
    public AVIAPI aviapi;
    @BindView(R.id.tv_sign_in)
    TextView tv_sign_in;
    @BindView(R.id.ed_emailid)
    EditText ed_emailid;
    @BindView(R.id.btn_reset_password)
    Button btnResetPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        tv_sign_in.setOnClickListener(this);
        ShoppingApplication.getContext().getComponent().inject(this);
        btnResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mailid = ed_emailid.getText().toString().trim();
                if (mailid.equals("")) {
                    ed_emailid.requestFocus();
                    ed_emailid.setError(getString(R.string.error_emailid));
                }
                else if (!UtilsDefault.isEmailValid(mailid)) {
                    ed_emailid.requestFocus();
                    ed_emailid.setError(getString(R.string.error_valid_email));
                } else {
                    forgotPassword(mailid);
                }
            }
        });
        //  setContentView(R.layout.activity_forgot_password);

    }
    @Override
    public void alert(String msg) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(msg);
        alertDialogBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        Intent intent = new Intent(ActivityForgotPassword.this, ActivityLogin.class);
                        startActivity(intent);
                        finish();


                    }
                });


        final AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.show();
    }

    public void forgotPassword(String email) {
        if (!UtilsDefault.isOnline()){
            Toast.makeText(ActivityForgotPassword.this, R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;
        }
        shwProgress();
        aviapi.forgotPass(email).enqueue(new Callback<CommonMsgModel>() {
            @Override
            public void onResponse(Call<CommonMsgModel> call,
                                   Response<CommonMsgModel> response) {
                hideprogress();
                if (response.body() != null) {
                    CommonMsgModel data = response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus() == Constants.STATUS_200) {
                        alert(data.getMessage());

                        // Toast.makeText(ActivityForgotPassword.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                        //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));


                    }
                    else if (data.getStatus()==Constants.API_TOKENEXPIRY_STATUS){
                        forgotPassword(email);
                    }

                    else {
                        alert( data.getMessage());
                     //   Toast.makeText(ActivityForgotPassword.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ActivityForgotPassword.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<CommonMsgModel> call, Throwable t) {
                hideprogress();

                Toast.makeText(ActivityForgotPassword.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected int getlayout() {
        return R.layout.activity_forgot_password;
    }

    @Override
    public void onClick(View v) {
        if (v == tv_sign_in) {
            finish();
            startActivity(new Intent(ActivityForgotPassword.this, ActivityLogin.class));
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        startActivity(new Intent(ActivityForgotPassword.this, ActivityLogin.class));
    }
}
