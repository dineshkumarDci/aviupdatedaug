package com.dci.avicustomer.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.avicustomer.R;
import com.dci.avicustomer.adapter.AdapterUpdatedFlightStatus;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.models.ModelNewFlightStatus;
import com.dci.avicustomer.models.ModelUpdatedFlightStatus;
import com.dci.avicustomer.retrofit.AVIAPI;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityUpdatedFlightStatus extends BaseActivity implements View.OnClickListener,
        DatePickerDialog.OnDateSetListener {
    @BindView(R.id.btn_search)
    Button btn_search;
    @BindView(R.id.search_lay)
    LinearLayout search_lay;
    @BindView(R.id.ed_flightnumber)
    EditText ed_flightnumber;
    @BindView(R.id.ed_airline)
    EditText ed_airline;
    @BindView(R.id.tv_selected_date)
    TextView tv_selected_date;
    @BindView(R.id.tv_total_flight_number)
    TextView tv_total_flight_number;
    @BindView(R.id.date_lay)
    RelativeLayout date_lay;
    @BindView(R.id.recycle_flightstatus)
    RecyclerView recyclerView;
    String selectedDate = "";
    final Calendar myCalendar = Calendar.getInstance();
    AdapterUpdatedFlightStatus adapterUpdatedFlightStatus;
    List<ModelUpdatedFlightStatus.Data> listStatus = new ArrayList<>();
    @BindView(R.id.nest_scroll)
    NestedScrollView nest_scroll;
    @BindView(R.id.lin_result_lay)
    LinearLayout lin_result_lay;
    @Inject
    AVIAPI aviapi;
    String selectedAirlineName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        ShoppingApplication.getContext().getComponent().inject(this);
        btn_search.setOnClickListener(this);
        date_lay.setOnClickListener(this);
        tv_title.setText(getString(R.string.sidemenu_flightstatus));
        ed_airline.setFocusable(false);
        ed_airline.setOnClickListener(this);
        initizilizeView();
    }

    @Override
    public void onBackPressed() {
       // super.onBackPressed();
        if (search_lay.getVisibility()==View.GONE){
            search_lay.setVisibility(View.VISIBLE);
            lin_result_lay.setVisibility(View.GONE);
        }
        else {
//            UtilsDefault.updateSharedPreference(Constants.SELECTED_AIRLINECODE, null);
//            UtilsDefault.updateSharedPreference(Constants.SELECTED_AIRLINENAME, null);
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (UtilsDefault.getSharedPreferenceValue(Constants.SELECTED_AIRLINENAME) != null &&
                !UtilsDefault.getSharedPreferenceValue(Constants.SELECTED_AIRLINENAME).equals("")) {
            ed_airline.setText(UtilsDefault.getSharedPreferenceValue(Constants.SELECTED_AIRLINENAME));
            selectedAirlineName = UtilsDefault.getSharedPreferenceValue(Constants.SELECTED_AIRLINECODE);
        }
        if (UtilsDefault.getSharedPreferenceValue(Constants.FS_SELECTED_AIRLINENUMBER) != null &&
                !UtilsDefault.getSharedPreferenceValue(Constants.FS_SELECTED_AIRLINENUMBER).equals("")) {
            ed_flightnumber.setText(UtilsDefault.getSharedPreferenceValue(Constants.FS_SELECTED_AIRLINENUMBER));

        }
        if (UtilsDefault.getSharedPreferenceValue(Constants.FS_SELECTED_TIME_fORMAT) != null &&
                !UtilsDefault.getSharedPreferenceValue(Constants.FS_SELECTED_TIME_fORMAT).equals("")) {
            tv_selected_date.setText(UtilsDefault.getSharedPreferenceValue(Constants.FS_SELECTED_TIME_fORMAT));


        }
        if (UtilsDefault.getSharedPreferenceValue(Constants.FS_SELECTED_TIME_NON_fORMAT) != null &&
                !UtilsDefault.getSharedPreferenceValue(Constants.FS_SELECTED_TIME_NON_fORMAT).equals("")) {
            selectedDate=(UtilsDefault.getSharedPreferenceValue(Constants.FS_SELECTED_TIME_NON_fORMAT));
        }

    }

    @Override
    protected int getlayout()
    {
        return R.layout.activity_updated_flight_status;
    }

    @Override
    public void onClick(View v) {
        if (v == btn_search) {
            String airport = ed_airline.getText().toString().trim();
            String fligntnum = ed_flightnumber.getText().toString().trim();
            if (selectedAirlineName == null || selectedAirlineName.equals("")) {
                Toast.makeText(this, getString(R.string.msg_select_airline), Toast.LENGTH_SHORT).show();
//                ed_airline.setError(getString(R.string.this_feild_empty));
//                ed_airline.requestFocus();
            } else if (fligntnum.equals("")) {
                ed_flightnumber.setError(getString(R.string.this_feild_empty));
                ed_flightnumber.requestFocus();
            } else if (selectedDate.equals("")) {
                Toast.makeText(this, R.string.select_date, Toast.LENGTH_SHORT).show();
            } else {

                UtilsDefault.updateSharedPreference(Constants.FS_SELECTED_AIRLINENUMBER,fligntnum);
                getflightStatus(airport, selectedAirlineName + fligntnum, selectedDate.trim());
            }
        }
        if (v == date_lay) {
            DatePickerDialog datePickerDialog = new DatePickerDialog(ActivityUpdatedFlightStatus.this,
                    this, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePickerDialog.show();
        }
        if (v == ed_airline) {
            startActivity(new Intent(ActivityUpdatedFlightStatus.this, ActivityAirlineSearch.class));
        }
    }
    public void initizilizeView(){

    }

    public void getflightStatus(String airlines, String flightid, String date) {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityUpdatedFlightStatus.this,
                    R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;
        }
        Log.d("finput", "getflightStatus: " + airlines + "-" + flightid + "-" + date);
        shwProgress();
        aviapi.getUpdatedFlightStatus(flightid, date).enqueue(new Callback<ModelUpdatedFlightStatus>() {
            @Override
            public void onResponse(Call<ModelUpdatedFlightStatus> call, Response<ModelUpdatedFlightStatus> response) {
                hideprogress();
                hideKeyboard();
                if (response.body() != null) {
                    ModelUpdatedFlightStatus data = response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus() == Constants.STATUS_200) {
                        if (data.getData() != null && data.getData().size() != 0) {
                            search_lay.setVisibility(View.GONE);
                            lin_result_lay.setVisibility(View.VISIBLE);
                            tv_total_flight_number.setText(data.getData().size()+" "+getString(R.string.numof_flight_status));
                            AdapterUpdatedFlightStatus adapterUpdatedFlightStatus =
                                    new AdapterUpdatedFlightStatus(ActivityUpdatedFlightStatus.this, data.getData(), 0);
                            recyclerView.setAdapter(adapterUpdatedFlightStatus);
                            recyclerView.setLayoutManager(new LinearLayoutManager(ActivityUpdatedFlightStatus.this));
                            //lin_result_lay.requestFocus();
                        }
                    } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                        getflightStatus(airlines, flightid, date);
                    } else {
                        lin_result_lay.setVisibility(View.GONE);
                        search_lay.setVisibility(View.VISIBLE);
                        Toast.makeText(ActivityUpdatedFlightStatus.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    // updateUi(null);
                    search_lay.setVisibility(View.VISIBLE);
                    lin_result_lay.setVisibility(View.GONE);

                    Toast.makeText(ActivityUpdatedFlightStatus.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ModelUpdatedFlightStatus> call, Throwable t) {
                hideprogress();
                search_lay.setVisibility(View.VISIBLE);
                lin_result_lay.setVisibility(View.GONE);
                Log.d("faill", "onFailure: " + t.getMessage());
                Toast.makeText(ActivityUpdatedFlightStatus.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, month);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        String mnth = "";
        if (month >= 9) {
            mnth = String.valueOf(month + 1);
        } else {
            int mntt = month + 1;
            mnth = "0" + mntt;
        }
        String day = "";
        if (dayOfMonth >9) {
            day = String.valueOf(dayOfMonth);
        } else {
            int dd = dayOfMonth;
            day = "0" + dd;
        }
        tv_selected_date.setText(UtilsDefault.dateFormat(year + "-" + mnth + "-" + day + " 01:34:01"));
        UtilsDefault.updateSharedPreference(Constants.FS_SELECTED_TIME_fORMAT,
                UtilsDefault.dateFormat(year + "-" + mnth + "-" + day + " 01:34:01"));
        selectedDate = String.valueOf(year) + mnth + String.valueOf(day);
        UtilsDefault.updateSharedPreference(Constants.FS_SELECTED_TIME_NON_fORMAT,selectedDate);

        Log.d("selectdate", "onDateSet: " + "" + year + "-" + mnth + "-" + dayOfMonth + "---" + selectedDate);
    }
}
