package com.dci.avicustomer.activities;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.dci.avicustomer.R;
import com.dci.avicustomer.adapter.AdapterFlightSchedule;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.PaginationScrollListener;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.ModelAddTrip;
import com.dci.avicustomer.models.ModelFlightSchedule;
import com.dci.avicustomer.models.ModelInputAddtrip;
import com.dci.avicustomer.models.ModelShopList;
import com.dci.avicustomer.models.ModelUpcomingtrips;
import com.dci.avicustomer.retrofit.AVIAPI;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityFlightSchedule extends BaseActivity
        implements View.OnClickListener, DatePickerDialog.OnDateSetListener, OnItemClickListener {
    @Inject
    public AVIAPI aviapi;
    List<ModelFlightSchedule.Datum> flightData = new ArrayList<>();
    String date = "";
    String time = "";
    AdapterFlightSchedule adapterFlightSchedule;
    @BindView(R.id.recycle_flight_schedule)
    RecyclerView recycle_flight_schedule;
    @BindView(R.id.date_lay)
    RelativeLayout date_lay;
    @BindView(R.id.time_lay)
    RelativeLayout time_lay;
    @BindView(R.id.btn_search)
    Button btn_search;
    @BindView(R.id.tv_selected_date)
    TextView tv_selected_date;
    @BindView(R.id.tv_selected_time)
    TextView tv_selected_time;


    int pagenum;
    final Calendar myCalendar = Calendar.getInstance();
    String selectedDate = "";
    String selectedTime = "";
    LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        ShoppingApplication.getContext().getComponent().inject(this);
        adapterFlightSchedule = new AdapterFlightSchedule(this, flightData, 1);
        adapterFlightSchedule.setonItemClick(this);
        linearLayoutManager = new LinearLayoutManager(this);
        recycle_flight_schedule.setLayoutManager(linearLayoutManager);
        recycle_flight_schedule.setVerticalScrollBarEnabled(true);
        recycle_flight_schedule.setHasFixedSize(true);
        if (UtilsDefault.getSharedPreferenceValue(Constants.TIME_ZONE) != null) {
            TimeZone tz = TimeZone.getTimeZone(UtilsDefault.getSharedPreferenceValue(Constants.TIME_ZONE));
            myCalendar.setTimeZone(tz);
        }
        recycle_flight_schedule.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                if (flightData.size() >= Constants.PAGE_SIZE) {
                    pagenum++;
                    isloading = true;
                    getFlightSchedule("");
                }
            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isloading;
            }
        });

        recycle_flight_schedule.setAdapter(adapterFlightSchedule);
        time_lay.setOnClickListener(this);
        date_lay.setOnClickListener(this);
        btn_search.setOnClickListener(this);
        tv_title.setText(getString(R.string.title_flight_schedule));
    }

    @Override
    protected int getlayout() {
        return R.layout.activity_flight_schedule;
    }

    public void getFlightSchedule(String airportcode) {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityFlightSchedule.this,
                    R.string.error_no_net,
                    Toast.LENGTH_SHORT).show();
            return;

        }
        shwProgress();
        Log.d("aviparms", "schedule: " + selectedDate + "time-" +
                selectedTime+"airid"+UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_CODE)+"airpotcode"+
                UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_CODE));

        String userid = "0";
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
            userid = UtilsDefault.getSharedPreferenceValue(Constants.USER_ID);
        }
        isloading = true;
        //Log.d("shopsInput", "getShopList: " + UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID) + "-page-" + pagenum + "-searchkey-" + searchKey + "-Tid-" + terminalid + "-ai-" + airport_type + "-aa-" + airport_area);
        aviapi.getFlightSchedule(UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_CODE),
                selectedDate, selectedTime, String.valueOf(pagenum),
                UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_TYPE)).
                enqueue(new Callback<ModelFlightSchedule>() {
                    @Override
                    public void onResponse(Call<ModelFlightSchedule> call,
                                           Response<ModelFlightSchedule> response) {
                        hideprogress();
                        hideKeyboard();
                        isloading = false;
                        if (response.body() != null) {
                            ModelFlightSchedule data = response.body();
                            String header = response.headers().get(getString(R.string.header_find_key));
                            if (flightData.size() == totalcount) {
                                isLastPage = true;
                            }

                            UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                            if (data.getStatus() == Constants.STATUS_200) {
                                totalcount = data.getTotalcount();

                                if (data.getData() == null && data.getData().size() == 0) {
                                    Toast.makeText(ActivityFlightSchedule.this,
                                            data.getMessage(), Toast.LENGTH_SHORT).show();
                                } else {
                                    flightData.addAll(data.getData());
                                    adapterFlightSchedule.notifyDataSetChanged();
                                    if (flightData.size() == data.getTotalcount()) {
                                        isLastPage = true;
                                    }


//                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ActivityShopList.this);
//                            recycle_shoplist.setLayoutManager(linearLayoutManager);
//                            recycle_shoplist.setHasFixedSize(true);
//                            AdapterShopList adapterRecycler = new AdapterShopList(ActivityShopList.this,
//                                    data.getData(),1);
//                            recycle_shoplist.setAdapter(adapterRecycler);
                                }


                                //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                            } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                                if (pagenum != 1)
                                    pagenum--;
                                getFlightSchedule(airportcode);
                            } else {
                                if (pagenum != 1)
                                    pagenum--;
                                Toast.makeText(ActivityFlightSchedule.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            if (pagenum != 1)
                                pagenum--;
                            Toast.makeText(ActivityFlightSchedule.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<ModelFlightSchedule> call, Throwable t) {
                        isloading = false;
                        if (pagenum != 1)
                            pagenum--;
                        hideprogress();
                        hideKeyboard();
                        Log.d("failure", "onFailure: " + t.getMessage());
                        Toast.makeText(ActivityFlightSchedule.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void getLatestTrip() {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityFlightSchedule.this, R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;
        }
        shwProgress();
        //  Log.d("aviparms", "search: "+UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)+search);
        String userid = "0";
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
            userid = UtilsDefault.getSharedPreferenceValue(Constants.USER_ID);
        }

        aviapi.getlatestTrip(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID),
                UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID), timeZone.getID()).enqueue(new Callback<ModelUpcomingtrips>() {
            @Override
            public void onResponse(Call<ModelUpcomingtrips> call, Response<ModelUpcomingtrips> response) {
                hideprogress();

                if (response.body() != null) {
                    ModelUpcomingtrips data = response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus() == Constants.STATUS_200) {
                        UtilsDefault.updateSharedPreference(Constants.AIRPORT_DOMESTIC_TYPE,
                                data.getUpTripTimeType());
                        UtilsDefault.updateSharedPreference(Constants.LATEST_DATE, data.getUpTripTime());
                        countDownStart();

                        //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                    } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                        getLatestTrip();
                    } else {

                        //Toast.makeText(ActivityMyTrips.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {


                    //   Toast.makeText(ActivityMyTrips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ModelUpcomingtrips> call, Throwable t) {
                hideprogress();
                Log.d("failure", "onFailure: " + t.getMessage());
                //Toast.makeText(ActivityMyTrips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void addTrip(ModelInputAddtrip modelInputAddtrip) {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(ActivityFlightSchedule.this, R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;

        }
        shwProgress();
        Log.d("aviparms", "schedule: " + selectedDate + "time-" + selectedTime);
        String userid = "0";
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
            userid = UtilsDefault.getSharedPreferenceValue(Constants.USER_ID);
        }
        Gson gson = new Gson();

        Log.d("addTrip", "addTrip: " + gson.toJson(modelInputAddtrip));
        modelInputAddtrip.setTime_zone(timeZone.getID());

        //Log.d("shopsInput", "getShopList: " + UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID) + "-page-" + pagenum + "-searchkey-" + searchKey + "-Tid-" + terminalid + "-ai-" + airport_type + "-aa-" + airport_area);
        aviapi.addtrip(modelInputAddtrip).enqueue(new Callback<ModelAddTrip>() {
            @Override
            public void onResponse(Call<ModelAddTrip> call, Response<ModelAddTrip> response) {
                hideprogress();
                hideKeyboard();

                if (response.body() != null) {
                    ModelAddTrip data = response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus() == Constants.STATUS_200) {
                        alertcommon(getString(R.string.title_flight_schedule), data.getMessage());
                        getLatestTrip();

                        //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                    } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {

                        addTrip(modelInputAddtrip);
                    } else {
                        alertcommon(getString(R.string.title_flight_schedule), data.getMessage());
                        //  Toast.makeText(ActivityFlightSchedule.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {

                    Toast.makeText(ActivityFlightSchedule.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ModelAddTrip> call, Throwable t) {

                hideprogress();
                hideKeyboard();
                Log.d("failure", "onFailure: " + t.getMessage());
                Toast.makeText(ActivityFlightSchedule.this,
                        R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        // TODO Auto-generated method stub
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, month);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        Log.d("rawdata", "onDateSet: "+year+"mont"+month+"day"+dayOfMonth);

        String mnth = "";
        if (month >= 9) {
            mnth = String.valueOf(month + 1);
        } else {
            int mntt = month + 1;
            mnth = "0" + mntt;
        }
        String day = "";
        if (dayOfMonth >9) {
            day = String.valueOf(dayOfMonth);
        } else {
            int dd = dayOfMonth;
            day = "0" + dd;
        }
        tv_selected_date.setText(UtilsDefault.dateFormat(year + "-" +
                mnth + "-" + day + " 01:34:01"));
        selectedDate = String.valueOf(year) + mnth + String.valueOf(day);
        Log.d("selectdate", "onDateSet: " + "" + year + "-" + mnth + "-" + dayOfMonth + "---" + selectedDate);
    }

    @Override
    public void onClick(View v) {
        if (v == date_lay) {
            DatePickerDialog datePickerDialog = new DatePickerDialog(ActivityFlightSchedule.this, this, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePickerDialog.show();
        }
        if (v == time_lay) {
            Calendar mcurrentTime = Calendar.getInstance();
            if (UtilsDefault.getSharedPreferenceValue(Constants.TIME_ZONE) != null) {
                TimeZone tz = TimeZone.getTimeZone(UtilsDefault.getSharedPreferenceValue(Constants.TIME_ZONE));
                mcurrentTime.setTimeZone(tz);
            }
            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);

            TimePickerDialog mTimePicker;
            mTimePicker = new TimePickerDialog(ActivityFlightSchedule.this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                    //it's after current
                    String hr = "";
                    String min = "";
                    if (selectedHour < 10) {
                        hr = "0" + selectedHour;
                    } else {
                        hr = String.valueOf(selectedHour);
                    }
                    if (selectedMinute < 10) {
                        min = "0" + selectedMinute;
                    } else {
                        min = String.valueOf(selectedMinute);
                    }

                    tv_selected_time.setText(hr + ":" + min);
                    selectedTime = String.valueOf(hr) + String.valueOf(min);


                }
            }, hour, minute, true);//Yes 24 hour time
            mTimePicker.setTitle("Select flight time");


            mTimePicker.show();
        }
        if (v == btn_search) {


            if (selectedDate.equals("")) {
                alertcommon(getString(R.string.title_flight_schedule), getString(R.string.selectdate));
            } else if (selectedTime.equals("")) {
                alertcommon(getString(R.string.title_flight_schedule), getString(R.string.selecttime));
            } else {

                SimpleDateFormat format = new SimpleDateFormat("MMM dd,yyyy kk:mm");
                Calendar current_cal = Calendar.getInstance();

                try {
                    if (UtilsDefault.getSharedPreferenceValue(Constants.TIME_ZONE) != null) {
                        TimeZone tz = TimeZone.getTimeZone(UtilsDefault.getSharedPreferenceValue(Constants.TIME_ZONE));
                        current_cal.setTimeZone(tz);
                        current_cal.add(Calendar.HOUR, -1);
                        format.setTimeZone(tz);
                    }
                    Date event_date = format.parse(tv_selected_date.getText() + " " + tv_selected_time.getText());
                    //Date current_date =  Calendar.getInstance().getTime();;

                    if (!event_date.after(current_cal.getTime())) {
                        Toast.makeText(this, getResources().getString(R.string.date_invalid),
                                Toast.LENGTH_SHORT).show();
                        return;
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                flightData.clear();
                adapterFlightSchedule.notifyDataSetChanged();
                pagenum = 1;
                isLastPage = false;
                getFlightSchedule("maa");
            }

        }
    }

    @Override
    public void OnItemClickListener(int pos, View view) {
        if (view.getId() == R.id.rel_bottom2) {
            ModelInputAddtrip modelInputAddtrip = new ModelInputAddtrip();
            modelInputAddtrip.setFlight_status(UtilsDefault.checkNull(flightData.get(pos).getFlightStatus()));
            modelInputAddtrip.setAirlineName(UtilsDefault.checkNull(flightData.get(pos).getAirlineName()));
            modelInputAddtrip.setAirport_id(Integer.parseInt(UtilsDefault.checkNull(UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID))));
            modelInputAddtrip.setUser_id(Integer.parseInt(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)));
            modelInputAddtrip.setFlight_code(UtilsDefault.checkNull(flightData.get(pos).getAirlineCode()) + UtilsDefault.checkNull(flightData.get(pos).getFlightNumber()));
            modelInputAddtrip.setDep_airport_code(UtilsDefault.checkNull(flightData.get(pos).getDepature_airportcode()));
            modelInputAddtrip.setArr_airport_code(UtilsDefault.checkNull(flightData.get(pos).getArrival_airportcode()));
            modelInputAddtrip.setArr_airport_city(UtilsDefault.checkNull(flightData.get(pos).getArrival_city()));
            modelInputAddtrip.setDep_airport_city(UtilsDefault.checkNull(flightData.get(pos).getDepature_city()));
            modelInputAddtrip.setArr_airport_country(UtilsDefault.checkNull(flightData.get(pos).getArrival_countryid()));
            modelInputAddtrip.setDep_airport_country(UtilsDefault.checkNull(flightData.get(pos).getDepature_countryid()));
            if (flightData.get(pos).getDepature_terminal() != null && !flightData.get(pos).getDepature_terminal().equals("")) {
                modelInputAddtrip.setDep_terminal(flightData.get(pos).getDepature_terminal());
            }
            if (flightData.get(pos).getArrival_terminal() != null && !flightData.get(pos).getArrival_terminal().equals("")) {
                modelInputAddtrip.setArr_terminal(flightData.get(pos).getArrival_terminal());
            }
            modelInputAddtrip.setDep_date(UtilsDefault.checkNull(flightData.get(pos).getDepature_date()));
            modelInputAddtrip.setArr_date(UtilsDefault.checkNull(flightData.get(pos).getArrival_date()));
            modelInputAddtrip.setDep_time(UtilsDefault.checkNull(flightData.get(pos).getDepature_time()));
            modelInputAddtrip.setArr_time(UtilsDefault.checkNull(flightData.get(pos).getArrival_time()));
            modelInputAddtrip.setDep_airport_name(UtilsDefault.checkNull(flightData.get(pos).
                    getArrival_airportname()));
            modelInputAddtrip.setArr_airport_name(UtilsDefault.checkNull(flightData.get(pos).
                    getDepature_airportname()));

            if (flightData.get(pos).getArrGate()!=null){
                modelInputAddtrip.setArrGate(flightData.get(pos).getArrGate());

            }
            if (flightData.get(pos).getDepGate()!=null){
                modelInputAddtrip.setDepGate(flightData.get(pos).getDepGate());
            }

            addTrip(modelInputAddtrip);
        } else {


            //recycle_flight_schedule.scr(View.FOCUS_DOWN);
            //linearLayoutManager.scrollToPosition(flightData.size() - 1);
        }

    }
}
