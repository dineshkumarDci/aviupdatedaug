package com.dci.avicustomer.activities;

import android.content.Intent;
import android.net.Uri;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dci.avicustomer.R;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.models.ModelFacilities;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityFacilitiesDetails extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.img_shop)
    ImageView img_shop;
    @BindView(R.id.tv_facilityname)
    TextView tv_facilityname;
    @BindView(R.id.tv_terminal_details)
    TextView tv_terminal_details;
    @BindView(R.id.tv_amenities)
    TextView tv_amenities;
    @BindView(R.id.web_describtion)
    WebView web_describtion;
    @BindView(R.id.img_phn_num)
    ImageView img_phn_num;
    @BindView(R.id.img_email)
    ImageView img_email;
    String phonenum, emailid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        img_email.setOnClickListener(this);
        img_phn_num.setOnClickListener(this);
        if (intent != null) {
            ModelFacilities.Datum dummyParentDataItem = (ModelFacilities.Datum) intent.getSerializableExtra("data");
            tv_facilityname.setText(dummyParentDataItem.getName());
            phonenum = dummyParentDataItem.getPhone();
            emailid = dummyParentDataItem.getEmail();
            tv_title.setText(dummyParentDataItem.getName());
            tv_terminal_details.setText(dummyParentDataItem.getAirport_details());
            if (dummyParentDataItem.getLoungefacility() != null && dummyParentDataItem.getLoungefacility().size() != 0) {
                for (ModelFacilities.Loungefacility data : dummyParentDataItem.getLoungefacility()) {
                    int index = dummyParentDataItem.getLoungefacility().indexOf(data);
                    if (index == dummyParentDataItem.getLoungefacility().size() - 1) {
                        tv_amenities.append(data.getFacility().getName());
                    } else {
                        tv_amenities.append(data.getFacility().getName() + ", ");
                    }
                }

                if (dummyParentDataItem.getConditions() != null && !dummyParentDataItem.getConditions().equals("")) {
                    web_describtion.loadDataWithBaseURL("", dummyParentDataItem.getConditions(),
                            "text/html", "UTF-8", "");
                }
                if (dummyParentDataItem.getImages() != null && !dummyParentDataItem.getImages().equals("")) {
                    Glide.with(this).load(dummyParentDataItem.getUrl() +
                            dummyParentDataItem.getImages()).into(img_shop);
                }

            }
            img_shop.setClipToOutline(true);

        }
        // ShoppingApplication.getContext().getComponent().inject(this);
        //tv_title.setText(R.string.edit_trip);

    }


    @Override
    protected int getlayout() {
        return R.layout.layout_activity_facilities_details;
    }

    @Override
    public void onClick(View v) {
        if (v == img_phn_num) {
            if (phonenum != null && !
                    phonenum.equals("")) {
                Uri u = Uri.parse("tel:" + phonenum.trim());
                Intent i = new Intent(Intent.ACTION_DIAL, u);
                startActivity(i);
            } else {
                Toast.makeText(this, R.string.no_phone_num, Toast.LENGTH_SHORT).show();
            }
        }
        if (v == img_email) {
            if (emailid != null && !
                    emailid.equals("")) {
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" +
                            emailid.trim()));

                    startActivity(intent);
                } catch (Exception e) {
                    Toast.makeText(this, "Sorry...You don't have any mail app", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(this, R.string.no_email, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
