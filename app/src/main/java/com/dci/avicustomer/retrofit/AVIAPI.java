package com.dci.avicustomer.retrofit;





import android.support.v4.content.res.FontResourcesParserCompat;

import com.dci.avicustomer.models.CommonMsgModel;
import com.dci.avicustomer.models.ModelAddDocuments;
import com.dci.avicustomer.models.ModelAddFavourite;
import com.dci.avicustomer.models.ModelAddTrip;
import com.dci.avicustomer.models.ModelApplyCoupon;
import com.dci.avicustomer.models.ModelAviTips;
import com.dci.avicustomer.models.ModelCartView;
import com.dci.avicustomer.models.ModelCompletedTrips;
import com.dci.avicustomer.models.ModelDocumentDetailsList;
import com.dci.avicustomer.models.ModelEatAndDrink;
import com.dci.avicustomer.models.ModelFacilities;
import com.dci.avicustomer.models.ModelFlightSchedule;
import com.dci.avicustomer.models.ModelFlightStatus;
import com.dci.avicustomer.models.ModelFloorMap;
import com.dci.avicustomer.models.ModelHome;
import com.dci.avicustomer.models.ModelInputAddtrip;
import com.dci.avicustomer.models.ModelLatestNews;
import com.dci.avicustomer.models.ModelMyOrders;
import com.dci.avicustomer.models.ModelMyTrips;
import com.dci.avicustomer.models.ModelNewFlightStatus;
import com.dci.avicustomer.models.ModelOTPSend;
import com.dci.avicustomer.models.ModelOfferList;
import com.dci.avicustomer.models.ModelOrderCheck;
import com.dci.avicustomer.models.ModelOrderTracking;
import com.dci.avicustomer.models.ModelRefresh;
import com.dci.avicustomer.models.ModelRestaurant;
import com.dci.avicustomer.models.ModelSearchAirline;
import com.dci.avicustomer.models.ModelSearchResult;
import com.dci.avicustomer.models.ModelShopList;
import com.dci.avicustomer.models.ModelSocialLogin;
import com.dci.avicustomer.models.ModelTransport;
import com.dci.avicustomer.models.ModelTripDetails;
import com.dci.avicustomer.models.ModelUpcomingtrips;
import com.dci.avicustomer.models.ModelUpdatedFlightStatus;
import com.dci.avicustomer.models.ModelUserProfile;
import com.dci.avicustomer.models.ModelUserReviews;
import com.dci.avicustomer.models.ModelUserTipList;
import com.dci.avicustomer.models.RegisterModel;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

public interface AVIAPI {


    @FormUrlEncoded
    @POST("login")
    Call<RegisterModel>login(@Field("email") String email,
                              @Field("password") String password,
                             @Field("role")int role,@Field("fcm_key") String fcm_key,
                             @Field("Device_type")String type);
    @FormUrlEncoded
    @POST("socialLogin")
    Call<ModelSocialLogin>socailLogin(@Field("email") String email,
                                 @Field("username") String username,@Field("login_type") String login_type,
                                      @Field("login_id") String login_id,
                                      @Field("profile_img") String profile_img,
                                      @Field("fcm_key") String fcm_key,@Field("Device_type")String type);


    @FormUrlEncoded
    @POST("forgotPassword")
    Call<CommonMsgModel>forgotPass(@Field("email") String email);


    @FormUrlEncoded
    @POST("checkCoupon")
    Call<ModelApplyCoupon>applyCoupon(@Field("airport") String airport,@Field("total")String total,
                                     @Field("code")String code,@Field("type")String type,@Field("id")String id);

    @FormUrlEncoded
    @POST("changePassword")
    Call<CommonMsgModel>changePassword(@Field("user_id") String user_id,@Field("current_password") String current_password,
    @Field("confirm_password") String confirm_password,@Field("new_password") String new_password);

    @FormUrlEncoded
    @POST("getAirportList")
    Call<ModelSearchResult>search(@Field("user_id") String user_id,
                                  @Field("keyword") String airport, @Field("lat") String lat , @Field("lng") String lng);

    @FormUrlEncoded
    @POST("getAirLineList")
    Call<ModelSearchAirline>searchArlines(@Field("keyword") String keyword);

    @FormUrlEncoded
    @POST("getAirportListLogin")
    Call<ModelSearchResult>searchLogin(@Field("user_id") String user_id,
                                  @Field("keyword") String keyword,@Field("lat") String lat , @Field("lng") String lng);
    @FormUrlEncoded
    @POST("restaurant")
    Call<ModelRestaurant>getRestroDetails(@Field("id") String id,
                                     @Field("userid") String userid,
                                          @Field("airport_id")String airport_id,
                                          @Field("terminal_id")String terminal_id);
    @FormUrlEncoded
    @POST("reviewList")
    Call<ModelUserReviews>getUserReviews(@Field("restaurant_id") String restaurant_id,
                                         @Field("page_no") int page_no);



    @FormUrlEncoded
    @POST("addToCart")
    Call<CommonMsgModel>addCart(@Field("id") String id,
                                @Field("user_id") String userid,
                                @Field("airport_id")String airport_id,@Field("terminal_id")String terminal_id,
                                @Field("type")String type);
    @FormUrlEncoded
    @POST("showShopList")
    Call<ModelShopList>shoplist(@Field("airport_id") String airport_id,
                                @Field("page_no") int page_no,@Field("keyword") String keyword,
                                @Field("terminal_id")String terminal_id,
                                @Field("type_id")String type_id,@Field("area_id")String area_id,
                                @Field("count")int count);
    @FormUrlEncoded
    @POST("eatsAndDrinks")
    Call<ModelEatAndDrink>getEatAndDrink(@Field("airport_id") String airport_id,
                                   @Field("page_no") int page_no, @Field("keyword") String keyword,
                                   @Field("terminal_id")String terminal_id,
                                   @Field("type_id")String type_id, @Field("area_id")String area_id,
                                         @Field("count")int count);

    @FormUrlEncoded
    @POST("flightSchedule")
    Call<ModelFlightSchedule>getFlightSchedule(@Field("airport_code") String airport_code,
                                          @Field("date") String date,
                                               @Field("time")String time,
                                               @Field("page")String page,
                                               @Field("airport_type")String AirportType);

    @FormUrlEncoded
    @POST("switchnotification")
    Call<CommonMsgModel>switchNotification(@Field("user_id") String user_id,
                                        @Field("switch_notification") String airport_id);
    @FormUrlEncoded
    @POST("addFavouriteAirport")
    Call<ModelAddFavourite>addfavourite(@Field("user_id") String user_id,
                                        @Field("airport_id") String airport_id);
    @FormUrlEncoded
    @POST("getCcavenueRSA")
    Call<JSONObject>getCCavenueRSA(@Field("order_id") String order_id);

    @FormUrlEncoded
    @POST("showUserTips")
    Call<ModelUserTipList>userTips(@Field("airport_id") String airport_id,
                                   @Field("page_no") String page_no);

    @FormUrlEncoded
    @POST("addUserTips")
    Call<CommonMsgModel>addUserTips(@Field("user_id") String user_id,
                                    @Field("airport_id") String airport_id,
                                    @Field("description") String description);

    @FormUrlEncoded
    @POST("showAviTips")
    Call<ModelAviTips>aviTIps(@Field("airport_id") String airport_id);

    @FormUrlEncoded
    @POST("transportList")
    Call<ModelTransport>getTransport(@Field("airport_id") String airport_id);

    @FormUrlEncoded
    @POST("logout")
    Call<CommonMsgModel>logout(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("showLatestNews")
    Call<ModelLatestNews>latestNews(@Field("user_id") String user_id,
                                    @Field("airport_id") String airport_id);

    @FormUrlEncoded
    @POST("offers")
    Call<ModelOfferList>getoffers(@Field("airport_id") String airport_id,
                                 @Field("terminal_id") String terminal_id, @Field("type")String type,
                                  @Field("page")String page,@Field("area_type")String area_type);

    @FormUrlEncoded
    @POST("homePage")
    Call<ModelHome>homedata(@Field("user_id") String user_id,
                                @Field("airport_id") String airport_id,@Field("terminal_id")String terminal_id,
                            @Field("area")String area,@Field("time_zone")String time_zone);
    @FormUrlEncoded
    @POST("listMyTrips")
    Call<ModelMyTrips>getMyTrips(@Field("user_id") String user_id,
                                 @Field("airport")String airport,
                                 @Field("dateTime")String date,@Field("time_zone")String time_zone);
    @FormUrlEncoded
    @POST("completedTrips")
    Call<ModelCompletedTrips>getCompletedTrips(@Field("user_id") String user_id,
                                               @Field("dateTime")String date,
                                               @Field("airport")String airport,
                                               @Field("page")int page,
                                               @Field("pagesize") int pagesize);

    @FormUrlEncoded
    @POST("myDocuments")
    Call<ModelDocumentDetailsList>getMyDocs(@Field("section") String section, @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("viewTrips")
    Call<ModelTripDetails>getMyTripsDetails(@Field("user_id") String user_id,@Field("id") String tripId,
                                            @Field("time_zone")String time_zone);

    @FormUrlEncoded
    @POST("flightStatus")
    Call<ModelNewFlightStatus>getFlightStatus(@Field("airlines") String airlines,
                                              @Field("flight_id") String flight_id, @Field("date")String date);

    @FormUrlEncoded
    @POST("flightStatusMulti")
    Call<ModelUpdatedFlightStatus>getUpdatedFlightStatus(@Field("flight_id") String flight_id, @Field("date")String date);
    @FormUrlEncoded
    @POST("deleteTrips")
    Call<CommonMsgModel>deleteTrip( @Field("user_id") String user_id,@Field("id") String id);
    @FormUrlEncoded
    @POST("addRatingTrips")
    Call<CommonMsgModel>addTripRating(@Field("user_id") String user_id,@Field("trip_id") int trip_id,
                                      @Field("from_rating") int from_rating,
                                    @Field("from_comment") String from_comment,@Field("to_rating") int to_rating,
                                    @Field("to_comment") String to_comment,@Field("airport_rating")
                                              int airport_rating,@Field("airport_comments")String airport_comments);

    @POST("getprofile")
    Call<ModelUserProfile>getUserProfile();


    @POST("getprofile")
    Call<CommonMsgModel>updateUserAirport(@Field("user_id") String user_id,@Field("airport_id") int airport_id);

    @FormUrlEncoded
    @POST("sendOTP")
    Call<ModelOTPSend>sendOTP(@Field("phone") String phone,@Field("country") String country,
                              @Field("email")String emailid);

    @FormUrlEncoded
    @POST("register")
    Call<RegisterModel> register(@Field("first_name") String first_name,
                              @Field("email") String email,@Field("mobile") String mobile,
                              @Field("password") String password,
                                 @Field("confirmpassword")String confirmpass,
                                 @Field("last_name") String last_name,
                                 @Field("country")String country,
                                 @Field("fcm_key")String fcm_key,@Field("Device_type")String type,
                                 @Field("CountryUniqueCode")String CountryUniqueCode);

    @Multipart
    @POST("addDocument")
    Call<ModelAddDocuments>uploadMultiFile(@Part List<MultipartBody.Part> file,
                                           @Part("section") RequestBody section,
                                           @Part("user_id")RequestBody user_id);


    @Multipart
    @POST("updateProfile")
    Call<ModelUserProfile>updateProfile(@Part("user_id") int id,
                                      @Part("fName") String fName, @Part("lName") String lName,
                                      @Part("email") String email,@Part("gender") String gender,
                                      @Part("dob") String dob,
                                      @Part("phone") String phone, @Part("country") String country,
                                        @Part MultipartBody.Part file);
    @FormUrlEncoded
    @POST("userPastOrder")
    Call<ModelMyOrders>getUserOrders(@Field("user_id") String user_id,@Field("page") int page,
                                     @Field("pagesize") int pagesize);

    @FormUrlEncoded
    @POST("refreshTrips")
    Call<ModelMyTrips>refreshTrip(@Field("id") String id, @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("deleteDocument")
    Call<CommonMsgModel>deleteDocuments(@Field("id") String id, @Field("user_id") String user_id,
                                        @Field("trip_id") int trip_id);
    @FormUrlEncoded
    @POST("renewCart")
    Call<CommonMsgModel>renewCart(@Field("id") String id, @Field("user_id") String user_id,
                                        @Field("airport_id")String airport_id,
                                  @Field("terminal_id")String terminal_id,@Field("type")String type);

    @FormUrlEncoded
    @POST("myFeedback")
    Call<CommonMsgModel>submitReview(@Field("user_id") String user_id,
                                    @Field("name") String airport_id,
                                    @Field("email") String description,@Field("phone") String phone,
                                     @Field("message") String message);
    @FormUrlEncoded
    @POST("showFacility")
    Call<ModelFacilities>getAirportFacility(@Field("user_id") String user_id,
                                         @Field("airport_id") String airport_id,
                                            @Field("terminal_id")String terminal_id,
                                            @Field("area_type")String area_type);


    @POST("addMyTrip")
    Call<ModelAddTrip>addtrip(@Body ModelInputAddtrip modelInputAddtrip);

    @POST("editMyTrip")
    Call<ModelAddTrip>editMyTrip(@Body ModelInputAddtrip modelInputAddtrip);

    @FormUrlEncoded
    @POST("floormaps")
    Call<ModelFloorMap>getFloorMap(@Field("airport_id") String airport_id);

    @FormUrlEncoded
    @POST("review")
    Call<CommonMsgModel>reviewOrder(@Field("order_id") String order_id,@Field("restaurant_id") int restaurant_id,
                                   @Field("user_id") String user_id,@Field("food_quality") int food_quality,
                                   @Field("service") int service,@Field("overAllExperience") int overAllExperience,
                                   @Field("comment") String comment);

    @FormUrlEncoded
    @POST("orderDetails")
    Call<ModelOrderTracking>getOrderDetails(@Field("order_id") String order_id,@Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("cartList")
    Call<ModelCartView>getCartList(@Field("user_id") String user_id,
                                   @Field("airport_id")String airport_id,@Field("terminal_id")String terminal_id,
                                   @Field("coupon")String coupon,@Field("place")String place);
    @FormUrlEncoded
    @POST("newOrder")
    Call<ModelOrderCheck>orderWebAuth(@Field("user_id") String user_id,
                                     @Field("airport_id")String airport_id, @Field("terminal_id")
                                              String terminal_id,
                                      @Field("total")String total,@Field("place")String place,
                                      @Field("dine_in_count")String dine_in_count,
                                      @Field("offer_code")String offer_code,@Field("discount")String discount);


    @FormUrlEncoded
    @POST("upComingTrips")
    Call<ModelUpcomingtrips>getlatestTrip(@Field("user_id") String airport_id,
                                          @Field("airport") String airport,
                                          @Field("time_zone")String time_zone);

}

