package com.dci.avicustomer.ccavenue;

public class ConstantsCCavenue {
	public static final String PARAMETER_SEP = "&";
	public static final String PARAMETER_EQUALS = "=";
	public static final String TRANS_URL = "https://secure.ccavenue.com/transaction/initTrans";
	public static final String DEMO_URL = "https://test.ccavenue.com/transaction/initTrans";
}