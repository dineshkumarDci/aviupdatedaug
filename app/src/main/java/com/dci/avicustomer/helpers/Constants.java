package com.dci.avicustomer.helpers;

import android.os.Environment;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dci.avicustomer.R;

import java.io.File;

import butterknife.BindView;

/**
 * Created by USER on 4/13/2017.
 */

public class Constants {


//   if (data.getCalculation().getCoupon() != null &&
//            !data.getCalculation().getCoupon().equals("")) {
//        lin_discount_lay.setVisibility(View.VISIBLE);
//        tv_discount_amnt.setText(data.getCalculation().getCurrencyCode() + " " + data.getCalculation().getCoupon());
//
//    }
//                            if (data.getCalculation().getConvenience_fee() != null
//            && !data.getCalculation().getConvenience_fee().equals("")) {
//        lin_convn_lay.setVisibility(View.VISIBLE);
//        tv_convn_fee.setText(data.getCalculation().getCurrencyCode() + " " + data.getCalculation().getConvenience_fee());
//
//    }
//                            if (data.getCalculation().getHandling_fee() != null &&
//            !data.getCalculation().getHandling_fee().equals("")) {
//        lin_handling_lay.setVisibility(View.VISIBLE);
//        tv_handling_fee.setText(data.getCalculation().getCurrencyCode() + " " + data.getCalculation().getHandling_fee());
//
//    }
//                            if (data.getCalculation().getGst_amt() != null && !data.getCalculation().getGst_amt().equals("")) {
//        lin_gst_lay.setVisibility(View.VISIBLE);
//        tv_gst.setText(data.getCalculation().getCurrencyCode() + " " + data.getCalculation().getGst_amt());
//
//    }
//
//                            if (data.getCalculation().getDelivery_fee() != null && !data.getCalculation().getDelivery_fee().equals("")) {
//        lin_deliver_lay.setVisibility(View.VISIBLE);
//        tv_service_charge.setText(data.getCalculation().getCurrencyCode() + " " + data.getCalculation().getDelivery_fee());
//
//    }
//
//    @BindView(R.id.lin_gst_lay)
//    LinearLayout lin_gst_lay;
//    @BindView(R.id.lin_convn_lay)
//    LinearLayout lin_convn_lay;
//    @BindView(R.id.lin_handling_lay)
//    LinearLayout lin_handling_lay;
//    @BindView(R.id.lin_deliver_lay)
//    LinearLayout lin_deliver_lay;
//    @BindView(R.id.lin_discount_lay)
//    LinearLayout lin_discount_lay;
//    @BindView(R.id.tv_gst)
//    TextView tv_gst;
//    @BindView(R.id.tv_convn_fee)
//    TextView tv_convn_fee;
//    @BindView(R.id.tv_handling_fee)
//    TextView tv_handling_fee;

    public static String password = "password";

    public static String USER_TYPE = "1";
    public static String NOTIFY_OFF = "0";
    public static String NOTIFY_ON = "1";
    public static String USER_WALLETE = "wallet";
    public static String AIRPORT_TYPE = "airport_type";
    public static String TERMS_TICKED = "terms";
    public static String AUTH_TOKEN = "token";
    public static String CART_PLUS = "1";
    public static String CART_MINUS = "2";
    public static String CART_DELETE = "3";
    public static String TIME_ZONE = "time_zone";
    public static String DEV_ANDROID = "Android";
    public static int STATUS_200 = 200;
    public static int STATUS_500 = 500;
    public static int STATUS_501 = 501;
    public static String USER_ID = "userid";
    public static String USER_IMAGE_PATH = "userimagepath";
    public static String FIRST_NAME = "first_name";
    public static String LAST_NAME = "last_name";
    public static String MOBILE_NUMBER = "mobilenume";
    public static String GENDER = "gender";
    public static String NOTIFICATION_STATUS = "notification_status";
    public static String DOB = "dob";
    public static String EMAILID = "emailid";
    public static String PROFILE_IMG = "prof_img";
    public static String LATEST_DATE = "latest_date";
    public static String PERSONEL_NOTE = "profile_note";
    public static String STATUS = "status";
    public static String AIRPORT_ID = "airport_id";
    public static String AIRPORT_NAME = "airport_name";
    public static String AIRPORT_CODE = "airport_code";
    public static String AIRPORT_CITY = "airport_city";
    public static String AIRPORT_DOMESTIC_TYPE = "domestictype";
    public static String TERMINAL_ID = "terminalid";
    public static String WEATHER_REPORT = "weatherreport";
    public static String WEATHER_CELCIUS = "celcius";
    public static String AIRPORT_LATITUDE = "lati";
    public static String AIRPORT_LONGITUDE = "longi";
    public static String WEATHER_MESSAGE = "weathermessage";
    public static String SECTION_BOARDING = "1";
    public static String SECTION_ETICKET = "4";
    public static String SECTION_PERSONAL = "2";
    public static String SECTION_VISA = "3";
    public static Integer MAX_FILE_SELECT = 5;
    public static Integer USER_ROLE = 3;
    public static Integer AREA_DEP = 2;
    public static Integer AREA_ARR = 1;
    public static int API_TOKENEXPIRY_STATUS = 401;
    public static int API_FAILURE_STATUS = 500;
    public static String AIRPORT_CONTRYCODE = "airport_country";
    public static String DELAYED = "delayed";
    public static String SELECTED_AIRLINECODE = "search_airline_code";
    public static String SELECTED_AIRLINENAME = "search_airline_name";

    public static String FS_SELECTED_TIME_NON_fORMAT = "fs_selected_time";
    public static String FS_SELECTED_TIME_fORMAT = "fs_selected_time_formated";
    public static String FS_SELECTED_AIRLINENUMBER = "fs_selected_airlinenum";



    public static String WALLET_BAL = "wallet_bal";


    public static String GUEST_LOGIN = "guestlogin";


    public static String user_email = "user_email";
    public static String user_picsurl = "user_pics";
    public static String wallet_amount = "wallet_amount";
    public static String loggedin = "loggedin";
    public static int PAGE_SIZE = 25;
    public static int NEW_PAGE_SIZE = 25;

    public static String REFER_CODE = "refer_code";
    public static String FCM_KEY = "fcm_key";
    public static String API_NAME = "apiname";
    public static String API_KEY = "apikey";
    public static String CASH_ON_DELIVERY = "3";
    public static String ONLINE = "1";
    public static String WALLET = "4";
    public static String NOTIFY_PRODUCT = "1";
    public static String NOTIFY_PRODUCT_PRODUCT_DETAIL = "4";
    public static String NOTIFY_WALLET = "2";
    public static String NOTIFY_COUNT = "notify_count";

    public static String FROM_COMBO = "combo";
    public static String FROM_WHATSAVAILABLE = "whatsavailable";
    public static String FROM_PRODUCT = "products";

    public static final int ORDER_PLACED = 1;
    public static final int ORDER_PROCESSED = 5;
    public static final int ORDER_CONFIRMED = 3;
    public static final int ORDER_READY = 6;
    public static final int ORDER_DELIVERED = 7;
    public static final int ORDER_CANCELED = 4;
    public static final int ORDER_PAYED = 2;


}
