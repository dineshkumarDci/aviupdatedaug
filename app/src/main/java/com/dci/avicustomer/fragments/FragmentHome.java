package com.dci.avicustomer.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.avicustomer.R;
import com.dci.avicustomer.activities.ActivityAviTips;
import com.dci.avicustomer.activities.ActivityCommonWebLoader;
import com.dci.avicustomer.activities.ActivityEatAndDrink;
import com.dci.avicustomer.activities.ActivityFacilities;
import com.dci.avicustomer.activities.ActivityFlightSchedule;
import com.dci.avicustomer.activities.ActivityFlightStatus;
import com.dci.avicustomer.activities.ActivityHome;
import com.dci.avicustomer.activities.ActivityLogin;
import com.dci.avicustomer.activities.ActivityMyTrips;
import com.dci.avicustomer.activities.ActivityShopList;
import com.dci.avicustomer.activities.ActivityTransport;
import com.dci.avicustomer.activities.ActivityUpdatedFlightStatus;
import com.dci.avicustomer.activities.ActivityUserTips;
import com.dci.avicustomer.adapter.AdapterHomeOffers;
import com.dci.avicustomer.adapter.AdapterLatestNews;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.CommonMsgModel;
import com.dci.avicustomer.models.ModelHome;
import com.dci.avicustomer.models.ModelUpcomingtrips;
import com.dci.avicustomer.retrofit.AVIAPI;
import com.kwabenaberko.openweathermaplib.constants.Units;
import com.kwabenaberko.openweathermaplib.implementation.OpenWeatherMapHelper;
import com.kwabenaberko.openweathermaplib.implementation.callbacks.CurrentWeatherCallback;
import com.kwabenaberko.openweathermaplib.models.currentweather.CurrentWeather;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.inject.Inject;

import belka.us.androidtoggleswitch.widgets.BaseToggleSwitch;
import belka.us.androidtoggleswitch.widgets.MultipleToggleSwitch;
import belka.us.androidtoggleswitch.widgets.ToggleSwitch;
import butterknife.BindView;
import butterknife.ButterKnife;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FragmentHome extends BaseFragments implements OnItemClickListener,
        View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    public static String TAG = "FragmentHome";
    @Inject
    public AVIAPI aviapi;
    @BindView(R.id.tv_airport_name)
    TextView tv_airport_name;
    @BindView(R.id.tv_user_name)
    TextView tv_user_name;

    @BindView(R.id.tv_airport_country)
    TextView tv_airport_country;
    @BindView(R.id.spinner_terminal)
    Spinner spinner_terminal;
    @BindView(R.id.tc_airport_time)
    TextClock tc_airport_time;
    @BindView(R.id.spinn_arrow)
    ImageView spinn_arrow;
    @BindView(R.id.lin_content_lay)
    LinearLayout lin_content_lay;
    @BindView(R.id.tv_airport_temp)
    TextView tv_airport_temp;
    @BindView(R.id.tv_airport_climate)
    TextView tv_airport_climate;
    @BindView(R.id.eat_drink_lay)
    CardView eat_drink_lay;
    @BindView(R.id.shop_lay)
    CardView shop_lay;
    @BindView(R.id.transport_lay)
    CardView transport_lay;
    @BindView(R.id.floormap_lay)
    CardView floormap_lay;
    @BindView(R.id.facilities_lay)
    CardView facilities_lay;
    @BindView(R.id.mytrip_lay)
    LinearLayout mytrip_lay;
    @BindView(R.id.flightstatus_lay)
    LinearLayout flightstatus_lay;
    @BindView(R.id.flight_schedule_lay)
    LinearLayout flight_schedule_lay;
    @BindView(R.id.recycle_offer)
    RecyclerView recycle_offer;
    @BindView(R.id.recycle_latest_news)
    RecyclerView recycle_latest_news;
    @BindView(R.id.tv_avi_tipdate)
    TextView tv_avi_tipdate;
    @BindView(R.id.tv_avi_tipdes)
    TextView tv_avi_tipdes;
    @BindView(R.id.tv_user_tipdate)
    TextView tv_user_tipdate;
    @BindView(R.id.tv_user_tipdes)
    TextView tv_user_tipdes;
    @BindView(R.id.img_blur)
    ImageView img_blur;
    @BindView(R.id.card_avitips)
    CardView card_avitips;
    @BindView(R.id.card_user_tip_lay)
    CardView card_user_tip_lay;
    @BindView(R.id.tv_news_nodata)
    TextView tv_news_nodata;
    @BindView(R.id.tv_restro_nodata)
    TextView tv_restro_nodata;
    @BindView(R.id.swiplayout)
    SwipeRefreshLayout swiplayout;
    @BindView(R.id.multiple_toggle_switch)
    ToggleSwitch multiple_toggle_switch;
    AlertDialog tripAlert, foodalert;

    List<ModelHome.ATerminal> list_terminals = new ArrayList<>();
    Boolean isSelected = false;
    int tripId = 0;
    String orderId = "";
    int restrId = 0;
    String area = "2";
    String terminalId = "";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public String getCurrentdateandTime() {
        Date date = new Date();
        // SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String formattedDate = dateFormat.format(date);
        Log.d(TAG, "getCurrentdateandTime: " + formattedDate);
        return formattedDate;
        //Toast.makeText(this, formattedDate, Toast.LENGTH_SHORT).show();
    }

    public void addFoodRating(String orderid, int restroId, int foodQuality, int Service,
                              int overAll, String comments) {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(getActivity(), R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;
        }
        shwProgress();
        //  Log.d("aviparms", "search: "+UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)+search);
        String userid = "0";
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
            userid = UtilsDefault.getSharedPreferenceValue(Constants.USER_ID);
        }

        aviapi.reviewOrder(orderid, restroId,
                UtilsDefault.getSharedPreferenceValue(Constants.USER_ID),
                foodQuality, Service, overAll, comments).enqueue(new Callback<CommonMsgModel>() {
            @Override
            public void onResponse(Call<CommonMsgModel> call, Response<CommonMsgModel> response) {
                hideprogress();

                if (response.body() != null) {
                    CommonMsgModel data = response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus() == Constants.STATUS_200) {
                        if (foodalert != null) {
                            foodalert.cancel();
                        }
                        Toast.makeText(context, data.getMessage(), Toast.LENGTH_SHORT).show();


                        //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                    } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                        addFoodRating(orderid, restroId, foodQuality, Service, overAll, comments);
                        //countDownStart();
                    } else {


                        Toast.makeText(context, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<CommonMsgModel> call, Throwable t) {
                hideprogress();
                Log.d("failure", "onFailure: " + t.getMessage());
                //Toast.makeText(ActivityMyTrips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void addTripRating(int tripid, int fromrate, String fromComment, int toRating, String toComment,
                              int airportRating, String airportComments) {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(getActivity(), R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;

        }
        shwProgress();
        //  Log.d("aviparms", "search: "+UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)+search);
        String userid = "0";
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
            userid = UtilsDefault.getSharedPreferenceValue(Constants.USER_ID);
        }

        aviapi.addTripRating(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID), tripid,
                fromrate, fromComment, toRating, toComment, airportRating, airportComments).enqueue(new Callback<CommonMsgModel>() {
            @Override
            public void onResponse(Call<CommonMsgModel> call,
                                   Response<CommonMsgModel> response) {
                hideprogress();

                if (response.body() != null) {
                    CommonMsgModel data = response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus() == Constants.STATUS_200) {
                        if (tripAlert != null) {
                            tripAlert.cancel();
                        }
                        Toast.makeText(context, data.getMessage(), Toast.LENGTH_SHORT).show();


                        //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                    } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                        addTripRating(tripid, fromrate,
                                fromComment, toRating, toComment, airportRating, airportComments);
                        //countDownStart();
                    } else {


                        Toast.makeText(context, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {


                    Toast.makeText(context, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<CommonMsgModel> call, Throwable t) {
                hideprogress();
                Log.d("failure", "onFailure: " + t.getMessage());
                //Toast.makeText(ActivityMyTrips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void getLatestTrip() {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(getActivity(), R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;

        }
        shwProgress();
        //  Log.d("aviparms", "search: "+UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)+search);
        String userid = "0";
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
            userid = UtilsDefault.getSharedPreferenceValue(Constants.USER_ID);
        }


        aviapi.getlatestTrip(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID),
                UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID),"").enqueue(new Callback<ModelUpcomingtrips>() {
            @Override
            public void onResponse(Call<ModelUpcomingtrips> call, Response<ModelUpcomingtrips> response) {
                hideprogress();
                if (response.body() != null) {
                    ModelUpcomingtrips data = response.body();
                    String header = response.headers().get(context.getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (data.getStatus() == Constants.STATUS_200) {

                        UtilsDefault.updateSharedPreference(Constants.LATEST_DATE,
                                data.getUpTripTime());
                        countDownStart();

                        //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                    } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                        getLatestTrip();
                        //countDownStart();
                    } else {
                        UtilsDefault.updateSharedPreference(Constants.LATEST_DATE, null);
                        countDownStart();

                        //Toast.makeText(ActivityMyTrips.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {


                    //   Toast.makeText(ActivityMyTrips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ModelUpcomingtrips> call, Throwable t) {
                hideprogress();
                Log.d("failure", "onFailure: " + t.getMessage());
                //Toast.makeText(ActivityMyTrips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void alertFoodRating(ModelHome.Data data) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setCancelable(true);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_rating_restaurant, null);
        alertDialogBuilder.setView(dialogView);
        RatingBar rating_food_quality = dialogView.findViewById(R.id.rating_food_quality);
        RatingBar rating_service = dialogView.findViewById(R.id.rating_service);
        RatingBar rating_overall = dialogView.findViewById(R.id.rating_overall);
        EditText edit_comment = dialogView.findViewById(R.id.edit_comment);
        TextView tv_restaurant=dialogView.findViewById(R.id.tv_restaurant);
        tv_restaurant.setSelected(true);
        if (data.getOrderRating().getRestaurantName()!=null&&
                !data.getOrderRating().getRestaurantName().equals("")){
            tv_restaurant.setText(data.getOrderRating().getRestaurantName());
        }

        ImageView img_close= dialogView.findViewById(R.id.img_close);
        Button btn_submit = dialogView.findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int ratingfoodquality = Math.round(rating_food_quality.getRating());
                int ratingservice = Math.round(rating_service.getRating());
                int ratingoverall = Math.round(rating_overall.getRating());
                if (ratingfoodquality == 0) {
                    Toast.makeText(context, context.getString(R.string.all_rating_msg), Toast.LENGTH_SHORT).show();

                } else if (ratingservice == 0) {
                    Toast.makeText(context, context.getString(R.string.all_rating_msg), Toast.LENGTH_SHORT).show();
                } else if (ratingoverall == 0) {
                    Toast.makeText(context, context.getString(R.string.all_rating_msg), Toast.LENGTH_SHORT).show();
                } else {
                    addFoodRating(orderId, restrId, ratingfoodquality, ratingservice, ratingoverall, edit_comment.getText().toString().trim());
                }
            }
        });
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                foodalert.dismiss();
            }
        });


        foodalert = alertDialogBuilder.create();
        foodalert.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        foodalert.show();
    }

    public void alertTripRating(ModelHome.Data data) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setCancelable(true);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_airport_review, null);
        alertDialogBuilder.setView(dialogView);
        TextView tv_departure_airport_name = dialogView.findViewById(R.id.tv_departure_airport_name);
        ImageView img_close= dialogView.findViewById(R.id.img_close);
        tv_departure_airport_name.setText(data.getTripsRating().getDep_airport_city() + " Airport");
        TextView tv_arrival_airport_name = dialogView.findViewById(R.id.tv_arrival_airport_name);
        tv_arrival_airport_name.setText(data.getTripsRating().getArr_airport_city() + " Airport");
        TextView tv_airline_name = dialogView.findViewById(R.id.tv_airline_name);
        tv_airline_name.setText(data.getTripsRating().getAirline());
        EditText edit_departure_comment = dialogView.findViewById(R.id.edit_departure_comment);
        EditText edit_arrival_comment = dialogView.findViewById(R.id.edit_arrival_comment);
        EditText edit_airline_comment = dialogView.findViewById(R.id.edit_airline_comment);
        RatingBar rating_dept = dialogView.findViewById(R.id.rating_dept);
        RatingBar rating_arrival = dialogView.findViewById(R.id.rating_arrival);
        RatingBar ratingairline = dialogView.findViewById(R.id.ratingairline);
        Button btn_submit = dialogView.findViewById(R.id.btn_submit);
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tripAlert.dismiss();
            }
        });
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int ratingfrom = Math.round(rating_arrival.getRating());
                int ratingto = Math.round(rating_dept.getRating());
                int ratingAirlines = Math.round(ratingairline.getRating());
                if (ratingfrom == 0) {
                    Toast.makeText(context, context.getString(R.string.all_rating_msg), Toast.LENGTH_SHORT).show();
                } else if (ratingto == 0) {
                    Toast.makeText(context, context.getString(R.string.all_rating_msg), Toast.LENGTH_SHORT).show();
                } else if (ratingAirlines == 0) {
                    Toast.makeText(context, context.getString(R.string.all_rating_msg), Toast.LENGTH_SHORT).show();
                } else {
                    addTripRating(tripId, ratingfrom, edit_departure_comment.getText().toString().trim(),
                            ratingto, edit_arrival_comment.getText().toString().trim(), ratingAirlines,
                            edit_airline_comment.getText().toString());
                }


            }
        });
        tripAlert = alertDialogBuilder.create();
        tripAlert.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        tripAlert.show();
    }

    @Override
    public void initizializeView(View view) {
        ShoppingApplication.getContext().getComponent().inject(this);
        ButterKnife.bind(this, view);
        swiplayout.setOnRefreshListener(this);


        tc_airport_time.setFormat12Hour("hh:mm a");
        tc_airport_time.setFormat24Hour(null);
        tv_airport_name.setSelected(true);
        spinn_arrow.setOnClickListener(this);
        tv_airport_temp.setSelected(true);
        spinner_terminal.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                view.setSelected(true);
                UtilsDefault.updateSharedPreference(Constants.TERMINAL_ID,
                        String.valueOf(list_terminals.get(position).getTerminal_id()));
                if (isSelected && list_terminals.size() != 0) {
                    terminalId = String.valueOf(list_terminals.get(position).getTerminal_id());

                    getHomedata(String.valueOf(list_terminals.get(position).getTerminal_id()));
                }
                isSelected = true;


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        intizializeClick();
        multiple_toggle_switch.
                setOnToggleSwitchChangeListener(new ToggleSwitch.OnToggleSwitchChangeListener() {

                    @Override
                    public void onToggleSwitchChangeListener(int position, boolean isChecked) {
                        if (position == 0) {
                            area = "2";
                            UtilsDefault.updateSharedPreference(Constants.AIRPORT_TYPE, "2");

                            getHomedata(terminalId);

                        } else {
                            area = "1";
                            UtilsDefault.updateSharedPreference(Constants.AIRPORT_TYPE, "1");
                            getHomedata(terminalId);
                        }

                        // Write your code ...


                    }
                });
        if (UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_TYPE) != null &&
                UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_TYPE).equals("1")) {
            multiple_toggle_switch.setCheckedTogglePosition(1);

        }
        else {
            multiple_toggle_switch.setCheckedTogglePosition(0);
            UtilsDefault.updateSharedPreference(Constants.AIRPORT_TYPE, "2");
        }
        //super.initizializeView(view);
    }

//    @Override
//    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//
//
//
//       // getHomedata("");
//        super.onViewCreated(view, savedInstanceState);
//
//    }



    public void intizializeClick() {
        img_blur.setVisibility(View.VISIBLE);
        tv_airport_climate.setSelected(true);
        card_avitips.setOnClickListener(this);
        card_user_tip_lay.setOnClickListener(this);
        flightstatus_lay.setOnClickListener(this);
        flight_schedule_lay.setOnClickListener(this);
        shop_lay.setOnClickListener(this);
        facilities_lay.setOnClickListener(this);
        mytrip_lay.setOnClickListener(this);
        transport_lay.setOnClickListener(this);
        floormap_lay.setOnClickListener(this);
        eat_drink_lay.setOnClickListener(this);
    }

    private String convertFahrenheitToCelcius(String fahrenheit) {
//        Double far=Double.parseDouble(fahrenheit);
//        Double b=far-32;
//        Double c=b*5/9;
//        int cel= (int) Math.round(c);
//        String r=String.valueOf(cel);
        float f = Float.parseFloat(fahrenheit);
        Double celsius = (f - 32.0) * 0.5555;

        // float cc=((f - 32) * 5 / 9);

        int round = (int) Math.round(celsius);
        return String.valueOf(round);
    }



    public void getHomedata(String terminalid) {
        if (!UtilsDefault.isOnline()) {
           alertcommon("Home", getString(R.string.error_no_net));
            hiderefresh();
            lin_content_lay.setVisibility(View.GONE);
            img_blur.setVisibility(View.VISIBLE);
             Toast.makeText(context, R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;
        }
        TimeZone timeZone = TimeZone.getDefault();
        Log.d("dainput", "getHomedata: " +
                UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID)+"ter"+
                terminalid+"area"+area+"timezone"+timeZone.getID());


        shwProgress();
       // terminalid=UtilsDefault.checkNull(UtilsDefault.getSharedPreferenceValue(Constants.TERMINAL_ID));
        aviapi.homedata(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID),
                UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID),
                UtilsDefault.checkNull(UtilsDefault.getSharedPreferenceValue(Constants.TERMINAL_ID)),
                area,timeZone.getID()).enqueue(new Callback<ModelHome>() {
            @Override
            public void onResponse(Call<ModelHome> call, Response<ModelHome> response) {
                hideprogress();
                hiderefresh();


                if (response.body() != null) {
                    ModelHome data = response.body();
//                    String header = response.headers().get("authorization");
//                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    //    Log.d(TAG, "onResponse: "+header);
                    // UtilsDefault.updateSharedPreference(Constants.AUTH_TOKEN, data.getJwt_token());
                    if (data.getStatus() == Constants.STATUS_200) {


                        if (data.getData().getUser_count()==0){
                            Toast.makeText(context, getString(R.string.accnt_not_available), Toast.LENGTH_SHORT).show();
                            UtilsDefault.clearSesson();
                            Intent intent=new Intent(context, ActivityLogin.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            getActivity().finish();
                            return;
                        }
                        lin_content_lay.setVisibility(View.VISIBLE);
                        img_blur.setVisibility(View.GONE);


                        if (data.getData().getTimezone() != null) {
                            UtilsDefault.updateSharedPreference(Constants.TIME_ZONE,
                                    data.getData().getTimezone());
                            Log.d(TAG, "onResponse: " + data.getData().getTimezone());
                            tc_airport_time.setTimeZone(data.getData().getTimezone());
                           // tc_airport_time.setFormat12Hour("kk:mm");

                        }
                        UtilsDefault.updateSharedPreference(Constants.AIRPORT_DOMESTIC_TYPE,
                                data.getData().getTripDateTimeType());
                        if (data.getData().getaAirport().getCity()!=null){
                            UtilsDefault.updateSharedPreference(Constants.AIRPORT_CITY,
                                    data.getData().getaAirport().getCity().getName());

                        }
                        UtilsDefault.updateSharedPreference(Constants.AIRPORT_CODE,data.getData().getaAirport().getCode());
                        UtilsDefault.updateSharedPreference(Constants.AIRPORT_LATITUDE,data.getData().getaAirport().getLatitude());
                        UtilsDefault.updateSharedPreference(Constants.AIRPORT_LONGITUDE,data.getData().getaAirport().getLongitude());
                        if (data.getData().getOffers() != null && data.getData().getOffers().size() != 0) {
                            tv_restro_nodata.setVisibility(View.GONE);
                            recycle_offer.setVisibility(View.VISIBLE);
                            List<ModelHome.Offers> list = new ArrayList<>();
                            list.clear();
                            list.addAll(data.getData().getOffers());
                            if (list.size() > 4) {
                                ModelHome.Offers dummy = new ModelHome().new Offers();
                                dummy.setId(0);
                                list.add(dummy);
                            }

                            recycle_offer.setLayoutManager(new LinearLayoutManager(context,
                                    LinearLayoutManager.HORIZONTAL, false));
                            AdapterHomeOffers adapterHomeOffers = new AdapterHomeOffers(context, list, 0, data.getData().getImagePath().getCoupons());
                            adapterHomeOffers.setonItemClick(FragmentHome.this);
                            recycle_offer.setAdapter(adapterHomeOffers);
                        } else {
                            tv_restro_nodata.setVisibility(View.VISIBLE);
                            recycle_offer.setVisibility(View.GONE);
                        }
                        if (data.getData().getaNews() != null && data.getData().getaNews().size() != 0) {
                            tv_news_nodata.setVisibility(View.GONE);
                            recycle_latest_news.setVisibility(View.VISIBLE);
                            List<ModelHome.ANews> list2 = new ArrayList<>();
                            list2.clear();
                            list2.addAll(data.getData().getaNews());
                            list2.add(new ModelHome().new ANews());

                            recycle_latest_news.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                            AdapterLatestNews adapterHomeOffers = new AdapterLatestNews(context, list2, 0);
                            adapterHomeOffers.setonItemClick(FragmentHome.this);
                            recycle_latest_news.setAdapter(adapterHomeOffers);
                        } else {
                            tv_news_nodata.setVisibility(View.VISIBLE);
                            recycle_latest_news.setVisibility(View.GONE);
                        }
                        if (data.getData().getaTerminal() != null && data.getData().getaTerminal().size()
                                != 0) {
                            if (list_terminals.size() == 0) {
                                list_terminals.clear();
                                list_terminals.addAll(data.getData().getaTerminal());

                                ArrayAdapter dataAdapter2 =
                                        new ArrayAdapter(context,
                                                R.layout.spinner_item,
                                                R.id.text,
                                                list_terminals);
                                spinner_terminal.setAdapter(dataAdapter2);
                                if (UtilsDefault.getSharedPreferenceValue(Constants.TERMINAL_ID) != null) {
                                    for (ModelHome.ATerminal terminal : list_terminals) {
                                        if (String.valueOf(terminal.getTerminal_id()).equalsIgnoreCase(
                                                UtilsDefault.getSharedPreferenceValue(Constants.TERMINAL_ID))) {
                                            spinner_terminal.setSelection(list_terminals.indexOf(terminal));

                                        }

                                    }
                                }
                            }
                        }

                        if (data.getData().getAviTips() != null && data.getData().getAviTips().size()
                                != 0 && data.getData().getAviTips().get(0).getDescription() != null) {
                            try {
                                byte[] base64 = Base64.decode(data.getData().getAviTips().get(0).getDescription(), Base64.DEFAULT);
                                String html = new String(base64, "UTF-8");

                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    tv_avi_tipdes.setText(Html.fromHtml(html, Html.FROM_HTML_MODE_COMPACT));
                                } else {
                                    tv_avi_tipdes.setText(Html.fromHtml(html));
                                }

                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }


                            //  tv_avi_tipdes.setText(data.getData().getAviTips().get(0).getDescription());
                        }
                        if (data.getData().getAviTips() != null &&
                                data.getData().getAviTips().size() != 0 &&
                                data.getData().getAviTips().get(0).getUpdated_at() != null) {


                            tv_avi_tipdate.setText(UtilsDefault.dateFormat(data.getData().getAviTips().
                                     get(0).getUpdated_at()));
                        }

                        if (data.getData().getUserTips() != null && data.getData().getUserTips().size() != 0 && data.getData().getUserTips().get(0).getDescription() != null) {
                            tv_user_tipdes.setText(data.getData().getUserTips().get(0).getDescription());
                            if (data.getData().getUserTips().get(0).getUsersFname()!=null){
                                tv_user_name.setText(data.getData().getUserTips().get(0).getUsersFname());

                            }
                        }
                        if (data.getData().getUserTips() != null && data.getData().getUserTips().size() != 0 && data.getData().getUserTips().get(0).getUpdated_at() != null) {
                            tv_user_tipdate.setText(UtilsDefault.dateFormat(data.getData().getUserTips().
                                    get(0).getUpdated_at()));

                        }
                        UtilsDefault.updateSharedPreference(Constants.AIRPORT_CONTRYCODE,
                                data.getData().getaAirport().getCountry().getSortname());
                        if (data.getData().getaAirport().getName() != null) {
                            UtilsDefault.updateSharedPreference(Constants.AIRPORT_NAME,
                                    data.getData().getaAirport().getName());
                            ((ActivityHome)context).setAirportName();

                            tv_airport_name.setText(data.getData().getaAirport().getCode());
                        }
                        if (data.getData().getaAirport().getCity().getName() != null) {
                            getweatherReport(String.valueOf(data.getData().getaAirport().getCity().getName()));

                            //tv_airport_name.setText(data.getData().getaAirport().getName());
                        }
                        if (data.getData().getaAirport().getAddress() != null) {
                            tv_airport_country.setText(data.getData().getaAirport().getCity().getName()
                                    + " , " + data.getData().getaAirport().getCountry().getSortname());
                        }
                        if (data.getData().getOrderRating() != null) {
                            orderId = data.getData().getOrderRating().getOrderID();
                            restrId = data.getData().getOrderRating().getRestaurant_id();
                            alertFoodRating(data.getData());
                        }
                        if (data.getData().getTripsRating() != null) {
                            tripId = data.getData().getTripsRating().getId();
                            alertTripRating(data.getData());
                        }
                        if (data.getData().getUpTripTime() != null && !data.getData().getUpTripTime().equals("")) {
                            UtilsDefault.updateSharedPreference(Constants.LATEST_DATE, data.getData().getUpTripTime());
                            countDownStart();
                        } else {
                            UtilsDefault.updateSharedPreference(Constants.LATEST_DATE, null);
                            countDownStart();
                        }


                        //  riseupToNextscreen();
                        // UtilsDefault.updateSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                    } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {

                        getHomedata(terminalid);
                    } else {
                        lin_content_lay.setVisibility(View.GONE);
                        img_blur.setVisibility(View.VISIBLE);
                        Toast.makeText(context, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }


                } else {
                    lin_content_lay.setVisibility(View.GONE);
                    img_blur.setVisibility(View.VISIBLE);
                    Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ModelHome> call, Throwable t) {
                hideprogress();
                hiderefresh();
                lin_content_lay.setVisibility(View.GONE);
                img_blur.setVisibility(View.VISIBLE);
                Log.d("onFailure", "onFailure: " + t.getMessage());

                Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getweatherReport(String cityname) {
        if (UtilsDefault.getSharedPreferenceValue(Constants.WEATHER_REPORT) == null ||
                UtilsDefault.getSharedPreferenceValue(Constants.WEATHER_REPORT).equals("")) {
            OpenWeatherMapHelper helper = new OpenWeatherMapHelper(getString(R.string.weather_api_key));
            helper.setUnits(Units.IMPERIAL);
            helper.getCurrentWeatherByCityName(cityname, new CurrentWeatherCallback() {
                @Override
                public void onSuccess(CurrentWeather currentWeather) {
                    if (currentWeather != null) {
                        try {
                            Log.v(TAG, "Coordinates: " + currentWeather.getCoord().getLat() + ", " + currentWeather.getCoord().getLon() + "\n"
                                    + "Weather Description: " + currentWeather.getWeather().get(0).getDescription() + "\n"
                                    + "Temperature: " + currentWeather.getMain().getTempMax() + "\n"
                                    + "Wind Speed: " + currentWeather.getWind().getSpeed() + "\n"
                                    + "City, Country: " + currentWeather.getName() + ", " + currentWeather.getSys().getCountry());
                            UtilsDefault.updateSharedPreference(Constants.WEATHER_REPORT, "yes");
                            UtilsDefault.updateSharedPreference(Constants.WEATHER_MESSAGE, currentWeather.getWeather().get(0).getMain());
                            UtilsDefault.updateSharedPreference(Constants.WEATHER_CELCIUS, String.valueOf(Math.round(currentWeather.getMain().getTempMax())));
                            tv_airport_climate.setText(currentWeather.getWeather().get(0).getMain());
                            tv_airport_temp.setText( convertFahrenheitToCelcius(UtilsDefault.getSharedPreferenceValue(Constants.WEATHER_CELCIUS)) +
                                    (char) 0x00B0 + "C"+" / "+
                                    UtilsDefault.getSharedPreferenceValue(Constants.WEATHER_CELCIUS) +(char) 0x00B0+"F");
//                            tv_airport_temp.setText(convertFahrenheitToCelcius(String.valueOf(Math.round(currentWeather.getMain().getTempMax())) + (char) 0x00B0 + "C"+
//                                            " / "+ String.valueOf(Math.round(currentWeather.getMain().getTempMax())) + (char) 0x00B0 + "F" )
//                                    );
                        } catch (Exception e) {
                            Log.d(TAG, "onSuccess: " + e.getMessage());
                        }

                    }

                }

                @Override
                public void onFailure(Throwable throwable) {
                    Log.v(TAG, throwable.getMessage());
                }
            });

        } else {
            tv_airport_climate.setText(UtilsDefault.getSharedPreferenceValue(Constants.WEATHER_MESSAGE));
            tv_airport_temp.setText( convertFahrenheitToCelcius(UtilsDefault.getSharedPreferenceValue(Constants.WEATHER_CELCIUS)) +
                    (char) 0x00B0 + "C"+" / "+
                    UtilsDefault.getSharedPreferenceValue(Constants.WEATHER_CELCIUS) + (char) 0x00B0 + "F");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        this.context=getActivity();
        countDownStart();
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_home_layout;
    }

    @Override
    public void OnItemClickListener(int pos, View view) {

    }

    @Override
    public void closeCommonAlert() {
        super.closeCommonAlert();

    }

    @Override
    public void onClick(View v) {
        if (v == spinn_arrow) {
            spinner_terminal.performClick();
        }
        if (v == card_avitips) {
            startActivity(new Intent(getActivity(), ActivityAviTips.class));
        }
        if (v == mytrip_lay) {
            if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) == null) {
                alertShowLogin(context.getString(R.string.login_messge));
                return;

            }
            startActivity(new Intent(getActivity(), ActivityMyTrips.class));
        }

        if (v == card_user_tip_lay) {
            if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) == null) {
                alertShowLogin(context.getString(R.string.login_messge));
                return;
            }
            startActivity(new Intent(getActivity(), ActivityUserTips.class));

        }
        if (v == floormap_lay) {
            startActivity(new Intent(getActivity(), ActivityCommonWebLoader.class));
        }
        if (v == transport_lay) {
            startActivity(new Intent(getActivity(), ActivityTransport.class));
        }
        if (v == flightstatus_lay) {
            if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) == null) {
                alertShowLogin(context.getString(R.string.login_messge));
                return;

            }
            //startActivity(new Intent(getActivity(), ActivityFlightStatus.class));
            startActivity(new Intent(getActivity(), ActivityUpdatedFlightStatus.class));
        }
        if (v == shop_lay) {
            startActivity(new Intent(getActivity(), ActivityShopList.class));
        }
        if (v == facilities_lay) {
            startActivity(new Intent(getActivity(), ActivityFacilities.class));
        }
        if (v == eat_drink_lay) {
            startActivity(new Intent(getActivity(), ActivityEatAndDrink.class));
        }
        if (v == flight_schedule_lay) {
            if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) == null) {
                alertShowLogin(context.getString(R.string.login_messge));
                return;

            }
            startActivity(new Intent(getActivity(), ActivityFlightSchedule.class));
        }
    }

    public void hiderefresh() {
        if (swiplayout != null && swiplayout.isRefreshing()) {
            swiplayout.setRefreshing(false);
            swiplayout.destroyDrawingCache();
            swiplayout.clearAnimation();
        }
    }

    @Override
    public void onRefresh() {
        getHomedata("");
    }
}
