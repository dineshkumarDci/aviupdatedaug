package com.dci.avicustomer.fragments;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dci.avicustomer.R;
import com.dci.avicustomer.activities.ActivityHome;
import com.dci.avicustomer.activities.ActivityLogin;
import com.dci.avicustomer.activities.ActivityMyTrips;


import com.dci.avicustomer.activities.ActivityUpdatedAddTrip;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.MyNotificationPublisher;
import com.dci.avicustomer.helpers.UtilsDefault;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;


public abstract class BaseFragments extends android.support.v4.app.Fragment {
    boolean isLoading = false;
    // boolean isLastpage = false;
    RecyclerView recyclerView;
    Boolean isStarted1 = false;
    Boolean isVisible1 = false;
    boolean isLastPage = false;
    int pagenum = 1;
    Context context;
    ProgressDialog dialog;
    TextView no_data, tv_clickhere;
    LinearLayout timer_lay;
    AlertDialog clickhere_alert, loginalert, commonalert2;
    //CountDownClock countDownClock;
    private Handler handler = new Handler();
    private Runnable runnable;
    TextView tv_hrs, tv_mins, tv_secs;
    Button btn_ok;
    int totalCount = -1;
    TimeZone timeZone;

    public void alertShowLogin(String title) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setCancelable(true);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_login_required, null);
        alertDialogBuilder.setView(dialogView);
        TextView titletv = dialogView.findViewById(R.id.tv_title);
        titletv.setText(title);
        Button btn_login = dialogView.findViewById(R.id.btn_login);


        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginalert.cancel();
                startActivity(new Intent(getActivity(), ActivityLogin.class));

            }
        });
        loginalert = alertDialogBuilder.create();
        loginalert.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        loginalert.show();
    }
    @Override
    public void onDetach() {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        super.onDetach();
    }



    @Override
    public void onResume() {
        super.onResume();


    }

    public void scheduleNotification(long delay, int notificationId) {
        //delay is after how much time(in millis) from current time you want to schedule the notification


        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, "")
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.appicons))
                .setSmallIcon(R.drawable.alarm_notify)
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText(context.getString(R.string.msg_boardingclose))
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            NotificationChannel channel = new NotificationChannel(channelId,
//                    "Channel human readable title",
//                    NotificationManager.IMPORTANCE_DEFAULT);
//            notificationManager.createNotificationChannel(channel);
//        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelId = getString(R.string.default_notification_channel_id);
            String name = getString(R.string.default_notification_channel_id);
            NotificationChannel channel = new NotificationChannel(channelId, name,
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(context.getString(R.string.msg_boardingclose));
            notificationManager.createNotificationChannel(channel);
            notificationBuilder.setChannelId(channelId);
        }


        Intent intent = new Intent(context, ActivityHome.class);
        PendingIntent activity = PendingIntent.getActivity(context, notificationId, intent,
                PendingIntent.FLAG_CANCEL_CURRENT);
        notificationBuilder.setContentIntent(activity);
        Notification notification = notificationBuilder.build();
        Intent notificationIntent = new Intent(context, MyNotificationPublisher.class);
        notificationIntent.putExtra(MyNotificationPublisher.NOTIFICATION_ID, notificationId);
        notificationIntent.putExtra(MyNotificationPublisher.NOTIFICATION, notification);


        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, notificationId,
                notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        long futureInMillis = SystemClock.elapsedRealtime() + delay;
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);

        if (Build.VERSION.SDK_INT >= 23) {
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                    futureInMillis, pendingIntent);
        } else if (Build.VERSION.SDK_INT >= 19) {
            alarmManager.setExact(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent);
        } else {
            alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent);
        }
       // alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent);
    }

    public void countDownStart() {
        Log.d("coundown", "countDownStart: " + "started");
        if(context!=null){
            if (UtilsDefault.getSharedPreferenceValue(Constants.LATEST_DATE) != null) {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                try {
                    Calendar allrmcal = Calendar.getInstance();
                    Date event_date = format.parse(UtilsDefault.getSharedPreferenceValue(Constants.LATEST_DATE));
                    allrmcal.setTime(event_date);
                    Log.d("timer", "countDownStart: " +
                            Calendar.getInstance().getTimeInMillis() + "settime" +
                            allrmcal.getTimeInMillis());
                    Calendar currentCal=Calendar.getInstance();
                    Date current_date =currentCal.getTime();
                    if (!current_date.after(event_date)) {
                        long delay=allrmcal.getTimeInMillis()-currentCal.getTimeInMillis();
                        scheduleNotification( delay, 1);

                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Log.d("triptime", "countDownStart: "+UtilsDefault.getSharedPreferenceValue(Constants.LATEST_DATE));
                Log.d("coundown", "countDownStart: " + "started" + UtilsDefault.getSharedPreferenceValue(Constants.LATEST_DATE));
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            handler.postDelayed(this, 1000);
                            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
                            Calendar curentcals = Calendar.getInstance();

//                        if (UtilsDefault.getSharedPreferenceValue(Constants.TIME_ZONE)!=null){
//                            TimeZone tz = TimeZone.getTimeZone(UtilsDefault.
//                                    getSharedPreferenceValue(Constants.TIME_ZONE));
//                            curentcal.setTimeZone(tz);
//                            format.setTimeZone(tz);
//                        }
                            Date event_date = format.parse(UtilsDefault.getSharedPreferenceValue
                                    (Constants.LATEST_DATE));
                            Date current_date = curentcals.getTime();
                            if (!current_date.after(event_date)) {
                                timer_lay.setVisibility(View.VISIBLE);
                                tv_clickhere.setVisibility(View.GONE);
                                long diff = event_date.getTime() - current_date.getTime();
                                long Days = diff / (24 * 60 * 60 * 1000);
                                long Hours = diff / (60 * 60 * 1000) % 24;
                                long Minutes = diff / (60 * 1000) % 60;
                                long Seconds = diff / 1000 % 60;
                                //

                                //  tv_days.setText(String.format("%02d", Days));

                                tv_hrs.setText(String.format("%02d", Hours));
                                tv_mins.setText(String.format("%02d", Minutes));
                                tv_secs.setText(String.format("%02d", Seconds));
                                if (UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_DOMESTIC_TYPE).
                                        equals("0")&&Integer.parseInt(String.format("%02d", Minutes)) < 25 &&
                                        String.format("%02d", Hours).equals("00")) {
                                    tv_hrs.setTextColor(getResources().getColor(R.color.darkred));
                                    tv_mins.setTextColor(getResources().getColor(R.color.darkred));
                                    tv_secs.setTextColor(getResources().getColor(R.color.darkred));

                                }
                               else if (UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_DOMESTIC_TYPE).
                                        equals("1")&&Integer.parseInt(String.format("%02d", Minutes)) < 45 &&
                                        String.format("%02d", Hours).equals("00")) {
                                    tv_hrs.setTextColor(getResources().getColor(R.color.darkred));
                                    tv_mins.setTextColor(getResources().getColor(R.color.darkred));
                                    tv_secs.setTextColor(getResources().getColor(R.color.darkred));

                                } else {
                                    tv_hrs.setTextColor(getResources().getColor(R.color.dark_blue));
                                    tv_mins.setTextColor(getResources().getColor(R.color.dark_blue));
                                    tv_secs.setTextColor(getResources().getColor(R.color.dark_blue));
                                }

                                if (String.format("%02d", Seconds).equals("00") &&
                                        String.format("%02d", Minutes).equals("00") &&
                                        String.format("%02d", Hours).equals("00")) {
                                    timer_lay.setVisibility(View.GONE);
                                    tv_clickhere.setVisibility(View.VISIBLE);
                                    handler.removeCallbacks(runnable);
                                }
                            } else {
                                timer_lay.setVisibility(View.GONE);
                                tv_clickhere.setVisibility(View.VISIBLE);
                                Log.d("coundown", "countDownStart: " + "not upcoming");
                                handler.removeCallbacks(runnable);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d("coundown", "countDownStart: " + e.getMessage());
                        }
                    }
                };
                handler.postDelayed(runnable, 0);
            } else {
                try {
                    timer_lay.setVisibility(View.GONE);
                    tv_clickhere.setVisibility(View.VISIBLE);
                    handler.removeCallbacks(runnable);

                } catch (Exception e) {

                }
            }
        }


    }


    @Override
    public View
    onCreateView(LayoutInflater inflater, ViewGroup container,
                 Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(getLayout(), container, false);
        dialog = new ProgressDialog(context);
        timeZone=TimeZone.getDefault();
        try {

            tv_secs = rootView.findViewById(R.id.tv_secs);
            tv_hrs = rootView.findViewById(R.id.tv_hrs);
            tv_mins = rootView.findViewById(R.id.tv_mins);
            tv_clickhere = rootView.findViewById(R.id.tv_clickhere);
            timer_lay = rootView.findViewById(R.id.timer_lay);
            tv_clickhere.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                        alertDialogBuilder.setCancelable(true);
                        LayoutInflater inflater = getActivity().getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.alert_common_alert_dialog, null);
                        alertDialogBuilder.setView(dialogView);
                        TextView titletv = dialogView.findViewById(R.id.tv_alert_title);
                        titletv.setText(getString(R.string.sidemenu_mytrips));
                        TextView tv_content = dialogView.findViewById(R.id.tv_content);
                        tv_content.setText(getString(R.string.no_trip_alert_message));
                        btn_ok = dialogView.findViewById(R.id.btn_ok);
                        btn_ok.setText(getString(R.string.add_trip));

                        btn_ok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (clickhere_alert != null) {
                                    clickhere_alert.cancel();
                                }
                               // startActivity(new Intent(context, ActivityMyTrips.class));
                                startActivity(new Intent(context, ActivityUpdatedAddTrip.class));
                            }
                        });


                        clickhere_alert = alertDialogBuilder.create();
                        clickhere_alert.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                        clickhere_alert.show();

                    } else {
                        alertShowLogin(getString(R.string.login_messge));
                    }

                }
            });

        } catch (Exception e) {

        }


        return rootView;

    }

    public void closeCommonAlert() {
        commonalert2.dismiss();
    }


    public void shwProgress() {
        dialog.setMessage(context.getString(R.string.please_wait));
        dialog.setCancelable(false);

        dialog.show();
        ProgressBar progressbar = (ProgressBar) dialog.findViewById(android.R.id.progress);
        progressbar.getIndeterminateDrawable().setColorFilter(Color.parseColor("#16172b"), android.graphics.PorterDuff.Mode.SRC_IN);
    }

    public void alertcommon(String title, String content) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setCancelable(false);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_common_alert_dialog, null);
        alertDialogBuilder.setView(dialogView);
        TextView titletv = dialogView.findViewById(R.id.tv_alert_title);
        titletv.setText(title);
        TextView tv_content = dialogView.findViewById(R.id.tv_content);
        tv_content.setText(content);
        Button btn_ok = dialogView.findViewById(R.id.btn_ok);


        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeCommonAlert();
            }
        });


        commonalert2 = alertDialogBuilder.create();
        commonalert2.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        commonalert2.show();
    }


    public void hideprogress() {
        if (dialog != null&&dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //  no_product=(TextView) rootView.findViewById(R.id.no_product);
//        if (list!=null&&list.size()!=0){
//            no_product.setVisibility(View.GONE);
//            products_recycle.setVisibility(View.VISIBLE);
//            adapterProducts=new AdapterProducts(getActivity(),list);
//            adapterProducts.setItemClickListener(this);
//            products_recycle.setLayoutManager(new GridLayoutManager(getActivity(),2));
//            products_recycle.setAdapter(adapterProducts);
//        }
//        else if (list.size()==0){
//            no_product.setVisibility(View.VISIBLE);
//
//            products_recycle.setVisibility(View.GONE);
//        }
        // Bundle bundle = getArguments();

        // init(rootView);
        initizializeView(view);


    }

    public abstract int getLayout();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }

    public void initizializeView(View view) {


    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted1 = false;
        handler.removeCallbacks(runnable);
    }

    @Override
    public void onStart() {
        super.onStart();
        isStarted1 = true;
        if (isVisible1) {

            clearData();
            // onRefresh();
            apiCall();
            //  calapi();
        }
        //onRefresh();

        // onRefresh();
        // calapi();
        //  getData("walkin");

    }

    public void alert(String title) {
        AlertDialog dialog2;
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);


        alertDialog.setMessage(title);
        alertDialog.setCancelable(false);

        alertDialog.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialog2 = alertDialog.create();
        dialog2.show();
    }

    public void apiCall() {

    }

    public void clearData() {

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible1 = isVisibleToUser;
        if (isVisible1 && isStarted1) {

            clearData();
            // onRefresh();
            apiCall();
            //  calapi();
        }

    }

    public void setupAdapter() {

    }
}
