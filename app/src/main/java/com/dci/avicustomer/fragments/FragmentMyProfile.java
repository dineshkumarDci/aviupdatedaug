package com.dci.avicustomer.fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.dci.avicustomer.BuildConfig;
import com.dci.avicustomer.R;
import com.dci.avicustomer.activities.ActivityCartView;
import com.dci.avicustomer.activities.ActivityFlightStatus;
import com.dci.avicustomer.activities.ActivityMyProfile;
import com.dci.avicustomer.activities.ActivityMyTrips;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.ImageFilePath;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.models.CommonMsgModel;
import com.dci.avicustomer.models.ModelUpcomingtrips;
import com.dci.avicustomer.models.ModelUserProfile;
import com.dci.avicustomer.retrofit.AVIAPI;
import com.github.siyamed.shapeimageview.CircularImageView;

import com.ybs.countrypicker.Country;
import com.ybs.countrypicker.CountryPicker;
import com.ybs.countrypicker.CountryPickerListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentMyProfile extends BaseFragments implements
        View.OnClickListener, DatePickerDialog.OnDateSetListener {
    @BindView(R.id.img_userimage)
    CircularImageView img_userimage;
    @BindView(R.id.img_edit)
    ImageView img_edit;
    @BindView(R.id.ed_username)
    EditText ed_username;
    @BindView(R.id.ed_lastname)
    EditText ed_lastname;
    @BindView(R.id.ed_emailid)
    EditText ed_emailid;
    @BindView(R.id.tv_gender)
    Spinner tv_gender;
    @BindView(R.id.tv_dateof_birth)
    TextView tv_dateof_birth;
    @BindView(R.id.tv_countrycode)
    TextView tv_countrycode;
    @BindView(R.id.ed_mobilenumber)
    EditText ed_mobilenumber;
    @BindView(R.id.tv_country)
    TextView tv_country;
    @BindView(R.id.gender_lay)
    RelativeLayout gender_lay;
    @BindView(R.id.date_birth_lay)
    RelativeLayout date_birth_lay;
    @BindView(R.id.countrycode_lay)
    RelativeLayout countrycode_lay;
    @BindView(R.id.country_lay)
    RelativeLayout country_lay;
    @BindView(R.id.btn_save_changes)
    Button btn_save_changes;
    @BindView(R.id.tester)
    ImageView tester;
    Uri imageUri;
    @Inject
    public AVIAPI aviapi;
    String countrycode = "";
    String country = "";
    String gender = "";
    String dob = "";
    List<String> genderlist = new ArrayList<>();
    final Calendar myCalendar = Calendar.getInstance();
    File selectedFile = null;
    boolean isServiceCalling=false;


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ShoppingApplication.getContext().getComponent().inject(this);
        ButterKnife.bind(this, view);
        btn_save_changes.setOnClickListener(this);
        countrycode_lay.setOnClickListener(this);
        gender_lay.setOnClickListener(this);
        country_lay.setOnClickListener(this);
        img_edit.setOnClickListener(this);
        date_birth_lay.setOnClickListener(this);
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(context, R.layout.gender_layout, R.id.text, getNoPax());
        tv_gender.setAdapter(spinnerArrayAdapter);
        tv_gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                gender = String.valueOf(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        getUserProfile();

    }


    @Override
    public void apiCall() {


    }
    public boolean getServiceCalling(){
        return isServiceCalling;

    }

    public List<String> getNoPax() {
        genderlist.clear();
        genderlist.add("Select Gender");
        genderlist.add("Male");
        genderlist.add("Female");
        return genderlist;
    }

    public void selectDatePicker() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, FragmentMyProfile.this, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, -14);
        datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
        //datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        // TODO Auto-generated method stub
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, month);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        String mnth = "";
        if (month >= 9) {
            mnth = String.valueOf(month + 1);
        } else {
            int mntt = month + 1;
            mnth = "0" + mntt;
        }
        String day = "";
        if (dayOfMonth > 9) {
            day = String.valueOf(dayOfMonth);
        } else {
            int dd = dayOfMonth;
            day = "0" + dd;
        }
        tv_dateof_birth.setText(UtilsDefault.dateFormat(year + "-" + mnth + "-" + day + " 01:34:01"));
        dob = String.valueOf(year) + "-" + mnth + "-" + String.valueOf(day);

        Log.d("selectdate", "onDateSet: " + "" + year + "-" + mnth + "-" + dayOfMonth + "---" + dob);

    }

    private byte[] compress(Uri selectedImage, File file) throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(context.getContentResolver().openInputStream(selectedImage), null, o);
        final int REQUIRED_SIZE = 400;
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        Bitmap bitmap = BitmapFactory.decodeStream(
                context.getContentResolver().openInputStream(selectedImage), null, o2);
        ExifInterface exif = null;
        try {

            File file1 = null;
            if (selectedFile == null) {
                file1 = new File(ImageFilePath.getPath(getActivity(), imageUri));

            } else {
                file1 = selectedFile;
            }

            exif = new ExifInterface(file1.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        int orientation = ExifInterface.ORIENTATION_NORMAL;

        if (exif != null)
            orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                bitmap = rotateBitmap(bitmap, 90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                bitmap = rotateBitmap(bitmap, 180);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                bitmap = rotateBitmap(bitmap, 270);
                break;
        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    public static Bitmap rotateBitmap(Bitmap bitmap, int degrees) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public MultipartBody.Part getMultiPart(Uri uri) {
        // File file = new File(upFileList.get(i).getPath());
        if (uri != null) {
            File file = new File(uri.getPath());
            byte[] bytes = new byte[0];
            try {
                bytes = compress(uri, file);
            } catch (Exception e) {
                e.printStackTrace();
            }
           /* String fileNames = new Date().getTime() +
                    file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("."));*/
            RequestBody fileBody = null;
            fileBody = RequestBody.create(MediaType.parse(context.getContentResolver().getType(uri)), bytes);
            MultipartBody.Part fileParts = MultipartBody.Part.createFormData("profile_img",
                    file.getName(), fileBody);
            return fileParts;
        } else {
            Log.d("data", "getMultiPart: ");
            //MultipartBody.Part fileParts = MultipartBody.Part.createFormData("profile_img",null,null);
            MultipartBody.Part filePart = MultipartBody.Part.createFormData("profile_img", "");
            return filePart;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        this.context = getActivity();

    }

    public void getUserProfile() {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(getActivity(),
                    R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;

        }
        shwProgress();
        //  Log.d("aviparms", "search: "+UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)+search);
        String userid = "0";
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
            userid = UtilsDefault.getSharedPreferenceValue(Constants.USER_ID);
        }

        Log.d("uid",
                "updateProfile: " + UtilsDefault.getSharedPreferenceValue(Constants.USER_ID));
        aviapi.getUserProfile().
                enqueue(new Callback<ModelUserProfile>() {
                    @Override
                    public void onResponse(Call<ModelUserProfile> call, Response<ModelUserProfile> response) {
                        hideprogress();
                        if (response.body() != null) {
                            ModelUserProfile data = response.body();
                            String header = response.headers().get(getString(R.string.header_find_key));
                            UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                            if (data.getStatus() == Constants.STATUS_200) {
                                country = data.getSortname();
                                tv_country.setText(data.getCountryName());
                                countrycode = data.getCountryCode();
                                tv_countrycode.setText(data.getCountryCode());
                                ed_username.setText(data.getData().getFirst_name());
                                ed_lastname.setText(data.getData().getLast_name());
                                ed_mobilenumber.setText(data.getData().getMobile());
                                ed_emailid.setText(data.getData().getEmail());
                                if (data.getData().getDob() != null && !data.getData().getDob().equals("")) {
                                    tv_dateof_birth.setText(UtilsDefault.dateFormat(data.getData().
                                            getDob() + " 01:34:01"));
                                }

                                dob = data.getData().getDob();
                                if (data.getData().getGender() != null && !data.getData().getGender().equals("")) {
                                    tv_gender.setSelection(Integer.parseInt(data.getData().getGender()));
                                }
//                                Glide.with(context).load(data.getPath()+data.getData().getProfile_img()).
//                                        placeholder(R.drawable.placeholder_icon).into(tester);
                                UtilsDefault.updateSharedPreference(Constants.USER_IMAGE_PATH, data.getPath());
                                if (data.getData().getProfile_img() != null && !data.getData().getProfile_img().equals("")) {
                                    loadUserProfile(data.getPath() + data.getData().getProfile_img());
//                                    Glide.with(context).load(data.getPath() + data.getData().getProfile_img())
//                                            .into(img_userimage);
                                }
//

                                UtilsDefault.updateSharedPreference(Constants.FIRST_NAME, data.getData().getFirst_name());
                                UtilsDefault.updateSharedPreference(Constants.LAST_NAME, data.getData().getLast_name());
                                UtilsDefault.updateSharedPreference(Constants.PROFILE_IMG, data.getData().getProfile_img());


                                //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                            } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                                getUserProfile();
                                //countDownStart();
                            } else {
                                Toast.makeText(context, data.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(context, R.string.server_error, Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<ModelUserProfile> call, Throwable t) {
                        hideprogress();
                        Log.d("failure", "onFailure: " + t.getMessage());
                        Toast.makeText(context, R.string.server_error, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        if (aviapi.updateProfile(0, null,
//                null, null, null, null, null, null, null) != null) {
//            aviapi.updateProfile(0, null,
//                    null, null, null, null, null, null, null).cancel();
//        }
    }


    @Override
    public void onStop() {
        super.onStop();

//        if (aviapi.updateProfile(0,null,
//                null,null,null,null,null,null,null)!=null){
//            aviapi.updateProfile(0,null,
//                    null,null,null,null,null,null,null).cancel();
//        }
    }

    public void loadUserProfile(String path) {
        Glide.with(this).load(path).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable final GlideException e,
                                        final Object model, final Target<Drawable> target,
                                        final boolean isFirstResource) {


                img_userimage.setImageDrawable(getResources().getDrawable(R.drawable.placeholder_icon));
                return false;
            }

            @Override
            public boolean onResourceReady(final Drawable resource,
                                           final Object model,
                                           final Target<Drawable> target,
                                           final DataSource dataSource,
                                           final boolean isFirstResource) {


                return false;
            }
        }).placeholder(R.drawable.placeholder_icon).into(img_userimage);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (aviapi.updateProfile(0, null,
                null, null, null, null, null, null, null) != null) {
            aviapi.updateProfile(0, null,
                    null, null, null, null, null, null, null).cancel();
        }
    }

    public void updateProfile() {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(getActivity(), R.string.error_no_net, Toast.LENGTH_SHORT).show();
            isServiceCalling=false;
            btn_save_changes.setEnabled(true);
            return;

        }
        shwProgress();
        //  Log.d("aviparms", "search: "+UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)+search);
        String userid = "0";
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
            userid = UtilsDefault.getSharedPreferenceValue(Constants.USER_ID);
        }

            String firstname = ed_username.getText().toString().trim();
            String lastname = ed_lastname.getText().toString().trim();
            String email = ed_emailid.getText().toString().trim();
            String mobilenum = ed_mobilenumber.getText().toString().trim();
            Log.d("inputparms", "updateProfile:" + firstname + lastname + email + "1" + dob + mobilenum + country +
                    getMultiPart(imageUri) + UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) +
                    UtilsDefault.getSharedPreferenceValue(Constants.USER_ID));
            Log.d("countrycode", "updateProfile: " + country);
            Log.d("uid", "updateProfile: " + UtilsDefault.getSharedPreferenceValue(Constants.USER_ID));
            aviapi.updateProfile(Integer.parseInt(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)),
                    firstname, lastname, email, gender, dob, mobilenum, country, getMultiPart(imageUri)).
                    enqueue(new Callback<ModelUserProfile>() {
                        @Override
                        public void onResponse(Call<ModelUserProfile> call, Response<ModelUserProfile> response) {
                            hideprogress();
                            isServiceCalling=false;
                            if (response.body() != null) {
                                ModelUserProfile data = response.body();
                                String header = response.headers().get(getString(R.string.header_find_key));
                                UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                                if (data.getStatus() == Constants.STATUS_200) {
                                    btn_save_changes.setEnabled(true);
                                    alertcommon(getString(R.string.title_my_profile), data.getMessage());
                                    UtilsDefault.updateSharedPreference(Constants.FIRST_NAME, data.getData().getFirst_name());
                                    UtilsDefault.updateSharedPreference(Constants.LAST_NAME, data.getData().getLast_name());
                                    UtilsDefault.updateSharedPreference(Constants.PROFILE_IMG, data.getData().getProfile_img());
                                    //UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                                } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                                    updateProfile();
                                    //countDownStart();
                                } else {
                                    btn_save_changes.setEnabled(true);
                                    Toast.makeText(context, data.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                btn_save_changes.setEnabled(true);
                                Toast.makeText(context, R.string.server_error, Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void onFailure(Call<ModelUserProfile> call, Throwable t) {
                            hideprogress();
                            isServiceCalling=false;
                            btn_save_changes.setEnabled(true);
                            Log.d("failure", "onFailure: " + t.getMessage());
                            Toast.makeText(context, R.string.server_error, Toast.LENGTH_SHORT).show();
                            //Toast.makeText(ActivityMyTrips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                        }
                    });



    }


    public void SetProfileImage(Uri uri, Bitmap bitmap, File file) {
        this.imageUri = uri;
        Bitmap bitmap1 = bitmap;
        // tester.setImageBitmap(bitmap);
        //  img_userimage.setImageResource(android.R.color.transparent);
        ExifInterface exif = null;
        try {
            String path = imageUri.toString();

//            File file = null;
//            file = new File(uri.toString());
            File file1 = null;
            if (file == null) {
                file1 = new File(ImageFilePath.getPath(getActivity(), imageUri));

            } else {
                selectedFile = file;
                file1 = file;
            }


            exif = new ExifInterface(file1.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        int orientation = ExifInterface.ORIENTATION_NORMAL;

        if (exif != null)
            orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                bitmap1 = rotateBitmap(bitmap1, 90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                bitmap1 = rotateBitmap(bitmap1, 180);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                bitmap1 = rotateBitmap(bitmap1, 270);
                break;
        }
        img_userimage.setImageResource(0);
        img_userimage.setBackground(null);
        img_userimage.setImageBitmap(null);
        img_userimage.setImageDrawable(null);
        img_userimage.destroyDrawingCache();
        img_userimage.setImageResource(android.R.color.transparent);
        img_userimage.setImageBitmap(bitmap1);

        //Toast.makeText(context, "calledfr", Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_my_profile;
    }

    public void showCountryCode(int from) {
        final CountryPicker picker = CountryPicker.newInstance("Select Country");  // dialog title
        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID) {
                picker.dismiss();
                countrycode = dialCode;
                tv_countrycode.setText(dialCode);

                tv_country.setText(name);
                Log.d("date", "onSelectCountry: ");
                Log.d("coundrypicker", "onSelectCountry: " + "name=" + name + "code-" + code + "-dcode=" + dialCode + "fla" + flagDrawableResID);
                // Implement your code here
                country = code;
            }
        });
        picker.show(getFragmentManager(), "COUNTRY_PICKER");
        // picker.setCountriesList(list);
    }

    @Override
    public void onClick(View v) {
        if (v == btn_save_changes) {
            String firstname = ed_username.getText().toString().trim();
            String lastname = ed_lastname.getText().toString().trim();
            String email = ed_emailid.getText().toString().trim();
            String mobilenum = ed_mobilenumber.getText().toString().trim();

            if (firstname.equals("")) {
                ed_username.setError(getString(R.string.error_uname));
                ed_username.requestFocus();
            } else if (lastname.equals("")) {
                ed_lastname.setError(getString(R.string.error_lastname));
                ed_lastname.requestFocus();
            } else if (email.equals("")) {
                ed_emailid.setError(getString(R.string.error_emailid));
                ed_emailid.requestFocus();
            } else if (!UtilsDefault.isEmailValid(email)) {
                ed_emailid.setError(getString(R.string.error_valid_email));
                ed_emailid.requestFocus();
            } else if (mobilenum.equals("")) {
                ed_mobilenumber.setError(getString(R.string.error_mobile_number));
                ed_mobilenumber.requestFocus();
            } else if (country.equals("")) {
                Toast.makeText(context, getString(R.string.error_country), Toast.LENGTH_SHORT).show();

            } else if (countrycode.equals("")) {
                Toast.makeText(context, getString(R.string.error_country_code), Toast.LENGTH_SHORT).show();
            } else if (gender.equals("0")) {
                Toast.makeText(context, getString(R.string.error_gender), Toast.LENGTH_SHORT).show();
            } else if (dob.equals("")) {
                Toast.makeText(context, getString(R.string.error_dob), Toast.LENGTH_SHORT).show();
            } else {
                btn_save_changes.setEnabled(false);
                isServiceCalling=true;
                updateProfile();
            }

        }
        if (v == country_lay) {
            showCountryCode(2);
        }
        if (v == countrycode_lay) {
            showCountryCode(1);
        }
        if (v == gender_lay) {
            tv_gender.performClick();
        }
        if (v == img_edit) {
            ((ActivityMyProfile) context).selectImage();
        }
        if (v == date_birth_lay) {
            selectDatePicker();
        }

    }
}
