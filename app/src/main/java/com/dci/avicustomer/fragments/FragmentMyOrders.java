package com.dci.avicustomer.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.avicustomer.R;
import com.dci.avicustomer.adapter.AdapterMyOrders;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.PaginationScrollListener;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.models.ModelMyOrders;
import com.dci.avicustomer.models.ModelUpcomingtrips;
import com.dci.avicustomer.retrofit.AVIAPI;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentMyOrders extends BaseFragments {

    @Inject
    public AVIAPI aviapi;
    @BindView(R.id.recycle_view)
    RecyclerView recycle_view;
    @BindView(R.id.tv_nodata)
    TextView tv_nodata;
    List<ModelMyOrders.Order>orderList=new ArrayList<>();
    AdapterMyOrders adapterMyOrders;

    @Override
    public void onResume() {
        super.onResume();
        this.context=getActivity();

    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ShoppingApplication.getContext().getComponent().inject(this);
        ButterKnife.bind(this, view);
        adapterMyOrders=new AdapterMyOrders(context,orderList,0);
        recycle_view.setAdapter(adapterMyOrders);
      //  recycle_view.setLayoutManager(new LinearLayoutManager(getActivity()));
        LinearLayoutManager linearLayout=new LinearLayoutManager(context);
        recycle_view.setLayoutManager(linearLayout);

        recycle_view.addOnScrollListener(new PaginationScrollListener(linearLayout) {
            @Override
            protected void loadMoreItems() {
                if (orderList.size() >= Constants.PAGE_SIZE) {
                    pagenum++;
                    getmyOrders();

                }

            }

            @Override
            public int getTotalPageCount() {
                return totalCount;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }

    @Override
    public void apiCall() {
        getmyOrders();
    }

    @Override
    public void clearData() {
        pagenum=1;
        orderList.clear();
        adapterMyOrders.notifyDataSetChanged();
        isLastPage=false;
    }

    public void getmyOrders() {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(getActivity(), R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;

        }
        shwProgress();
        //  Log.d("aviparms", "search: "+UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)+search);
        String userid = "0";
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
            userid = UtilsDefault.getSharedPreferenceValue(Constants.USER_ID);
        }

        aviapi.getUserOrders(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID),pagenum,
                Constants.NEW_PAGE_SIZE).enqueue(new Callback<ModelMyOrders>() {
            @Override
            public void onResponse(Call<ModelMyOrders> call, Response<ModelMyOrders> response) {
                hideprogress();
                if (response.body() != null) {
                   // Log.d("ddat", "onResponse: "+totalCount+"size"+orderList.size());
                    ModelMyOrders data = response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);

                    if (totalCount==orderList.size()){
                        isLastPage=true;

                    }
                    if (data.getStatus() == Constants.STATUS_200) {
                        totalCount=data.getCount();
                        if (data.getOrders()!=null&&data.getOrders().size()!=0){
                            //orderList.clear();
                            orderList.addAll(data.getOrders());
                            adapterMyOrders.notifyDataSetChanged();
                            tv_nodata.setVisibility(View.GONE);
                            recycle_view.setVisibility(View.VISIBLE);
                        }
                        else {
                            if(orderList.size()==0){
                                tv_nodata.setVisibility(View.VISIBLE);
                                recycle_view.setVisibility(View.GONE);
                            }

                        }
                        if(data.getCount()==orderList.size()){
                            isLastPage=true;
                        }


                        //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                    } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                        if (pagenum != 1)
                            pagenum--;
                        getmyOrders();

                        //countDownStart();
                    } else {
                        if (pagenum != 1)
                            pagenum--;
                        if(orderList.size()==0){
                            tv_nodata.setVisibility(View.VISIBLE);
                            recycle_view.setVisibility(View.GONE);
                        }

                        Toast.makeText(context, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (pagenum != 1)
                        pagenum--;
                      Toast.makeText(context, R.string.server_error, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ModelMyOrders> call, Throwable t) {
                hideprogress();
                if (pagenum != 1)
                    pagenum--;
                Log.d("failure", "onFailure: " + t.getMessage());
                Toast.makeText(context, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_myorders;
    }
}
