package com.dci.avicustomer.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.avicustomer.R;
import com.dci.avicustomer.activities.ActivityOfferPage;
import com.dci.avicustomer.adapter.AdapterHomeOffers;
import com.dci.avicustomer.adapter.AdapterLatestNews;
import com.dci.avicustomer.adapter.AdapterOfferList;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.PaginationScrollListener;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.models.ModelHome;
import com.dci.avicustomer.models.ModelOfferList;
import com.dci.avicustomer.retrofit.AVIAPI;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentOfferList extends BaseFragments {

    @BindView(R.id.recycle_offer_list)
    RecyclerView recycle_offer_list;
    @Inject
    public AVIAPI aviapi;
    String type="1";
    AdapterOfferList adapterOfferList;
    List<ModelOfferList.Datum>offerlist=new ArrayList<>();
    LinearLayoutManager linearLayoutManager;
    ModelOfferList modelOfferList=new ModelOfferList();
    String imgShops="";

    @Override
    public void onViewCreated(@NonNull View view,
                              @Nullable Bundle savedInstanceState) {
        ButterKnife.bind(this, view);
        ShoppingApplication.getContext().getComponent().inject(this);
        type=getArguments().getString("type");
        adapterOfferList=new AdapterOfferList(getActivity(),offerlist);
        recycle_offer_list.setAdapter(adapterOfferList);
        linearLayoutManager=new LinearLayoutManager(getActivity());
        recycle_offer_list.setLayoutManager(linearLayoutManager);
        recycle_offer_list.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                if (offerlist.size() >= Constants.PAGE_SIZE) {
                    pagenum++;
                    getOffers(type);
                }
            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });

    }
    public String getBaseUrl(){
        return imgShops;
    }
    @Override
    public void onResume() {
        super.onResume();
        this.context=getActivity();

    }
    @Override
    public void clearData() {
        offerlist.clear();
        adapterOfferList.notifyDataSetChanged();
        pagenum=1;
        isLastPage=false;
    }

    @Override
    public void apiCall() {
        getOffers(type);
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_offer_list;
    }

    public void getOffers(String type) {
        if (!UtilsDefault.isOnline()) {
            alertcommon(getString(R.string.title_offer),getString(R.string.error_no_net));
            //Toast.makeText(context, R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;
        }
        shwProgress();
        aviapi.getoffers(UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID),
                UtilsDefault.getSharedPreferenceValue(Constants.TERMINAL_ID),
                type,String.valueOf(pagenum),UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_TYPE)).enqueue(new Callback<ModelOfferList>() {
            @Override
            public void onResponse(Call<ModelOfferList> call, Response<ModelOfferList> response) {
                hideprogress();
                if (response.body() != null) {
                    ModelOfferList data = response.body();

//                    String header = response.headers().get("authorization");
//                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    //    Log.d(TAG, "onResponse: "+header);
                    // UtilsDefault.updateSharedPreference(Constants.AUTH_TOKEN, data.getJwt_token());
                    if (totalCount==offerlist.size()){
                        isLastPage=true;
                    }
                    if (data.getStatus() == Constants.STATUS_200) {
                        if(data.getOffer().getData()!=null&&data.getOffer().getData().size()!=0){
                            modelOfferList.setImagePath(data.getImagePath());
                            modelOfferList.setOffer(data.getOffer());
                            offerlist.addAll(data.getOffer().getData());
                            adapterOfferList.setImagePath(data.getImagePath().getRestaurant()
                                    ,data.getImagePath().getCoupons(),data.getImagePath().getShopimage());
                            adapterOfferList.notifyDataSetChanged();
                        }
                        else {
                            Toast.makeText(context, R.string.nodata, Toast.LENGTH_SHORT).show();
                        }
                        if (offerlist.size()==data.getOffer().getTotal()){
                            isLastPage=true;
                        }
                        if (data.getTopOffer()!=null&&data.getTopOffer().size()!=0){
                            ((ActivityOfferPage)getActivity()).UpdateTopOffer(data.getTopOffer(),data.getImagePath().getRestaurant(),
                                    data.getImagePath().getCoupons(),data.getImagePath().getShopimage());
                        }
                        imgShops=data.getImagePath().getShopimage();
                        totalCount=data.getOffer().getTotal();

                        //  riseupToNextscreen();
                        // UtilsDefault.updateSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                    }
                    else if (data.getStatus()==Constants.API_TOKENEXPIRY_STATUS){
                        getOffers(type);

                    }
                    else {
                        if (pagenum != 1)
                            pagenum--;
                        Toast.makeText(context, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (pagenum != 1)
                        pagenum--;
                    Toast.makeText(context, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ModelOfferList> call, Throwable t) {
                hideprogress();
                if (pagenum != 1)
                    pagenum--;
                Log.d("onFailure", "onFailure: " + t.getMessage());
                Toast.makeText(context, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
