package com.dci.avicustomer.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.avicustomer.R;
import com.dci.avicustomer.activities.ActivityMyTrips;
import com.dci.avicustomer.adapter.AdapterCompletedTrips;
import com.dci.avicustomer.adapter.AdapterMytrips;
import com.dci.avicustomer.helpers.Constants;
import com.dci.avicustomer.helpers.PaginationScrollListener;
import com.dci.avicustomer.helpers.ShoppingApplication;
import com.dci.avicustomer.helpers.UtilsDefault;
import com.dci.avicustomer.interfaces.OnItemClickListener;
import com.dci.avicustomer.models.ModelCompletedTrips;
import com.dci.avicustomer.models.ModelMyTrips;
import com.dci.avicustomer.retrofit.AVIAPI;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentMyCompletedTrips extends BaseFragments implements OnItemClickListener {

    @Inject
    public AVIAPI aviapi;
    @BindView(R.id.recycle_view)
    RecyclerView recycle_view;
    @BindView(R.id.tv_nodata)
    TextView tv_nodata;
    private List<ModelCompletedTrips.Datum> list=new ArrayList<>();
    AdapterCompletedTrips adapterCompletedTrips;
    int pagenum=1;
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ShoppingApplication.getContext().getComponent().inject(this);
        ButterKnife.bind(this, view);
        adapterCompletedTrips=new AdapterCompletedTrips(context,list);
        LinearLayoutManager linearLayout=new LinearLayoutManager(context);
        recycle_view.setLayoutManager(linearLayout);
        recycle_view.setAdapter(adapterCompletedTrips);
        recycle_view.addOnScrollListener(new PaginationScrollListener(linearLayout) {
            @Override
            protected void loadMoreItems() {
                if (list.size() >= Constants.PAGE_SIZE) {
                    pagenum++;
                   getCompletedTrips();

                }

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

    }

    @Override
    public void apiCall() {
        getCompletedTrips();
    }

    @Override
    public void clearData() {
        list.clear();
        adapterCompletedTrips.notifyDataSetChanged();
        pagenum=1;
        isLastPage=false;
    }
    @Override
    public void onResume() {
        super.onResume();
        this.context=getActivity();

    }

    public void getCompletedTrips() {
        if (!UtilsDefault.isOnline()) {
            Toast.makeText(context, R.string.error_no_net, Toast.LENGTH_SHORT).show();
            return;

        }
        shwProgress();
        //  Log.d("aviparms", "search: "+UtilsDefault.getSharedPreferenceValue(Constants.USER_ID)+search);
        String userid = "0";
        if (UtilsDefault.getSharedPreferenceValue(Constants.USER_ID) != null) {
            userid = UtilsDefault.getSharedPreferenceValue(Constants.USER_ID);
        }
        Log.d("currentTime", "getMyTrip: "+UtilsDefault.getCurrentdateandTime());
        aviapi.getCompletedTrips(UtilsDefault.getSharedPreferenceValue(Constants.USER_ID),
                UtilsDefault.getCurrentdateandTime(),UtilsDefault.getSharedPreferenceValue(Constants.AIRPORT_ID)
                ,pagenum,Constants.NEW_PAGE_SIZE).enqueue(new Callback<ModelCompletedTrips>() {
            @Override
            public void onResponse(Call<ModelCompletedTrips> call,
                                   Response<ModelCompletedTrips> response) {
                hideprogress();
                if (response.body() != null) {
                    ModelCompletedTrips data = response.body();
                    String header = response.headers().get(getString(R.string.header_find_key));
                    UtilsDefault.updateSharedPreference(Constants.API_KEY, header);
                    if (totalCount==list.size()){
                        isLastPage=true;
                    }
                    if (data.getStatus() == Constants.STATUS_200) {
                        totalCount=data.getCount();
                        if (data.getData()!=null&&data.getData().size()!=0){
                          //  list.clear();
                            list.addAll(data.getData());
                            adapterCompletedTrips.notifyDataSetChanged();
                            recycle_view.setVisibility(View.VISIBLE);
                            tv_nodata.setVisibility(View.GONE);
                        }

                        else {
                            if(list.size()==0){
                                tv_nodata.setVisibility(View.VISIBLE);
                                recycle_view.setVisibility(View.GONE);
                            }


                            //  alertcommon(getString(R.string.title_my_trip),getString(R.string.nodata));
                        }
                        if (data.getCount()==Constants.NEW_PAGE_SIZE){
                            isLastPage=true;
                        }



                        //   UtilsDefault.updateFcmSharedPreference(Constants.USER_ID,String.valueOf(data.getData().getId()));
                    } else if (data.getStatus() == Constants.API_TOKENEXPIRY_STATUS) {
                        if (pagenum != 1)
                            pagenum--;
                        getCompletedTrips();


                    } else {
                        if(list.size()==0){
                            tv_nodata.setVisibility(View.VISIBLE);
                            recycle_view.setVisibility(View.GONE);
                        }
                        if (pagenum != 1)
                            pagenum--;
                        alertcommon(getString(R.string.title_my_trip),data.getMessage());
                        //Toast.makeText(ActivityMyTrips.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if(list.size()==0){
                        tv_nodata.setVisibility(View.VISIBLE);
                        recycle_view.setVisibility(View.GONE);
                    }
                    if (pagenum != 1)
                        pagenum--;

                    tv_nodata.setVisibility(View.VISIBLE);
                    alertcommon(getString(R.string.title_my_trip),getString(R.string.server_error));

                    //   Toast.makeText(ActivityMyTrips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ModelCompletedTrips> call, Throwable t) {

                hideprogress();
                if (pagenum != 1)
                    pagenum--;

                if(list.size()==0){
                    tv_nodata.setVisibility(View.VISIBLE);
                    recycle_view.setVisibility(View.GONE);
                }

                alertcommon(getString(R.string.title_my_trip),getString(R.string.server_error));
                Log.d("failure", "onFailure: " + t.getMessage());
                //Toast.makeText(ActivityMyTrips.this, R.string.server_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_myorders;
    }

    @Override
    public void OnItemClickListener(int pos, View view) {

    }
}
